﻿
//
// Define the 'app' module.
//
function SetDragCovered() {
    angular.element('#divTempInserter').scope().SetDragCovered();
}

function DragInsert() {
    angular.element('#divTempInserter').scope().TempInsert("drag", 0, 0);
}

function SwitchToForEach2() {
    try {
        angular.element('#divForEachExpander').scope().SwitchToForEach();
    }
    catch (ex) {
        //alert(ex.message);
    }
}

function SwitchRoot2() {
    try {
        angular.element('#divTempExpander').scope().SwitchRoot();
    }
    catch (ex) {
        //alert(ex.message + " , " + ex.number);
    }
}

function keyPress(evt) {
    if (!$("input").is(':focus')) {//arrow keys and space should be disabled only when input controls do NOT have user focus (as they destroy UI)
        if (evt.keyCode == 37 || evt.keyCode == 38 || evt.keyCode == 39 || evt.keyCode == 40 || evt.keyCode == 32 || evt.keyCode == 20 || evt.keyCode == 35 || evt.keyCode == 36 || evt.keyCode == 21)
            evt.preventDefault();
    }
    angular.element('#divTempInserter').scope().ApplyKey(evt);
}

var app = angular.module('app', ['designcontainer', 'ngScrollbars'])

//
// Simple service to create a prompt.
//
    .factory('prompt', function() {

        /* Uncomment the following to test that the prompt service is working as expected.
    return function () {
        return "Test!";
    }
    */

        // Return the browsers prompt function.
        return prompt;
    })

//
// Application controller.
//
angular.module('app').controller('AppCtrl', function AppCtrl($scope, $rootScope, $compile, prompt, $element, $timeout, commonActivityService, $http) {
    $scope.config = {
        theme: 'light',
    }
    $scope.msgDic = "";
    $scope.variableBox = false;
    $scope.argumentBox = false;
    $scope.importBox = false;
    $scope.toggleArgVar = function (name) {
        var donthing = false;
        var show = false;
        if (name == 'var') {
            if ($scope.argumentBox || $scope.importBox)
                donthing = true;
            if ($scope.variableBox) {
                show = false;
                $scope.variableBox = false;
                $scope.argumentBox = false;
                $scope.importBox = false;
                angular.element('.bottomAction').css('bottom', 10);
            } else {
                if (!$scope.argumentBox && !$scope.importBox)
                    show = true;
                $timeout(function () {
                    angular.element('.bottomAction').css('bottom', 310);
                }, 600)
                $scope.variableBox = true;
                $scope.argumentBox = false;
                $scope.importBox = false;
            }
        } else if(name=='arg') {
            if ($scope.variableBox || $scope.importBox)
                donthing = true;
            if ($scope.argumentBox) {
                show = false;
                $scope.variableBox = false;
                $scope.argumentBox = false;
                $scope.importBox = false;
                angular.element('.bottomAction').css('bottom', 10);
            } else {
                if (!$scope.variableBox && !$scope.importBox)
                    show = true;
                $scope.variableBox = false;
                $scope.argumentBox = true;
                $scope.importBox = false;
                $timeout(function () {
                    angular.element('.bottomAction').css('bottom', 310);
                }, 600)
            }
        }
        else if (name == 'imp') {
            if ($scope.variableBox || $scope.argumentBox)
                donthing = true;
            if ($scope.importBox) {
                show = false;
                $scope.variableBox = false;
                $scope.argumentBox = false;
                $scope.importBox = false;
                angular.element('.bottomAction').css('bottom', 10);
            } else {
                if (!$scope.variableBox && !$scope.argumentBox)
                    show = true;
                $scope.variableBox = false;
                $scope.argumentBox = false;
                $scope.importBox = true;
                $timeout(function () {
                    angular.element('.bottomAction').css('bottom', 310);
                }, 600)
            }
        }
        if (!donthing) {
            if (show) {
                var currentdivContainerHeight = $('#divContainer').css("height").replace("px", "");
                $('#divContainer').css("height", (parseInt(currentdivContainerHeight) - 300) + "px");
            }
            else {
                var currentdivContainerHeight = $('#divContainer').css("height").replace("px", "");
                $('#divContainer').css("height", (parseInt(currentdivContainerHeight) + 300) + "px");
            }
        }
    }

    $scope.nestedFlowchart = true;
    $scope.showNestedFlowcharts = function () {//user clicks on the Flowcharts icon on top toolbar, flowchart switch panel should be displayed only if there are more than one flowchart available
        var canshow = false;
        var visibleactivities = 0;
        for (var _x = 0; _x < numSupportedFlowcharts; _x++) {
            var activityName = "lblActivity" + (_x + 1);
            if (document.getElementById(activityName).style.display == "") {
                visibleactivities++;
            }
        }
        if (visibleactivities > 1)
            canshow = true;

        if (!canshow)
            return;

        if ($('#divHierarchy').css("display") == "none") {
            $('#divHierarchy').css("display", "");
        } else {
            $('#divHierarchy').css("display", "none");
        }
    }

    ApplyVariableFilter = function (vr) {
        if (vr != null) {
            var tr = $compile($scope.GetVarRow(vr))($scope);
            angular.element('#tbl').append(tr);
            totalvarindex++;
        }
        else {
            var tr = $compile($scope.GetVarRow())($scope);
            angular.element('#tbl').append(tr);
        }
    }

    $scope.GetVarRow = function (vr) {
        try
        {
            var name = "";
            var def = "";
            var scope = "";
            var type = "";
            if (vr != null) {
                name = vr.Name;
                def = vr.Default;
                scope = vr.Scope;
                type = vr.Type;
            }
            else {
                var default_variable_index = 1;
                var max_num = 0;
                for (var k = 1 ; k < $("#tbl").find("tr").length; k++) {
                    if ($("#tbl").find("tr")[k].getElementsByTagName("td")[0].childNodes[0].value.toLowerCase().substring(0, 8) == "variable") {
                        var rest = $("#tbl").find("tr")[k].getElementsByTagName("td")[0].childNodes[0].value.toLowerCase().replace("variable", "");
                        try {
                            var x = parseInt(rest);
                            if (x > max_num)
                                max_num = x;
                        }
                        catch (ex) {

                        }
                    }
                }
                if (max_num == 0) {
                    default_variable_index = 1;
                }
                else {
                    default_variable_index = max_num + 1;
                }

                for (var r = 0 ; r < Variables.length; r++) {
                    if (Variables[r].Name == "variable" + default_variable_index) {
                        default_variable_index++;
                    }
                }
                var default_variable_name = "variable" + default_variable_index;
                name = "";//default_variable_name;
            }
            var typearray = [];
            typearray.push("String");
            typearray.push("Number");
            typearray.push("Boolean");
            typearray.push("Object");
            typearray.push("Email");
            typearray.push("List");
            typearray.push("DateTime");
            typearray.push("Relation");
            typearray.push("Guid");
            typearray.push("Phone number");
            typearray.push("Raw file");
            typearray.push("Location");
            typearray.push("Image");
            typearray.push("Auto increment ID");
            typearray.push("Timestamp");
            typearray.push("State data");
            typearray.push("Currency");
            var typeselected = [];

            for (var m = 0 ; m < typearray.length; m++) {
                if (type == typearray[m]) {
                    typeselected[m] = " selected='selected' ";
                }
            }

            var scopeid = "cmbScope" + totalvarindex;
            var scopeComboText = "";

            $scope.chartViewModel.FindCorrectScopeFlowChartItems(currentRootNodeID);

            for (var m = 0 ; m < CorrectFlowChartValuesForVariables.length; m++) {
                var s = "";
                if (vr == null) {
                    if (currentRootNodeID == CorrectFlowChartValuesForVariables[m])
                        s = " selected='selected' ";
                }
                scopeComboText += "<option " + s + " value='" + CorrectFlowChartValuesForVariables[m] + "' >" + CorrectFlowChartListForVariables[m] + "</option>";
            }
            return '<tr class="variableRow animated fadeIn" index="' + totalvarindex + '">' +
            '<td><input value="' + name + '" type="text" class="form-control" placeholder="variable name" index="' + totalvarindex + '"  data-ng-focus="addRow()" data-ng-click="addRow()" data-ng-blur="addRow($event)" /></td>' +
            '<td><select index="' + totalvarindex + '"  class="form-control" ng-blur="VarTypeChange($event)">' +

            ' <option ' + typeselected[0] + '>String</option>'+
            ' <option ' + typeselected[1] + '>Number</option>'+
            ' <option ' + typeselected[2] + '>Boolean</option>'+
            ' <option ' + typeselected[3] + '>Object</option>'+
            ' <option ' + typeselected[4] + '>Email</option>'+
            ' <option ' + typeselected[5] + '>List</option>'+
            ' <option ' + typeselected[6] + '>DateTime</option>'+
            ' <option ' + typeselected[7] + '>Relation</option>'+
            ' <option ' + typeselected[8] + '>Guid</option>'+
            ' <option ' + typeselected[9] + '>Phone number</option>'+
            ' <option ' + typeselected[10] + '>Raw file</option>'+
            ' <option ' + typeselected[11] + '>Location</option>'+
            ' <option ' + typeselected[12] + '>Image</option>'+
            ' <option ' + typeselected[13] + '>Auto increment ID</option>'+
            ' <option ' + typeselected[14] + '>Timestamp</option>'+
            ' <option ' + typeselected[15] + '>State data</option>'+
            ' <option ' + typeselected[16] + '>Currency</option>' +

            ' </select>' +
            '</td>' +
            '<td>' +
            '<select index="' + totalvarindex + '"  class="form-control" id="' + scopeid + '" ng-blur="VarScopeChange($event)">' +
            scopeComboText +
            '</select>' +
            '</td>' +
            '<td><input type="text" value="' + def + '" class="form-control" index="' + totalvarindex + '" placeholder="default value" ng-blur="VarDefChange($event)"  onkeyup="VarDefaultValidation(event)"/></td>' +
            '<td style="width:140px;">' +
            '<img src="content/images/warningvalue.png"  style="display:none;"/><img src="content/images/warning.png" style="display:none;" />' +
            '<button type="button" index="' + totalvarindex + '" class="btn btn-sm btn-danger pull-right" data-ng-click="removeRow($event)" style="margin-top:5px;"><span class="fa fa-trash-o fa-lg" style="color:white;"></span></button></td>' +
            '</tr>';
        }
        catch (ex) {
            //alert(ex.message + " , " + ex.number);
        }
    }

    $rootScope.ChangeFlowchartIndex = function () {
        commonActivityService.ChangeFlowchartIndex();
    }

    $scope.GetArgRow = function (arg) {
        var name = "";
        var placeholder = "default value";
        var def = "";
        var dir = "";
        var type = "";
        var req = false;
        if (arg != null) {
            name = arg.Name;
            def = arg.Default;
            dir = arg.Direction;
            type = arg.Type;
            req = arg.Required;
        }
        else {
            var default_variable_index = 1;
            var max_num = 0;
            for (var k = 1 ; k < $("#tblArg").find("tr").length; k++) {
                if ($("#tblArg").find("tr")[k].getElementsByTagName("td")[0].childNodes[0].value.toLowerCase().substring(0, 8) == "argument") {
                    var rest = $("#tblArg").find("tr")[k].getElementsByTagName("td")[0].childNodes[0].value.toLowerCase().replace("argument", "");
                    try {
                        var x = parseInt(rest);
                        if (x > max_num)
                            max_num = x;
                    }
                    catch (ex) {

                    }
                }
            }
            if (max_num == 0) {
                default_variable_index = 1;
            }
            else {
                default_variable_index = max_num + 1;
            }

            for (var r = 0 ; r < Arguments.length; r++) {
                if (Arguments[r].Name == "argument" + default_variable_index) {
                    default_variable_index++;
                }
            }
            var default_variable_name = "argument" + default_variable_index;
            name = "";//default_variable_name;
        }

        var typearray = [];
        typearray.push("String");
        typearray.push("Number");
        typearray.push("Boolean");
        typearray.push("Object");
        typearray.push("Email");
        typearray.push("List");
        typearray.push("DateTime");
        typearray.push("Relation");
        typearray.push("Guid");
        typearray.push("Phone number");
        typearray.push("Raw file");
        typearray.push("Location");
        typearray.push("Image");
        typearray.push("Auto increment ID");
        typearray.push("Timestamp");
        typearray.push("State data");
        typearray.push("Currency");
        var typeselected = [];
        var reqchecked = "";
        if (req)
            reqchecked = " checked ";


        for (var m = 0 ; m < typearray.length; m++) {
            if (type == typearray[m]) {
                typeselected[m] = " selected='selected' ";
            }
        }
        var disabled = "";
        var dirselected = [];
        if (dir == "In")
            dirselected[0] = " selected='selected' ";
        else if (dir == "Out") {
            dirselected[1] = " selected='selected' ";
            placeholder = "default value not supported";
            def = "";
            disabled = " disabled='disabled' ";
        }
        else if (dir == "In/Out") {
            dirselected[2] = " selected='selected' ";
            placeholder = "default value not supported";
            def = "";
            disabled = " disabled='disabled' ";
        }

        return '<tr class="variableRow animated fadeIn argRow" index="' + totalargindex + '" >' +
        '<td><input type="text" class="form-control" index="' + totalargindex + '" value="' + name + '" placeholder="argument name" onkeyup="ArgNameValidation(event)" data-ng-focus="addRowArg()" data-ng-click="addRowArg()" data-ng-blur="addRowArg($event)"/></td>' +
        '<td><select index="' + totalargindex + '"  class="form-control" ng-blur="ArgDirChange($event)"  onchange="ArgDirChangeEvent(event)">' +
        ' <option ' + dirselected[0] + '>In</option>' +
        ' <option ' + dirselected[1] + '>Out</option>' +
        ' <option ' + dirselected[2] + '>In/Out</option>' +
        ' </select>' +
        '</td>' +
        '<td>' +
        '<select index="' + totalargindex + '"  class="form-control"  ng-blur="ArgTypeChange($event)">' +
            ' <option ' + typeselected[0] + '>String</option>' +
            ' <option ' + typeselected[1] + '>Number</option>' +
            ' <option ' + typeselected[2] + '>Boolean</option>' +
            ' <option ' + typeselected[3] + '>Object</option>' +
            ' <option ' + typeselected[4] + '>Email</option>' +
            ' <option ' + typeselected[5] + '>List</option>' +
            ' <option ' + typeselected[6] + '>DateTime</option>' +
            ' <option ' + typeselected[7] + '>Relation</option>' +
            ' <option ' + typeselected[8] + '>Guid</option>' +
            ' <option ' + typeselected[9] + '>Phone number</option>' +
            ' <option ' + typeselected[10] + '>Raw file</option>' +
            ' <option ' + typeselected[11] + '>Location</option>' +
            ' <option ' + typeselected[12] + '>Image</option>' +
            ' <option ' + typeselected[13] + '>Auto increment ID</option>' +
            ' <option ' + typeselected[14] + '>Timestamp</option>' +
            ' <option ' + typeselected[15] + '>State data</option>' +
            ' <option ' + typeselected[16] + '>Currency</option>' +

        '</select>' +
        '</td>' +
        '<td><input value="' + def + '" type="text" class="form-control" index="' + totalargindex + '" ' + disabled + ' ng-blur="ArgDefChange($event)"  onkeyup="ArgDefaultValidation(event)" placeholder="' + placeholder + '" /></td>' +
        '<td style="width:100px"><input type="checkbox" onchange="ArgReqChange(event)" ' + reqchecked + '/></td><td style="width:140px;">' +
        '<img src="content/images/warningvalue.png"  style="display:none;"/><img src="content/images/warning.png" style="display:none;" />' +
        '<button index="' + totalargindex + '" type="button" class="btn btn-sm btn-danger pull-right" data-ng-click="removeRowArg()" style="margin-top:5px;"><span class="fa fa-trash-o fa-lg" style="color:white;"></span></button></td>' +
        '</tr>';
    }

    //


    //

    $scope.AddNewFlowchartToScopeCombos = function (index) {
        document.getElementById("cmbScope" + index).options.length = 0;
        for (var i = 0 ; i < CorrectFlowChartListForVariables.length; i++) {//loop through flowcharts
            var z = document.createElement("option");
            z.setAttribute("value", CorrectFlowChartValuesForVariables[i]);
            var correct_text = CorrectFlowChartListForVariables[i];
            if (CorrectFlowChartValuesForVariables[i] == currentRootNodeID) {
                correct_text = lblFakeTitle.value;
            }
            var t = document.createTextNode(correct_text);
            z.appendChild(t);
            document.getElementById("cmbScope" + index).appendChild(z);
        }
        for (var i = 0 ; i < document.getElementById("cmbScope" + index).options.length; i++) {
            if (document.getElementById("cmbScope" + index).options[i].value == currentFlowchartIDForVariables) {
                document.getElementById("cmbScope" + index).selectedIndex = i;
                document.getElementById("cmbScope" + index).setAttribute("prevval", currentFlowchartIDForVariables);
                var cmbscp = document.getElementById("cmbScope" + index);
                document.getElementById("cmbScope" + index).setAttribute("prevtext", cmbscp.options[cmbscp.selectedIndex].text);
                break;
            }
        }
    }

    $scope.addRow = function (event) {
        if (event) {
            var rowIndex = $(event.target).parent().parent().index();
            if ($scope.getRowcount() == rowIndex + 1 && event.target.value) {
                try {
                    var lastrow = $('#tbl tr:last');
                    var varname = lastrow.find("td")[0].children[0].value;
                    var vardef = lastrow.find("td")[3].children[0].value;
                    var vartype = lastrow.find("td")[1].children[0].options[lastrow.find("td")[1].children[0].selectedIndex].value;
                    var varscope = lastrow.find("td")[2].children[0].options[lastrow.find("td")[2].children[0].selectedIndex].text;
                    var varscopeid = lastrow.find("td")[2].children[0].options[lastrow.find("td")[2].children[0].selectedIndex].value;
                    Variables.push({ Name: varname, Default: vardef, Type: vartype, Scope: varscope, ScopeID: varscopeid, Index: totalvarindex });
                    totalvarindex++;
                }
                catch (ex) {
                    //alert(ex.message + " , " + ex.number);
                }
                var tr = $compile($scope.GetVarRow())($scope);
                angular.element('#tbl').append(tr);
                $scope.AddNewFlowchartToScopeCombos(parseInt(totalvarindex) - 1);
            }
            else {//update mode
                try {
                    for (var i = 0 ; i < $('#tbl').find("tr").length; i++) {
                        $('#tbl').find("tr")[i].getElementsByTagName("td")[4].children[1].style.display = "none";
                    }
                    var duplicate = false;

                    //find duplicate names and their row index
                    var lastnames = new Array();
                    var duplicatename;
                    for (var i = 0 ; i < $('#tbl').find("tr").length; i++) {
                        if (lastnames.indexOf($('#tbl').find("tr")[i].getElementsByTagName("td")[0].children[0].value) == -1) {
                            lastnames.push($('#tbl').find("tr")[i].getElementsByTagName("td")[0].children[0].value);
                        }
                        else {
                            duplicate = true;
                            duplicatename = $('#tbl').find("tr")[i].getElementsByTagName("td")[0].children[0].value;
                        }
                    }

                    if (duplicate) {
                        for (var i = 0 ; i < $('#tbl').find("tr").length; i++) {
                            if ($('#tbl').find("tr")[i].getElementsByTagName("td")[0].children[0].value == duplicatename) {
                                $('#tbl').find("tr")[i].getElementsByTagName("td")[4].children[1].style.display = "";
                                $('#tbl').find("tr")[i].getElementsByTagName("td")[4].children[1].title = "'" + duplicatename + "'" + " already exists in this scope.";
                            }
                        }
                        return;
                    }

                    var invalid = false;
                    for (var i = 0 ; i < $('#tbl').find("tr").length; i++) {
                        var n = $('#tbl').find("tr")[i].getElementsByTagName("td")[0].children[0].value;
                        var badname = false;
                        if (n.indexOf(" ") != -1) {
                            badname = true;
                        }
                        if (!badname) {
                            if (n.replace(/ /g, "") == "")
                                badname = true;
                        }

                        if (!badname) {
                            var firstChar = n.substr(0, 1);
                            if (firstChar == "0" || firstChar == "1" || firstChar == "2" || firstChar == "3" || firstChar == "4" || firstChar == "5" || firstChar == "6" || firstChar == "7"
                                 || firstChar == "8" || firstChar == "9" || firstChar == "/" || firstChar == "\\" || firstChar == "~" || firstChar == "!" || firstChar == "@" || firstChar == "#"
                                 || firstChar == "$" || firstChar == "%" || firstChar == "^" || firstChar == "&" || firstChar == "*" || firstChar == "(" || firstChar == ")" || firstChar == "-"
                                 || firstChar == "+" || firstChar == "=")
                                badname = true;
                        }
                        if (n == "")
                            badname = false;
                        if (badname) {
                            invalid = true;
                            $('#tbl').find("tr")[i].getElementsByTagName("td")[4].children[1].style.display = "";
                            $('#tbl').find("tr")[i].getElementsByTagName("td")[4].children[1].title = "Invalid variable name.";
                        }
                    }
                    if (invalid) {
                        return;
                    }

                    for (var i = 0 ; i < Variables.length; i++) {
                        if (Variables[i].Index == $(event.target).parent().parent().attr("index")) {
                            Variables[i].Name = $(event.target)[0].value;
                            break;
                        }
                    }
                }
                catch (ex) {
                    //alert(ex.message + " , " + ex.number);
                }
            }
        } else {
            var rowIndex = $(document.activeElement).parent().parent().index();
            if ($scope.getRowcount() == rowIndex + 1 && $(document.activeElement)[0].value) {
                try {
                    var lastrow = $('#tbl tr:last');
                    var varname = lastrow.find("td")[0].children[0].value;
                    var vardef = lastrow.find("td")[3].children[0].value;
                    var vartype = lastrow.find("td")[1].children[0].options[lastrow.find("td")[1].children[0].selectedIndex].value;
                    var varscope = lastrow.find("td")[2].children[0].options[lastrow.find("td")[2].children[0].selectedIndex].text;
                    var varscopeid = lastrow.find("td")[2].children[0].options[lastrow.find("td")[2].children[0].selectedIndex].value;
                    Variables.push({ Name: varname, Default: vardef, Type: vartype, Scope: varscope, ScopeID: varscopeid, Index: totalvarindex });
                    totalvarindex++;
                }
                catch (ex) {
                    //alert(ex.message + " , " + ex.number);
                }
                var tr = $compile($scope.GetVarRow())($scope);
                angular.element('#tbl').append(tr);
                $scope.AddNewFlowchartToScopeCombos(parseInt(totalvarindex) - 1);
            }
            else {//update mode
                try {
                    for (var i = 0 ; i < $('#tbl').find("tr").length; i++) {
                        $('#tbl').find("tr")[i].getElementsByTagName("td")[4].children[1].style.display = "none";
                    }
                    var duplicate = false;

                    //find duplicate names and their row index
                    var lastnames = new Array();
                    var duplicatename;
                    for (var i = 0 ; i < $('#tbl').find("tr").length; i++) {
                        if (lastnames.indexOf($('#tbl').find("tr")[i].getElementsByTagName("td")[0].children[0].value) == -1) {
                            lastnames.push($('#tbl').find("tr")[i].getElementsByTagName("td")[0].children[0].value);
                        }
                        else {
                            duplicate = true;
                            duplicatename = $('#tbl').find("tr")[i].getElementsByTagName("td")[0].children[0].value;
                        }
                    }

                    if (duplicate) {
                        for (var i = 0 ; i < $('#tbl').find("tr").length; i++) {
                            if ($('#tbl').find("tr")[i].getElementsByTagName("td")[0].children[0].value == duplicatename) {
                                $('#tbl').find("tr")[i].getElementsByTagName("td")[4].children[1].style.display = "";
                                $('#tbl').find("tr")[i].getElementsByTagName("td")[4].children[1].title = "'" + duplicatename + "'" + " already exists in this scope.";
                            }
                        }
                        return;
                    }

                    var invalid = false;
                    for (var i = 0 ; i < $('#tbl').find("tr").length; i++) {
                        var n = $('#tbl').find("tr")[i].getElementsByTagName("td")[0].children[0].value;
                        var badname = false;
                        if (n.indexOf(" ") != -1) {
                            badname = true;
                        }
                        if (!badname) {
                            if (n.replace(/ /g, "") == "")
                                badname = true;
                        }

                        if (!badname) {
                            var firstChar = n.substr(0, 1);
                            if (firstChar == "0" || firstChar == "1" || firstChar == "2" || firstChar == "3" || firstChar == "4" || firstChar == "5" || firstChar == "6" || firstChar == "7"
                                 || firstChar == "8" || firstChar == "9" || firstChar == "/" || firstChar == "\\" || firstChar == "~" || firstChar == "!" || firstChar == "@" || firstChar == "#"
                                 || firstChar == "$" || firstChar == "%" || firstChar == "^" || firstChar == "&" || firstChar == "*" || firstChar == "(" || firstChar == ")" || firstChar == "-"
                                 || firstChar == "+" || firstChar == "=")
                                badname = true;
                        }
                        if (n == "")
                            badname = false;
                        if (badname) {
                            invalid = true;
                            $('#tbl').find("tr")[i].getElementsByTagName("td")[4].children[1].style.display = "";
                            $('#tbl').find("tr")[i].getElementsByTagName("td")[4].children[1].title = "Invalid variable name.";
                        }
                    }
                    if (invalid) {
                        return;
                    }

                    for (var i = 0 ; i < Variables.length; i++) {
                        if (Variables[i].Index == $(document.activeElement).parent().parent().attr("index")) {
                            Variables[i].Name = $(document.activeElement)[0].value;
                            break;
                        }
                    }
                }
                catch (ex) {
                    //alert(ex.message + " , " + ex.number);
                }
            }
        }
        UpdateVariableCombo();
    }

    $scope.VarTypeChange = function (e) {
        var rowIndex = $(e.target).parent().parent().index();
        var varIndex = e.target.getAttribute("index");
        for (var i = 0 ; i < Variables.length; i++) {
            if (Variables[i].Index == varIndex) {
                var myrow = $("#tbl").find("tr:eq(" + rowIndex + ")");
                Variables[i].Type = myrow.find("td")[1].children[0].options[myrow.find("td")[1].children[0].selectedIndex].value;
                break;
            }
        }
    }

    $scope.VarScopeChange = function (e) {
        var rowIndex = $(e.target).parent().parent().index();
        var varIndex = e.target.getAttribute("index");
        for (var i = 0 ; i < Variables.length; i++) {
            if (Variables[i].Index == varIndex) {
                var myrow = $("#tbl").find("tr:eq(" + rowIndex + ")");
                Variables[i].Scope = myrow.find("td")[2].children[0].options[myrow.find("td")[2].children[0].selectedIndex].text;
                Variables[i].ScopeID = myrow.find("td")[2].children[0].options[myrow.find("td")[2].children[0].selectedIndex].value;
                break;
            }
        }
    }

    $scope.ArgTypeChange = function (e) {
        var rowIndex = $(e.target).parent().parent().index();
        var varIndex = e.target.getAttribute("index");
        for (var i = 0 ; i < Arguments.length; i++) {
            if (Arguments[i].Index == varIndex) {
                var myrow = $("#tblArg").find("tr:eq(" + rowIndex + ")");
                Arguments[i].Type = myrow.find("td")[2].children[0].options[myrow.find("td")[2].children[0].selectedIndex].value;
                break;
            }
        }
    }

    VarDefaultValidation = function (e) {
        try
        {
            var rowIndex = $(e.target).parent().parent().index();
            var bad = false;
            var myrow = $("#tbl").find("tr:eq(" + rowIndex + ")");
            var typeCmb = myrow.find("td")[1].children[0];
            var type = typeCmb.options[typeCmb.selectedIndex].value;
            var defvalue = myrow.find("td")[3].children[0].value;
            myrow.find("td")[4].children[0].style.display = "none";
            if (type == "Number") {
                if (isNaN(defvalue)) {
                    bad = true;
                    myrow.find("td")[4].children[0].title = "Error(s) encountered processing expression '" + defvalue + "'";
                }
            }
            else if (type == "Boolean") {
                if (defvalue != "True" && defvalue != "False") {
                    if (defvalue != "") {
                        bad = true;
                        myrow.find("td")[4].children[0].title = "Error(s) encountered processing expression '" + defvalue + "'";
                    }
                }
            }

            if (bad) {
                myrow.find("td")[4].children[0].style.display = "";
            }
        }
        catch (ex) {
            //alert(ex.message + " , " + ex.number);
        }
    }

    ArgReqChange = function (e) {
        console.clear();
        var rowIndex = $(e.target).parent().parent().index();
        var varIndex = $(e.target).parent().parent().attr("index");
        console.log(varIndex);
        for (var i = 0 ; i < Arguments.length; i++) {
            if (Arguments[i].Index == varIndex) {
                console.log($("#tblArg").find("tr:eq(" + rowIndex + ")").find("td")[4].children[0].checked);
                Arguments[i].Required = $("#tblArg").find("tr:eq(" + rowIndex + ")").find("td")[4].children[0].checked;
                break;
            }
        }
    }

    ArgDefaultValidation = function (e) {
        try {
            var rowIndex = $(e.target).parent().parent().index();
            var bad = false;
            var myrow = $("#tblArg").find("tr:eq(" + rowIndex + ")");
            var typeCmb = myrow.find("td")[2].children[0];
            var type = typeCmb.options[typeCmb.selectedIndex].value;
            var defvalue = myrow.find("td")[3].children[0].value;
            myrow.find("td")[5].children[0].style.display = "none";
            if (type == "Number") {
                if (isNaN(defvalue)) {
                    bad = true;
                    myrow.find("td")[5].children[0].title = "Error(s) encountered processing expression '" + defvalue + "'";
                }
            }
            else if (type == "Boolean") {
                if (defvalue != "True" && defvalue != "False") {
                    if (defvalue != "") {
                        bad = true;
                        myrow.find("td")[5].children[0].title = "Error(s) encountered processing expression '" + defvalue + "'";
                    }
                }
            }

            if (bad) {
                myrow.find("td")[5].children[0].style.display = "";
            }
        }
        catch (ex) {
            //alert(ex.message + " , " + ex.number);
        }
    }

    ArgDirChangeEvent = function (e) {
        var rowIndex = $(e.target).parent().parent().index();
        var myrow = $("#tblArg").find("tr:eq(" + rowIndex + ")");
        //Argument Direction set to Out or In/Out? Default value should be disabled
        try {
            var dir = myrow.find("td")[1].children[0].options[myrow.find("td")[1].children[0].selectedIndex].value;
            if (dir == "Out" || dir == "In/Out") {
                myrow.find("td")[3].children[0].setAttribute("placeholder", "default value not supported");
                myrow.find("td")[3].children[0].disabled = true;
                myrow.find("td")[3].children[0].value = "";
            }
            else {
                myrow.find("td")[3].children[0].setAttribute("placeholder", "default value");
                myrow.find("td")[3].children[0].disabled = false;
                myrow.find("td")[3].children[0].value = "";
            }
        }
        catch (ex) {
            //alert(ex.message + " , " + ex.number);
        }
    }

    $scope.ArgDirChange = function (e) {
        var rowIndex = $(e.target).parent().parent().index();
        var varIndex = e.target.getAttribute("index");
        for (var i = 0 ; i < Arguments.length; i++) {
            if (Arguments[i].Index == varIndex) {
                var myrow = $("#tblArg").find("tr:eq(" + rowIndex + ")");
                Arguments[i].Direction = myrow.find("td")[1].children[0].options[myrow.find("td")[1].children[0].selectedIndex].value;
                break;
            }
        }
    }

    $scope.VarDefChange = function (e) {
        var rowIndex = $(e.target).parent().parent().index();
        var varIndex = e.target.getAttribute("index");
        for (var i = 0 ; i < Variables.length; i++) {
            if (Variables[i].Index == varIndex) {
                Variables[i].Default = $("#tbl").find("tr:eq(" + rowIndex + ")").find("td")[3].children[0].value;
                break;
            }
        }
    }

    $scope.ArgDefChange = function (e) {
        var rowIndex = $(e.target).parent().parent().index();
        var varIndex = e.target.getAttribute("index");
        for (var i = 0 ; i < Arguments.length; i++) {
            if (Arguments[i].Index == varIndex) {
                Arguments[i].Default = $("#tblArg").find("tr:eq(" + rowIndex + ")").find("td")[3].children[0].value;
                break;
            }
        }
    }

    $scope.removeRow = function(e) {
        if (e && e.type != 'click') {
            if (e.charCode == 26) {
                if ($scope.getRowcount() > 1) {
                    $('#tbl tr:last').remove()
                }
            }
        } else {
            var rowIndex = $(document.activeElement).parent().parent().index();
            if ($scope.getRowcount() > 1) {
                var varIndex = $("#tbl").find("tr:eq(" + rowIndex + ")").attr("index");
                for (var i = 0 ; i < Variables.length; i++) {
                    if (Variables[i].Index == varIndex) {
                        Variables.splice(i, 1);
                        break;
                    }
                }
                $("#tbl").find("tr:eq(" + rowIndex + ")").remove();
            }
        }
        UpdateVariableCombo();
    }
    $scope.getRowcount = function() {
        return ($('#tbl tr').length);
    }
    var totalargindex = 0;
    $scope.addRowArg = function (event) {
        if (event) {
            var rowIndex = $(event.target).parent().parent().index();
            if ($scope.getRowcountArg() == rowIndex + 1 && event.target.value) {
                var lastrow = $('#tblArg tr:last');
                var argname = lastrow.find("td")[0].children[0].value;
                var argdef = lastrow.find("td")[3].children[0].value;
                var argtype = lastrow.find("td")[2].children[0].options[lastrow.find("td")[2].children[0].selectedIndex].value;
                var argdir = lastrow.find("td")[1].children[0].options[lastrow.find("td")[1].children[0].selectedIndex].text;
                var argreq = lastrow.find("td")[4].children[0].checked;
                Arguments.push({ Name: argname, Default: argdef, Type: argtype, Direction: argdir, Index: totalargindex, Required: argreq });
                totalargindex++;
                var tr = $compile($scope.GetArgRow())($scope);
                angular.element('#tblArg').append(tr);
            }
            else {//update mode
                for (var i = 0 ; i < $('#tblArg').find("tr").length; i++) {
                    $('#tblArg').find("tr")[i].getElementsByTagName("td")[5].children[1].style.display = "none";
                }
                var duplicate = false;

                //find duplicate names and their row index
                var lastnames = new Array();
                var duplicatename;
                for (var i = 0 ; i < $('#tblArg').find("tr").length; i++) {
                    if (lastnames.indexOf($('#tblArg').find("tr")[i].getElementsByTagName("td")[0].children[0].value) == -1) {
                        lastnames.push($('#tblArg').find("tr")[i].getElementsByTagName("td")[0].children[0].value);
                    }
                    else {
                        duplicate = true;
                        duplicatename = $('#tblArg').find("tr")[i].getElementsByTagName("td")[0].children[0].value;
                    }
                }

                if (duplicate) {
                    for (var i = 0 ; i < $('#tblArg').find("tr").length; i++) {
                        if ($('#tblArg').find("tr")[i].getElementsByTagName("td")[0].children[0].value == duplicatename) {
                            $('#tblArg').find("tr")[i].getElementsByTagName("td")[5].children[1].style.display = "";
                            $('#tblArg').find("tr")[i].getElementsByTagName("td")[5].children[1].title = "'" + duplicatename + "'" + " already exists in this scope.";
                        }
                    }
                    return;
                }

                var invalid = false;
                for (var i = 0 ; i < $('#tblArg').find("tr").length; i++) {
                    var n = $('#tblArg').find("tr")[i].getElementsByTagName("td")[0].children[0].value;
                    var badname = false;
                    if (n.indexOf(" ") != -1) {
                        badname = true;
                    }
                    if (!badname) {
                        if (n.replace(/ /g, "") == "")
                            badname = true;
                    }

                    if (!badname) {
                        var firstChar = n.substr(0, 1);
                        if (firstChar == "0" || firstChar == "1" || firstChar == "2" || firstChar == "3" || firstChar == "4" || firstChar == "5" || firstChar == "6" || firstChar == "7"
                             || firstChar == "8" || firstChar == "9" || firstChar == "/" || firstChar == "\\" || firstChar == "~" || firstChar == "!" || firstChar == "@" || firstChar == "#"
                             || firstChar == "$" || firstChar == "%" || firstChar == "^" || firstChar == "&" || firstChar == "*" || firstChar == "(" || firstChar == ")" || firstChar == "-"
                             || firstChar == "+" || firstChar == "=")
                            badname = true;
                    }

                    if (n == "")
                        badname = false;
                    if (badname) {
                        invalid = true;
                        $('#tblArg').find("tr")[i].getElementsByTagName("td")[5].children[1].style.display = "";
                        $('#tblArg').find("tr")[i].getElementsByTagName("td")[5].children[1].title = "Invalid argument name.";
                    }
                }
                if (invalid) {
                    return;
                }

                for (var i = 0 ; i < Arguments.length; i++) {
                    if (Arguments[i].Index == $(event.target).parent().parent().attr("index")) {
                        Arguments[i].Name = event.target.value;
                        break;
                    }
                }
            }
        } else {
            var rowIndex = $(document.activeElement).parent().parent().index();
            if ($scope.getRowcountArg() == rowIndex + 1 && $(document.activeElement)[0].value) {
                var lastrow = $('#tblArg tr:last');
                var argname = lastrow.find("td")[0].children[0].value;
                var argdef = lastrow.find("td")[3].children[0].value;
                var argtype = lastrow.find("td")[2].children[0].options[lastrow.find("td")[2].children[0].selectedIndex].value;
                var argdir = lastrow.find("td")[1].children[0].options[lastrow.find("td")[1].children[0].selectedIndex].text;
                var argreq = lastrow.find("td")[4].children[0].checked;
                Arguments.push({ Name: argname, Default: argdef, Type: argtype, Direction: argdir, Index: totalargindex, Required: argreq });
                totalargindex++;
                var tr = $compile($scope.GetArgRow())($scope);
                angular.element('#tblArg').append(tr);
            }
            else {//update mode
                try
                {
                    for (var i = 0 ; i < $('#tblArg').find("tr").length; i++) {
                        $('#tblArg').find("tr")[i].getElementsByTagName("td")[5].children[1].style.display = "none";
                    }
                    var duplicate = false;

                    //find duplicate names and their row index
                    var lastnames = new Array();
                    var duplicatename;
                    for (var i = 0 ; i < $('#tblArg').find("tr").length; i++) {
                        if (lastnames.indexOf($('#tblArg').find("tr")[i].getElementsByTagName("td")[0].children[0].value) == -1) {
                            lastnames.push($('#tblArg').find("tr")[i].getElementsByTagName("td")[0].children[0].value);
                        }
                        else {
                            duplicate = true;
                            duplicatename = $('#tblArg').find("tr")[i].getElementsByTagName("td")[0].children[0].value;
                        }
                    }

                    if (duplicate) {
                        for (var i = 0 ; i < $('#tblArg').find("tr").length; i++) {
                            if ($('#tblArg').find("tr")[i].getElementsByTagName("td")[0].children[0].value == duplicatename) {
                                $('#tblArg').find("tr")[i].getElementsByTagName("td")[5].children[1].style.display = "";
                                $('#tblArg').find("tr")[i].getElementsByTagName("td")[5].children[1].title = "'" + duplicatename + "'" + " already exists in this scope.";
                            }
                        }
                        return;
                    }

                    var invalid = false;
                    for (var i = 0 ; i < $('#tblArg').find("tr").length; i++) {
                        var n = $('#tblArg').find("tr")[i].getElementsByTagName("td")[0].children[0].value;
                        var badname = false;
                        if (n.indexOf(" ") != -1) {
                            badname = true;
                        }
                        if (!badname) {
                            if (n.replace(/ /g, "") == "")
                                badname = true;
                        }

                        if (!badname) {
                            var firstChar = n.substr(0, 1);
                            if (firstChar == "0" || firstChar == "1" || firstChar == "2" || firstChar == "3" || firstChar == "4" || firstChar == "5" || firstChar == "6" || firstChar == "7"
                                 || firstChar == "8" || firstChar == "9" || firstChar == "/" || firstChar == "\\" || firstChar == "~" || firstChar == "!" || firstChar == "@" || firstChar == "#"
                                 || firstChar == "$" || firstChar == "%" || firstChar == "^" || firstChar == "&" || firstChar == "*" || firstChar == "(" || firstChar == ")" || firstChar == "-"
                                 || firstChar == "+" || firstChar == "=")
                                badname = true;
                        }

                        if (badname) {
                            invalid = true;
                            $('#tblArg').find("tr")[i].getElementsByTagName("td")[5].children[1].style.display = "";
                            $('#tblArg').find("tr")[i].getElementsByTagName("td")[5].children[1].title = "Invalid argument name.";
                        }
                    }
                    if (invalid) {
                        return;
                    }

                    for (var i = 0 ; i < Arguments.length; i++) {
                        if (Arguments[i].Index == $(document.activeElement).parent().parent().attr("index")) {
                            Arguments[i].Name = $(document.activeElement)[0].value;
                            break;
                        }
                    }
                }
                catch (ex) {
                    //alert(ex.message + " , " + ex.number);
                }
            }
        }
        UpdateVariableCombo();
    }

    $scope.removeRowArg = function (e) {
        if (e) {
            if (e.charCode == 26) {
                if ($scope.getRowcount() > 1) {
                    $('#tblArg tr:last').remove()
                }
            }
        } else {
            var rowIndex = $(document.activeElement).parent().parent().index();
            if ($scope.getRowcountArg() > 1) {
                var argIndex = $("#tblArg").find("tr:eq(" + rowIndex + ")").attr("index");
                for (var i = 0 ; i < Arguments.length; i++) {
                    if (Arguments[i].Index == argIndex) {
                        Arguments.splice(i, 1);
                        break;
                    }
                }
                $("#tblArg").find("tr:eq(" + rowIndex + ")").remove();
            }
        }
        UpdateVariableCombo();
    }
    $scope.getRowcountArg = function() {
        return ($('#tblArg tr').length);
    }


//
    // Code for the delete key.
    //
    var deleteKeyCode = 46;

    //
    // Code for control key.
    //
    var ctrlKeyCode = 65;

    $scope.FindMinDistance = function(src, dest1, dest2, dest3) {
        var dest;
        var dist1, dist2, dist3;
        dist1 = Math.sqrt(Math.pow(src.x - dest1.x, 2) + Math.pow(src.y - dest1.y, 2));
        dist2 = Math.sqrt(Math.pow(src.x - dest2.x, 2) + Math.pow(src.y - dest2.y, 2));
        dist3 = Math.sqrt(Math.pow(src.x - dest3.x, 2) + Math.pow(src.y - dest3.y, 2));

        if (dist1 < dist2) {
            if (dist3 < dist1) {
                dest = dest3;
            } else {
                dest = dest1;
            }
        } else {
            if (dist3 < dist2) {
                dest = dest3;
            } else {
                dest = dest2;
            }
        }

        return dest;
    };

    $scope.InputConnectorPosition = function (typeIndex, src, inputid) {
        var dir = "";
        try {
            if (nodeInputOutputConnector[typeIndex - 1].input2x != null) { //we have multiple inputs? test src/dest distance value for all of them and use the nearest dest (how to find the distance? lalal...)
                var tmpNode = $scope.chartViewModel.GetNodeById(inputid);
                var p3 = { x: 1000000, y: 1000000 };
                if (nodeInputOutputConnector[typeIndex - 1].input3x != null) {
                    p3 = { x: nodeInputOutputConnector[typeIndex - 1].input3x, y: nodeInputOutputConnector[typeIndex - 1].input3y };
                }

                var dest = $scope.FindMinDistance(src, {
                    x: parseInt(tmpNode.data.x) + parseInt(nodeInputOutputConnector[typeIndex - 1].inputx),
                    y: parseInt(tmpNode.data.y) + parseInt(nodeInputOutputConnector[typeIndex - 1].inputy)
                },
                {
                    x: parseInt(tmpNode.data.x) + parseInt(nodeInputOutputConnector[typeIndex - 1].input2x),
                    y:
                        parseInt(tmpNode.data.y) + parseInt(nodeInputOutputConnector[typeIndex - 1].input2y)
                },
                {
                    x: parseInt(tmpNode.data.x) + parseInt(p3.x),
                    y: parseInt(tmpNode.data.y) + parseInt(p3.y)
                });
                if (dest.x == parseInt(tmpNode.data.x) + parseInt(nodeInputOutputConnector[typeIndex - 1].inputx) &&
                    dest.y == parseInt(tmpNode.data.y) + parseInt(nodeInputOutputConnector[typeIndex - 1].inputy))
                    dir = nodeInputOutputConnector[typeIndex - 1].inputdir;
                else if (dest.x == parseInt(tmpNode.data.x) + parseInt(nodeInputOutputConnector[typeIndex - 1].input2x) &&
                    dest.y == parseInt(tmpNode.data.y) + parseInt(nodeInputOutputConnector[typeIndex - 1].input2y))
                    dir = nodeInputOutputConnector[typeIndex - 1].input2dir;
                else if (dest.x == parseInt(tmpNode.data.x) + parseInt(nodeInputOutputConnector[typeIndex - 1].input3x) &&
                    dest.y == parseInt(tmpNode.data.y) + parseInt(nodeInputOutputConnector[typeIndex - 1].input3y))
                    dir = nodeInputOutputConnector[typeIndex - 1].input3dir;

                return { x: parseInt(dest.x) - parseInt(tmpNode.data.x), y: parseInt(dest.y) - parseInt(tmpNode.data.y), dir: dir };
            } else {
                dir = nodeInputOutputConnector[typeIndex - 1].inputdir;
                return { x: nodeInputOutputConnector[typeIndex - 1].inputx, y: nodeInputOutputConnector[typeIndex - 1].inputy, dir: dir };
            }
        } catch (ex) {
            //alert(ex.message + " , " + ex.number);
        }
    };

    $scope.OutputConnectorPosition1 = function(typeIndex) {
        var dir = nodeInputOutputConnector[typeIndex - 1].output1dir;
        try {
            return { x: nodeInputOutputConnector[typeIndex - 1].output1x, y: nodeInputOutputConnector[typeIndex - 1].output1y, dir: dir };
        } catch (ex) {
            //alert(ex.message + " , " + ex.number);
        }
    };

    $scope.OutputConnectorPosition2 = function(typeIndex) {
        var dir = nodeInputOutputConnector[typeIndex - 1].output2dir;
        try {
            return { x: nodeInputOutputConnector[typeIndex - 1].output2x, y: nodeInputOutputConnector[typeIndex - 1].output2y, dir: dir };
        } catch (ex) {

        }
    };


    //
    // Set to true when the ctrl key is down.
    //
    var ctrlDown = false;

    //
    // Code for A key.
    //
    //var aKeyCode = 17;

    //
    // Code for esc key.
    //
    var escKeyCode = 27;

    //
    // Selects the next node id.
    //
    var nextNodeID = 10;

    //
    // Setup the data-model for the chart.
    //
    var chartDataModel = {
        nodes: [
        ],


    };

    //
    // Event handler for key-down on the flowchart.
    //

    $scope.mouseMove = function() {

    }

    $scope.mouseUp = function(evt) {

    }

    $scope.keyDown = function(evt) {
        if (evt.keyCode === ctrlKeyCode) {
            ctrlDown = true;
        }
    };

    //
    // Event handler for key-up on the flowchart.
    //
    $scope.keyUp = function(evt) {
        try {
            var currentnodeindex = $scope.chartViewModel.GetSelectedNodeIndex();
        } catch (ex) {
            //alert(ex.message + " , " + ex.number);
        }

        if (evt.keyCode == escKeyCode) {
            // Escape.
            $scope.chartViewModel.deselectAll();
        }

        if (evt.keyCode === ctrlKeyCode) {
            ctrlDown = false;
        }
    };

    $scope.AlignTop = function() {
        $scope.chartViewModel.AlignTop();
    };

    $scope.AlignBottom = function() {
        $scope.chartViewModel.AlignBottom();
    };

    $rootScope.ApplyClick = function (index, item) {
        $scope.chartViewModel.ToolClick(index, item);
    };

    $scope.ToolClick = function(index) {
        $scope.chartViewModel.ToolClick(index);
    };

    $scope.AlignRight = function() {
        $scope.chartViewModel.AlignRight();
    };

    $scope.DeleteSelectedObjects = function() {
        $scope.chartViewModel.DeleteSelectedObjects();
    };

    $scope.AlignLeft = function() {
        $scope.chartViewModel.AlignLeft();
    };

    $scope.TextChange = function() {
        $scope.chartViewModel.TextChange();
    };


    $scope.GetItemName = function(index) {
        return names[index];
    };

    $scope.GetItemIcon = function(index) {
        return icons[index];
    };

    $rootScope.$on('ShowCommonActivity', function() {
        $scope.Init()
    });
    $scope.Init = function () {
        divContainer.addEventListener("scroll", function () {
            $("#txtTitle").css("top", (90 - divContainer.scrollTop) + "px");
        }, false);
        //dynamically create flowchart labels as soon as program is initialized
        for (var _x = 0; _x < numSupportedFlowcharts; _x++) {
            try {
                var activityHTML = "";
                var displaynone = "";
                if (_x > 0) {
                    displaynone = " style='display:none;' ";
                }
                else {
                    activityHTML += "<div class='nestedTriangle'></div><div class='nestedTriangle1'></div>";
                }
                activityHTML += "<div " + displaynone + " id='lblActivity" + (_x + 1) + "' onclick=VirtualSetRoot('0;0') class='nestedwfBox pull-right' ><div class='nestedwfBox_content'><h5 class='grayColor'>Flowchart</h5><span class='fa fa-cogs fa-3x   '></span></div></div>";
                var tempActivityHTML = $compile(activityHTML)($scope);
                angular.element(document.getElementById('divHierarchy')).append(tempActivityHTML);
            }
            catch (ex) {
                //alert(ex.message + " , " + ex.number);
            }
        }

        try {
            var initJSON = [];
            initJSON.push({ name: "if", icon: "content/images/workflow/component/decision.png", outputCount: 2, backColor: "#ffccdd", cap: "True_False", sw: 105, sh: 125 });
            initJSON.push({ name: "assign", icon: "content/images/workflow/component/assign.png", outputCount: 1, backColor: "#2ab989", cap: "", sw: 300, sh: 100 });
            initJSON.push({ name: "loop", icon: "content/images/workflow/component/assign.png", outputCount: 1, backColor: "#2ab989", cap: "", sw: 60, sh: 80 });
            initJSON.push({ name: "showform", icon: "content/images/workflow/component/assign.png", outputCount: 1, backColor: "#2ab989", cap: "", sw: 60, sh: 80 });
            initJSON.push({ name: "webservice", icon: "content/images/workflow/component/assign.png", outputCount: 1, backColor: "#2ab989", cap: "", sw: 60, sh: 80 });
            initJSON.push({ name: "flowchart", icon: "content/images/workflow/component/start.png", outputCount: 0, backColor: "lightgray", cap: "", sw: 100, sh: 200 });
            initJSON.push({ name: "Flowchart", icon: "content/images/workflow/component/start.png", outputCount: 1, backColor: "orange", cap: "", sw: 200, sh: 50 });
            initJSON.push({ name: "start", icon: "content/images/algorithm5.png", outputCount: 1, backColor: "orange", cap: "", sw: 100, sh: 100 });
            initJSON.push({ name: "keyvalue", icon: "content/images/algorithm5.png", outputCount: 1, backColor: "orange", cap: "", sw: 185, sh: 65 });
            initJSON.push({ name: "connector", icon: "content/images/algorithm5.png", outputCount: 1, backColor: "orange", cap: "", sw: 140, sh: 150 });
            initJSON.push({ name: "collector", icon: "content/images/algorithm5.png", outputCount: 1, backColor: "orange", cap: "", sw: 140, sh: 140 });
            initJSON.push({ name: "workflow", icon: "content/images/algorithm5.png", outputCount: 1, backColor: "orange", cap: "", sw: 180, sh: 140 });
            initJSON.push({ name: "foreach", icon: "content/images/workflow/component/assign.png", outputCount: 1, backColor: "#2ab989", cap: "", sw: 240, sh: 60 });

            var templatenames = new Array();
            var dialoghtml = "<table style='width:100%;'>";
            var name = "";
            var icon = "";
            var i = 1;

            for (var k = 0 ; k < initJSON.length; k++) {
                $scope.chartViewModel.names.push(initJSON[k].name);
                $scope.chartViewModel.icons.push(initJSON[k].icon);
                $scope.chartViewModel.outputCountList.push(initJSON[k].outputCount);
                $scope.chartViewModel.backColorList.push(initJSON[k].backColor);
                $scope.chartViewModel.captionList.push(initJSON[k].cap);
                $scope.chartViewModel.ShapeWidthList.push(initJSON[k].sw);
                $scope.chartViewModel.ShapeHeightList.push(initJSON[k].sh);
                var style = "";
                if (i == 7 || i == 8 || i == 4 || i == 5 || i == 3)
                    style = "display:none";
                var rowhtml = "<div class='col-lg-12 col-xs-12' id='lr" + i.toString() + "'style='text-align:center;margin-bottom:15px;" + style + "' onclick='alert()' onmouseover='RowOver2(this.id)' onmouseout='RowOut(this.id)' draggable='true' ng-click='ctrl.ToolClick(" + i.toString() + ")'><img src='" + initJSON[k].icon + "' class='imgLeft' index='" + i + "' style='cursor:grabbing;' /><h4 style='text-align:center' >" + initJSON[k].name + "</h4></div>";
                var temp = $compile(rowhtml)($scope);
                angular.element(document.getElementsByClassName('commonActivies')).append(temp);
                i++;
            }

        } catch (ex) {

        }
        try {
            divDrop.style.position = "relative";
            if (divDrop.childNodes.length > 1) {
                var child = document.getElementsByClassName("ActivityItem");

                if (child.length != 0) {
                    $(divDrop).width(child[0].style.width);
                    $(divDrop).height(child[0].style.height);
                }
            }

            divDrop.style.left = (((window.innerWidth - divDrop.clientWidth) / 2) - 80) + "px";
            divDrop.style.top = (((window.innerHeight - divDrop.clientHeight) / 2) - 15) + "px";
        } catch (ex) {

        }
        divContainer.style.height = ($(window).height() - 115) + "px";//30

        var isOpera = !!window.opera || navigator.userAgent.indexOf(' OPR/') >= 0;
        // Opera 8.0+ (UA detection to detect Blink/v8-powered Opera)
        var isFirefox = typeof InstallTrigger !== 'undefined'; // Firefox 1.0+
        var isSafari = Object.prototype.toString.call(window.HTMLElement).indexOf('Constructor') > 0;
        // At least Safari 3+: "[object HTMLElementConstructor]"
        var isChrome = !!window.chrome && !isOpera; // Chrome 1+
        var isIE = /*@cc_on!@*/false || !!document.documentMode; // At least IE6

        if (isFirefox)
            divContainer.style.width = ($(window).width() - 0) + "px";
        else if (isChrome)
            divContainer.style.width = ($(window).width() - 0) + "px";

        try {
            document.createEvent("TouchEvent");
            imgResize.style.width = "30px";
            imgResize.style.height = "30px";
            btnTouchScroll.style.display = "";
            btnTouchMultiSelect.style.display = "";
            lblCopyTouch.style.display = "";
            lblPasteTouch.style.display = "";
        } catch (e) {
            //return false;//non-touch device (desktop browser)
        }
    };

    TouchMultiSelect = function () {
        if (btnTouchMultiSelect.innerHTML == "MultiSelect On") {
            btnTouchMultiSelect.innerHTML = "MultiSelect Off";
        }
        else {
            btnTouchMultiSelect.innerHTML = "MultiSelect On";
        }
    };

    TouchScroll = function () {
        if (btnTouchScroll.innerHTML == "Scroll On") {
            btnTouchScroll.innerHTML = "Scroll Off";
        }
        else {
            btnTouchScroll.innerHTML = "Scroll On";
        }
    };

    $scope.Undo = function(tmpMode) {
        if (UndoList.length > 0) {
            try {
                var last = UndoList[UndoList.length - 1];
                var tmpX = last.x;
                var tmpY = last.y;
                var flwWidth;
                var connectionInfo;
                var flwHeight;
                var xArray = jQuery.extend(true, {}, last.xArray);
                var yArray = jQuery.extend(true, {}, last.yArray);
                var tmpX2;
                var tmpY2;
                var beforePath;
                var inputDir;
                var outputDir;
                var PathdestNodeID;
                var PathsrcNodeID;
                var beforePathPointArray;
                var xArray2 = new Array();
                var yArray2 = new Array();
                var undoitem = UndoList.pop();
                var oldInputDir, oldOutputDir, newInputDir, newOutputDir, oldSource, oldDest, newSource, newDest, oldInputPos, connectionIndex,
                oldOutputPos, newInputPos, newOutputPos, switchMode, connectorIndex, newconnectorIndex, oldconnectorIndex;

                if (last.action != "addconnection" && last.action != "deletenode" && last.action != "paste" && last.action != "changewidth" && last.action != "changeheight" && last.action != "dragresize" &&
                    last.action != "changetitle" && last.action != "flowchartdrilldown" && last.action != "changetitleNode" && last.action != "assignTextChange1" && last.action != "assignTextChange2" &&
                    last.action != "ifTextChange" && last.action != "pathChange" && last.action != "flowchartdrillup" && last.action != "deleteconnection" && last.action != "pathConnectorMove" &&
                    last.action != "varCreate" && last.action != "varDelete" && last.action != "varName" && last.action != "varType" && last.action != "varScope" && last.action != "varDefault" &&
                    last.action != "argCreate" && last.action != "argDelete" && last.action != "argName" && last.action != "argType" && last.action != "argDir" && last.action != "argDefault"
                    && last.action != "lineswitch") {
                    if (last.action != "nodemove") {
                        var tmpn = $scope.chartViewModel.GetNodeByGUID(undoitem.GUID);
                        undoitem.x = tmpX2 = tmpn.data.x;
                        undoitem.y = tmpY2 = tmpn.data.y;
                    }
                    if (last.action == "nodemove") {
                        undoitem.xArray.length = 0;
                        undoitem.yArray.length = 0;
                        flwWidth = last.flwWidthBefore;
                        flwHeight = last.flwHeightBefore;
                        for (var q = 0; q < undoitem.GUIDArray.length; q++) {
                            var tmpn2 = $scope.chartViewModel.GetNodeByGUID(undoitem.GUIDArray[q]);
                            undoitem.xArray.push(tmpn2.data.x);
                            undoitem.yArray.push(tmpn2.data.y);
                            xArray2.push(tmpn2.data.x);
                            yArray2.push(tmpn2.data.y);
                        }
                    }
                } else {
                    undoitem.x = tmpX2 = 0;
                    undoitem.y = tmpY2 = 0;
                }
                undoitem.id = undoitem.id;

                if (last.action == "addconnection" || last.action == "deleteconnection") {
                    inputDir = last.inputDir;
                    outputDir = last.outputDir;
                }

                last.x = tmpX;
                last.y = tmpY;
                undoitem.x = tmpX2;
                undoitem.y = tmpY2;

                if (tmpMode != null) {
                    undoitem.x = tmpX;
                    undoitem.y = tmpY;
                }

                if (last.action == "addnode") {
                    flwWidth = last.flwWidthBefore;
                    flwHeight = last.flwHeightBefore;
                }

                if (last.action == "nodemove") {
                    undoitem.xArray = xArray2;
                    undoitem.yArray = yArray2;
                }
                var slider1 = 0;
                var slider2 = 0;
                if (last.action == "changewidth" || last.action == "changeheight") {
                    slider1 = last.slider1;
                    slider2 = last.slider2;
                }
                var titleBefore = "";
                var titleAfter = "";
                if (last.action == "changetitle" || last.action == "changetitleNode") {
                    titleBefore = last.titleBefore;
                    titleAfter = last.titleAfter;
                }

                var assignTextBefore = "";
                var assignTextAfter = "";
                if (last.action == "assignTextChange1" || last.action == "assignTextChange2") {
                    assignTextBefore = last.assignTextBefore;
                    assignTextAfter = last.assignTextAfter;
                }

                var ifTextBefore = "";
                var ifTextAfter = "";
                if (last.action == "ifTextChange") {
                    ifTextBefore = last.ifTextBefore;
                    ifTextAfter = last.ifTextAfter;
                }

                if (last.action == "lineswitch") {
                    oldInputDir = last.oldInputDir;
                    switchMode = last.switchMode;
                    oldOutputDir = last.oldOutputDir;
                    newInputDir = last.newInputDir;
                    newOutputDir = last.newOutputDir;
                    oldSource = last.oldSource;
                    oldDest = last.oldDest;
                    newSource = last.newSource;
                    newDest = last.newDest;
                    oldInputPos = last.oldInputPos;
                    connectionIndex = last.connectionIndex;
                    oldOutputPos = last.oldOutputPos;
                    newInputPos = last.newInputPos;
                    newOutputPos = last.newOutputPos;
                    newconnectorIndex = last.newconnectorIndex;
                    oldconnectorIndex = last.oldconnectorIndex;
                }

                var tmpYOffsetBefore = "";
                var tmpYOffsetAfter = "";
                var tmpXOffsetBefore = "";
                var tmpXOffsetAfter = "";
                var tmpYOffsetBefore1 = "";
                var tmpYOffsetAfter1 = "";
                var tmpYOffsetBefore2 = "";
                var tmpYOffsetAfter2 = "";
                var srcNodeID = "";
                var dstNodeID = "";

                if (last.action == "pathChange") {
                    srcNodeID = last.srcNodeID;
                    dstNodeID = last.dstNodeID;
                    tmpYOffsetBefore = last.tmpYOffsetBefore;
                    tmpYOffsetAfter = last.tmpYOffsetAfter;
                    tmpXOffsetBefore = last.tmpXOffsetBefore;
                    tmpXOffsetAfter = last.tmpXOffsetAfter;
                    tmpYOffsetBefore1 = last.tmpYOffsetBefore1;
                    tmpYOffsetAfter1 = last.tmpYOffsetAfter1;
                    tmpYOffsetBefore2 = last.tmpYOffsetBefore2;
                    tmpYOffsetAfter2 = last.tmpYOffsetAfter2;
                }
                var GUIDArray = new Array();
                if (last.action == "nodemove") {
                    GUIDArray = last.GUIDArray;
                }
                var srcNodeGUID;
                var dstNodeGUID;
                if (last.action == "addconnection" || last.action == "deleteconnection") {
                    srcNodeGUID = last.srcNodeGUID;
                    dstNodeGUID = last.dstNodeGUID;
                }
                var GUID;
                if (last.action == "addnode" || last.action == "deletenode") {
                    GUID = last.GUID;
                }
                var dstFlowchart;
                if (last.action == "flowchartdrilldown" || last.action == "flowchartdrillup") {
                    dstFlowchart = last.dstFlowchart;
                }
                var srcFlowchart;
                if (last.action == "flowchartdrillup" || last.action == "flowchartdrilldown") {
                    srcFlowchart = last.srcFlowchart;
                }
                var sizeBefore;
                var sizeAfter;
                if (last.action == "dragresize") {
                    sizeBefore = last.sizeBefore;
                    sizeAfter = last.sizeAfter;
                }
                var varGUID;
                var argGUID;
                var beforeValue;
                var afterValue;
                var varargName;
                var varargDefault;
                var varargType;
                var varScope;
                var varScopeID;
                var argDir;

                if (last.action == "varName" || last.action == "varType" || last.action == "varScope" || last.action == "varDefault") {
                    beforeValue = last.beforeValue;
                    afterValue = last.afterValue;
                    varGUID = last.varGUID;
                }

                if (last.action == "varCreate" || last.action == "varDelete") {
                    varGUID = last.varGUID;
                    varargName = last.varargName;
                    varargDefault = last.varargDefault;
                    varargType = last.varargType;
                    varScope = last.varScope;
                    varScopeID = last.varScopeID;
                }

                if (last.action == "argName" || last.action == "argType" || last.action == "argDir" || last.action == "argDefault") {
                    beforeValue = last.beforeValue;
                    afterValue = last.afterValue;
                    argGUID = last.argGUID;
                }

                if (last.action == "argCreate" || last.action == "argDelete") {
                    argGUID = last.argGUID;
                    varargName = last.varargName;
                    varargDefault = last.varargDefault;
                    varargType = last.varargType;
                    argDir = last.argDir;
                }
                if (last.action == "paste") {
                    connectionInfo = last.connectionInfo;
                }
                if (last.action == "pathConnectorMove") {
                    beforePath = last.beforePath;
                    beforePathPointArray = last.beforePathPointArray;
                    PathdestNodeID = last.PathdestNodeID;
                    PathsrcNodeID = last.PathsrcNodeID;
                }

                if (tmpMode == null) {
                    $scope.PerformUndoRedoAction({
                        action: last.action,
                        id: last.id,
                        x: tmpX,
                        y: tmpY,
                        parent: last.parent,
                        typeIndex: last.typeIndex,
                        onumber: last.onumber,
                        nodeindices: last.nodeindices,
                        ix: last.ix,
                        iy: last.iy,
                        o1x: last.o1x,
                        o1y: last.o1y,
                        o2x: last.o2x,
                        o2y: last.o2y,
                        sourceNodeID: last.sourceNodeID,
                        destNodeID: last.destNodeID,
                        slider1: slider1,
                        titleBefore: titleBefore,
                        titleAfter: titleAfter,
                        slider2: slider2,
                        assignTextBefore: assignTextBefore,
                        assignTextAfter: assignTextAfter,
                        ifTextBefore: ifTextBefore,
                        ifTextAfter: ifTextAfter,
                        srcNodeID: srcNodeID,
                        dstNodeID: dstNodeID,
                        GUIDArray: GUIDArray,
                        yArray: yArray,
                        xArray: xArray,
                        PathsrcNodeID: PathsrcNodeID,
                        tmpYOffsetBefore: tmpYOffsetBefore,
                        tmpYOffsetAfter: tmpYOffsetAfter,
                        tmpXOffsetBefore: tmpXOffsetBefore,
                        tmpXOffsetAfter: tmpXOffsetAfter,
                        PathdestNodeID: PathdestNodeID,
                        tmpYOffsetBefore1: tmpYOffsetBefore1,
                        tmpYOffsetAfter1: tmpYOffsetAfter1,
                        tmpYOffsetBefore2: tmpYOffsetBefore2,
                        tmpYOffsetAfter2: tmpYOffsetAfter2,
                        srcNodeGUID: srcNodeGUID,
                        dstNodeGUID: dstNodeGUID,
                        GUID: GUID,
                        dstFlowchart: dstFlowchart,
                        srcFlowchart: srcFlowchart,
                        pasteX: last.pasteX,
                        pasteY: last.pasteY,
                        pasteIndex: last.pasteIndex,
                        pasteParent: last.pasteParent,
                        sizeAfter: sizeAfter,
                        sizeBefore: sizeBefore,
                        beforePathPointArray: beforePathPointArray,
                        varGUID: varGUID,
                        argGUID: argGUID,
                        beforeValue: beforeValue,
                        afterValue: afterValue,
                        flwWidthBefore: flwWidth,
                        flwHeightBefore: flwHeight,
                        beforePath: beforePath,
                        varargName: varargName,
                        varargDefault: varargDefault,
                        varargType: varargType,
                        varScope: varScope,
                        argDir: argDir,
                        varScopeID: varScopeID,
                        connectionInfo: connectionInfo,
                        inputDir: inputDir,
                        outputDir: outputDir,
                        oldInputDir: oldInputDir,
                        oldOutputDir: oldOutputDir,
                        newInputDir: newInputDir,
                        newOutputDir: newOutputDir,
                        oldSource: oldSource,
                        oldDest: oldDest,
                        newSource: newSource,
                        newDest: newDest,
                        oldInputPos: oldInputPos,
                        connectionIndex: connectionIndex,
                        oldOutputPos: oldOutputPos,
                        newInputPos: newInputPos,
                        newOutputPos: newOutputPos,
                        switchMode: switchMode,
                        oldconnectorIndex: oldconnectorIndex,
                        newconnectorIndex: newconnectorIndex
                    }, true);
                } else {
                    RedoList.push(undoitem);
                    return last;
                }

                RedoList.push(undoitem);
            } catch (ex) {
                //alert(ex.message + " , " + ex.number);
            }
        }
    };

    $scope.Redo = function() {
        if (RedoList.length > 0) {
            var last = RedoList[RedoList.length - 1];
            var tmpX = last.x;
            var tmpY = last.y;
            var xArray = jQuery.extend(true, {}, last.xArray);
            var yArray = jQuery.extend(true, {}, last.yArray);
            var tmpX2;
            var tmpY2;
            var xArray2 = new Array();
            var connectionInfo;
            var yArray2 = new Array();
            var flwWidth;
            var flwHeight;
            var afterPath;
            var afterPathPointArray;
            var inputDir;
            var outputDir;
            var PathsrcNodeID;
            var PathdestNodeID;
            var oldInputDir, oldOutputDir, newInputDir, newOutputDir, oldSource, oldDest, newSource, newDest, oldInputPos, connectionIndex,
            oldOutputPos, newInputPos, newOutputPos, switchMode, oldconnectorIndex, newconnectorIndex;

            var redoitem = RedoList.pop();
            try {
                if (redoitem.action != "nodemove") {
                    var tmpn = $scope.chartViewModel.GetNodeByGUID(redoitem.GUID);
                    redoitem.x = tmpX2 = tmpn.data.x;
                    redoitem.y = tmpY2 = tmpn.data.y;
                } else {

                }
                redoitem.xArray.length = 0;
                redoitem.yArray.length = 0;
                for (var q = 0; q < redoitem.GUIDArray.length; q++) {
                    var tmpn2 = $scope.chartViewModel.GetNodeByGUID(redoitem.GUIDArray[q]);
                    redoitem.xArray.push(tmpn2.data.x);
                    redoitem.yArray.push(tmpn2.data.y);
                    xArray2.push(tmpn2.data.x);
                    yArray2.push(tmpn2.data.y);
                }

            } catch (ex) {

            }
            last.x = tmpX;
            last.y = tmpY;
            redoitem.x = tmpX2;
            redoitem.y = tmpY2;
            redoitem.xArray = xArray2;
            redoitem.yArray = yArray2;

            if (last.action == "addconnection" || last.action == "deleteconnection") {
                inputDir = last.inputDir;
                outputDir = last.outputDir;
            }

            var slider1 = 0;
            var slider2 = 0;
            if (last.action == "changewidth" || last.action == "changeheight") {
                slider1 = last.slider1;
                slider2 = last.slider2;
            }
            var titleBefore = "";
            var titleAfter = "";
            if (last.action == "changetitle" || last.action == "changetitleNode") {
                titleBefore = last.titleBefore;
                titleAfter = last.titleAfter;
            }

            if (last.action == "pathConnectorMove") {
                afterPath = last.afterPath;
                afterPathPointArray = last.afterPathPointArray;
                PathsrcNodeID = last.PathsrcNodeID;
                PathdestNodeID = last.PathdestNodeID;
            }

            var assignTextBefore = "";
            var assignTextAfter = "";
            if (last.action == "assignTextChange1" || last.action == "assignTextChange2") {
                assignTextBefore = last.assignTextBefore;
                assignTextAfter = last.assignTextAfter;
            }

            var ifTextBefore = "";
            var ifTextAfter = "";
            if (last.action == "ifTextChange") {
                ifTextBefore = last.ifTextBefore;
                ifTextAfter = last.ifTextAfter;
            }

            var tmpYOffsetBefore = "";
            var tmpYOffsetAfter = "";
            var tmpXOffsetBefore = "";
            var tmpXOffsetAfter = "";
            var tmpYOffsetBefore1 = "";
            var tmpYOffsetAfter1 = "";
            var tmpYOffsetBefore2 = "";
            var tmpYOffsetAfter2 = "";
            var srcNodeID = "";
            var dstNodeID = "";
            if (last.action == "pathChange") {
                srcNodeID = last.srcNodeID;
                dstNodeID = last.dstNodeID;
                tmpYOffsetBefore = last.tmpYOffsetBefore;
                tmpYOffsetAfter = last.tmpYOffsetAfter;
                tmpXOffsetBefore = last.tmpXOffsetBefore;
                tmpXOffsetAfter = last.tmpXOffsetAfter;
                tmpYOffsetBefore1 = last.tmpYOffsetBefore1;
                tmpYOffsetAfter1 = last.tmpYOffsetAfter1;
                tmpYOffsetBefore2 = last.tmpYOffsetBefore2;
                tmpYOffsetAfter2 = last.tmpYOffsetAfter2;
            }

            var GUIDArray = new Array();
            if (last.action == "nodemove") {
                GUIDArray = last.GUIDArray;
                flwWidth = last.flwWidthAfter;
                flwHeight = last.flwHeightAfter;
            }

            if (last.action == "addnode") {
                flwWidth = last.flwWidthAfter;
                flwHeight = last.flwHeightAfter;
            }

            var srcNodeGUID;
            var dstNodeGUID;
            if (last.action == "addconnection" || last.action == "deleteconnection") {
                srcNodeGUID = last.srcNodeGUID;
                dstNodeGUID = last.dstNodeGUID;
            }
            var GUID;
            if (last.action == "addnode" || last.action == "deletenode") {
                GUID = last.GUID;
            }

            var dstFlowchart;
            if (last.action == "flowchartdrilldown" || last.action == "flowchartdrillup") {
                dstFlowchart = last.dstFlowchart;
            }

            var srcFlowchart;
            if (last.action == "flowchartdrillup" || last.action == "flowchartdrilldown") {
                srcFlowchart = last.srcFlowchart;
            }

            var sizeBefore;
            var sizeAfter;
            if (last.action == "dragresize") {
                sizeBefore = last.sizeBefore;
                sizeAfter = last.sizeAfter;
            }

            if (last.action == "lineswitch") {
                oldInputDir = last.oldInputDir;
                switchMode = last.switchMode;
                oldconnectorIndex = last.oldconnectorIndex;
                oldOutputDir = last.oldOutputDir;
                newInputDir = last.newInputDir;
                newOutputDir = last.newOutputDir;
                oldSource = last.oldSource;
                oldDest = last.oldDest;
                newSource = last.newSource;
                newDest = last.newDest;
                oldInputPos = last.oldInputPos;
                connectionIndex = last.connectionIndex;
                oldOutputPos = last.oldOutputPos;
                newInputPos = last.newInputPos;
                newOutputPos = last.newOutputPos;
                newconnectorIndex = last.newconnectorIndex;
            }

            var varGUID;
            var argGUID;
            var beforeValue;
            var afterValue;
            var varargName;
            var varargDefault;
            var varargType;
            var varScope;
            var argDir;
            var varScopeID;

            if (last.action == "varName" || last.action == "varType" || last.action == "varScope" || last.action == "varDefault") {
                beforeValue = last.beforeValue;
                afterValue = last.afterValue;
                varGUID = last.varGUID;
            }

            if (last.action == "varCreate" || last.action == "varDelete") {
                varGUID = last.varGUID;
                varargName = last.varargName;
                varargDefault = last.varargDefault;
                varargType = last.varargType;
                varScope = last.varScope;
                varScopeID = last.varScopeID;
            }

            if (last.action == "argName" || last.action == "argType" || last.action == "argDir" || last.action == "argDefault") {
                beforeValue = last.beforeValue;
                afterValue = last.afterValue;
                argGUID = last.argGUID;
            }

            if (last.action == "argCreate" || last.action == "argDelete") {
                argGUID = last.argGUID;
                varargName = last.varargName;
                varargDefault = last.varargDefault;
                varargType = last.varargType;
                argDir = last.argDir;
            }

            if (last.action == "paste") {
                connectionInfo = last.connectionInfo;
            }

            $scope.PerformUndoRedoAction({
                action: last.action,
                id: last.id,
                x: tmpX,
                y: tmpY,
                parent: last.parent,
                typeIndex: last.typeIndex,
                onumber: last.onumber,
                nodeindices: last.nodeindices,
                ix: last.ix,
                iy: last.iy,
                o1x: last.o1x,
                o1y: last.o1y,
                o2x: last.o2x,
                o2y: last.o2y,
                sourceNodeID: last.sourceNodeID,
                destNodeID: last.destNodeID,
                slider1: slider1,
                titleBefore: titleBefore,
                titleAfter: titleAfter,
                slider2: slider2,
                assignTextBefore: assignTextBefore,
                assignTextAfter: assignTextAfter,
                ifTextBefore: ifTextBefore,
                ifTextAfter: ifTextAfter,
                srcNodeID: srcNodeID,
                dstNodeID: dstNodeID,
                GUIDArray: GUIDArray,
                yArray: yArray,
                xArray: xArray,
                tmpYOffsetBefore: tmpYOffsetBefore,
                tmpYOffsetAfter: tmpYOffsetAfter,
                tmpXOffsetBefore: tmpXOffsetBefore,
                tmpXOffsetAfter: tmpXOffsetAfter,
                tmpYOffsetBefore1: tmpYOffsetBefore1,
                tmpYOffsetAfter1: tmpYOffsetAfter1,
                tmpYOffsetBefore2: tmpYOffsetBefore2,
                tmpYOffsetAfter2: tmpYOffsetAfter2,
                PathdestNodeID: PathdestNodeID,
                srcNodeGUID: srcNodeGUID,
                dstNodeGUID: dstNodeGUID,
                GUID: GUID,
                dstFlowchart: dstFlowchart,
                srcFlowchart: srcFlowchart,
                pasteX: last.pasteX,
                pasteY: last.pasteY,
                pasteIndex: last.pasteIndex,
                pasteParent: last.pasteParent,
                sizeAfter: sizeAfter,
                sizeBefore: sizeBefore,
                afterPathPointArray: afterPathPointArray,
                PathsrcNodeID: PathsrcNodeID,
                varGUID: varGUID,
                argGUID: argGUID,
                beforeValue: beforeValue,
                afterValue: afterValue,
                flwWidthAfter: flwWidth,
                flwHeightAfter: flwHeight,
                afterPath: afterPath,
                varargName: varargName,
                varargDefault: varargDefault,
                varargType: varargType,
                varScope: varScope,
                argDir: argDir,
                varScopeID: varScopeID,
                connectionInfo: connectionInfo,
                inputDir: inputDir,
                outputDir: outputDir,
                oldInputDir: oldInputDir,
                oldOutputDir: oldOutputDir,
                newInputDir: newInputDir,
                newOutputDir: newOutputDir,
                oldSource: oldSource,
                oldDest: oldDest,
                newSource: newSource,
                newDest: newDest,
                oldInputPos: oldInputPos,
                connectionIndex: connectionIndex,
                oldOutputPos: oldOutputPos,
                newInputPos: newInputPos,
                newOutputPos: newOutputPos,
                switchMode: switchMode,
                oldconnectorIndex: oldconnectorIndex,
                newconnectorIndex: newconnectorIndex
            }, false);
            UndoList.push(redoitem);
        }
    };

    $scope.PerformUndoRedoAction = function (action, undo) {
        for (var i = 0; i < $scope.chartViewModel.nodes.length; i++) {
            $scope.chartViewModel.nodes[i].deselect();
        }

        var currentAction = action;
        if (currentAction.action == "paste") {
            $scope.chartViewModel.deselectAll();
            if (undo) {
                for (var i = 0; i < currentAction.nodeindices.length; i++) {
                    $scope.chartViewModel.GetNodeByGUID(currentAction.nodeindices[i]).select();
                }
                $scope.chartViewModel.deleteSelected(false);
            } else {
                try {
                    for (var i = 0; i < currentAction.nodeindices.length; i++) {
                        var index = currentAction.pasteIndex[i];
                        var sw = $scope.chartViewModel.GetShapeWidthList(parseInt(index) - 1);
                        var sh = $scope.chartViewModel.GetShapeHeightList(parseInt(index) - 1);
                        $scope.chartViewModel.CreateNewNode($scope.chartViewModel.GetNumNodes(), currentAction.pasteX[i], currentAction.pasteY[i],
                            currentAction.pasteParent[i], sw, sh, index, false, currentAction.nodeindices[i]);
                    }

                    for (var i = 0; i < currentAction.connectionInfo.length; i++) {
                        try {
                            var conn = currentAction.connectionInfo[i];
                            var A_index;
                            var B_index;

                            for (var j = 0; j < $scope.chartViewModel.GetNumNodes() ; j++) {
                                if ($scope.chartViewModel.GetNode(j).data.id == conn.sourceNodeID) {
                                    A_index = j;
                                }
                                if ($scope.chartViewModel.GetNode(j).data.id == conn.destNodeID) {
                                    B_index = j;
                                }
                            }
                            var totalNumNodes = $scope.chartViewModel.GetNumNodes();
                            var source = $scope.chartViewModel.GetNode((totalNumNodes - currentAction.nodeindices.length) + conn.srcIndex);
                            var dest = $scope.chartViewModel.GetNode((totalNumNodes - currentAction.nodeindices.length) + conn.destIndex);
                            source.data.connected = true;
                            dest.data.connected = true;
                            source.data.numChild++;
                            $scope.chartViewModel.createNewConnection(source.outputConnectors[source.data.numChild - 1],
                                dest.inputConnectors[0], false,
                                conn.ix,
                                conn.iy,
                                conn.o1x, conn.o1y, conn.o2x, conn.o2y, conn.onumber, conn._cap, "", conn.inputdir, conn.outputdir, $scope.chartViewModel);
                        } catch (ex) {
                            //alert(ex.message + "," + currentAction.action);
                        }
                    }
                } catch (ex) {
                    //alert(ex.message + "," + currentAction.action);
                }
            }
        }
        else if (currentAction.action == "lineswitch") {
            var finalInputDir, finalOutputDir, finalSource, finalDest, finalInputPos, finalOutputPos, connectionIndex, finalconnectorIndex;
            connectionIndex = currentAction.connectionIndex;
            if (undo) {
                finalInputDir = currentAction.oldInputDir;
                finalOutputDir = currentAction.oldOutputDir;
                finalSource = currentAction.oldSource;
                finalDest = currentAction.oldDest;
                finalInputPos = currentAction.oldInputPos;
                finalOutputPos = currentAction.oldOutputPos;
                finalconnectorIndex = currentAction.oldconnectorIndex;
            }
            else {
                finalInputDir = currentAction.newInputDir;
                finalOutputDir = currentAction.newOutputDir;
                finalSource = currentAction.newSource;
                finalDest = currentAction.newDest;
                finalInputPos = currentAction.newInputPos;
                finalOutputPos = currentAction.newOutputPos;
                finalconnectorIndex = currentAction.newconnectorIndex;
            }
            try{
                if (currentAction.switchMode == "src") {
                    $scope.chartViewModel.connections[connectionIndex].source.parentNode().data.numChild--;
                    $scope.chartViewModel.connections[connectionIndex].source.parentNode().data.activeConnector = "";
                    if ($scope.chartViewModel.connections[connectionIndex].source.parentNode().data.numChild == 0 &&
                        $scope.chartViewModel.connections[connectionIndex].source.parentNode().data.activeConnectorInput == "")
                        $scope.chartViewModel.connections[connectionIndex].source.parentNode().data.connected = false;
                    $scope.chartViewModel.connections[connectionIndex].source.parentNode().data.connectorclicked = false;
                }
                else {
                    var indexToBeDeleted = "";
                    if ($scope.chartViewModel.connections[connectionIndex].inputDir == "up") {
                        indexToBeDeleted = "0";
                    }
                    else if ($scope.chartViewModel.connections[connectionIndex].inputDir == "right") {
                        indexToBeDeleted = "1";
                    }
                    else if ($scope.chartViewModel.connections[connectionIndex].inputDir == "down") {
                        indexToBeDeleted = "2";
                    }
                    else if ($scope.chartViewModel.connections[connectionIndex].inputDir == "left") {
                        indexToBeDeleted = "3";
                    }

                    $scope.chartViewModel.connections[connectionIndex].dest.parentNode().data.activeConnectorInput =
                        $scope.chartViewModel.connections[connectionIndex].dest.parentNode().data.activeConnectorInput.replace(indexToBeDeleted, "");
                    if ($scope.chartViewModel.connections[connectionIndex].dest.parentNode().data.numChild == 0 &&
                        $scope.chartViewModel.connections[connectionIndex].dest.parentNode().data.activeConnectorInput == "")
                        $scope.chartViewModel.connections[connectionIndex].dest.parentNode().data.connected = false;
                }

                $scope.chartViewModel.connections[connectionIndex].inputDir = finalInputDir;
                $scope.chartViewModel.connections[connectionIndex].outputDir = finalOutputDir;
                $scope.chartViewModel.connections[connectionIndex].source = finalSource;
                $scope.chartViewModel.connections[connectionIndex].dest = finalDest;
                $scope.chartViewModel.connections[connectionIndex].inputx = finalInputPos.x;
                $scope.chartViewModel.connections[connectionIndex].inputy = finalInputPos.y;
                $scope.chartViewModel.connections[connectionIndex].ox1 = finalOutputPos.x1;
                $scope.chartViewModel.connections[connectionIndex].oy1 = finalOutputPos.y1;
                $scope.chartViewModel.connections[connectionIndex].ox2 = finalOutputPos.x2;
                $scope.chartViewModel.connections[connectionIndex].oy2 = finalOutputPos.y2;
                if (currentAction.switchMode == "src") {
                    $scope.chartViewModel.connections[connectionIndex].source.parentNode().data.numChild++;
                    $scope.chartViewModel.connections[connectionIndex].source.parentNode().data.connected = true;
                }
                else {
                    $scope.chartViewModel.connections[connectionIndex].dest.parentNode().data.activeConnectorInput += finalconnectorIndex;
                    $scope.chartViewModel.connections[connectionIndex].dest.parentNode().data.connected = true;
                }
            }
            catch (ex) {
                //alert(ex.message + " , " + ex.number);
            }
        }
        else if (currentAction.action == "varCreate") {
            var table = document.getElementById('tblVariables');
            if (undo) {
                for (var i = 0; i < table.getElementsByTagName("tr").length; i++) {
                    if (table.getElementsByTagName("tr")[i].getAttribute("GUID") == currentAction.varGUID) {
                        table.deleteRow(i);
                        break;
                    }
                }
                for (var i = 0; i < tmpVariables.length; i++) {
                    if (tmpVariables[i].varGUID == currentAction.varGUID) {
                        tmpVariables.splice(i, 1);
                        break;
                    }
                }
            } else {
                var varJSON = {
                    guid: currentAction.varGUID,
                    name: currentAction.varargName,
                    def: currentAction.varargDefault,
                    type: currentAction.varargType,
                    scope: currentAction.varScope,
                    scopeid: currentAction.varScopeID
                };
                AddVariable(varJSON);
            }
        } else if (currentAction.action == "pathConnectorMove") {
            var path;
            var PathPointArray;
            if (undo) {
                path = currentAction.beforePath;
                PathPointArray = currentAction.beforePathPointArray;
            } else {
                path = currentAction.afterPath;
                PathPointArray = currentAction.afterPathPointArray;
            }
            for (var i = 0; i < $scope.chartViewModel.connections.length; i++) {
                var conn = $scope.chartViewModel.connections[i];
                if (currentAction.PathsrcNodeID == conn.source.parentNode().data.id && currentAction.PathdestNodeID == conn.dest.parentNode().data.id) {
                    if (path.substr(0, 1) == "X") //lastPath
                    {
                        conn.customLineData = "";
                        conn.lastPath = path.replace("X", "");
                    } else
                        conn.customLineData = path.replace("Y", "").replace("X", "");
                    conn.PathPointArray = PathPointArray;
                }
            }
        } else if (currentAction.action == "varDelete") {
            var table = document.getElementById('tblVariables');
            if (undo) {
                var varJSON = {
                    guid: currentAction.varGUID,
                    name: currentAction.varargName,
                    def: currentAction.varargDefault,
                    type: currentAction.varargType,
                    scope: currentAction.varScope,
                    scopeid: currentAction.varScopeID
                };
                AddVariable(varJSON);
            } else {
                for (var i = 0; i < table.getElementsByTagName("tr").length; i++) {
                    if (table.getElementsByTagName("tr")[i].getAttribute("GUID") == currentAction.varGUID) {
                        table.deleteRow(i);
                        break;
                    }
                }
                for (var i = 0; i < tmpVariables.length; i++) {
                    if (tmpVariables[i].varGUID == currentAction.varGUID) {
                        tmpVariables.splice(i, 1);
                        break;
                    }
                }
            }
        } else if (currentAction.action == "varName") {
            var table = document.getElementById('tblVariables');
            var value;
            if (undo) {
                value = currentAction.beforeValue;
            } else {
                value = currentAction.afterValue;
            }
            for (var i = 0; i < tmpVariables.length; i++) {
                if (tmpVariables[i].varGUID == currentAction.varGUID) {
                    tmpVariables[i].Name = value;
                }
            }
            for (var i = 0; i < table.getElementsByTagName("tr").length; i++) {
                if (table.getElementsByTagName("tr")[i].getAttribute("GUID") == currentAction.varGUID) {
                    table.getElementsByTagName("tr")[i].getElementsByTagName("td")[0].childNodes[0].value = value;
                    table.getElementsByTagName("tr")[i].getElementsByTagName("td")[0].childNodes[0].setAttribute("prev", value);
                    break;
                }
            }
        } else if (currentAction.action == "varType") {
            var table = document.getElementById('tblVariables');
            var value;
            if (undo) {
                value = currentAction.beforeValue;
            } else {
                value = currentAction.afterValue;
            }
            for (var i = 0; i < tmpVariables.length; i++) {
                if (tmpVariables[i].varGUID == currentAction.varGUID) {
                    tmpVariables[i].Type = value;
                }
            }
            for (var i = 0; i < table.getElementsByTagName("tr").length; i++) {
                if (table.getElementsByTagName("tr")[i].getAttribute("GUID") == currentAction.varGUID) {
                    var obj = table.getElementsByTagName("tr")[i].getElementsByTagName("td")[1].childNodes[0];
                    for (var j = 0; j < obj.options.length; j++) {
                        if (obj.options[j].value == value) {
                            obj.selectedIndex = j;
                        }
                    }
                    table.getElementsByTagName("tr")[i].getElementsByTagName("td")[1].childNodes[0].setAttribute("prev", value);
                    break;
                }
            }
        } else if (currentAction.action == "varScope") {
            var table = document.getElementById('tblVariables');
            var value;
            if (undo) {
                value = currentAction.beforeValue;
            } else {
                value = currentAction.afterValue;
            }
            for (var i = 0; i < tmpVariables.length; i++) {
                if (tmpVariables[i].varGUID == currentAction.varGUID) {
                    tmpVariables[i].Type = value;
                }
            }
            for (var i = 0; i < table.getElementsByTagName("tr").length; i++) {
                if (table.getElementsByTagName("tr")[i].getAttribute("GUID") == currentAction.varGUID) {
                    var obj = table.getElementsByTagName("tr")[i].getElementsByTagName("td")[2].childNodes[0];
                    for (var j = 0; j < obj.options.length; j++) {
                        if (obj.options[j].value == value) {
                            obj.selectedIndex = j;
                        }
                    }
                    table.getElementsByTagName("tr")[i].getElementsByTagName("td")[2].childNodes[0].setAttribute("prev", value);
                    break;
                }
            }
        } else if (currentAction.action == "varDefault") {
            var table = document.getElementById('tblVariables');
            var value;
            if (undo) {
                value = currentAction.beforeValue;
            } else {
                value = currentAction.afterValue;
            }
            for (var i = 0; i < tmpVariables.length; i++) {
                if (tmpVariables[i].varGUID == currentAction.varGUID) {
                    tmpVariables[i].Default = value;
                }
            }
            for (var i = 0; i < table.getElementsByTagName("tr").length; i++) {
                if (table.getElementsByTagName("tr")[i].getAttribute("GUID") == currentAction.varGUID) {
                    table.getElementsByTagName("tr")[i].getElementsByTagName("td")[3].childNodes[0].value = value;
                    table.getElementsByTagName("tr")[i].getElementsByTagName("td")[3].childNodes[0].setAttribute("prev", value);
                }
            }
        } else if (currentAction.action == "argCreate") {
            var table = document.getElementById('tblArguments');
            if (undo) {
                for (var i = 0; i < table.getElementsByTagName("tr").length; i++) {
                    if (table.getElementsByTagName("tr")[i].getAttribute("GUID") == currentAction.argGUID) {
                        table.deleteRow(i);
                        break;
                    }
                }
                for (var i = 0; i < Arguments.length; i++) {
                    if (Arguments[i].argGUID == currentAction.argGUID) {
                        Arguments.splice(i, 1);
                        break;
                    }
                }
            } else {
                var argJSON = {
                    guid: currentAction.argGUID,
                    name: currentAction.varargName,
                    def: currentAction.varargDefault,
                    type: currentAction.varargType,
                    dir: currentAction.argDir
                };
                AddArgument(argJSON);
            }
        } else if (currentAction.action == "argDelete") {
            var table = document.getElementById('tblArguments');
            if (undo) {
                var argJSON = {
                    guid: currentAction.argGUID,
                    name: currentAction.varargName,
                    def: currentAction.varargDefault,
                    type: currentAction.varargType,
                    dir: currentAction.argDir
                };
                AddArgument(argJSON);
            } else {
                for (var i = 0; i < table.getElementsByTagName("tr").length; i++) {
                    if (table.getElementsByTagName("tr")[i].getAttribute("GUID") == currentAction.argGUID) {
                        table.deleteRow(i);
                        break;
                    }
                }
                for (var i = 0; i < Arguments.length; i++) {
                    if (Arguments[i].argGUID == currentAction.argGUID) {
                        Arguments.splice(i, 1);
                        break;
                    }
                }
            }
        } else if (currentAction.action == "argName") {
            var table = document.getElementById('tblArguments');
            if (undo) {

            } else {

            }
        } else if (currentAction.action == "argType") {
            var table = document.getElementById('tblArguments');
            var value;
            if (undo) {
                value = currentAction.beforeValue;
            } else {
                value = currentAction.afterValue;
            }
            for (var i = 0; i < Arguments.length; i++) {
                if (Arguments[i].argGUID == currentAction.argGUID) {
                    Arguments[i].Type = value;
                }
            }
            for (var i = 0; i < table.getElementsByTagName("tr").length; i++) {
                if (table.getElementsByTagName("tr")[i].getAttribute("GUID") == currentAction.argGUID) {
                    var obj = table.getElementsByTagName("tr")[i].getElementsByTagName("td")[2].childNodes[0];
                    for (var j = 0; j < obj.options.length; j++) {
                        if (obj.options[j].value == value) {
                            obj.selectedIndex = j;
                        }
                    }
                    table.getElementsByTagName("tr")[i].getElementsByTagName("td")[2].childNodes[0].setAttribute("prev", value);
                    break;
                }
            }
        } else if (currentAction.action == "argDir") {
            var table = document.getElementById('tblArguments');
            var value;
            if (undo) {
                value = currentAction.beforeValue;
            } else {
                value = currentAction.afterValue;
            }
            for (var i = 0; i < Arguments.length; i++) {
                if (Arguments[i].argGUID == currentAction.argGUID) {
                    Arguments[i].Direction = value;
                }
            }
            for (var i = 0; i < table.getElementsByTagName("tr").length; i++) {
                if (table.getElementsByTagName("tr")[i].getAttribute("GUID") == currentAction.argGUID) {
                    var obj = table.getElementsByTagName("tr")[i].getElementsByTagName("td")[1].childNodes[0];
                    for (var j = 0; j < obj.options.length; j++) {
                        if (obj.options[j].value == value) {
                            obj.selectedIndex = j;
                            if (j == 1 || j == 2) {
                                table.getElementsByTagName("tr")[i].getElementsByTagName("td")[3].childNodes[0].value = "Default value not supported";
                            } else {
                                table.getElementsByTagName("tr")[i].getElementsByTagName("td")[3].childNodes[0].value = "";
                            }
                        }
                    }
                    table.getElementsByTagName("tr")[i].getElementsByTagName("td")[1].childNodes[0].setAttribute("prev", value);
                    break;
                }
            }
        } else if (currentAction.action == "argDefault") {
            var table = document.getElementById('tblArguments');
            if (undo) {

            } else {

            }
        } else if (currentAction.action == "changetitleNode") {
            if (undo) {
                FlowchartTitles += action.id + "=" + action.titleBefore + ",";
            } else {
                FlowchartTitles += action.id + "=" + action.titleAfter + ",";
            }
        } else if (currentAction.action == "changetitle") {
            var val = "";
            if (undo) {
                lblFakeTitle.value = currentAction.titleBefore;
                val = currentAction.titleBefore;
            } else {
                lblFakeTitle.value = currentAction.titleAfter;
                val = currentAction.titleAfter;
            }

            var current_item;
            for (var i = 1; i < (numSupportedFlowcharts + 1) ; i++) {
                var name = "lblActivity" + i.toString();
                if (document.getElementById(name).style.display != "none") {
                    current_item = i;
                }
            }

            var lblname = "lblActivity" + current_item;
            $("#" + lblname).html(" > " + val);

        } else if (currentAction.action == "pathChange") {

            if (undo) {
                for (var i = 0; i < $scope.chartViewModel.connections.length; i++) {
                    if ($scope.chartViewModel.connections[i].source.parentNode().data.id == currentAction.srcNodeID &&
                        $scope.chartViewModel.connections[i].dest.parentNode().data.id == currentAction.dstNodeID) {
                        $scope.chartViewModel.connections[i].tmpYOffset = currentAction.tmpYOffsetBefore;
                        $scope.chartViewModel.connections[i].tmpXOffset = currentAction.tmpXOffsetBefore;
                        $scope.chartViewModel.connections[i].tmpYOffset1 = currentAction.tmpYOffsetBefore1;
                        $scope.chartViewModel.connections[i].tmpYOffset2 = currentAction.tmpYOffsetBefore2;
                    }
                }
            } else {
                for (var i = 0; i < $scope.chartViewModel.connections.length; i++) {
                    if ($scope.chartViewModel.connections[i].source.parentNode().data.id == currentAction.srcNodeID &&
                        $scope.chartViewModel.connections[i].dest.parentNode().data.id == currentAction.dstNodeID) {
                        $scope.chartViewModel.connections[i].tmpYOffset = currentAction.tmpYOffsetAfter;
                        $scope.chartViewModel.connections[i].tmpXOffset = currentAction.tmpXOffsetAfter;
                        $scope.chartViewModel.connections[i].tmpYOffset1 = currentAction.tmpYOffsetAfter1;
                        $scope.chartViewModel.connections[i].tmpYOffset2 = currentAction.tmpYOffsetAfter2;
                    }
                }
            }
        } else if (currentAction.action == "ifTextChange") {
            var text = "txtIfCondition" + currentAction.id;
            if (undo) {
                document.getElementById(text).value = currentAction.ifTextBefore;
            } else {
                document.getElementById(text).value = currentAction.ifTextAfter;
            }
        } else if (currentAction.action == "assignTextChange1") {
            var text = "txtAssignVariable" + currentAction.id;
            if (undo) {
                document.getElementById(text).value = currentAction.assignTextBefore;
            } else {
                document.getElementById(text).value = currentAction.assignTextAfter;
            }
        } else if (currentAction.action == "assignTextChange2") {
            var text = "txtAssignVariable2-" + currentAction.id;

            if (undo) {
                document.getElementById(text).value = currentAction.assignTextBefore;
            } else {
                document.getElementById(text).value = currentAction.assignTextAfter;
            }
        }
        //else if (currentAction.action == "flowchartdrilldown") {
        //    var drilltext = " up to ";
        //    if (undo) {
        //        var real_activity_index = 0;
        //        for (var q = 1; q < (numSupportedFlowcharts + 1) ; q++) {
        //            var n = "lblActivity" + q;
        //            if (document.getElementById(n).style.display == "") {
        //                real_activity_index = q - 2;
        //            }
        //        }
        //        $scope.SetRoot(currentAction.srcFlowchart, false, drilltext);
        //    } else {

        //        //
        //        for (var i = 1; i < (numSupportedFlowcharts + 1) ; i++) {
        //            var name = "lblActivity" + i.toString();
        //            document.getElementById(name).style.display = "none";
        //        }

        //        //var min = parseInt(currentAction.dstFlowchart.split(';')[1]) + 1 - 4;
        //        //if (min < 1)
        //        //    min = 1;
        //        //for (var i = min; i <= parseInt(currentAction.dstFlowchart.split(';')[1]) + 1; i++) {
        //        //    var name = "lblActivity" + i.toString();
        //        //    document.getElementById(name).style.display = "";
        //        //}

        //        //

        //        $scope.chartViewModel.SwitchRoot(currentAction.dstFlowchart, false);
        //    }
        //} else if (currentAction.action == "flowchartdrillup") {
        //    if (undo) {
        //        $scope.SetRoot(currentAction.srcFlowchart, false, " down to ");
        //    } else {
        //        $scope.SetRoot(currentAction.dstFlowchart, false, " up to ");
        //    }
        //}
        else if (currentAction.action == "dragresize") {
            var finalW;
            var finalH;
            if (undo) {
                finalW = currentAction.sizeBefore.w;
                finalH = currentAction.sizeBefore.h;
            } else {
                finalW = currentAction.sizeAfter.w;
                finalH = currentAction.sizeAfter.h;
            }
            divDrop.style.width = finalW + 'px';
            divDrop.style.height = finalH + 'px';

            divSettings.style.display = "none";
            divAddIcon.style.display = "none";
            divAddDialog.style.display = "none";

            $("#divChart").width(finalW);
            $("#divFlowChartParent").width(finalW);
            $("#divFlowChartParentTitle").width(finalW);
            var svg = document.getElementsByTagName("svg");
            svg[0].setAttribute("width", finalW);
            svg[0].style.width = (finalW) + "px";
            CenterDrop();
            $("#divChart").height(finalH);
            svg[0].setAttribute("height", finalH);
            svg[0].style.height = (finalH) + "px";
        } else if (currentAction.action == "changeheight") {
            var h = 240;
            if (bigMode) {
                h = initialHeight;
            }
            var svg = document.getElementsByTagName("svg");
        } else if (currentAction.action == "changewidth") {
            var w = 310;
            if (bigMode) {
                w = initialWidth;
            }
            var svg = document.getElementsByTagName("svg");
            CenterDrop();
        } else if (currentAction.action == "deleteconnection") {
            if (undo) {
                try {
                    if ($scope.chartViewModel.GetNodeByGUID(currentAction.dstNodeGUID) == null || $scope.chartViewModel.GetNodeByGUID(currentAction.srcNodeGUID) == null) { //destination node of the connection is missing? so first create the node, then make the connection
                        var tmpAction = $scope.Undo("tmpmode");
                        if (tmpAction.action == "deletenode") {
                            var index = tmpAction.typeIndex;
                            var sw = $scope.chartViewModel.GetShapeWidthList(index - 1);
                            var sh = $scope.chartViewModel.GetShapeHeightList(index - 1);
                            $scope.chartViewModel.CreateNewNode($scope.chartViewModel.GetNumNodes(), tmpAction.x, tmpAction.y, tmpAction.parent, sw, sh, index, false, tmpAction.GUID);
                        }
                    }

                    if ($scope.chartViewModel.GetNodeByGUID(currentAction.dstNodeGUID) == null || $scope.chartViewModel.GetNodeByGUID(currentAction.srcNodeGUID) == null) { //destination node of the connection is missing? so first create the node, then make the connection
                        var tmpAction = $scope.Undo("tmpmode");
                        if (tmpAction.action == "deletenode") {
                            var index = tmpAction.typeIndex;
                            var sw = $scope.chartViewModel.GetShapeWidthList(parseInt(index) - 1);
                            var sh = $scope.chartViewModel.GetShapeHeightList(parseInt(index) - 1);
                            $scope.chartViewModel.CreateNewNode($scope.chartViewModel.GetNumNodes(), tmpAction.x, tmpAction.y, tmpAction.parent, sw, sh, index, false, tmpAction.GUID);
                        }
                    }

                    $scope.chartViewModel.GetNodeByGUID(currentAction.srcNodeGUID).data.numChild++;
                    $scope.chartViewModel.GetNodeByGUID(currentAction.dstNodeGUID).data.connected = true;
                    $scope.chartViewModel.GetNodeByGUID(currentAction.srcNodeGUID).data.connected = true;
                    $scope.chartViewModel.createNewConnection($scope.chartViewModel.GetNodeByGUID(currentAction.srcNodeGUID).outputConnectors[0],
                        $scope.chartViewModel.GetNodeByGUID(currentAction.dstNodeGUID).inputConnectors[$scope.chartViewModel.GetNodeByGUID(currentAction.dstNodeGUID).data.nextInputConnector], true,
                        parseInt(currentAction.ix), parseInt(currentAction.iy), parseInt(currentAction.o1x), parseInt(currentAction.o1y),
                        parseInt(currentAction.o2x), parseInt(currentAction.o2y), currentAction.onumber, "", "", currentAction.inputDir, currentAction.outputDir, $scope.chartViewModel);
                } catch (ex) {
                    //alert(ex.message + " , " + ex.number);
                }
            } else {
                try {
                    $scope.chartViewModel.GetNodeByGUID(currentAction.srcNodeGUID).data.numChild--;
                    if ($scope.chartViewModel.GetNodeByGUID(currentAction.srcNodeGUID).data.numChild == 0 && $scope.chartViewModel.GetNodeByGUID(currentAction.srcNodeGUID).data.activeConnectorInput == "")
                        $scope.chartViewModel.GetNodeByGUID(currentAction.srcNodeGUID).data.connected = false;
                    if ($scope.chartViewModel.GetNodeByGUID(currentAction.dstNodeGUID).numChild == 0)
                        $scope.chartViewModel.GetNodeByGUID(currentAction.dstNodeGUID).data.connected = false;
                    for (var i = 0; i < $scope.chartViewModel.connections.length; i++) {
                        var conn = $scope.chartViewModel.connections[i];
                        if (conn.data.source.GUID == currentAction.srcNodeGUID && conn.data.dest.GUID == currentAction.dstNodeGUID) {
                            $scope.chartViewModel.connections.splice(i, 1);
                        }
                    }
                } catch (ex) {
                    //alert(ex.message + " , " + ex.number);
                }
            }
        } else if (currentAction.action == "addconnection") {
            if (undo) {
                try {
                    $scope.chartViewModel.GetNodeByGUID(currentAction.srcNodeGUID).data.numChild--;

                    var indexToBeDeleted = "";
                    if (currentAction.inputDir == "up") {
                        indexToBeDeleted = "0";
                    }
                    else if (currentAction.inputDir == "right") {
                        indexToBeDeleted = "1";
                    }
                    else if (currentAction.inputDir == "down") {
                        indexToBeDeleted = "2";
                    }
                    else if (currentAction.inputDir == "left") {
                        indexToBeDeleted = "3";
                    }

                    $scope.chartViewModel.GetNodeByGUID(currentAction.dstNodeGUID).data.activeConnectorInput =
                        $scope.chartViewModel.GetNodeByGUID(currentAction.dstNodeGUID).data.activeConnectorInput.replace(indexToBeDeleted, "");
                    $scope.chartViewModel.GetNodeByGUID(currentAction.srcNodeGUID).data.activeConnector = "";

                    if ($scope.chartViewModel.GetNodeByGUID(currentAction.srcNodeGUID).data.numChild == 0 && $scope.chartViewModel.GetNodeByGUID(currentAction.srcNodeGUID).data.activeConnectorInput == "")
                        $scope.chartViewModel.GetNodeByGUID(currentAction.srcNodeGUID).data.connected = false;
                    if ($scope.chartViewModel.GetNodeByGUID(currentAction.dstNodeGUID).data.numChild == 0 &&
                        $scope.chartViewModel.GetNodeByGUID(currentAction.dstNodeGUID).data.activeConnectorInput == "")
                        $scope.chartViewModel.GetNodeByGUID(currentAction.dstNodeGUID).data.connected = false;

                    $scope.chartViewModel.GetNodeByGUID(currentAction.dstNodeGUID).data.connectorclicked = false;
                    $scope.chartViewModel.GetNodeByGUID(currentAction.srcNodeGUID).data.connectorclicked = false;


                    $scope.chartViewModel.GetNodeByGUID(currentAction.srcNodeGUID).data.outputConnectors[parseInt(currentAction.onumber) - 1].connected = false;
                    for (var i = 0; i < $scope.chartViewModel.connections.length; i++) {
                        var conn = $scope.chartViewModel.connections[i];
                        if (conn.data.source.GUID == currentAction.srcNodeGUID && conn.data.dest.GUID == currentAction.dstNodeGUID) {
                            $scope.chartViewModel.connections.splice(i, 1);
                        }
                    }
                } catch (ex) {
                    //alert(ex.message + " , " + ex.number);
                }
            } else //redo
            {
                try {
                    $scope.chartViewModel.GetNodeByGUID(currentAction.srcNodeGUID).data.numChild++;
                    $scope.chartViewModel.GetNodeByGUID(currentAction.dstNodeGUID).data.connected = true;
                    $scope.chartViewModel.GetNodeByGUID(currentAction.srcNodeGUID).data.connected = true;
                    $scope.chartViewModel.createNewConnection($scope.chartViewModel.GetNodeByGUID(currentAction.srcNodeGUID).outputConnectors[0],
                        $scope.chartViewModel.GetNodeByGUID(currentAction.dstNodeGUID).inputConnectors[$scope.chartViewModel.GetNodeByGUID(currentAction.dstNodeGUID).data.nextInputConnector], true,
                        parseInt(currentAction.ix), parseInt(currentAction.iy), parseInt(currentAction.o1x), parseInt(currentAction.o1y),
                        parseInt(currentAction.o2x), parseInt(currentAction.o2y), currentAction.onumber, currentAction.caption, "", currentAction.inputDir,
                        currentAction.outputDir, $scope.chartViewModel);
                } catch (ex) {
                    //alert(ex.message + " , " + ex.number);
                }
            }
        } else if (currentAction.action == "addnode") {
            if (undo) {
                var nodeGUID = currentAction.GUID;
                for (var i = 0; i < $scope.chartViewModel.nodes.length; i++) {
                    try {
                        if ($scope.chartViewModel.nodes[i].data.GUID == nodeGUID) {
                            $scope.chartViewModel.nodes[i].select();
                            $scope.chartViewModel.deleteSelected(false);
                        }
                    } catch (ex) {

                    }
                }
            } else if (!undo) {
                try {
                    var index = currentAction.typeIndex;

                    var sw = $scope.chartViewModel.GetShapeWidthList(index - 1);
                    var sh = $scope.chartViewModel.GetShapeHeightList(index - 1);
                    $scope.chartViewModel.CreateNewNode($scope.chartViewModel.GetNumNodes(), currentAction.x, currentAction.y, currentAction.parent, sw, sh, index, false, currentAction.GUID);
                } catch (ex) {
                    //alert(ex.message + "," + currentAction.action);
                }
            }
        } else if (currentAction.action == "nodemove") {
            for (var i = 0; i < currentAction.GUIDArray.length; i++) {
                $scope.chartViewModel.GetNodeByGUID(currentAction.GUIDArray[i]).data.x = parseInt(currentAction.xArray[i]);
                $scope.chartViewModel.GetNodeByGUID(currentAction.GUIDArray[i]).data.y = parseInt(currentAction.yArray[i]);
            }
            try {
                var finalW;
                var finalH;
                if (undo) {
                    finalW = currentAction.flwWidthBefore.replace("px", "");
                    finalH = currentAction.flwHeightBefore.replace("px", "");
                } else {
                    finalW = currentAction.flwWidthAfter.replace("px", "");
                    finalH = currentAction.flwHeightAfter.replace("px", "");
                }
                var svg = document.getElementsByTagName("svg");
                divDrop.style.width = (finalW) + 'px';
                $("#divChart").width(finalW);
                $("#divFlowChartParent").width(finalW);
                $("#divFlowChartParentTitle").width(finalW);
                svg[0].setAttribute("width", finalW);
                svg[0].style.width = (finalW) + "px";
                CenterDrop();

                divDrop.style.height = (finalH) + 'px';
                $("#divChart").height(finalH);
                svg[0].setAttribute("height", finalH);
                svg[0].style.height = (finalH) + "px";
            } catch (ex) {
                //alert(ex.message + "," + currentAction.action);
            }
        } else if (currentAction.action == "deletenode") {
            if (undo) {
                try {
                    var index = currentAction.typeIndex;
                    var sw = $scope.chartViewModel.GetShapeWidthList(index - 1);
                    var sh = $scope.chartViewModel.GetShapeHeightList(index - 1);
                    $scope.chartViewModel.CreateNewNode($scope.chartViewModel.GetNumNodes(), currentAction.x, currentAction.y, currentAction.parent, sw, sh, index, false, currentAction.GUID);
                } catch (ex) {
                    //alert(ex.message + " , " + ex.number);
                }
            } else if (!undo) { //redo
                //var nodeid = currentAction.id;
                var nodeGUID = currentAction.GUID;
                for (var i = 0; i < $scope.chartViewModel.nodes.length; i++) {
                    try {
                        if ($scope.chartViewModel.nodes[i].data.GUID.toString() == nodeGUID) {//nodeid
                            $scope.chartViewModel.nodes[i].select();
                            $scope.chartViewModel.deleteSelected(false);
                        }
                    } catch (ex) {

                    }
                }
            }
        }

        var realheight = parseInt(divDrop.style.height.replace("px", ""));
        var realwidth = parseInt(divDrop.style.width.replace("px", ""));
        setTimeout(function () {
            try
            {
                $scope.chartViewModel.FindMinWidthHeight();

                if (minHeight + 40 > parseInt(realheight)) {
                    divSettings.style.display = "none";
                    divAddIcon.style.display = "none";
                    divAddDialog.style.display = "none";

                    var h = (minHeight + 40);
                    if (h <= maxFlowchartHeight + initialHeight) {
                        divDrop.style.height = (h) + "px";
                        $("#divChart").height((h));
                        var svg = document.getElementsByTagName("svg");
                        svg[0].setAttribute("height", h);
                        svg[0].style.height = (h) + "px";
                    }
                }

                if (minWidth + 40 > realwidth) {
                    divSettings.style.display = "none";
                    divAddIcon.style.display = "none";
                    divAddDialog.style.display = "none";
                    var w = parseInt(minWidth) + 40;

                    if (w <= (maxFlowchartWidth + initialWidth)) {
                        divDrop.style.width = w + "px";
                        $("#divChart").width((w));
                        $("#divFlowChartParent").width((w));
                        $("#divFlowChartParentTitle").width((w));
                        var svg = document.getElementsByTagName("svg");
                        svg[0].setAttribute("width", w);
                        svg[0].style.width = (w) + "px";
                    }
                    CenterDrop();
                }
            }
            catch (ex) {
                //alert(ex.message + " , " + ex.number);
            }
        }, 100);
    }

    $scope.grid = true;
    $scope.squer = false;
    $scope.gridTitle = "Show Background With Grid";
    $scope.ToggleGrid = function() {
        if ($scope.grid) {
            $scope.grid = false;
            $scope.squer = true;
            $scope.gridTitle = "Remove Grid From Background";
        } else {
            $scope.grid = true;
            $scope.squer = false;
            $scope.gridTitle = "Show Background With Grid";
        }
        $scope.chartViewModel.ShowGrid = !$scope.chartViewModel.ShowGrid;
        if ($scope.chartViewModel.ShowGrid)
            $(".draggable-container").attr("class", "draggable-container container-designer-grid");
        else
            $(".draggable-container").attr("class", "draggable-container");
    };

    $scope.up = true;
    $scope.down = false;
    $scope.updownheaderTitle = "Hide Header";
    $scope.toggleMenus = function() {
        if ($scope.up) {
            $scope.up = false;
            $scope.down = true;
            $scope.updownheaderTitle = "Show Header";
            $(".mainHeader").css('display', 'none');
            $('.wfdHeader').css('top', 0);
            $('.leftCmp').css('top', 50);
            $('.mainNav').css('top', 50);
            $('.mainBody').css('padding-top', 0);
            $('#divFiller').css("display", "none");
            var currentdivContainerHeight = $('#divContainer').css("height").replace("px", "");
            $('#divContainer').css("height", (parseInt(currentdivContainerHeight) + 55) + "px");
        } else {
            $scope.up = true;
            $scope.down = false;
            $scope.updownheaderTitle = "Hide Header";
            $('#divFiller').css("display", "");
            $(".mainHeader").css('display', 'block');
            $('.leftCmp').css('top', 110);
            $('.mainNav').css('top', 110);
            $('.wfdHeader').css('top', 60);
            $('.mainBody').css('padding-top', "60px");
            var currentdivContainerHeight = $('#divContainer').css("height").replace("px", "");
            $('#divContainer').css("height", (parseInt(currentdivContainerHeight) - 55) + "px");
        }
    };

    $scope.hideTooltip = true;
    $scope.displayTooltip = false;
    $scope.tooltipTitle = "Hide Tooltip";
    $scope.ToggleInfo = function() {
        if ($scope.hideTooltip) {
            $scope.hideTooltip = false;
            $scope.displayTooltip = true;
            $scope.tooltipTitle = "Display Tooltip";
        } else {
            $scope.hideTooltip = true;
            $scope.displayTooltip = false;
            $scope.tooltipTitle = "Hide Tooltip";
        }
        $scope.chartViewModel.ShowInfo = !$scope.chartViewModel.ShowInfo;
    };
    $scope.bl = true;
    $scope.toggleTootlip = function() {
        if ($scope.bl && ($('.leftCmp').css('left') == "-320px")) {
            $('.mainNav').css('left', -50);
            $scope.bl = false;
        } else if (!$scope.bl) {
            $('.mainNav').css('left', 0);
            $scope.bl = true;
        }
    }
    $scope.closeToolbarContent = function(e) {
        $(".leftCmp").animate({ "left": "-320px" }, "fast");
        $(".content").animate({ "margin-left": 0 }, "fast");
        $rootScope.$broadcast('tooltipController:onCloseTooltip');
    }
    var xamlRootNodeID = 0;

    $scope.LoadXAMLChild = function($child, parentID, directFromTop, nt, falsenode) { //this function takes one XAML child as argument and extracts its data, then calls its child (if any)
        //find the first direct child of current $child, is it a FlowStep?
        var nodetype = "";
        var ifCondition = "";
        if (!directFromTop) {
            if ($($child).find(">")[0].tagName == "FlowStep") {
                $child = $($child).find(">")[0];
            } else {
                nodetype = "FlowDecision";
                try {
                    ifCondition = $($child).find(">").attr("Condition").trim().replace("[", "").replace("]", "");
                } catch (ex) {

                }
            }
        }
        //if it is not a flowstep, directly extract the node (FLOWDECISION)
        var locarray = $($child).find("av_Point");
        var loc = $(locarray[0]).text().split(',');
        var assignTo = "";
        var assignValue = "";
        var parent = parentID;
        var connected = false;
        var index;
        var $nextNode;
        if (nt != null) {
            if (nt != "") {
                nodetype = "FlowDecision";
                try {
                    ifCondition = $($child).attr("Condition").trim().replace("[", "").replace("]", "");
                } catch (ex) {

                }
            }
        }
        var DisplayName = "Flowchart";
        if (nodetype != "FlowDecision") {
            $nextNode = $($child).find(">")[1];
        }

        if (nodetype != "FlowDecision") {
            nodetype = $nextNode.tagName;
        }

        if (nodetype == "Flowchart") {
            try {
                DisplayName = $($nextNode).attr("DisplayName");
                if (DisplayName == undefined)
                    DisplayName = "Flowchart";
            } catch (ex) {
                //alert(ex.message + " , " + ex.number);
            }

            index = 7;
            currentActivityNumber++;

            var xamlWidth = initialWidth;
            var xamlHeight = initialHeight;

            try {
                var size = $($nextNode).attr("sap_VirtualizedContainerService.HintSize").split(',');
                xamlWidth = parseInt(size[0]);
                xamlHeight = parseInt(size[1]);

                if (xamlWidth < parseInt(initialWidth)) {
                    xamlWidth = initialWidth;
                }
                if (xamlHeight < parseInt(initialHeight)) {
                    xamlHeight = initialHeight;
                }

                FlowchartSizes += $scope.chartViewModel.GetNumNodes() + "=" + xamlWidth + ";" + xamlHeight + ",";
            } catch (ex) {

            }

            flowChartWidth[currentActivityNumber - 1] = xamlWidth;
            flowChartHeight[currentActivityNumber - 1] = xamlHeight;

        } else if (nodetype == "FlowDecision") {
            index = 1;
        } else if (nodetype == "Assign") {
            index = 2;
            //how many children has our ASSIGN node?
            var numChild = $($nextNode).find(">").length;
            var assignchildren = $($nextNode).find(">");
            if (numChild == 3) { //both TO and VALUE tags exist, so try to extract them
                assignTo = $(assignchildren[0]).text().trim().replace("[", "").replace("]", "");
                assignValue = $(assignchildren[1]).text().trim().replace("[", "").replace("]", "");
            } else if (numChild == 2) {
                if (assignchildren[0].tagName == "Assign_To") {
                    assignTo = $(assignchildren[0]).text().trim().replace("[", "").replace("]", "");
                } else if (assignchildren[0].tagName == "Assign_Value") {
                    assignValue = $(assignchildren[1]).text().trim().replace("[", "").replace("]", "");
                }
            }
        }
        var sw = $scope.chartViewModel.GetShapeWidthList(index - 1);
        var sh = $scope.chartViewModel.GetShapeHeightList(index - 1);
        var tmpid = $scope.chartViewModel.GetNumNodes();

        if (parent != "") {
            xamlRootNodeID = $scope.chartViewModel.GetNodeById(parent).data.rootNodeID;
        }
        $scope.chartViewModel.CreateNewNode(tmpid, parseInt(loc[0]), parseInt(loc[1]) + 30, parent, sw, sh, index, connected, nextGUID, xamlRootNodeID);
        nextGUID++;
        if (nodetype == "Assign") {
            setTimeout(function() { $("#txtAssignVariable" + tmpid).val(assignTo); }, 100);
            setTimeout(function() { $("#txtAssignVariable2-" + tmpid).val(assignValue); }, 100);
        } else if (nodetype == "FlowDecision") {
            setTimeout(function() { $("#txtIfCondition" + tmpid).val(ifCondition); }, 100);
        } else if (nodetype == "Flowchart") {
            setTimeout(function() { FlowchartTitles += tmpid + "=" + DisplayName + ","; }, 100);
        }

        if (parent != "") {
            var source = $scope.chartViewModel.GetNodeById(parent);
            var dest = $scope.chartViewModel.GetNodeById($scope.chartViewModel.GetNumNodes() - 1);
            source.data.connected = dest.data.connected = true;
            var o1 = $scope.OutputConnectorPosition1(source.data.typeIndex);
            var o2 = $scope.OutputConnectorPosition2(source.data.typeIndex);
            source.data.numChild++;
            var first_empty_child_index = source.data.numChild;
            if (source.data.typeIndex == 1) { //it is IF node? so find the FIRST empty output connector.
                if (!source.outputConnectors[0].data.connected) {
                    first_empty_child_index = 1;
                } else {
                    first_empty_child_index = 2;
                }
                if (falsenode != null) {
                    if (falsenode == "FALSENODE") {
                        first_empty_child_index = 2;
                    }
                }
            }

            var outputPoint;
            if (first_empty_child_index == 1) {
                outputPoint = { x: o1.x, y: o1.y, dir: o1.dir };
            } else {
                outputPoint = { x: o2.x, y: o2.y, dir: o2.dir };
            }
            var inputPoint = $scope.InputConnectorPosition($scope.chartViewModel.GetNode($scope.chartViewModel.GetNumNodes() - 1).data.typeIndex,
                { x: parseInt(source.data.x) + parseInt(outputPoint.x), y: parseInt(source.data.x) + parseInt(outputPoint.x) },
                $scope.chartViewModel.GetNode($scope.chartViewModel.GetNumNodes() - 1).data.id);
            $scope.chartViewModel.createNewConnection(source.outputConnectors[source.data.numChild - 1],
                dest.inputConnectors[0], false, inputPoint.x, inputPoint.y,
                o1.x, o1.y, o2.x, o2.y, first_empty_child_index, "", xamlRootNodeID, inputPoint.dir, outputPoint.dir, $scope.chartViewModel);
        }

        if (nodetype != "FlowDecision") {
            //create flowchart items (if any)
            if (nodetype == "Flowchart") {
                CorrectFlowChartListForVariables.push(DisplayName);
                CorrectFlowChartValuesForVariables.push(tmpid);
                if ($($nextNode).find(">").length >= 3) {
                    //now go to children (if any)
                    xamlRootNodeID = tmpid;
                    //create flowchart START node, then use it as ancestor of all other nodes in the newly created flowchart
                    var loc2 = $($nextNode).find("av_Point")[0].innerHTML.split(',');
                    var newFlowchartStartID = $scope.chartViewModel.GetNumNodes();

                    $scope.chartViewModel.CreateNewNode(newFlowchartStartID, parseInt(loc2[0]), parseInt(loc2[1]) + 30, "", sw, sh, 8, false, nextGUID, xamlRootNodeID);
                    nextGUID++;
                    var extra = 0;
                    //any variables?
                    var $var = $($nextNode).children("Flowchart_Variables");

                    if ($var.length > 0) {
                        //extra = 1;
                        var $varChild = $var.find(">");

                        setTimeout(function() {
                            for (var i = 0; i < $varChild.length; i++) {
                                var type = $($varChild[i]).attr("x_TypeArguments").replace("x_", "");
                                if ($($varChild[i]).attr("Default") != null)
                                    def = $($varChild[i]).attr("Default");
                                var def = "";
                                var varObject = {
                                    name: $($varChild[i]).attr("Name"),
                                    def: def.replace("[", "").replace("]", ""),
                                    type: type,
                                    guid: nextVarGUID,
                                    scope: DisplayName,
                                    scopeid: tmpid
                                };
                                AddVariable(varObject);
                                nextVarGUID++;
                            }
                        }, 100);
                    }
                    //end variables

                    flowChartIDStart += tmpid + ","; //we create START node manually, no need for it to be created via automatic switch process
                    if ($($nextNode).find("FlowStep").length > 0 && $($nextNode).find(">")[2 + extra].tagName != "FlowDecision") { //first child is not a FLOWDECISION
                        $scope.LoadXAMLChild($($nextNode).find("FlowStep")[0], newFlowchartStartID, true, "");
                    } else { //perhaps our first child is a FLOWDECISION
                        if ($($nextNode).find(">")[2 + extra].tagName == "FlowDecision") {
                            $scope.LoadXAMLChild($($nextNode).find(">")[2 + extra], newFlowchartStartID, true, "FlowDecision");
                        }
                    }
                }
            }
            //then return back to the main hierarchy and create remaining items in the current flowchart

            //more child? so drill down up to the last one...
            if ($($child).children("FlowStep_Next").length > 0) {
                $scope.LoadXAMLChild($($child).children("FlowStep_Next")[0], tmpid, false);
            }
        } else { //flowdecision (IF) has an slightly different hierarchy schema, in fact it can have up to two children (in contrast to other node types)
            if (!directFromTop) {
                $nextNode = $($child).find(">");
            } else {
                $nextNode = $child;
            }
            var numChild = $($nextNode).find(">").length;
            var assignchildren = $($nextNode).find(">");
            if (numChild == 3) //both TRUE & FALSE
            {
                //TRUE node
                $scope.LoadXAMLChild($($nextNode).find(">")[1], tmpid, false);

                //FALSE node
                $scope.LoadXAMLChild($($nextNode).find(">")[2], tmpid, false);
            } else if (numChild == 2) //TRUE or FALSE
            {
                var singleNodeType = ($($nextNode).find(">")[1].tagName);
                //which node? TRUE or FALSE?
                $scope.LoadXAMLChild($($nextNode).find(">")[1], tmpid, false, "", "FALSENODE");
            } else if (numChild == 1) { //neither TRUE nor FALSE

            }
        }
    };

    var xmlDoc;
    var $xml;

    $scope.ResetApplication = function () {
        try
        {
            if ($scope.variableBox || $scope.argumentBox || $scope.importBox) {
                var currentdivContainerHeight = $('#divContainer').css("height").replace("px", "");
                $('#divContainer').css("height", (parseInt(currentdivContainerHeight) + 300) + "px");
            }
            angular.element('.bottomAction').css('bottom', 10);
            $scope.variableBox = false;
            $scope.argumentBox = false;
            $scope.importBox = false;
            Arguments.length = 0;
            Variables.length = 0;
            xmlReference = 0;
            flowChartHasStart.length = 0;
            FlowChartTitleChangerVisible = false;
            FlowchartTitles = "";
            FlowchartSizes = "";
            flowChartHasStart.push(true);
            flowChartIDStart = "";
            lastFlowChartIndex = 1;

            nextGUID = 0;
            nextVarGUID = 0;
            nextArgGUID = 0;
            TempDeletedObjectList.length = 0;
            TempDeletedLineList.length = 0;
            currentRootNodeID = 0;
            currentActivityNumber = 0;
            nodeRootIDList.length = 0;

            $scope.chartViewModel.nodes.length = 0;
            $scope.chartViewModel.connections.length = 0;
            UndoList.length = 0;
            RedoList.length = 0;

            for (var i = 1; i < (numSupportedFlowcharts + 1) ; i++) {
                var name = "lblActivity" + i.toString();
                document.getElementById(name).style.display = "none";
            }
            $('#divHierarchy').css("display", "none");
        }
        catch (ex) {
            //alert(ex.message);
        }
    };

    $scope.LoadXAML = function() { //this function is the main entry point for XAML parsing/loading, the first node (flowchart) is parsed using LOADXAMLChild function, which in turn calls all childs
        $scope.ResetApplication();
        try {
            $("#divJSONLoader").css("display", "none");
            divXamlBack.style.display = 'none';
            if (txtLoadJSON.value == "")
                return;
            var text = txtLoadJSON.value.replace(/:/g, "_");
            text = text.replace(/sap_WorkflowViewStateService.ViewState/g, "sap_WorkflowViewStateService_ViewState");
            text = text.replace(/Assign.To>/g, "Assign_To>");
            text = text.replace(/Assign.Value>/g, "Assign_Value>");
            text = text.replace(/FlowStep.Next/g, "FlowStep_Next");
            text = text.replace(/Flowchart.Variables/g, "Flowchart_Variables");
            text = text.replace(/x:TypeArguments/g, "x_TypeArguments");
            text = text.replace(/x:Members/g, "x_Members");
            text = text.replace(/sap_VirtualizedContainerService.HintSize/g, "sap_VirtualizedContainerService.HintSize");
            xmlDoc = $.parseXML(text);
            $xml = $(xmlDoc).find("Flowchart");
            var extra = 0;
            var $arg = $(xmlDoc).find("x_Members");
            var $test = $(xmlDoc)[0];
            var activityName = "";
            var fullpath = $xml.attr("sad_XamlDebuggerXmlReader.FileName");
            var filename = fullpath.replace(/^.*[\\\/]/, '').replace(/.xaml/i, "");

            if ($arg.length > 0) { //is there any arguments?
                var $argChild = $arg.find(">");
                for (var i = 0; i < $argChild.length; i++) {
                    var def = "";
                    var name = $($argChild[i]).attr("Name");
                    //look into Activity attributes, and find possible default value of the current argument, there would be no default value if corresponding attribute is not found.
                    try {
                        def = $xml.parent().attr("this_" + filename + "." + name).replace("[", "").replace("]", "");
                    } catch (ex) {

                    }
                    var dir = "";
                    var x = $($argChild[i]).attr("Type").split('_');
                    if (x[0] == "InArgument(x")
                        dir = "In";
                    else if (x[0] == "OutArgument(x")
                        dir = "Out";
                    else if (x[0] == "InOutArgument(x")
                        dir = "In/Out";
                    var argObject = { name: name, def: def, type: x[1].replace(")", "").replace("Int32", "Number"), dir: dir, guid: nextArgGUID };
                    Arguments.push({ Name: name, Default: def, Type: x[1].replace(")", "").replace("Int32", "Number"), Direction: dir, Index: i });

                    $scope.GetArgRow(argObject);
                    totalargindex++;
                    nextArgGUID++;
                }
            }

            var DisplayName = "Flowchart";
            try {
                DisplayName = $xml.attr("DisplayName");
                if (DisplayName == undefined) {
                    DisplayName = "Flowchart";
                }
            } catch (ex) {
            }

            var index = 6;
            var sw = initialWidth;
            var sh = initialHeight;
            var size = $xml.attr("sap_VirtualizedContainerService.HintSize").split(',');
            var xamlWidth = parseInt(size[0]); //$($xml).find("x_Double[x_Key=Width]").text();
            var xamlHeight = parseInt(size[1]); //$($xml).find("x_Double[x_Key=Height]").text();
            if (xamlWidth != "") {
                sw = xamlWidth;
            }
            if (xamlHeight != "") {
                sh = xamlHeight;
            }
            var loc = $($xml).find("av_Point")[0].innerHTML.split(',');

            var childParent = "";
            var startHasChild = false;
            if ($($xml).find("av_PointCollection")[0].innerHTML != "") {
                startHasChild = true;
            }

            $scope.chartViewModel.CreateNewNode($scope.chartViewModel.GetNumNodes(), parseInt(loc[0]), parseInt(loc[1]) + 30, "", sw, sh, index, false, nextGUID);
            nextGUID++;
            currentActivityNumber++;
            //use default width/height if no width/height is set in XAML
            var w = initialWidth;
            var h = initialHeight;
            //width/height info found in XAML? so use them as our flowchart width/height
            if (xamlWidth != "") {
                w = xamlWidth;
            }
            if (xamlHeight != "") {
                h = xamlHeight;
            }

            bigMode = true;
            imgResize.style.display = "";
            divTools.style.display = "";
            //btnToXAML.style.display = "";
            btnToCSharp.style.display = "";
            //btnFromCSharp.style.display = "";
            btnSave.style.display = "";
            btnVariables.style.display = btnArguments.style.display = "";
            btnImports.style.display = "";

            divDrop.style.width = w + "px";
            divDrop.style.height = h + "px";
            divDrop.style.backColor = "white";
            document.getElementsByClassName("ActivityItem")[0].style.backgroundColor = "white";
            divChart.style.width = w + "px";
            divChart.style.height = h + "px";
            var svg = document.getElementsByTagName("svg");
            svg[0].setAttribute("height", h);
            svg[0].style.height = h + "px";
            svg[0].setAttribute("width", w);
            svg[0].style.width = w + "px";
            var newnode = $scope.chartViewModel.GetNodeById(0);

            newnode.data.x = 0;
            newnode.data.y = 0;
            CenterDrop();

            //divDrop.style.top = "50px";

            flowChartWidth[0] = w;
            flowChartHeight[0] = h;

            //add (initial) start node
            sw = $scope.chartViewModel.GetShapeWidthList(index - 1);
            sh = $scope.chartViewModel.GetShapeHeightList(index - 1);
            $scope.chartViewModel.CreateNewNode($scope.chartViewModel.GetNumNodes(), parseInt(loc[0]), parseInt(loc[1]) + 30, "", sw, sh, 8, false, nextGUID);
            nextGUID++;

            try {
                setTimeout(function() {
                    document.getElementById('divFlowChartParentTitle').style.width = (w + "px");
                    document.getElementById('divFlowChartParent').style.width = (w + "px");
                    lblFakeTitle.value = DisplayName;
                    lblActivity1.innerHTML = DisplayName;
                    FlowchartTitles += currentRootNodeID + "=" + DisplayName + ",";
                }, 50);
            } catch (ex) {
                //alert(ex.message + " , " + ex.number);
            }
            //end add (initial) start node

            if ($($xml[0]).children("Flowchart_Variables").length > 0) {
                extra = 1;
            }

            //any variables?
            setTimeout(function() {
                var $var = $($xml[0]).children("Flowchart_Variables");
                if ($var.length > 0) {
                    var $varChild = $var.find(">");
                    for (var i = 0; i < $varChild.length; i++) {
                        var type = $($varChild[i]).attr("x_TypeArguments").replace("x_", "");
                        type = type.replace("Int32", "Number");
                        var def = "";
                        if ($($varChild[i]).attr("Default") != null)
                            def = $($varChild[i]).attr("Default").replace("[", "").replace("]", "");
                        var varObject = {
                            name: $($varChild[i]).attr("Name"),
                            def: def,
                            type: type,
                            guid: nextVarGUID,
                            scope: DisplayName,
                            scopeid: "0"
                        };
                        AddVariable(varObject);
                        nextVarGUID++;
                    }
                }
            }, 100);
            //end variables

            //now go to children (if any)
            if (startHasChild) {
                childParent = $scope.chartViewModel.GetNumNodes() - 1;
            }
            if ($($xml).find("FlowStep").length > 0 && $($xml).find(">")[2 + extra].tagName != "FlowDecision") { //first child is not a FLOWDECISION
                $scope.LoadXAMLChild($($xml).find("FlowStep")[0], childParent, true);
            } else { //perhaps our first child is a FLOWDECISION
                if ($($xml).find(">")[2 + extra] != null) {
                    if ($($xml).find(">")[2 + extra].tagName == "FlowDecision") {
                        $scope.LoadXAMLChild($($xml).find(">")[2 + extra], childParent, true, "FlowDecision");
                    }
                }
            }
        } catch (ex) {
            //alert(ex.message + " , " + ex.number);
        }
        setTimeout(function() {
            $scope.chartViewModel.FilterVariables(0);
            $scope.chartViewModel.FindMinWidthHeight();
        }, 120);
    };

    $scope.ClearSelected = function () {
        $scope.chartViewModel.deleteSelected();
    };

    $scope.ClearAll = function() {
        try {
            $scope.chartViewModel.ClearAll();
            $('#imgEraser').css("display", "none");
            $scope.chartViewModel.FindMinWidthHeight();
        } catch (ex) {

        }
    };

    $scope.Test = function() {
    }

    VirtualSetRoot = function (index) {
        $scope.SetRoot(index);
    }

    $scope.SetRoot = function (index, norecord, drilltext) {
        $scope.chartViewModel.Lock = false;
        try
        {
            if (ForeachActive) {
                $scope.chartViewModel.GetNodeById(currentRootNodeID).data.ForeachItemName = document.getElementById('txtForeachItemName').value;
                $scope.chartViewModel.GetNodeById(currentRootNodeID).data.ForeachListName = document.getElementById('txtForeachListName').value;
            }
        }
        catch (ex) {
            console.log(ex.message);
        }
        divForeachBar.style.display = "none";
        imgResize.style.display = "";
        try {
            UndoList.length = 0;
            RedoList.length = 0;

            FlowchartSizes += currentRootNodeID + "=" + divDrop.style.width.replace("px", "") + ";" + divDrop.style.height.replace("px", "") + ",";
            $('#imgEraser').css("display", "none");
            if (drilltext == null)
                drilltext = " up to ";
            if (norecord == null) {
                var srcFlowchart;
                for (var i = 1; i < (numSupportedFlowcharts + 1); i++) {
                    var name = "lblActivity" + i.toString();
                    if (document.getElementById(name).style.display == "") {
                        srcFlowchart = i - 1;
                    }
                }

                var UndoObject = { action: "flowchartdrillup", dstFlowchart: index.split(';')[0] + ";" + srcFlowchart, srcFlowchart: currentRootNodeID + ";" + srcFlowchart };
                //RedoList.length = 0;
                //UndoList.push(UndoObject);
            }

            var current_scene = 0;
            for (var i = 1; i < (numSupportedFlowcharts + 1); i++) {
                var name = "lblActivity" + i.toString();
                if (document.getElementById(name).style.display != "none") {
                    current_scene = i - 1;
                }
            }

            flowChartWidth[current_scene] = divDrop.style.width.replace("px", "");
            flowChartHeight[current_scene] = divDrop.style.height.replace("px", "");
            var sizes = $scope.chartViewModel.FindFlowchartSize(index.split(';')[0]).split(';');
            var w = sizes[0];
            var h = sizes[1];
            flowChartTitle[current_scene] = lblFakeTitle.value;
            if (!ForeachActive)
                lblFakeTitle.value = $scope.chartViewModel.FindFlowchartTitle(index.split(';')[0]);
            ForeachActive = false;

            divDrop.style.height = h + "px";
            $("#divChart").height(h);
            var svg = document.getElementsByTagName("svg");
            svg[0].setAttribute("height", h);
            svg[0].style.height = (h) + "px";

            divDrop.style.width = w + "px";

            $("#divChart").width(w);
            svg[0].setAttribute("width", w);
            svg[0].style.width = (w) + "px";

            try {
                document.getElementById('divFlowChartParentTitle').style.width = (w + "px");
                document.getElementById('divFlowChartParent').style.width = (w + "px");
            } catch (ex) {

            }

            CenterDrop();

            var foreachret =  $scope.chartViewModel.ChangeRoot(index.split(';')[0]);
            $scope.chartViewModel.FilterVariables(index.split(';')[0]);
            for (var i = 1; i < (numSupportedFlowcharts + 1) ; i++) {
                var name = "lblActivity" + i.toString();
                document.getElementById(name).style.display = "none";
            }

            var min = parseInt(index.split(';')[1]) + 1 - 4;
            if (min < 1)
                min = 1;
            for (var i = min; i <= parseInt(index.split(';')[1]) + 1; i++) {
                var name = "lblActivity" + i.toString();
                document.getElementById(name).style.display = "";
            }
            $scope.chartViewModel.FindMinWidthHeight();
            txtTitle.style.display = 'none';
            lblFakeTitle.style.display = '';
            $scope.chartViewModel.deselectAll();
            $("#lblSwitchAlert").html("Drilling" + drilltext + lblFakeTitle.value);
            $("#divSwitchAlert").fadeIn(1500, function() {
                $("#divSwitchAlert").fadeOut(1500);
            });
            $('#divHierarchy').css("display", "none");
            $scope.nestedFlowchart = true;

            if (index.split(';')[2] == "foreach") {
                $scope.chartViewModel.Lock = true;
                ForeachActive = true;
                divForeachBar.style.display = "";
                imgResize.style.display = "none";

                if (foreachret) {
                    lblForEachLabel.style.display = "none";
                }
                else {
                    lblForEachLabel.style.display = "";
                }

            }
        }
        catch (ex) {
            //alert(ex.message + " , " + ex.number);
        }
    };

    $scope.SwitchToForEach = function () {
        $scope.chartViewModel.SwitchToForEach();
    };

    $scope.SwitchRoot = function () {
        $scope.chartViewModel.SwitchRoot();
    };

    $scope.SetDragCovered = function() {
        try {
            for (var i = 0; i < $scope.chartViewModel.GetNumNodes(); i++) {

                if ($scope.chartViewModel.GetNode(i).data.numChild == $scope.chartViewModel.GetNode(i).data.outputConnectors.length && $scope.chartViewModel.GetNode(i).data.typeIndex != 6) {
                    $scope.chartViewModel.GetNode(i).data.covered = "nodrop";
                }
            }
        } catch (ex) {
            //alert(ex.message + " , " + ex.number);
        }
    };

    $scope.PrepareXMLNode = function(n, dom, text) {
        n = dom.createElement('x:String');
        n.innerHTML = text;
        return n;
    };

    $scope.PrepareXMLNode2 = function(n, dom, text) {
        n = dom.createElement('AssemblyReference');
        n.innerHTML = text;
        return n;
    };

    $scope.LoadFromJSON = function(json) {
        txtJSON.value = txtJSONCode.value;
        $("#divJONSDialog").css("display", "none");
        divXamlBack.style.display = 'none';
    };

    var jsonCounter = 0;

    $scope.LoadJSON = function () {
        $scope.ResetApplication();
        $("#divJONSDialog").css("display", "none");
        divXamlBack.style.display = 'none';
        var maxGUID = -1;
        var txt = txtJSONCode.value;
        try {
            var json = JSON.parse(txt);
            var nodeinfo = json.nodeinfo;
            var settings = json.settings;

            Variables = json.Variables;
            Arguments = json.Arguments;
            References = json.References;
            $scope.grid = settings.grid;
            flowChartIDStart = settings.flowChartIDStart;
            if ($scope.grid)
                $scope.ToggleGrid();
            $scope.grid = true;
            lblWorkflowName.value = settings.FlowchartName;
            FlowchartSizes = settings.FlowchartSizes;
            FlowchartTitles = settings.FlowchartTitles;
            var connectioninfo = json.connectioninfo;
            //loop through nodes
            for (var i = 0 ; i < nodeinfo.length; i++) {
                $scope.chartViewModel.CreateNewNode(nodeinfo[i].guid, nodeinfo[i].x, nodeinfo[i].y, nodeinfo[i].parent, nodeinfo[i].w,
                    nodeinfo[i].h, nodeinfo[i].typeindex, nodeinfo[i].connected, nodeinfo[i].guid);
                if (parseInt(nodeinfo[i].guid) > maxGUID) {
                    maxGUID = nodeinfo[i].guid;
                }
                $scope.chartViewModel.nodes[$scope.chartViewModel.nodes.length - 1].data.rootNodeID = nodeinfo[i].parentFlowchart;
                $scope.chartViewModel.nodes[$scope.chartViewModel.nodes.length - 1].data.ForeachItemName = nodeinfo[i].foreachItemName;
                $scope.chartViewModel.nodes[$scope.chartViewModel.nodes.length - 1].data.ForeachListName = nodeinfo[i].foreachListName;
                if (nodeinfo[i].typeindex == 6) {
                    divDrop.style.backgroundColor = "white";
                    commonActivityService.ChangeFlowchartIndex();
                    document.getElementsByClassName("ActivityItem")[0].style.backgroundColor = "white";
                    currentActivityNumber++;
                    program_init = true;
                    bigMode = true;
                    imgResize.style.display = "";
                    divTools.style.display = "";
                    //btnToXAML.style.display = "";
                    btnToCSharp.style.display = "";
                    //btnFromCSharp.style.display = "";
                    btnSave.style.display = "";
                    btnVariables.style.display = btnArguments.style.display = "";
                    btnImports.style.display = "";

                    var w = initialWidth;
                    var h = initialHeight;
                    var wh = nodeinfo[i].flowchartSize.split(';');
                    w = wh[0];
                    h = wh[1];
                    divDrop.style.width = w + "px";
                    divDrop.style.height = h + "px";
                    divChart.style.width = w + "px";
                    divChart.style.height = h + "px";
                    var svg = document.getElementsByTagName("svg");
                    svg[0].setAttribute("height", h);
                    svg[0].style.height = h + "px";
                    svg[0].setAttribute("width", w);
                    svg[0].style.width = w + "px";

                    CenterDrop();
                    setTimeout(function () {
                        $('#divFlowChartParent').width(w);
                        $('#divFlowChartParentTitle').width(w);
                        lblFakeTitle.value = $scope.chartViewModel.FindFlowchartTitle(0);
                        document.getElementById('lblActivity1').childNodes[0].childNodes[0].innerHTML = lblFakeTitle.value;
                        divDrop.style.top = "0px";
                    }, 100);
                }

                //only main flowchart (root node) items should be visible initially
                if (nodeinfo[i].parentFlowchart == 0)
                    $scope.chartViewModel.nodes[$scope.chartViewModel.nodes.length - 1].data.visible = true;
                else//other items should be invisible
                    $scope.chartViewModel.nodes[$scope.chartViewModel.nodes.length - 1].data.visible = false;
                //set foreach parent of the newly created node
                $scope.chartViewModel.nodes[$scope.chartViewModel.nodes.length - 1].data.rootNodeIDForeach = nodeinfo[i].foreachParent;

            }//end loop through nodes
            nextGUID = parseInt(maxGUID) + 1;//new nextGUID value equals to the maximum GUID value plus one
            //make another loop through nodes, and set internal properties of items
            //set internal data of the items
            setTimeout(function () {
                for (var i = 0 ; i < nodeinfo.length; i++) {
                    if (nodeinfo[i].typeindex == 1) {//it is a flow decision item
                        if (nodeinfo[i].IfCondition != "") {
                            try {
                                var xID = "txtIfCondition" + nodeinfo[i].guid;
                                document.getElementById(xID).value = nodeinfo[i].IfCondition;
                            }
                            catch (ex) {

                            }
                        }
                    }
                    else if (nodeinfo[i].typeindex == 2)//it is an assign item
                    {
                        try {
                            document.getElementById('txtAssignVariable' + nodeinfo[i].guid).value = nodeinfo[i].AssignOperator1;
                            document.getElementById('txtAssignVariable2-' + nodeinfo[i].guid).value = nodeinfo[i].AssignOperator2;
                        }
                        catch (ex) {

                        }
                    }
                }
            }, 100);

            for (var i = 0 ; i < connectioninfo.length; i++) {
                $scope.chartViewModel.GetNodeByGUID(connectioninfo[i].srcNodeID).data.numChild++;
                $scope.chartViewModel.createNewConnection($scope.chartViewModel.GetNodeByGUID(connectioninfo[i].srcNodeID).outputConnectors[0],
                    $scope.chartViewModel.GetNodeByGUID(connectioninfo[i].destNodeID).inputConnectors[$scope.chartViewModel.GetNodeById(connectioninfo[i].destNodeID).data.nextInputConnector], true,
                    parseInt(connectioninfo[i].ix), parseInt(connectioninfo[i].iy), parseInt(connectioninfo[i].o1x), parseInt(connectioninfo[i].o1y),
                    parseInt(connectioninfo[i].o2x), parseInt(connectioninfo[i].o2y), connectioninfo[i].onumber, "", "", connectioninfo[i].inputDir,
                    connectioninfo[i].outputDir, $scope.chartViewModel);

                $scope.chartViewModel.connections[$scope.chartViewModel.connections.length - 1]._rootNodeID = connectioninfo[i].parentFlowchart;
                $scope.chartViewModel.connections[$scope.chartViewModel.connections.length - 1].rootNodeIDForeach = connectioninfo[i].foreachParent;
            }

            for (var i = 0 ; i < connectioninfo.length; i++) {
                if (connectioninfo[i].parentFlowchart == 0)
                    $scope.chartViewModel.connections[i]._visible = true;
                else
                    $scope.chartViewModel.connections[i]._visible = false;
            }

            var table = document.getElementById('tblArg');//initially clear table
            for (var i = table.getElementsByTagName("tr").length - 1 ; i >= 0; i--) {
                table.deleteRow(i);
            }

            //load arguments into the project
            for (var i = 0 ; i < Arguments.length; i++) {
                var tr = $compile($scope.GetArgRow(Arguments[i]))($scope);
                angular.element('#tblArg').append(tr);
            }

            var tr = $compile($scope.GetArgRow())($scope);
            angular.element('#tblArg').append(tr);

            table = document.getElementById('tbl');//initially clear table
            for (var i = table.getElementsByTagName("tr").length - 1 ; i >= 0; i--) {
                table.deleteRow(i);
            }

            //load variables into the project
            for (var i = 0 ; i < Variables.length; i++) {
                if (Variables[i].ScopeID == 0) {
                    var tr = $compile($scope.GetVarRow(Variables[i]))($scope);
                    angular.element('#tbl').append(tr);
                }
            }

            tr = $compile($scope.GetVarRow())($scope);
            angular.element('#tbl').append(tr);

            //load project references
            for (var i = 0 ; i < References.length; i++) {
                SelectReference(References[i]);
            }
        }
        catch (ex) {
            //alert(ex.message + " , " + ex.number);
        }

        var realheight = parseInt(divDrop.style.height.replace("px", ""));
        var realwidth = parseInt(divDrop.style.width.replace("px", ""));
        $scope.chartViewModel.FindMinWidthHeight();
        if (minHeight + 40 > parseInt(realheight)) {
            divSettings.style.display = "none";
            divAddIcon.style.display = "none";
            divAddDialog.style.display = "none";

            var h = (minHeight + 40);
            if (h <= maxFlowchartHeight + initialHeight) {
                divDrop.style.height = (h) + "px";
                $("#divChart").height((h));
                var svg = document.getElementsByTagName("svg");
                svg[0].setAttribute("height", h);
                svg[0].style.height = (h) + "px";
            }
        }

        if (minWidth > realwidth) {
            divSettings.style.display = "none";
            divAddIcon.style.display = "none";
            divAddDialog.style.display = "none";
            divDrop.style.width = (minWidth) + "px";
            var w = minWidth;
            $("#divChart").width((w));
            $("#divFlowChartParent").width((w));
            $("#divFlowChartParentTitle").width((w));
            var svg = document.getElementsByTagName("svg");
            svg[0].setAttribute("width", w);
            svg[0].style.width = (w) + "px";
            CenterDrop();
        }
    };

    $scope.SaveJSON = function () {
        try
        {
            var jsonobject;
            var nodearry = [];
            var connectionarray = [];
            var grid = !$scope.grid;
            FlowchartSizes += currentRootNodeID + "=" + divDrop.style.width.replace("px", "") + ";" + divDrop.style.height.replace("px", "") + ",";
            var settings = { FlowchartTitles: FlowchartTitles, FlowchartSizes: FlowchartSizes, FlowchartName: lblWorkflowName.value, grid: grid, flowChartIDStart: flowChartIDStart };
            console.clear();

            for (var i = 0 ; i < $scope.chartViewModel.nodes.length; i++) {
                var myNode = $scope.chartViewModel.nodes[i];
                var ifCondition = "";
                var AssignOperator1 = "";
                var AssignOperator2 = "";
                if (myNode.data.typeIndex == 1)//this node is a flow decision
                {
                    try {
                        ifCondition = document.getElementById('txtIfCondition' + myNode.data.id).value;
                    }
                    catch (ex) {

                    }
                }
                else if (myNode.data.typeIndex == 2)//this node is an assign item
                {
                    try {
                        AssignOperator1 = document.getElementById('txtAssignVariable' + myNode.data.id).value;
                        AssignOperator2 = document.getElementById('txtAssignVariable2-' + myNode.data.id).value;
                    }
                    catch (ex) {

                    }
                }

                if (ForeachActive) {
                    $scope.chartViewModel.GetNodeById(currentRootNodeID).data.ForeachItemName = document.getElementById('txtForeachItemName').value;
                    $scope.chartViewModel.GetNodeById(currentRootNodeID).data.ForeachListName = document.getElementById('txtForeachListName').value;
                }

                var tmpNode = {
                    typeindex: myNode.data.typeIndex, x: myNode.data.x, y: myNode.data.y, parentFlowchart: myNode.data.rootNodeID, parent: myNode.data.parent,
                    h: myNode.data.shapeHeight, w: myNode.data.shapeWidth, foreachParent: myNode.data.rootNodeIDForeach, foreachItemName: myNode.data.ForeachItemName,
                    connected: myNode.data.connected, z: myNode.data.z, AssignOperator1: AssignOperator1, AssignOperator2: AssignOperator2, foreachListName: myNode.data.ForeachListName,
                    IfCondition: ifCondition, guid: myNode.data.GUID, shapeWidth: myNode.data.shapeWidth, shapeHeight: myNode.data.shapeHeight,
                    flowchartSize: $scope.chartViewModel.FindFlowchartSize(myNode.data.id, true)
                };
                nodearry.push(tmpNode);
            }

            for (var i = 0 ; i < $scope.chartViewModel.connections.length; i++) {
                var myConnection = $scope.chartViewModel.connections[i];
                var tmpConnection = {
                    srcNodeID: myConnection.source.parentNode().data.GUID, destNodeID: myConnection.dest.parentNode().data.GUID, pointList: myConnection.GetPathPoint(),
                    caption: myConnection._caption, parentFlowchart: myConnection.rootNodeID(), inputDir: myConnection.inputDir, outputDir: myConnection.outputDir,
                    ix: myConnection.inputx, iy: myConnection.inputy, o1x: myConnection.ox1, o1y: myConnection.oy1, o2x: myConnection.ox2, o2y: myConnection.oy2,
                    onumber: myConnection.sourceOutputID, foreachParent: myConnection.rootNodeIDForeach
                };
                connectionarray.push(tmpConnection);
            }
            jsonobject = { nodeinfo: nodearry, connectioninfo: connectionarray, settings: settings, Variables: Variables, Arguments: Arguments, References: References };

            $('.divXamlBack').css('display', 'block');
            divCSharp.style.display = "";
            btnEvaluateCode.style.display = "none";
            txtCSharp.value = JSON.stringify(jsonobject);
        }
        catch (ex) {
            //alert(ex.message + " , " + ex.number);
        }
    };

    var CSharp_Code = "";
    var FunctionCodes = "";
    var IfIndex = 1;
    var FinalFunctionIDs = [];
    var DicIndex = 1;
    var IndentCount = 1;

    $scope.IndentCode = function (count) {
        var indent = "";
        for (var i = 0 ; i < count * 4; i++) {
            indent += " ";
        }
        return indent;
    };

    $scope.ShowCSharpLoad = function () {
        $('.divXamlBack').css('display', 'block');
        divLoadCSharp.style.display = "";
    };

    $scope.EvaluateCSharp = function () {
        try {
            var src = txtCSharp.value;//.replace(RegExp("<br>", "g"), '\r\n');
            alert(src);
            var settings = {
                "async": true,
                "crossDomain": true,
                "url": "http://13.69.148.176:8098/api/actions",
                "method": "POST",
                "headers": {
                    "content-type": "application/json",
                    "data-type": "json",
                    "cache-control": "no-cache",
                    "postman-token": "0b0c8cca-3383-0378-d3f5-5f3d385cc367"
                },
                //"data": "{\r\n    \"token\":\"SRzO5+KboLWwY6raPKKk6xW66lkfASQbYKK7IFMnrLZ7WduYPljDIo0T0hlhACNzrgkgig2zObmnHHaN0wqdl3XX8lJb0tvACCOsQyRL36emiOtm+scIMRUK9id3tASrR70bIO8NZWpI11t9scu3198VyAJQAxKrSsirEwL9pDGJP7D/h1WqsA==\",\r\n    \"wfid\":\"826b7111-58cb-4dd4-8714-cbb23dd62148\", \r\n    \"args\":\"{'source':'using System;\r\nusing System.Collections.Generic;\r\nusing System.Linq;\r\nusing System.Text;\r\nusing System.Activities;\r\n\r\npublic sealed class EvaluateSourceCodeActivity : CodeActivity\r\n    {\r\n        /// <summary>\r\n        /// Source code to evaluate\r\n        /// </summary>\r\n        public InArgument<string> SoruceCode { get; set; }\r\n\r\n        /// <summary>\r\n        /// Current projectID\r\n        /// </summary>\r\n        public InArgument<string> ProjectID { get; set; }\r\n\r\n        /// <summary>\r\n        /// array of compiled code warnings\r\n        /// </summary>\r\n        public OutArgument<string[]> Warnings { get; set; }\r\n\r\n        /// <summary>\r\n        /// Array of compiled code errors\r\n        /// </summary>\r\n        public OutArgument<string[]> Errors { get; set; }\r\n        /// <summary>\r\n        /// Activity execute method\r\n        /// </summary>\r\n        /// <param name=\\\"context\\\"></param>\r\n        protected override void Execute(CodeActivityContext context)\r\n        {\r\n            string source = context.GetValue(this.SoruceCode);\r\n            string projectID = context.GetValue(ProjectID);\r\n            string[] wars = null, errs = null;\r\n            System.IO.File.Delete(\\\"c:\\\\test2.txt\\\");\r\n            Warnings.Set(context, wars);\r\n            Errors.Set(context, errs);\r\n        }\r\n    }\r\n'}\"\r\n    \r\n}"
                "data": "{\r\n    \"token\":\"SRzO5+KboLWwY6raPKKk6xW66lkfASQbYKK7IFMnrLZ7WduYPljDIo0T0hlhACNzrgkgig2zObmnHHaN0wqdl3XX8lJb0tvACCOsQyRL36emiOtm+scIMRUK9id3tASrR70bIO8NZWpI11t9scu3198VyAJQAxKrSsirEwL9pDGJP7D/h1WqsA==\",\r\n    \"wfid\":\"826b7111-58cb-4dd4-8714-cbb23dd62148\", \r\n    \"args\":\"{'source':'" + src + "'}\"\r\n    \r\n}"
            }

            $.ajax(settings).done(function (response) {
                alert(response.ErrorsArray.length + " error(s) and " + response.WarningsArray.length + " warning(s) were found");
            }).fail(function (jqXHR, textStatus) {
                alert("Request failed: " + jqXHR.status);
            });
        }
        catch (ex) {
            //alert(ex.message);
        }
    };

    SelectReference = function (ref) {
        var combo = document.getElementById('cmbImport');
        var reference = combo.options[combo.selectedIndex].text;
        if (ref != null) {
            reference = ref;
        }
        if ((combo.selectedIndex != 0 && References.indexOf(reference) == -1) || ref != null) {
            var tableRef = document.getElementById('tblImport').getElementsByTagName('tbody')[0];
            // Insert a row in the table at the last row
            var newRow = tableRef.insertRow(tableRef.rows.length);
            // Insert a cell in the row at index 0
            var newCell = newRow.insertCell(0);
            // Append a text node to the cell
            var newText = document.createTextNode(reference);
            newCell.appendChild(newText);
            if (ref == null)
                References.push(reference);
            $("#cmbImport option[value='" + reference + "']").remove();//remove the selected reference from combo box after it has been successfully added to the project
        }
    }

    $scope.LoadCSharp = function () {
        Variables.length = 0;
        var code = txtCSharpCode.value;
        var lines = code.split('\n');
        for (var i = lines.length - 1; i >= 0; i--) {
            lines[i] = lines[i].trim();
            if (lines[i].trim() == "") {
                lines[i].remove();
            }
        }
        var typelist = [];
        typelist.push({ csharp: "string", j: "String" });
        typelist.push({ csharp: "bool", j: "Boolean" });
        typelist.push({ csharp: "int", j: "Number" });
        typelist.push({ csharp: "float", j: "Number" });
        typelist.push({ csharp: "long", j: "Number" });
        typelist.push({ csharp: "decimal", j: "Number" });
        typelist.push({ csharp: "datetime", j: "DateTime" });
        for (var i = 0 ; i < lines.length; i++) {
            if (lines[i].indexOf(" class ") != -1) {
                //lblWorkflowName = "";
            }
            else if (lines[i].substring(0, 3) == "if ") {

            }
            for (var k = 0 ; k < typelist.length; k++) {
                if (lines[i].substring(0, typelist[k]) == typelist[k].csharp) {
                    var varname = "";
                    var vardef = "";
                    if (typelist[k].csharp != "string") {
                        var parts = lines[i].replace(";", "").split('=');
                        vardef = parts[1].replace(/ /g, '');
                        varname = parts[0].replace(/^(www\.)/,"");
                    }
                    Variables.push({ Name: "", Default: vardef, Type: typelist[k].j, ScopeID: 0 });
                }
            }
        }

        for (var i = 0 ; i < Variables.length; i++) {
            if (Variables[i].ScopeID == 0) {
                var tr = $compile($scope.GetVarRow(Variables[i]))($scope);
                angular.element('#tbl').append(tr);
            }
        }

        divLoadCSharp.style.display = 'none';
        divXamlBack.style.display = 'none';
    };

    $scope.InsertVariablesIntoCSharpCode = function (rootNodeID) {//this function finds variables related to the flowchart specified by parameter
        var result = "";
        for (var i = 0 ; i < Variables.length; i++) {//loop through all variables defined by user and find variables belonging to the current flowchart
            if (Variables[i].ScopeID == rootNodeID) {
                var realDefault = Variables[i].Default;
                var def = '';
                if (Variables[i].Type.toLowerCase() == "string") {//insert the default value between " & " when variable type is STRING
                    def += '"';
                    def += Variables[i].Default;
                    def += '"';
                }
                else if (Variables[i].Type.toLowerCase() == "number") {
                    def = Variables[i].Default;
                }
                else if (Variables[i].Type.toLowerCase() == "boolean") {
                    if (Variables[i].Default == "") {
                        def = "true";
                    }
                    else {
                        def = Variables[i].Default.toLowerCase();
                    }
                }
                var publicString = "public ";
                var variableIndent = 1;
                if (rootNodeID != 0) {//these variables are defined in a function (and not in the root class?) so their indentation should be more than root variables, also they shouldn't be defined public
                    publicString = "";
                    variableIndent = 2;
                }

                if (realDefault != '')//our variable has a default value, so it should be considered while defining the CSharp variable
                    result += $scope.IndentCode(variableIndent) + publicString + Variables[i].Type.toLowerCase().replace("number", "float").replace("boolean", "bool") + " " + Variables[i].Name + " = " + def + ";\n";
                else//our variable has no default value, so there is no need to define a default value in the C# generated code
                    result += $scope.IndentCode(variableIndent) + publicString + Variables[i].Type.toLowerCase().replace("number", "float").replace("boolean", "bool") + " " + Variables[i].Name + ";\n";
            }
        }
        return result;
    };

    $scope.MakeJavaScript = function () {

    };

    //this function checkes nodes validity before exporting (DECISION condition, or ASSIGN left/right operands....)
    $scope.ValidateBeforeExport = function () {
        var result = "";
        try
        {
            for (var i = 1 ; i < $scope.chartViewModel.nodes.length; i++) {//loop through all nodes
                var node = $scope.chartViewModel.nodes[i];
                var nodetype = node.data.typeIndex;//get current node type

                if (nodetype == "2")//assign
                {
                    try {
                        var txt1 = document.getElementById("txtAssignVariable" + node.data.id);//left-side operand
                        var txt2 = document.getElementById("txtAssignVariable2-" + node.data.id);//right-side operand
                        if (txt1.value.trim() == "" || txt2.value.trim() == "") {//return error if any of the opernds are empty
                            return "Assign node has validation errors";
                        }
                    }
                    catch (ex) {
                        return "Assign node contains error";
                    }
                }
                else if (nodetype == "1")//if
                {
                    try
                    {
                        var txt = document.getElementById("txtIfCondition" + node.data.id);//DECISION condition
                        if (txt.value.trim() == "") {//return error if the condition is empty
                            return "Decision node condition is missing";
                        }
                    }
                    catch (ex) {
                        return "Decision node contains error";
                    }
                }

            }
        }
        catch (ex) {
            //alert(ex.message);
        }

        return result;
    };

    $scope.MakeCSharp = function () {
        txtCSharp.value = "";
        var validationError = $scope.ValidateBeforeExport();
        if (validationError != "")//check nodes validity before export, alert and return if there is any error in the nodes structure
        {
            alert(validationError);
            return;
        }
        try {
            IndentCount = 2;//reset indices used to indent the code and name the variables
            FunctionCodes = "";
            IfIndex = 1;
            CSharp_Code = "";

            //initially, load project references
            for (var i = 0 ; i < References.length; i++) {
                CSharp_Code += "using " + References[i] + ";" + "\n";
            }
            CSharp_Code += "\n";

            //only display functions that are included in the items chain and therefore should be contained in the C# code, 
            //unconnected items should not be included in the final C# code
            FinalFunctionIDs.length = 0;
            DicIndex = 1;
            var classname = lblWorkflowName.value.replace(/ /g, '_');//change any possible blanks in the workflow name into the underscore character, this name would be used as our main class name
            classname = "CustomCodeActivity : CodeActivity";
            CSharp_Code += "/// <summary>\n";
            CSharp_Code += "/// Add a query to project queries list\n";
            CSharp_Code += "/// </summary>\n";

            CSharp_Code += "public sealed class " + classname + "\n";
            CSharp_Code += "{" + "\n";

            //define variables
            CSharp_Code += $scope.InsertVariablesIntoCSharpCode(0);//define the main class (top root) variables
            //end variables

            //define arguments (arguments defined in the workflow are treated as C# properties
            for (var i = 0 ; i < Arguments.length; i++) {//loop through arguments
                if (!Arguments[i].Required) {//only show non-required arguments
                    var realDefault = Arguments[i].Default;
                    var def = '';//create the default value
                    if (Arguments[i].Type.toLowerCase() == "string") {
                        def += '"';
                        def += Arguments[i].Default;
                        def += '"';
                    }
                    else if (Arguments[i].Type.toLowerCase() == "number") {
                        def = Arguments[i].Default;
                    }
                    else if (Arguments[i].Type.toLowerCase() == "boolean") {
                        if (Arguments[i].Default == "") {
                            def = "true";
                        }
                        else {
                            def = Arguments[i].Default.toLowerCase();
                        }
                    }
                    var argType = Arguments[i].Type.toLowerCase().replace("number", "float").replace("boolean", "bool");//detect argument type
                    //argument has any default value? so assign it to the backstore
                    var argDef = "";
                    if (realDefault != "") {
                        argDef = " = " + def;
                    }
                    var argReq = false;
                    if (Arguments[i].Required)
                        argReq = true;
                    var getAccess = "public ";
                    var setAccess = "public ";
                    if (Arguments[i].Direction == "In")
                        getAccess = "internal ";
                    else if (Arguments[i].Direction == "Out")
                        setAccess = "internal ";
                    //CSharp_Code += $scope.IndentCode(1) + "private " + argType + " _" + Arguments[i].Name.toLowerCase() + argDef + ";\n";//property backstore variable
                    //CSharp_Code += $scope.IndentCode(1) + "public " + argType + " " + Arguments[i].Name + "\n";
                    //CSharp_Code += $scope.IndentCode(1) + "{" + "\n";
                    //CSharp_Code += $scope.IndentCode(1 + 1) + getAccess + "get" + "\n";///define get accessor
                    //CSharp_Code += $scope.IndentCode(1 + 1) + "{" + "\n";
                    //CSharp_Code += $scope.IndentCode(3 + 1) + "return this._" + Arguments[i].Name.toLowerCase() + ";\n";
                    //CSharp_Code += $scope.IndentCode(1 + 1) + "}" + "\n";
                    //CSharp_Code += $scope.IndentCode(1 + 1) + setAccess + "set" + "\n";//define set accessor
                    //CSharp_Code += $scope.IndentCode(1 + 1) + "{" + "\n";
                    //CSharp_Code += $scope.IndentCode(3 + 1) + "this._" + Arguments[i].Name.toLowerCase() + " = value" + ";\n";
                    //CSharp_Code += $scope.IndentCode(1 + 1) + "}" + "\n";
                    //CSharp_Code += $scope.IndentCode(1) + "}" + "\n";
                    CSharp_Code += $scope.IndentCode(1) + " public InArgument<" + argType + "> " + Arguments[i].Name + " { get; set; }";

                    CSharp_Code += "\n";
                }
            }//end loop through arguments

            for (var i = 0 ; i < Arguments.length; i++) {//loop through arguments
                if (Arguments[i].Required) {//show this header only if there is at least one required argument
                    CSharp_Code += $scope.IndentCode(1) + " [RequiredArgument()]\n";
                    break;
                }
            }

            //define arguments (arguments defined in the workflow are treated as C# properties
            for (var i = 0 ; i < Arguments.length; i++) {//loop through arguments
                if (Arguments[i].Required) {//only show required arguments
                    var realDefault = Arguments[i].Default;
                    var def = '';//create the default value
                    if (Arguments[i].Type.toLowerCase() == "string") {
                        def += '"';
                        def += Arguments[i].Default;
                        def += '"';
                    }
                    else if (Arguments[i].Type.toLowerCase() == "number") {
                        def = Arguments[i].Default;
                    }
                    else if (Arguments[i].Type.toLowerCase() == "boolean") {
                        if (Arguments[i].Default == "") {
                            def = "true";
                        }
                        else {
                            def = Arguments[i].Default.toLowerCase();
                        }
                    }
                    var argType = Arguments[i].Type.toLowerCase().replace("number", "float").replace("boolean", "bool");//detect argument type
                    //argument has any default value? so assign it to the backstore
                    var argDef = "";
                    if (realDefault != "") {
                        argDef = " = " + def;
                    }
                    var argReq = false;
                    if (Arguments[i].Required)
                        argReq = true;
                    var getAccess = "public ";
                    var setAccess = "public ";
                    if (Arguments[i].Direction == "In")
                        getAccess = "internal ";
                    else if (Arguments[i].Direction == "Out")
                        setAccess = "internal ";
                    //CSharp_Code += $scope.IndentCode(1) + "private " + argType + " _" + Arguments[i].Name.toLowerCase() + argDef + ";\n";//property backstore variable
                    //CSharp_Code += $scope.IndentCode(1) + "public " + argType + " " + Arguments[i].Name + "\n";
                    //CSharp_Code += $scope.IndentCode(1) + "{" + "\n";
                    //CSharp_Code += $scope.IndentCode(1 + 1) + getAccess + "get" + "\n";///define get accessor
                    //CSharp_Code += $scope.IndentCode(1 + 1) + "{" + "\n";
                    //CSharp_Code += $scope.IndentCode(3 + 1) + "return this._" + Arguments[i].Name.toLowerCase() + ";\n";
                    //CSharp_Code += $scope.IndentCode(1 + 1) + "}" + "\n";
                    //CSharp_Code += $scope.IndentCode(1 + 1) + setAccess + "set" + "\n";//define set accessor
                    //CSharp_Code += $scope.IndentCode(1 + 1) + "{" + "\n";
                    //CSharp_Code += $scope.IndentCode(3 + 1) + "this._" + Arguments[i].Name.toLowerCase() + " = value" + ";\n";
                    //CSharp_Code += $scope.IndentCode(1 + 1) + "}" + "\n";
                    //CSharp_Code += $scope.IndentCode(1) + "}" + "\n";
                    CSharp_Code += $scope.IndentCode(1) + " public InArgument<" + argType + "> " + Arguments[i].Name + " { get; set; }";

                    CSharp_Code += "\n";
                }
            }//end loop through arguments

            //CSharp_Code += $scope.IndentCode(1) + "public void " + classname + "()\n";//constructor function
            //CSharp_Code += $scope.IndentCode(1) + "{" + "\n";//open the constructor function
            //now recursively look for nodes and make up the code structure
            //find the main start node (start node of the first flowchart), and use it as the entrypoint for recursive algorithm
            var mainStartIndex = 1;
            for (var i = 0 ; i < $scope.chartViewModel.nodes.length; i++) {
                if ($scope.chartViewModel.nodes[i].data.typeIndex == 8 && $scope.chartViewModel.nodes[i].data.rootNodeID == 0) {
                    mainStartIndex = i;
                    break;
                }
            }

            CSharp_Code += $scope.IndentCode(1) + "/// <summary>\n";
            CSharp_Code += $scope.IndentCode(1) + "/// Updates a workflow definition\n";
            CSharp_Code += $scope.IndentCode(1) + "/// </summary>\n";
            CSharp_Code += $scope.IndentCode(1) + "/// <param name=\"context\">Workflow runtime instance context</param>\n";
            CSharp_Code += $scope.IndentCode(1) + "protected override void Execute(CodeActivityContext context)\n";
            CSharp_Code += $scope.IndentCode(1) + "{\n";

            CSharp_Code += $scope.RecursivelyFindNodes($scope.chartViewModel.nodes[mainStartIndex]);//now start the recursive function with our main start item, let's go....

            CSharp_Code += $scope.IndentCode(1) + "}\n";


            $('.divXamlBack').css('display', 'block');
            divCSharp.style.display = "";
            //btnEvaluateCode.style.display = "";
            //CSharp_Code += $scope.IndentCode(1) + "}" + "\n";//close the constructor function

            //functions start from here
            //loop through all nodes, find START nodes and again start the "RecursivelyFindNodes" function to create the function(s) body
            var startNodeList = [];
            for (var i = 0 ; i < $scope.chartViewModel.nodes.length; i++) {
                if ($scope.chartViewModel.nodes[i].data.typeIndex == 8 && $scope.chartViewModel.nodes[i].data.rootNodeID != 0) {
                    startNodeList.push($scope.chartViewModel.nodes[i]);
                }
            }

            //now take a loop around found START nodes, and create corresponding functions based on the START nodes (ALL functions begin with the START node)
            for (var i = 0 ; i < startNodeList.length; i++) {
                if (FinalFunctionIDs.indexOf(startNodeList[i].data.rootNodeID) != -1) {
                    CSharp_Code += $scope.IndentCode(1) + "\n";//add an empty line

                    var realname = $scope.chartViewModel.FindFlowchartTitle(startNodeList[i].data.rootNodeID);
                    var funcname = "";
                    if (realname == "Flowchart")//default name
                        funcname = "CustomFunction" + startNodeList[i].data.rootNodeID;
                    else
                        funcname = realname;

                    CSharp_Code += $scope.IndentCode(1) + "public void " + funcname + "()\n";//declare function signature
                    CSharp_Code += $scope.IndentCode(1) + "{" + "\n";//open the function
                    CSharp_Code += $scope.InsertVariablesIntoCSharpCode(startNodeList[i].data.rootNodeID);//defind the main class (top root) variables
                    CSharp_Code += $scope.RecursivelyFindNodes(startNodeList[i]);
                    CSharp_Code += $scope.IndentCode(1) + "}" + "\n";//close the function
                }
            }
            //functions end here

            //CSharp_Code += "/// <summary>\n";
            //CSharp_Code += "/// Workflow guid\n";
            //CSharp_Code += "/// </summary>\n";
            //CSharp_Code += "[RequiredArgument()]\n";
            //CSharp_Code += "public InArgument<string> Guid { get; set; }\n";
            //CSharp_Code += "/// <summary>\n";
            //CSharp_Code += "/// Workflow definition text\n";
            //CSharp_Code += "/// </summary>\n";
            //CSharp_Code += "public InArgument<string> Definition { get; set; }\n";

            //CSharp_Code += "/// <summary>\n";
            //CSharp_Code += "/// Current edit project Id\n";
            //CSharp_Code += "/// </summary>\n";
            //CSharp_Code += "[RequiredArgument()]\n";
            //CSharp_Code += "public InArgument<string> ProjectID { get; set; }\n";

            //CSharp_Code += "/// <summary>\n";
            //CSharp_Code += "/// Current defnition source language\n";
            //CSharp_Code += "/// </summary>\n";
            //CSharp_Code += "public InArgument<string> CodeLang { get; set; }\n";

            //CSharp_Code += "/// <summary>\n";
            //CSharp_Code += "/// Added workflow definition object\n";
            //CSharp_Code += "/// </summary>\n";
            //CSharp_Code += "public OutArgument<object> Output { get; set; }\n";

            CSharp_Code += "}";//close the class body
            txtCSharp.value += "\n";
            txtCSharp.value += CSharp_Code;
        }
        catch (ex) {
            //alert(ex.message);
        }
    };

    $scope.RecursivelyFindNodes = function (node, secondchild, foreachitem) {
        var coderesult = "";
        var parenttypeIndex = node.data.typeIndex;
        try {
            //loop through all nodes, and find child(ren) node(s) of the current node (i.e. node passed in as the parameter)
            var childrenGUIDs = node.data.childrenGUIDs;//get list of current node children GUIDs
            if (foreachitem != null)//we are looking for a foreach child item?
            {
                var tmpCh = jQuery.extend(true, [], node.data.childrenGUIDs);
                childrenGUIDs.length = 0;
                childrenGUIDs.push({ guid: node.data.GUID, number: 2 });
                node.data.childrenGUIDs = tmpCh;
            }
            for (var c = 0 ; c < childrenGUIDs.length; c++) {//loop through all children of this node
                var childNode = $scope.chartViewModel.GetNodeByGUID(childrenGUIDs[c].guid);//find current child node
                var validChild = true;
                if (secondchild != null) {
                    if (foreachitem != null) {
                        validChild = true;
                    }
                    else {
                        validChild = false;
                        if (secondchild) {
                            if (childrenGUIDs[c].number == 2) {
                                validChild = true;
                            }
                        }
                        else {
                            if (childrenGUIDs[c].number == 1) {
                                validChild = true;
                            }
                        }
                    }
                }
                if (validChild) {
                    var childtypeIndex = childNode.data.typeIndex;
                    if (childtypeIndex == "1")//if
                    {
                        var ifcondition = document.getElementById("txtIfCondition" + childNode.data.id).value;
                        if (childNode.data.outputConnectors[1].connected && !childNode.data.outputConnectors[0].connected) {//only the FALSE branch has children
                            coderesult += $scope.IndentCode(IndentCount) + "if (!(" + ifcondition + "))" + "\n";
                            coderesult += $scope.IndentCode(IndentCount) + "{" + "\n";
                            IndentCount++;
                            coderesult += $scope.RecursivelyFindNodes(childNode, true);
                            IndentCount--;
                            coderesult += $scope.IndentCode(IndentCount) + "} " + "\n";
                        }
                        else if (childNode.data.outputConnectors[0].connected && !childNode.data.outputConnectors[1].connected) {//only the TRUE branch has children
                            coderesult += $scope.IndentCode(IndentCount) + "if (" + ifcondition + ")" + "\n";
                            coderesult += $scope.IndentCode(IndentCount) + "{" + "\n";
                            IndentCount++;
                            coderesult += $scope.RecursivelyFindNodes(childNode, false);
                            IndentCount--;
                            coderesult += $scope.IndentCode(IndentCount) + "} " + "\n";
                        }
                        else if (childNode.data.outputConnectors[1].connected && childNode.data.outputConnectors[0].connected) {//both TRUE and FALSE have children
                            coderesult += $scope.IndentCode(IndentCount) + "if (" + ifcondition + ")" + "\n";
                            coderesult += $scope.IndentCode(IndentCount) + "{" + "\n";
                            IndentCount++;
                            coderesult += $scope.RecursivelyFindNodes(childNode, false);
                            IndentCount--;
                            coderesult += $scope.IndentCode(IndentCount) + "} " + "\n";
                            coderesult += $scope.IndentCode(IndentCount) + "else" + "\n";
                            coderesult += $scope.IndentCode(IndentCount) + "{" + "\n";
                            IndentCount++;
                            coderesult += $scope.RecursivelyFindNodes(childNode, true);
                            IndentCount--;
                            coderesult += $scope.IndentCode(IndentCount) + "} " + "\n";
                        }
                        IfIndex++;
                    }
                    else if (childtypeIndex == "10")//connection
                    {
                        coderesult += $scope.IndentCode(IndentCount) + "sdk.connection();" + "\n";
                    }
                    else if (childtypeIndex == "11")//collector
                    {
                        coderesult += $scope.IndentCode(IndentCount) + "sdk.collector();" + "\n";
                    }
                    else if (childtypeIndex == "12")//workflow
                    {
                        coderesult += $scope.IndentCode(IndentCount) + "sdk.workflow();" + "\n";
                    }
                    else if (childtypeIndex == "2")//assign
                    {
                        var cmb1 = document.getElementById("txtAssignVariable" + childNode.data.id);
                        var cmb2 = document.getElementById("txtAssignVariable2-" + childNode.data.id);
                        var assignleftside = "";
                        try {
                            assignleftside = cmb1.value;
                        }
                        catch (ex) {

                        }
                        coderesult += $scope.IndentCode(IndentCount) + assignleftside + " = " + cmb2.value + ";" + "\n";
                    }
                    else if (childtypeIndex == "7")//flowchart
                    {
                        //find flowchart (function) name, if default name (Flowchart) is used, then use the "CustomFunctionX" naming method, otherwise use real flowchart name for function
                        var realname = $scope.chartViewModel.FindFlowchartTitle(childNode.data.id);
                        var funcname = "";
                        if (realname == "Flowchart")//default name
                            funcname = "CustomFunction" + childNode.data.id;
                        else
                            funcname = realname;
                        FinalFunctionIDs.push(childNode.data.id);
                        coderesult += $scope.IndentCode(IndentCount) + funcname + "();" + "\n";
                    }
                    else if (childtypeIndex == "9") {
                        var dicList = [];
                        for (var m = 0 ; m < 1000; m++) {
                            try {
                                var key = document.getElementById("txtKey-" + childNode.data.id + "-" + m).value;
                                var value = document.getElementById("txtValue-" + childNode.data.id + "-" + m).value;
                                if (key.trim() != "") {
                                    dicList.push({ key: key, value: value });
                                }
                            }
                            catch (ex) { }
                        }
                        coderesult += $scope.IndentCode(IndentCount) + "Dictionary<string, string> dic" + DicIndex + " = new Dictionary<string, string>();" + "\n";
                        for (var m = 0 ; m < dicList.length; m++) {
                            coderesult += $scope.IndentCode(IndentCount) + "dic" + DicIndex + '.Add("' + dicList[m].key + '",' + dicList[m].value + ');' + "\n";
                        }
                        DicIndex++;
                    }
                    else if (childtypeIndex == "13") {
                        if (ForeachActive) {//if we are currently in a foreach loop, get item & list name directly from UI
                            if (currentRootNodeID == childNode.data.id) {
                                childNode.data.ForeachItemName = document.getElementById('txtForeachItemName').value;
                                childNode.data.ForeachListName = document.getElementById('txtForeachListName').value;
                            }
                        }

                        var itemname = childNode.data.ForeachItemName;//find real item name of foreach loop (item name is a property of the node object)
                        var listname = childNode.data.ForeachListName;
                        if (itemname.trim() == "") {//if user hasn't specified any item name, use the default phrase: item
                            itemname = "item";
                        }
                        if (listname.trim() == "") {
                            listname = "list";
                        }
                        coderesult += $scope.IndentCode(IndentCount) + "foreach (string " + itemname + " in " + listname + ")" + "\n";//construct the foreach body
                        coderesult += $scope.IndentCode(IndentCount) + "{" + "\n";
                        //find foreach child and insert it in the loop
                        console.clear();
                        for (var q = 0 ; q < $scope.chartViewModel.nodes.length; q++) {
                            if ($scope.chartViewModel.nodes[q].data.rootNodeIDForeach == childNode.data.id) {
                                var childForeach = $scope.chartViewModel.nodes[q];
                                console.log(childForeach.data.parentIDs.length);
                                if (childForeach.data.parentIDs.length == 0)//it is the direct child of FOREACH item?
                                {
                                    IndentCount++;
                                    coderesult += $scope.RecursivelyFindNodes(childForeach, true, true);
                                    IndentCount--;
                                    break;
                                }
                                //break;
                            }
                        }

                        coderesult += $scope.IndentCode(IndentCount) + "}" + "\n";
                    }
                    if (childtypeIndex != 1 && foreachitem == null) {
                        coderesult += $scope.RecursivelyFindNodes(childNode);//recursively traverse the next found node (only if it is not a DECISION item)
                    }
                }

            }//end loop through child(ren) of the current node
        }//end try
        catch (ex) {
            //alert(ex.message);
        }
        return coderesult;
    };

    $scope.MakeXML3 = function () {
        var validationError = $scope.ValidateBeforeExport();
        if (validationError != "")//check nodes validity before export, alert and return if there is any error in the nodes structure
        {
            alert(validationError);
            return;
        }

        refIDInfo = "";
        XMLdata2 = "";
        XMLdata = "";
        xmlReference = 0;
        var default_argument_values = "";
        if (Arguments.length != 0) {
            for (var q = 0; q < Arguments.length; q++) {
                if (Arguments[q].Direction != "Out" && Arguments[q].Direction != "In/Out") {
                    default_argument_values += "         this:Activity1." + Arguments[q].Name + "=\"" + Arguments[q].Default + "\" ";
                }
            }
        }
        //for (var q = 0 ; q < importedLibraries.length; q++) {
        //    XMLdata2 += " xmlns:srh=clr-namespace:System.Runtime.Hosting;assembly=mscorlib";
        //}
        XMLdata2 += "<Activity mc:Ignorable=\"sap sads\" x:Class=\"ActivityLibrary4.Activity1\" " + default_argument_values + " sap:VirtualizedContainerService.HintSize=\"" + flowChartWidth[0] + "," + flowChartHeight[0] + "\" mva:VisualBasic.Settings=\"Assembly references and imported namespaces for internal implementation\"\n";
        XMLdata2 += "xmlns=\"http://schemas.microsoft.com/netfx/2009/xaml/activities\"\n";
        XMLdata2 += "xmlns:av=\"http://schemas.microsoft.com/winfx/2006/xaml/presentation\"\n";
        XMLdata2 += "xmlns:mc=\"http://schemas.openxmlformats.org/markup-compatibility/2006\"\n";
        XMLdata2 += "xmlns:mv=\"clr-namespace:Microsoft.VisualBasic;assembly=System\"\n";
        XMLdata2 += "xmlns:mva=\"clr-namespace:Microsoft.VisualBasic.Activities;assembly=System.Activities\"\n";
        XMLdata2 += "xmlns:s=\"clr-namespace:System;assembly=mscorlib\"\n";
        XMLdata2 += "xmlns:s1=\"clr-namespace:System;assembly=System\"\n";
        XMLdata2 += "xmlns:s2=\"clr-namespace:System;assembly=System.Xml\"\n";
        XMLdata2 += "xmlns:s3=\"clr-namespace:System;assembly=System.Core\"\n";
        XMLdata2 += "xmlns:s4=\"clr-namespace:System;assembly=System.ServiceModel\"\n";
        XMLdata2 += "xmlns:sad=\"clr-namespace:System.Activities.Debugger;assembly=System.Activities\"\n";
        XMLdata2 += "xmlns:sads=\"http://schemas.microsoft.com/netfx/2010/xaml/activities/debugger\"\n";
        XMLdata2 += "xmlns:sap=\"http://schemas.microsoft.com/netfx/2009/xaml/activities/presentation\"\n";
        XMLdata2 += "xmlns:scg=\"clr-namespace:System.Collections.Generic;assembly=System\"\n";
        XMLdata2 += "xmlns:scg1=\"clr-namespace:System.Collections.Generic;assembly=System.ServiceModel\"\n";
        XMLdata2 += "xmlns:scg2=\"clr-namespace:System.Collections.Generic;assembly=System.Core\"\n";
        XMLdata2 += "xmlns:scg3=\"clr-namespace:System.Collections.Generic;assembly=mscorlib\"\n";
        XMLdata2 += "xmlns:sd=\"clr-namespace:System.Data;assembly=System.Data\"\n";
        XMLdata2 += "xmlns:sl=\"clr-namespace:System.Linq;assembly=System.Core\"\n";
        XMLdata2 += "xmlns:st=\"clr-namespace:System.Text;assembly=mscorlib\"\n";
        if (Arguments.length != 0) {
            XMLdata2 += " xmlns:this=\"clr-namespace:ActivityLibrary4\"\n";
        }
        XMLdata2 += "xmlns:x=\"http://schemas.microsoft.com/winfx/2006/xaml\">\n";

        if (Arguments.length != 0) {
            XMLdata2 += "    <x:Members>\n";
            for (var q = 0; q < Arguments.length; q++) {
                var dir = Arguments[q].Direction;
                if (dir == "Property")
                    dir = "x:" + Arguments[q].Type;
                else {
                    dir = Arguments[q].Direction + "Argument(x:" + Arguments[q].Type.replace("Number", "Int32").replace("null", "Object").replace("Whitespace", "Object") + ")";
                }
                XMLdata2 += "    <x:Property Name='" + Arguments[q].Name + "' Type='" + dir + "' />\n";
            }
            XMLdata2 += "    </x:Members>\n";
        }

        XMLdata2 += "<Flowchart DisplayName=\"" + $scope.chartViewModel.FindFlowchartTitle(0) + "\" sap:VirtualizedContainerService.HintSize=\"" + flowChartWidth[0] + "," + flowChartHeight[0] + "\">\n";
        var numVars = 0;

        for (var q = 0; q < Variables.length; q++) {
            if (Variables[q].ScopeID == 0) {
                numVars++;
            }
        }

        if (numVars != 0) {
            XMLdata2 += "    <Flowchart.Variables>\n";
            for (var q = 0; q < Variables.length; q++) {
                if (Variables[q].ScopeID == 0) {
                    var varDef = "";
                    if (Variables[q].Default != "")
                        varDef = 'Default="' + Variables[q].Default + '"';
                    XMLdata2 += "      <Variable x:TypeArguments='x:" + Variables[q].Type.replace("Number", "Int32").replace("null", "Object").replace("Whitespace", "Object") + "' " + varDef + " Name='" + Variables[q].Name + "' />\n";
                }
            }
            XMLdata2 += "    </Flowchart.Variables>\n";
        }

        XMLdata2 += $scope.RecursiveFindNode($scope.chartViewModel.nodes[1]);

        var tmpArray = new Array();
        var parts = refIDInfo.split(',');
        for (var q = 0; q < parts.length; q++) {
            if (parts[q] != "") {
                var rows = parts[q].split('=');
                if (rows[0] == 0) {
                    tmpArray.push(rows[1]);
                }
            }
        }

        var refIndex = 0;
        for (var k = 0; k < $scope.chartViewModel.nodes.length; k++) {
            if ($scope.chartViewModel.nodes[k].data.rootNodeID == 0) {
                if (refIndex < tmpArray.length) {
                    XMLdata2 += "    <x:Reference>__ReferenceID" + tmpArray[refIndex] + "</x:Reference>\n";
                    refIndex++;
                }
            }
        }

        XMLdata2 += "</Flowchart>\n";
        XMLdata2 += "</Activity>\n";

        $('.divXamlBack').css('display', 'block');
        divCSharp.style.display = "";
        btnEvaluateCode.style.display = "none";

        for (var i = 0 ; i < $scope.chartViewModel.nodes.length; i++) {
            XMLdata2 = XMLdata2.replace("_XAML_REF_REPLACE_" + $scope.chartViewModel.nodes[i].data.id + "_XAML_REF_REPLACE_", $scope.chartViewModel.nodes[i].data.XAMLReferenceID);

        }

        txtCSharp.value = XMLdata2;
    };

    $scope.RecursiveFindNode = function (node, positionoffset) {
        var data = "";
        var nodetype = ClassNames[node.data.typeIndex - 1];
        var foreachchildmode = false;//are we working on direct child of a foreach node? if so, change the process a little, in this mode we have no parent for our child
        if (node.data.rootNodeIDForeach != "") {
            foreachchildmode = true;
        }
        var parentNode;
        if (!foreachchildmode) {
            parentNode = $scope.chartViewModel.GetNodeById(node.data.parentIDs[node.data.parentIDs.length - 1]);//$scope.chartViewModel.GetNodeById(node.data.parent);
        }
        try {
            var valid = true;
            if ((nodetype == "start" && node.data.parent == 0) || foreachchildmode)
                valid = false;
            if (valid) {
                if (ClassNames[parentNode.data.typeIndex - 1] != "start" && ClassNames[parentNode.data.typeIndex - 1] != "if") {
                    data += "<FlowStep.Next>\n";
                }
                if (ClassNames[parentNode.data.typeIndex - 1] != "start") {
                    refIDInfo += node.data.rootNodeID + "=" + xmlReference + ",";
                }
                if (nodetype != "if") {
                    if (node.data.parentIDs.length > 1)
                        node.data.XAMLReferenceID = "__ReferenceID" + xmlReference;
                    data += "<FlowStep  x:Name='__ReferenceID" + xmlReference + "'>\n";
                }
            }
            if (nodetype != "if") {
                data += "   <sap:WorkflowViewStateService.ViewState>\n";
                data += "    <scg3:Dictionary x:TypeArguments='x:String, x:Object'>\n";
                data += "      <x:Boolean x:Key='IsExpanded'>False</x:Boolean>\n";
                var shapeLocationX = node.data.x;
                if (node.data.typeIndex == 2) {
                    shapeLocationX = shapeLocationX - 92;
                } else if (node.data.typeIndex == 7) {
                    shapeLocationX = shapeLocationX - 70;
                }
                if (positionoffset != null) {
                    shapeLocationX += parseInt(positionoffset);
                    if (shapeLocationX < 0) {
                        shapeLocationX = 0;
                    }
                }
                if (shapeLocationX < 0) {
                    shapeLocationX = 0;
                }
                data += "      <av:Point x:Key='ShapeLocation'>" + shapeLocationX + "," + node.data.y + "</av:Point>\n";
            }

            for (var k = 0; k < $scope.chartViewModel.connections.length; k++) { //loop through all connections and select connections that belong to the current flowchart
                if ($scope.chartViewModel.connections[k].source.parentNode().data.id == node.data.id) {
                    if (nodetype != "if") {
                        var finalConnectorLocation = $scope.chartViewModel.connections[k].GetPathPoint();
                        if (finalConnectorLocation == undefined || finalConnectorLocation == "undefined") {
                            var outputx;
                            var outputy;

                            if ($scope.chartViewModel.connections[k].sourceOutputID == 1) {
                                outputy = $scope.chartViewModel.connections[k].oy1;
                                outputx = $scope.chartViewModel.connections[k].ox1;
                            }
                            else {
                                outputy = $scope.chartViewModel.connections[k].oy2;
                                outputx = $scope.chartViewModel.connections[k].ox2;
                            }

                            finalConnectorLocation = (parseInt($scope.chartViewModel.connections[k].source.parentNode().x()) + parseInt(outputx) + 15) + "," +
                                (parseInt($scope.chartViewModel.connections[k].source.parentNode().y()) + parseInt(outputy)) + " ";
                        }

                        data += "      <av:PointCollection x:Key='ConnectorLocation'>" + finalConnectorLocation + "</av:PointCollection>\n";
                    }
                }
            } //end loop through connections

            if (nodetype != "if") {
                data += "    </scg3:Dictionary>\n";
                data += "  </sap:WorkflowViewStateService.ViewState>\n";
            }

            if (nodetype == "start") {
                data += "<Flowchart.StartNode>\n";
                //is there any child for this start node?
                var haschild = false;

                for (var s = 0; s < $scope.chartViewModel.nodes.length; s++) {
                    if ($scope.chartViewModel.nodes[s].data.parentIDs.indexOf(node.data.id) != -1) {
                        haschild = true;
                        break;
                    }
                }

                if (haschild) {
                    if (node.data.parentIDs.length > 1)
                        node.data.XAMLReferenceID = "__ReferenceID" + xmlReference;
                    data += "<x:Reference>__ReferenceID" + xmlReference + "</x:Reference>\n";
                }
                data += "</Flowchart.StartNode>\n";
            }

            if (nodetype == "assign") {
                var operator1 = document.getElementById("txtAssignVariable" + node.data.id).value;
                var operator2 = document.getElementById("txtAssignVariable2-" + node.data.id).value;

                data += "  <Assign sap:VirtualizedContainerService.HintSize='242,60'>\n";
                data += "    <Assign.To>\n";
                data += "      <OutArgument x:TypeArguments='x:Object'>[" + operator1 + "]</OutArgument>\n";
                data += "    </Assign.To>\n";
                data += "    <Assign.Value>\n";
                data += "      <InArgument x:TypeArguments='x:Object'>[" + operator2 + "]</InArgument>\n";
                data += "    </Assign.Value>\n";
                data += "    <sap:WorkflowViewStateService.ViewState>\n";
                data += "      <scg3:Dictionary x:TypeArguments='x:String, x:Object'>\n";
                data += "        <x:Boolean x:Key='IsExpanded'>True</x:Boolean>\n";
                data += "      </scg3:Dictionary>\n";
                data += "    </sap:WorkflowViewStateService.ViewState>\n";
                data += "  </Assign>\n";
            }
            else if (nodetype == "foreach") {
                if (ForeachActive) {//if we are currently in a foreach loop, get item & list name directly from UI
                    if (currentRootNodeID == node.data.id) {
                        node.data.ForeachItemName = document.getElementById('txtForeachItemName').value;
                        node.data.ForeachListName = document.getElementById('txtForeachListName').value;
                    }
                }

                var itemname = node.data.ForeachItemName;//find real item name of foreach loop (item name is a property of the node object)
                var listname = node.data.ForeachListName;
                if (itemname.trim() == "") {//if user hasn't specified any item name, use the default phrase: item
                    itemname = "item";
                }
                if (listname.trim() == "") {//if user hasn't specified any list name, use the default phrase: list
                    listname = "list";
                }

                data += "<ForEach x:TypeArguments='x:Int32' sap:VirtualizedContainerService.HintSize='287,208' Values='[" + listname + "]'>\n";
                data += "  <sap:WorkflowViewStateService.ViewState>\n";
                data += "    <scg3:Dictionary x:TypeArguments='x:String, x:Object'>\n";
                data += "      <x:Boolean x:Key='IsExpanded'>True</x:Boolean>\n";
                data += "    </scg3:Dictionary>\n";
                data += "  </sap:WorkflowViewStateService.ViewState>\n";
                data += "  <ActivityAction x:TypeArguments='x:Int32'>\n";
                data += "    <ActivityAction.Argument>\n";
                data += "      <DelegateInArgument x:TypeArguments='x:Int32' Name='" + itemname + "' />\n";
                data += "    </ActivityAction.Argument>\n";
                for (var q = 0 ; q < $scope.chartViewModel.nodes.length; q++) {
                    if ($scope.chartViewModel.nodes[q].data.rootNodeIDForeach == node.data.id) {
                        var childForeach = $scope.chartViewModel.nodes[q];
                        if (childForeach.data.parentIDs.length == 0)//it is the direct child of FOREACH item?
                        {
                            data += $scope.RecursiveFindNode(childForeach, -1 * parseInt(badposition));
                            break;
                        }
                        break;
                    }
                }

                data += "  </ActivityAction>\n";
                data += "</ForEach>\n";
            }
            else if (nodetype == "if") {
                if (node.data.parentIDs.length > 1)
                    node.data.XAMLReferenceID = "__ReferenceID" + xmlReference;
                data += "  <FlowDecision x:Name='__ReferenceID" + xmlReference + "' Condition='[" + document.getElementById("txtIfCondition" + node.data.id).value.replace("<", "&lt;") + "]' sap:VirtualizedContainerService.HintSize='70,87'>\n";
                var truewritten = false;
                var falsewritten = false;
                data += "<sap:WorkflowViewStateService.ViewState>\n";
                data += "<scg3:Dictionary x:TypeArguments=\"x:String, x:Object\">\n";
                data += "<x:Boolean x:Key=\"IsExpanded\">True</x:Boolean>\n";
                data += "<av:Point x:Key=\"ShapeLocation\">" + node.data.x + "," + node.data.y + "</av:Point>\n";
                if (node.data.outputConnectors[0].connected) { //TRUE
                    for (var k = 0; k < $scope.chartViewModel.connections.length; k++) { //loop through all connections and select connections that belong to the current flowchart
                        if ($scope.chartViewModel.connections[k].source.parentNode().data.id == node.data.id && $scope.chartViewModel.connections[k].sourceOutputID == 1) {
                            if (!truewritten) {

                                var finalConnectorLocation = $scope.chartViewModel.connections[k].GetPathPoint();
                                if (finalConnectorLocation == undefined || finalConnectorLocation == "undefined") {
                                    var outputx;
                                    var outputy;

                                    if ($scope.chartViewModel.connections[k].sourceOutputID == 1) {
                                        outputy = $scope.chartViewModel.connections[k].oy1;
                                        outputx = $scope.chartViewModel.connections[k].ox1;
                                    }
                                    else {
                                        outputy = $scope.chartViewModel.connections[k].oy2;
                                        outputx = $scope.chartViewModel.connections[k].ox2;
                                    }

                                    finalConnectorLocation = (parseInt($scope.chartViewModel.connections[k].source.parentNode().x()) + parseInt(outputx) + 15) + "," +
                                        (parseInt($scope.chartViewModel.connections[k].source.parentNode().y()) + parseInt(outputy)) + " ";
                                }

                                truewritten = true;
                                data += "<av:PointCollection x:Key=\"TrueConnector\">" + finalConnectorLocation + "</av:PointCollection>\n";
                            }
                        }
                    } //end loop through connections
                }
                if (node.data.outputConnectors[1].connected) { //FALSE
                    for (var k = 0; k < $scope.chartViewModel.connections.length; k++) { //loop through all connections and select connections that belong to the current flowchart
                        if ($scope.chartViewModel.connections[k].source.parentNode().data.id == node.data.id && $scope.chartViewModel.connections[k].sourceOutputID == 2) {
                            if (!falsewritten) {

                                var finalConnectorLocation = $scope.chartViewModel.connections[k].GetPathPoint2();
                                if (finalConnectorLocation == undefined || finalConnectorLocation == "undefined") {
                                    if ($scope.chartViewModel.connections[k].sourceOutputID == 2) {
                                        var outputx;
                                        var outputy;

                                        if ($scope.chartViewModel.connections[k].sourceOutputID == 1) {
                                            outputy = $scope.chartViewModel.connections[k].oy1;
                                            outputx = $scope.chartViewModel.connections[k].ox1;
                                        }
                                        else {
                                            outputy = $scope.chartViewModel.connections[k].oy2;
                                            outputx = $scope.chartViewModel.connections[k].ox2;
                                        }
                                        finalConnectorLocation = (parseInt($scope.chartViewModel.connections[k].source.parentNode().x()) + parseInt(outputx) + 15) + "," +
                                            (parseInt($scope.chartViewModel.connections[k].source.parentNode().y()) + parseInt(outputy)) + " ";
                                    }
                                }

                                falsewritten = true;
                                data += "<av:PointCollection x:Key=\"FalseConnector\">" + finalConnectorLocation + "</av:PointCollection>\n";
                            }
                        }
                    } //end loop through connections
                }
                data += "</scg3:Dictionary>\n";
                data += "</sap:WorkflowViewStateService.ViewState>\n";

                var falseNode;
                var trueNode;

                var badposition = 0;
                //how many children has our IF node? TWO? if so, check horizontal distance between them it shouldn't be fewer than a specific value, otherwise we should give each of them an offset
                //so they appear correctly (without overlap) in Visual Studio
                var num_if_children = 0;

                if (node.data.outputConnectors[0].connected) { //TRUE
                    for (var j = 0; j < $scope.chartViewModel.nodes.length; j++) { //loop through all nodes and select nodes that belong to the current flowchart
                        var tmpNode = $scope.chartViewModel.nodes[j];
                        if (tmpNode.data.parent == node.data.id && node.data.outputConnectors[0].connected && tmpNode.data.childNumberOfParent == 1) {
                            trueNode = tmpNode;
                            num_if_children++;
                        }
                    }
                }
                if (node.data.outputConnectors[1].connected) { //FALSE
                    for (var j = 0; j < $scope.chartViewModel.nodes.length; j++) { //loop through all nodes and select nodes that belong to the current flowchart
                        var tmpNode = $scope.chartViewModel.nodes[j];
                        if (tmpNode.data.parent == node.data.id && node.data.outputConnectors[1].connected && tmpNode.data.childNumberOfParent == 2) {
                            falseNode = tmpNode;
                            num_if_children++;
                        }
                    }
                }

                if (num_if_children == 2) {
                    var bad_dist = Math.abs(parseInt(trueNode.data.x) - parseInt(falseNode.data.x));
                    if (bad_dist < 240) {
                        badposition = bad_dist;
                    }
                }
                //

                if (node.data.outputConnectors[0].connected) { //TRUE
                    data += "<FlowDecision.True>\n";
                    for (var j = 0; j < $scope.chartViewModel.nodes.length; j++) { //loop through all nodes and select nodes that belong to the current flowchart
                        var tmpNode = $scope.chartViewModel.nodes[j];
                        if (tmpNode.data.parent == node.data.id && node.data.outputConnectors[0].connected && tmpNode.data.childNumberOfParent == 1) {
                            trueNode = tmpNode;
                        }
                    }
                    xmlReference++;
                    data += $scope.RecursiveFindNode(trueNode, -1 * parseInt(badposition));
                    data += "</FlowDecision.True>\n";
                }
                if (node.data.outputConnectors[1].connected) { //FALSE
                    data += "<FlowDecision.False>\n";
                    for (var j = 0; j < $scope.chartViewModel.nodes.length; j++) { //loop through all nodes and select nodes that belong to the current flowchart
                        var tmpNode = $scope.chartViewModel.nodes[j];
                        if (tmpNode.data.parent == node.data.id && node.data.outputConnectors[1].connected && tmpNode.data.childNumberOfParent == 2) {
                            falseNode = tmpNode;
                        }
                    }
                    xmlReference++;
                    data += $scope.RecursiveFindNode(falseNode, parseInt(badposition));
                    data += "</FlowDecision.False>\n";
                }

                data += "  </FlowDecision>\n";
            } else if (nodetype == "flowchart2") {
                data += "  <Flowchart DisplayName=\"" + $scope.chartViewModel.FindFlowchartTitle(node.data.id) + "\" sap:VirtualizedContainerService.HintSize=\"" + flowChartWidth[node.data.currentActivityNumber - 1] + "," + flowChartHeight[node.data.currentActivityNumber - 1] + "\">\n";

                //variables
                var numVars = 0;
                for (var q = 0; q < Variables.length; q++) {
                    if (Variables[q].ScopeID == node.data.id) {
                        numVars++;
                    }
                }
                if (numVars != 0) {
                    data += "    <Flowchart.Variables>\n";
                    for (var q = 0; q < Variables.length; q++) {
                        if (Variables[q].ScopeID == node.data.id) {
                            data += "      <Variable x:TypeArguments='x:" + Variables[q].Type.replace("Number", "Int32") + "' Default='" + Variables[q].Default + "' Name='" + Variables[q].Name + "' />\n";
                        }
                    }
                    data += "    </Flowchart.Variables>\n";
                }
                //end variables

                //find start node of next flowchart
                var tmpNode;
                //find index of start node in flowchart
                var startNodeIndex = 0;
                for (var n = 0; n < $scope.chartViewModel.nodes.length; n++) {
                    if ($scope.chartViewModel.nodes[n].data.rootNodeID == node.data.id && ClassNames[$scope.chartViewModel.nodes[n].data.typeIndex - 1] == "start") {
                        startNodeIndex = n;
                        tmpNode = $scope.chartViewModel.nodes[n];
                    }
                }

                xmlReference++;
                if (tmpNode != null) {
                    data += $scope.RecursiveFindNode(tmpNode);//pass start node of the next flowchart to the recursive function
                }
                var tmpArray = new Array();
                var parts = refIDInfo.split(',');
                for (var q = 0; q < parts.length; q++) {
                    if (parts[q] != "") {
                        var rows = parts[q].split('=');
                        if (rows[0] == node.data.id) {
                            tmpArray.push(rows[1]);
                        }
                    }
                }
                var refIndex = 0;
                //for (var k = startNodeIndex + 2 ; k < $scope.chartViewModel.nodes.length; k++) {
                for (var k = 0; k < $scope.chartViewModel.nodes.length; k++) {
                    if ($scope.chartViewModel.nodes[k].data.rootNodeID == node.data.id) {
                        if (refIndex < tmpArray.length) {
                            if (node.data.parentIDs.length > 1)
                                node.data.XAMLReferenceID = "__ReferenceID" + tmpArray[refIndex];
                            data += "    <x:Reference>__ReferenceID" + tmpArray[refIndex] + "</x:Reference>\n";

                            refIndex++;
                        }
                    }
                }
                data += "          </Flowchart>\n";
            }

            XMLdata += data;

            //loop through all nodes and find node(s) that is(are) child of the current node, then call function for them.
            if (nodetype != "if") {
                for (var j = 0; j < $scope.chartViewModel.nodes.length; j++) { //loop through all nodes and select nodes that belong to the current flowchart
                    var tmpNode = $scope.chartViewModel.nodes[j];
                    if (tmpNode.data.parentIDs.indexOf(node.data.id) != -1) {
                        if (nodetype != "start") {
                            xmlReference++;
                        }
                        data += $scope.RecursiveFindNode(tmpNode);
                    }
                } //end loop through nodes
            }

            if (valid) {
                if (nodetype != "if") {
                    //reference items that have more than one parent
                    //if (parentNode.data.typeIndex == 1) {                        //look for any multiple parent node that may be child of this node
                    //    if (node.data.childNumberOfParent == 1) {//this node is a TRUE branch node of an IF item?
                    //        for (var q = 0 ; q < $scope.chartViewModel.nodes.length; q++) {
                    //            if ($scope.chartViewModel.nodes[q].data.parentIDs.indexOf(node.data.id) != -1) {
                    //                if ($scope.chartViewModel.nodes[q].data.parentIDs[0] == node.data.id) {
                    //                    if ($scope.chartViewModel.nodes[q].data.parentIDs.length > 1)//this node is a multi-parent node? so re-reference it in the XAML structure
                    //                    {
                    //                        data += "<FlowStep.Next>\n";
                    //                        data += "<x:Reference>_XAML_REF_REPLACE_" + $scope.chartViewModel.nodes[q].data.id + "_XAML_REF_REPLACE_</x:Reference>\n";
                    //                        data += "</FlowStep.Next>\n";
                    //                        break;
                    //                    }
                    //                }
                    //            }
                    //        }
                    //    }
                    //    else if (node.data.childNumberOfParent == 2) {//this node is a FALSE branch node of an IF item?
                    //        for (var q = 0 ; q < $scope.chartViewModel.nodes.length; q++) {
                    //            if ($scope.chartViewModel.nodes[q].data.parentIDs.indexOf(node.data.id) != -1) {
                    //                if ($scope.chartViewModel.nodes[q].data.parentIDs[0] == node.data.id) {
                    //                    if ($scope.chartViewModel.nodes[q].data.parentIDs.length > 1)//this node is a multi-parent node? so re-reference it in the XAML structure
                    //                    {
                    //                        data += "<FlowStep.Next>\n";
                    //                        data += "<x:Reference>_XAML_REF_REPLACE_" + $scope.chartViewModel.nodes[q].data.id + "_XAML_REF_REPLACE_</x:Reference>\n";
                    //                        data += "</FlowStep.Next>\n";
                    //                        break;
                    //                    }
                    //                }
                    //            }
                    //        }
                    //    }
                    //}

                    data += "</FlowStep>\n";
                }
                if (ClassNames[parentNode.data.typeIndex - 1] != "start" && ClassNames[parentNode.data.typeIndex - 1] != "if") {
                    data += "</FlowStep.Next>\n";
                }
            }
        } catch (ex) {
            //alert(ex.message);
        }
        return data;
    };

    $scope.CopyTouch = function () {
        $scope.CopyItems();
    };

    var touchPasteMode = false;

    $scope.PasteTouch = function () {
        touchPasteMode = true;
    };

    $scope.CopyItems = function() {
        ClipboardConnectionOffset = 0;
        ClipboarIDs.length = 0;
        ClipboardConnection.length = 0;
        ClipboardOffsets.length = 0;
        var firstX;
        var firstY;
        var nodelist = new Array();
        for (var i = 0; i < $scope.chartViewModel.GetNumNodes(); i++) {
            var tmpnode = $scope.chartViewModel.GetNode(i);
            if (tmpnode.data.visible) {
                if (tmpnode.selected() && tmpnode.data.typeIndex != 8 && tmpnode.data.typeIndex != 6) {//start item should not be COPIED, as we should have only ONE start item in each flowchart
                    nodelist.push(tmpnode.data.id);
                    ClipboarIDs.push(tmpnode.data.typeIndex);
                    if (ClipboardOffsets.length == 0) {
                        ClipboardOffsets.push({ x: 0, y: 0 });
                        firstX = tmpnode.data.x;
                        firstY = tmpnode.data.y;
                    } else {
                        ClipboardOffsets.push({ x: tmpnode.data.x - firstX, y: tmpnode.data.y - firstY });
                    }
                }
            }
        } //end nodes
        var connlist = new Array();
        //connections
        try {
            for (var i = 0; i < $scope.chartViewModel.connections.length; i++) {
                if ($scope.chartViewModel.connections[i].visible) {
                    var conn = $scope.chartViewModel.connections[i];
                    var source_found = false;
                    var dest_found = false;

                    for (var j = 0; j < nodelist.length; j++) {
                        if (conn.data.source.nodeID == nodelist[j])
                            source_found = true;
                        if (conn.data.dest.nodeID == nodelist[j])
                            dest_found = true;
                    }
                    if (source_found && dest_found) {
                        var srcIndex;
                        var destIndex;

                        for (var j = 0; j < nodelist.length; j++) {
                            if (nodelist[j] == conn.data.source.nodeID) {
                                srcIndex = j;
                                break;
                            }
                        }

                        for (var j = 0; j < nodelist.length; j++) {
                            if (nodelist[j] == conn.data.dest.nodeID) {
                                destIndex = j;
                                break;
                            }
                        }

                        var connobject = {
                            sourceNodeIndex: srcIndex,
                            destNodeIndex: destIndex,
                            ix: conn.inputx,
                            iy: conn.inputy,
                            o1x: conn.ox1,
                            o2x: conn.ox2,
                            o1y: conn.oy1,
                            o2y: conn.oy2,
                            _cap: conn._caption,
                            onumber: conn.sourceOutputID,
                            inputdir: conn.inputDir,
                            outputdir: conn.outputDir
                        };
                        ClipboardConnection.push(connobject);
                    }
                }
            }
        } catch (ex) {
            //alert(ex.message);
        }
        //end connections
    };

    Array.prototype.move = function (old_index, new_index) {
        if (new_index >= this.length) {
            var k = new_index - this.length;
            while ((k--) + 1) {
                this.push(undefined);
            }
        }
        this.splice(new_index, 0, this.splice(old_index, 1)[0]);
        return this; // for testing purposes
    };

    var PasteInvalid = false;
    var numValidPastes = 0;
    var PasteArrayReplaceCount = 0;
    $scope.ApplyKey = function (evt) {
        if (divXamlBack.style.display == "block" || divXamlBack.style.display == "")
            return;

        if ($scope.chartViewModel.Lock && !ForeachActive)
            return;
        if (currentEditAssignNode1 != -1) {
            $scope.chartViewModel.GetNodeById(currentEditAssignNode1).data.AssignOperator1 = evt.target.value;
        }
        if (currentEditAssignNode2 != -1) {
            $scope.chartViewModel.GetNodeById(currentEditAssignNode2).data.AssignOperator2 = evt.target.value;
        }
        if (currentEditIfCondition != -1) {
            $scope.chartViewModel.GetNodeById(currentEditIfCondition).data.condition = evt.target.value;
        }

        if (evt.keyCode == 27) {
            try {
                //$scope.chartViewModel.mouseUp();
                //LineSwitchConnectionIndex = -1;
                //SelectedPointIndex = -1;
                //SelectedConnectionIndex = -1;
                //$scope.chartViewModel.deselectAll();
                //$scope.chartViewModel.GhostPath = "";
                //LineSwitchMode = "";

                //GhostPathSrc = { x: -1000, y: -1000 };

                //
                GhostPathSrc = { x: -1000, y: -1000 };
                $scope.chartViewModel.GhostPath = "";
                LineSwitchMode = "";
                SelectedPointIndex = -1;
                ResizeOperationActive = false;
                ChainedPointIndex = -1;

                $scope.chartViewModel.connections[LineSwitchConnectionIndex].PathPointArray[0] = $scope.chartViewModel.connections[LineSwitchConnectionIndex].OriginalFirstPoint;
                $scope.chartViewModel.connections[LineSwitchConnectionIndex].PathPointArray[$scope.chartViewModel.connections[LineSwitchConnectionIndex].PathPointArray.length - 1] =
                    $scope.chartViewModel.connections[LineSwitchConnectionIndex].OriginalLastPoint;
                LineSwitchConnectionIndex = -1;
                //var cn = $scope.chartViewModel.connections[SelectedConnectionIndex];
                //console.log(tmpCorrectPathPoint);
                //cn.PathPointArray = tmpCorrectPathPoint;
                tmpCorrectPathPoint = null;
                SelectedConnectionIndex = -1;
                //

                for (var k = 0; k < moveSprites.length; k++) {
                    divDrop.removeChild(moveSprites[k]);
                }
                moveSprites.length = 0;
                moveDeltaX = 0;
                moveDeltaY = 0;
                spriteCancel = true;
            }
            catch (ex) {
                //alert(ex.message);
            }
        }

        var currentnodeindex = $scope.chartViewModel.GetSelectedNodeIndex();
        if (evt.ctrlKey) {
            if (evt.keyCode == 69) //E-->XML 3
            {
                $scope.MakeXML3();
            }
            else if (evt.keyCode == 65) { //select ALL
                for (var i = 0; i < $scope.chartViewModel.GetNumNodes() ; i++) {//loop through all nodes
                    var tmpnode = $scope.chartViewModel.GetNode(i);
                    if (tmpnode.data.visible && tmpnode.data.typeIndex != 6) {//select all visible nodes (except flowchart body)
                        tmpnode.select();
                    }
                }
            } else if (evt.keyCode == 86) { //PASTE//ForeachActive
                PasteInvalid = false;
                numValidPastes = 0;
                PasteArrayReplaceCount = 0;
                ClipboardConnectionOffset++;
                for (var i = 0; i < ClipboarIDs.length; i++) {
                    if (PasteInvalid) {
                        return;
                    }
                    $scope.TempInsert("paste", ClipboarIDs[i], i);
                }
                var pasteParent = new Array();
                var nodeindices = new Array();
                var pasteIndex = new Array();
                var pasteX = new Array();
                var pasteY = new Array();
                for (var i = 0 ; i < ClipboarIDs.length - parseInt(numValidPastes); i++) {//if there has been any invalid pastes, consider it so that our next operations are based on correct stats
                    ClipboarIDs.pop();
                }
                for (var k = $scope.chartViewModel.GetNumNodes() - ClipboarIDs.length; k < $scope.chartViewModel.GetNumNodes(); k++) {
                    nodeindices.push($scope.chartViewModel.GetNode(k).data.GUID);
                    pasteIndex.push($scope.chartViewModel.GetNode(k).data.typeIndex);
                    pasteParent.push($scope.chartViewModel.GetNode(k).data.parent);
                    pasteX.push($scope.chartViewModel.GetNode(k).data.x);
                    pasteY.push($scope.chartViewModel.GetNode(k).data.y);
                }
                var connectionInfo = [];

                for (var i = 0; i < ClipboardConnection.length; i++) {
                    try {
                        var conn = ClipboardConnection[i];
                        var A_index;
                        var B_index;

                        for (var j = 0; j < $scope.chartViewModel.GetNumNodes(); j++) {
                            if ($scope.chartViewModel.GetNode(j).data.id == conn.sourceNodeID) {
                                A_index = j;
                            }
                            if ($scope.chartViewModel.GetNode(j).data.id == conn.destNodeID) {
                                B_index = j;
                            }
                        }
                        var totalNumNodes = $scope.chartViewModel.GetNumNodes();
                        var source = $scope.chartViewModel.GetNode((totalNumNodes - ClipboarIDs.length) + conn.sourceNodeIndex);
                        var dest = $scope.chartViewModel.GetNode((totalNumNodes - ClipboarIDs.length) + conn.destNodeIndex);
                        if (source.data.numChild < source.data.outputcount) {//is the source node eligible to have new connections?
                            source.data.connected = true;
                            dest.data.connected = true;
                            source.data.numChild++;
                            var tmpConnInfo = {
                                srcIndex: conn.sourceNodeIndex,
                                destIndex: conn.destNodeIndex,
                                ix: conn.ix,
                                iy: conn.iy,
                                o1x: conn.o1x,
                                o2x: conn.o2x,
                                o1y: conn.o1y,
                                o2y: conn.o2y,
                                _cap: conn._cap,
                                onumber: conn.onumber,
                                inputdir: conn.inputdir,
                                outputdir: conn.outputdir
                            };
                            connectionInfo.push(tmpConnInfo);
                            $scope.chartViewModel.createNewConnection(source.outputConnectors[source.data.numChild - 1],
                                dest.inputConnectors[0], false,
                                conn.ix,
                                conn.iy,
                                conn.o1x, conn.o1y, conn.o2x, conn.o2y, conn.onumber, conn._cap, "", conn.inputdir, conn.outputdir, $scope.chartViewModel);
                        }
                    } catch (ex) {
                        //alert(ex.message);
                    }
                }
                //end paste connections

                var UndoObject = {
                    action: "paste",
                    nodeindices: nodeindices,
                    pasteParent: pasteParent,
                    pasteIndex: pasteIndex,
                    pasteX: pasteX,
                    pasteY: pasteY,
                    connectionInfo: connectionInfo
                };
                $scope.chartViewModel.AddToUndoList(UndoObject);
                if (PasteArrayReplaceCount != 0) {
                    UndoList.move(UndoList.length - 1, UndoList.length - (1 + PasteArrayReplaceCount));
                }
            } else if (evt.keyCode == 90) //UNDO
            {
                $scope.Undo();
            } else if (evt.keyCode == 89) { //REDO
                $scope.Redo();
            }
        }

        var selectedobjectarray = new Array();
        selectedobjectarray.length = 0;
        for (var i = 0; i < $scope.chartViewModel.GetNumNodes(); i++) {
            if ($scope.chartViewModel.GetNode(i).data.visible && $scope.chartViewModel.GetNode(i).selected()) {
                selectedobjectarray.push(i);
            }
        }

        if (evt.keyCode == 46) {
            $scope.chartViewModel.deleteSelected();
            $scope.chartViewModel.FindMinWidthHeight();
        }

        if (currentnodeindex != -1) {
            var GUIDArray = new Array();
            var xArray = new Array();
            var yArray = new Array();
            for (var j = 0; j < $scope.chartViewModel.getSelectedNodes().length; j++) {
                GUIDArray.push($scope.chartViewModel.getSelectedNodes()[j].data.GUID);
                xArray.push($scope.chartViewModel.getSelectedNodes()[j].data.x);
                yArray.push($scope.chartViewModel.getSelectedNodes()[j].data.y);
            }

            //nodemove
            if ((evt.keyCode == 39 || evt.keyCode == 37 || (evt.keyCode == 38) || (evt.keyCode == 40)) && !ForeachActive) {
                var moveNode = $scope.chartViewModel.GetNode(selectedobjectarray[0]);
                var UndoObject = {
                    action: "nodemove",
                    id: moveNode.data.id,
                    x: moveNode.data.x,
                    y: moveNode.data.y,
                    xArray: xArray,
                    yArray: yArray,
                    GUIDArray: GUIDArray,
                    flwWidthBefore: divDrop.style.width,
                    flwHeightBefore: divDrop.style.height
                };
                $scope.chartViewModel.AddToUndoList(UndoObject);
            }

            if (evt.keyCode == 39 && !ForeachActive) { //right
                for (i = 0; i < selectedobjectarray.length; i++) {
                    $scope.chartViewModel.ChangePosition(selectedobjectarray[i], snapX, 0);
                }
            } else if (evt.keyCode == 37 && !ForeachActive) { //left
                //check all selected objects and allow change position only if none of them goes boyend the borders
                var validmode = true;
                for (var j = 0; j < selectedobjectarray.length; j++) {
                    if (parseInt($scope.chartViewModel.GetNodeX(selectedobjectarray[j])) - parseInt(snapX) < 0) {
                        validmode = false;
                    }
                }
                if (validmode) {
                    for (i = 0; i < selectedobjectarray.length; i++) {
                        $scope.chartViewModel.ChangePosition(selectedobjectarray[i], -1 * snapX, 0);
                    }
                }
            } else if (evt.keyCode == 38 && !ForeachActive) { //up arrow
                var validmode = true;
                for (var j = 0; j < selectedobjectarray.length; j++) {
                    var extra = 15;
                    if ($scope.chartViewModel.GetNode(selectedobjectarray[j]).data.typeIndex == 8)
                        extra = 30;
                    if (parseInt($scope.chartViewModel.GetNodeY(selectedobjectarray[j])) - parseInt(snapY) < extra) {
                        validmode = false;
                    }
                }
                if (validmode) {
                    for (i = 0; i < selectedobjectarray.length; i++) {
                        $scope.chartViewModel.ChangePosition(selectedobjectarray[i], 0, -1 * snapY);
                    }
                }
            } else if (evt.keyCode == 40 && !ForeachActive) { //down
                for (i = 0; i < selectedobjectarray.length; i++) {
                    $scope.chartViewModel.ChangePosition(selectedobjectarray[i], 0, snapY);
                }
            } else if (evt.keyCode == 46) {
                $scope.chartViewModel.FindMinWidthHeight();
                $scope.chartViewModel.deleteSelected();
            } else if (evt.keyCode == 9) //TAB
            {
                $scope.chartViewModel.deselectAll();
                //switch between nodes
                var visible_objects = new Array();

                for (var i = 0; i < $scope.chartViewModel.GetNumNodes(); i++) {
                    if ($scope.chartViewModel.GetNodeVisible(i)) {
                        visible_objects.push(i);
                    }
                }
                var current_selected;
                for (var i = 0; i < visible_objects.length; i++) {
                    if (currentnodeindex == visible_objects[i]) {
                        current_selected = i;
                    }
                }

                var next_selected;
                if (current_selected < visible_objects.length - 1) {
                    next_selected = current_selected + 1;
                } else {
                    next_selected = 0;
                }
                try {
                    var n = $scope.chartViewModel.GetNode(visible_objects[next_selected]); //.data._selected = true;
                    n.select();
                } catch (ex) {
                    //alert(ex.message + " , " + ex.number);
                }
            }

            if (evt.ctrlKey) {
                if (evt.keyCode == 67) { //COPY
                    $scope.CopyItems();
                } else if (evt.keyCode == 88) //CUT
                {
                    $scope.CopyItems();
                    $scope.chartViewModel.deleteSelected();
                }
            }

            var bottomY = -10000;
            var rightX = -10000;
            //increase flowchart size if the moving objects exceeds flowchart bounds
            for (var k = 0; k < $scope.chartViewModel.GetNumNodes(); k++) { //nodes
                if ($scope.chartViewModel.GetNode(k).data.y > bottomY && $scope.chartViewModel.GetNodeVisible(k) && $scope.chartViewModel.GetNode(k).data.typeIndex != 6) {
                    bottomY = $scope.chartViewModel.GetNode(k).data.y;
                }

                if ($scope.chartViewModel.GetNode(k).data.x > rightX && $scope.chartViewModel.GetNodeVisible(k) && $scope.chartViewModel.GetNode(k).data.typeIndex != 6) {
                    rightX = $scope.chartViewModel.GetNode(k).data.x;
                }
            }

            var bottomPathY = -10000;
            var rightPathX = -10000;
            for (var k = 0; k < $scope.chartViewModel.connections.length; k++) //connections
            {
                var connection = $scope.chartViewModel.connections[k];
                if (connection.visible()) {
                    var path = connection.PathPointArray;
                    for (var j = 0; j < path.length; j++) {
                        if (parseInt(path[j].y) > bottomPathY) {
                            bottomPathY = path[j].y;
                        }
                        if (parseInt(path[j].x) > rightPathX) {
                            rightPathX = path[j].x;
                        }
                    }
                }
            }

            var sw = $scope.chartViewModel.GetNodeWidth(currentnodeindex);
            var sh = $scope.chartViewModel.GetNodeHeight(currentnodeindex);
            if (bottomPathY > bottomY) {
                bottomY = bottomPathY;
                sh = snapY;
            }

            if (rightPathX > rightX) {
                rightX = rightPathX;
                sw = snapX;
            }
            if (rightPathX > minWidth) {
                minWidth = rightPathX;
            }
            if (evt.keyCode == 39) { //right
                minWidth += 30;
            }
            var realheight = parseInt(divDrop.style.height.replace("px", ""));
            var realwidth = parseInt(divDrop.style.width.replace("px", ""));
            if (minHeight + 40 > realheight) {
                divSettings.style.display = "none";
                divAddIcon.style.display = "none";
                divAddDialog.style.display = "none";
                if (minHeight <= maxFlowchartHeight + initialHeight) {
                    var h = minHeight + 40;
                    divDrop.style.height = h + "px";
                    divDrop.style.height = (h) + "px";
                    $("#divChart").height((h));
                    var svg = document.getElementsByTagName("svg");
                    svg[0].setAttribute("height", h);
                    svg[0].style.height = (h) + "px";
                }
            }
            $scope.ZoomViewOnItem();

            if (minWidth > realwidth) {
                divSettings.style.display = "none";
                divAddIcon.style.display = "none";
                divAddDialog.style.display = "none";
                if (minWidth <= maxFlowchartWidth + initialWidth) {
                    divDrop.style.width = minWidth + "px";
                    var w = minWidth;
                    $("#divChart").width((w));
                    $("#divFlowChartParent").width((w));
                    $("#divFlowChartParentTitle").width((w));
                    var svg = document.getElementsByTagName("svg");
                    svg[0].setAttribute("width", w);
                    svg[0].style.width = (w) + "px";
                }
                CenterDrop();
            }
            $scope.ZoomViewOnItem();

            UndoList[UndoList.length - 1].flwWidthAfter = divDrop.style.width;
            UndoList[UndoList.length - 1].flwHeightAfter = divDrop.style.height;
        }
    };

    $scope.ZoomViewOnItem = function() {
        //check all 4 corners of item, and change scroll if any corner is out of current view.
        //which corner is out of view? scroll container based on invisible corner(s)
        var left = 0;
        var right = 0;
        var top = 0;
        var bottom = 0;
        var leftarray = [];
        var toparray = [];
        //find bounding box coordinates sorrounding all selected items
        for (var i = 0; i < $scope.chartViewModel.nodes.length; ++i) {
            var node = $scope.chartViewModel.nodes[i];
            if (node.selected() && node.visible()) {
                {
                    leftarray.push(node.data.x);
                    toparray.push(node.data.y);
                }
            }
        }

        var minX = 1000000;
        var minY = 1000000;
        var maxX = -1000000;
        var maxY = -1000000;
        for (var i = 0; i < leftarray.length; i++) {
            if (parseInt(leftarray[i]) < parseInt(minX)) {
                minX = parseInt(leftarray[i]);
            }
            if (parseInt(toparray[i]) < parseInt(minY)) {
                minY = parseInt(toparray[i]);
            }
            if (parseInt(toparray[i]) + 150 > parseInt(maxY)) {
                maxY = parseInt(toparray[i]) + 150;
            }
            if (parseInt(leftarray[i]) + 120 > parseInt(maxX)) {
                maxX = parseInt(leftarray[i]) + 120;
            }
        }

        left = minX;
        top = minY;
        right = maxX;
        bottom = maxY;

        var screentop = divContainer.scrollTop - 50;
        var screenbottom = screentop + (window.innerHeight - 30 - 25);
        var screenleft = divContainer.scrollLeft;
        var screenright = screenleft + (window.innerWidth - 100);
        if (bottom > screenbottom) {
            divContainer.scrollTop += snapY;
        } else if (top < screentop) {
            divContainer.scrollTop -= snapY;
        }
        if (right > screenright) {
            divContainer.scrollLeft += snapX;
        } else if (left < screenleft) {
            divContainer.scrollLeft -= snapX;
        }
    };

    $scope.RoundNumbers = function (number) {
        var result = number;
        var x = number % 10;
        if (x <= 5) {
            result = number - x;
        }
        else {
            result = number + (10 - x);
        }
        return result;
    };

    $scope.TempInsert = function (mode, pasteindex, copyindex) {
        $scope.chartViewModel.VoidDrag();
        if (noFlowchartMode)
            return;
        var x = dropX + divContainer.scrollLeft;
        var y = dropY + divContainer.scrollTop;
        if (touchPasteMode)
            mode = "paste";
        if (mode == "paste") {
            if (touchPasteMode) {
                try
                {

                    for (var i = 0; i < ClipboarIDs.length; i++) {
                        //if (PasteInvalid) {
                        //    return;
                        //}
                        //$scope.TempInsert("paste", ClipboarIDs[i], i);
                        pasteindex = ClipboarIDs[i];
                    }
                    copyindex = 0;


                    x = parseInt(mouseX) + parseInt(ClipboardOffsets[copyindex].x) + parseInt(divContainer.scrollLeft);
                    y = parseInt(mouseY) + parseInt(ClipboardOffsets[copyindex].y) + parseInt(divContainer.scrollTop);
                    if (x < 0)
                        x = 20;
                    if (y < 0)
                        y = 30;
                    touchPasteMode = false;
                }
                catch (ex) {
                    //alert(ex.message);
                }
            }
            else {
                x = parseInt(mouseX) + parseInt(ClipboardOffsets[copyindex].x) + parseInt(divContainer.scrollLeft);
                y = parseInt(mouseY) + parseInt(ClipboardOffsets[copyindex].y) + parseInt(divContainer.scrollTop);
                if (x < 0)
                    x = 20;
                if (y < 0)
                    y = 30;
            }
        }

        try {
            var parent = "";
            for (var k = 0; k < $scope.chartViewModel.nodes.length ; k++) {//loop through all nodes and find node that may be host (parent) of our newly created node
                var tnd = $scope.chartViewModel.GetNodeById($scope.chartViewModel.nodes[k].data.id);
                if (x >= parseInt(tnd.data.x) && x <= parseInt(tnd.data.x) + parseInt(tnd.data.shapeWidth)
                    && y >= parseInt(tnd.data.y) && y <= parseInt(tnd.data.y) + parseInt(tnd.data.shapeHeight) &&
                    tnd.data.typeIndex != 6 && tnd.data.visible) {
                    parent = tnd.data.id;
                }
            }

            var node;
            var bigInserted = false;
            var nodex;
            var nodey;
            var connected = false;

            if (mode != "paste") {
                if (dropIndex == -1) {
                    return;
                }
            }

            var index = dropIndex;

            dropIndex = -1;
            if (mode == "paste") {
                index = pasteindex;
            }

            //check maximum number of children for a FOREACH node (currently 1)
            if (ForeachActive) {
                //find number of current ForEach node
                var numForeachChildren = 0;
                var foreachFirstChildType = null;
                for (var n = 0 ; n < $scope.chartViewModel.nodes.length; n++) {
                    if ($scope.chartViewModel.nodes[n].data.rootNodeIDForeach == SelectedForEachNode.data.id) {
                        if (foreachFirstChildType == null) {
                            foreachFirstChildType = $scope.chartViewModel.nodes[n].data.typeIndex;
                        }
                        numForeachChildren++;
                    }
                }
                if (numForeachChildren == 1 && foreachFirstChildType != 1) {
                    return;
                }
            }

            if (parent.toString() == "") {
                var finalX = 0;
                var finalY = 0;
                for (var i = 0; i < 1000; i++) {
                    if (x >= i * snapX && x < (i + 1) * snapX) {
                        finalX = i * snapX;
                    }
                }
                for (var i = 2; i < 1000; i++) {
                    if (y >= i * snapY && y < (i + 1) * snapY) {
                        finalY = i * snapY;
                    }
                }

                nodex = finalX;
                nodey = finalY - (snapY / 2);
                numValidPastes++;
            } else {

                node = $scope.chartViewModel.GetNodeById(parent);
                node.data.connected = true;
                connected = true;

                if (node.data.numChild >= node.data.outputcount) {
                    PasteInvalid = true;
                    return;
                }
                else {
                    numValidPastes++;
                }

                if (node.data.numChild >= node.data.outputConnectors.length)//check maximum number of allowed children for every node
                    return;

                node.data.numChild++;

                var b = 150;
                if (node.data.outputcount == 2) {
                    b = 0;
                }
                if (node.data.numChild == 1) {
                    nodex = node.x() + (-150) + (b * node.data.numChild);
                } else {
                    nodex = node.x() + (150) + (b * node.data.numChild);
                }

                if (node.data.typeIndex == 1) //IF node
                {
                    if (!node.outputConnectors[0].data.connected) {
                        nodex = node.x() + (-90) - parseInt($scope.chartViewModel.GetShapeWidthList(index - 1));
                    } else {
                        nodex = node.x() + (90) + parseInt(node.data.shapeWidth);
                    }

                    nodey = parseInt(node.y()) - ((parseInt($scope.chartViewModel.GetShapeHeightList(index - 1)) - parseInt(node.data.shapeHeight)) / 2);
                }

                if (node.data.typeIndex != 1) {
                    nodey = parseInt(node.y()) + 150;
                }

                if (node.data.typeIndex != 1) { //parent item is NOT a FLOWDECISION (IF)
                    nodey = parseInt(node.y()) + parseInt(node.data.shapeHeight) + 70;
                    nodex = parseInt(node.x()) - ((parseInt($scope.chartViewModel.GetShapeWidthList(index - 1)) - parseInt(node.data.shapeWidth)) / 2);
                }

                if (nodex < 20)
                    nodex = 20;
            }

            var sw = $scope.chartViewModel.GetShapeWidthList(index - 1);
            var sh = $scope.chartViewModel.GetShapeHeightList(index - 1);

            if (index - 1 == 5 && $scope.chartViewModel.GetNumNodes() == 0) {
                sw = initialWidth;
                sh = initialHeight;
            }

            if (index - 1 == 6 || index - 1 == 5) {
                currentActivityNumber++;
            }
            var caps = "";
            try
            {
                caps = $scope.chartViewModel.GetCaptionList(index).split('_');
            }
            catch (ex) {

            }
            if (dropIndex != 6 && $scope.chartViewModel.GetNumNodes() == 0) {
                nodex = nodey = 0;
            }

            if ($scope.chartViewModel.GetNumNodes() == 0) //program init?
            {
                divDrop.style.backgroundColor = "white";
                commonActivityService.ChangeFlowchartIndex();
                document.getElementsByClassName("ActivityItem")[0].style.backgroundColor = "white";
                program_init = true;
                if (index != 6) {
                    noFlowchartMode = true;
                }
            }

            if (ForeachActive) {
                lblForEachLabel.style.display = "none";
                nodex = (600 - parseInt(sw)) / 2;
                nodey = (600 - parseInt(sh)) / 2;
            }

            $scope.chartViewModel.CreateNewNode($scope.chartViewModel.GetNumNodes(), nodex, nodey, parent, sw, sh, index, connected, nextGUID);
            if (ForeachActive) {
                divDrop.style.backgroundColor = "white";
                document.getElementsByClassName("ActivityItem")[0].style.backgroundColor = "white";
                if (SelectedForEachNode != null) {
                    $scope.chartViewModel.nodes[$scope.chartViewModel.nodes.length - 1].data.rootNodeIDForeach = SelectedForEachNode.data.id;
                }
            }

            $scope.chartViewModel.nodes[$scope.chartViewModel.nodes.length - 1].SetConnctorData(dropConnectorData);

            $scope.chartViewModel.FindMinWidthHeight();
            //check minimum height after new node has been added, and increase flowchart height if new item is out of the flowchart
            var realheight = parseInt(divDrop.style.height.replace("px", ""));
            var realwidth = parseInt(divDrop.style.width.replace("px", ""));
            if (minHeight + 40 > realheight && !ForeachActive) {
                divSettings.style.display = "none";
                divAddIcon.style.display = "none";
                divAddDialog.style.display = "none";
                if (minHeight <= maxFlowchartHeight + initialHeight) {
                    var h = minHeight + 40;
                    divDrop.style.height = h + "px";
                    divDrop.style.height = (h) + "px";
                    $("#divChart").height((h));
                    var svg = document.getElementsByTagName("svg");
                    svg[0].setAttribute("height", h);
                    svg[0].style.height = (h) + "px";
                }
            }
            //

            nextGUID++;
            close_mode = false;
            close_mode_add = false;

            if (program_init) {
                try {
                    if (mode != "paste") {
                        var tnode = $scope.chartViewModel.nodes[$scope.chartViewModel.nodes.length - 1];

                        var UndoObject = {
                            action: "addnode",
                            id: tnode.data.id,
                            typeIndex: index,
                            x: nodex,
                            y: nodey,
                            GUID: nextGUID - 1,
                            flwWidthBefore: divDrop.style.width,
                            flwHeightBefore: divDrop.style.height
                        };
                        RedoList.length = 0;
                        $scope.chartViewModel.AddToUndoList(UndoObject);
                    }
                } catch (ex) {
                    //alert(ex.message + " , " + ex.number);
                }
            }

            try {
                if (parent.toString() != "") {
                    var inputDir = "up";
                    var outputDir = "down";
                    var destIndex = 2;
                    var srcIndex = 0;

                    var mycaption = node.outputConnectors[node.data.numChild - 1].caption();
                    var outy = 150;
                    if (node.data.typeIndex == 8)
                        outy = 120;
                    var o1 = $scope.OutputConnectorPosition1(node.data.typeIndex);
                    var o2 = $scope.OutputConnectorPosition2(node.data.typeIndex);
                    var _cap = "";
                    var first_empty_child_index = node.data.numChild;
                    if (node.data.typeIndex == 1) { //it is IF node? so find the FIRST empty output connector.
                        if (!node.outputConnectors[0].data.connected) {
                            first_empty_child_index = 1;
                        } else {
                            first_empty_child_index = 2;
                        }
                    }

                    var outputPoint;
                    if (first_empty_child_index == 1) {
                        outputPoint = { x: o1.x, y: o1.y, dir: o1.dir };
                    } else {
                        outputPoint = { x: o2.x, y: o2.y, dir: o2.dir };
                    }

                    var inputPoint = $scope.InputConnectorPosition($scope.chartViewModel.GetNode($scope.chartViewModel.GetNumNodes() - 1).data.typeIndex,
                        { x: parseInt(node.data.x) + parseInt(outputPoint.x), y: parseInt(node.data.y) + parseInt(outputPoint.y) },
                        $scope.chartViewModel.GetNode($scope.chartViewModel.GetNumNodes() - 1).data.id);

                    //
                    var newlyCreatedNode = $scope.chartViewModel.GetNode($scope.chartViewModel.GetNumNodes() - 1);
                    var outputCurrentConnector = node.data.numChild;
                    if (newlyCreatedNode.data.typeIndex == 1)//destination item is a FlowDecision
                    {
                        srcIndex = 1;
                    }
                    inputDir = "up";
                    outputDir = "down";
                    if (newlyCreatedNode.data.typeIndex != 1) {//new item is NOT a FLOWDECISION (IF)
                        nodey = node.y() + (parseInt(node.data.shapeHeight)) + 70;
                        nodex = parseInt(node.x()) - ((parseInt(newlyCreatedNode.data.shapeWidth) - parseInt(node.data.shapeWidth)) / 2);
                    }
                    if (node.data.typeIndex == 1) {//parent item is a FLOWDECISION (IF)
                        if (node.data.numChild == 1 || (node.data.outputConnectors[1].connected && node.data.numChild == 2)) {
                            destIndex = 4;
                            inputDir = "up";
                            outputDir = "left";
                            srcIndex = 0;
                            nodex = node.x() - (parseInt(newlyCreatedNode.data.shapeWidth)) - 135;
                            nodey = parseInt(node.y()) - ((parseInt(newlyCreatedNode.data.shapeHeight) - parseInt(node.data.shapeHeight)) / 2) + 120;
                            outputCurrentConnector = 1;
                            if (newlyCreatedNode.data.typeIndex == 1) {//IF-IF
                                inputDir = "up";
                                outputDir = "left";
                                srcIndex = 1;
                                nodey = parseInt(node.y()) + 120;
                            }
                        }
                        else {
                            destIndex = 5;
                            inputDir = "up";
                            outputDir = "right";
                            srcIndex = 0;
                            nodex = parseInt(node.x()) + (parseInt(node.data.shapeWidth)) + 135;
                            nodey = parseInt(node.y()) - ((parseInt(newlyCreatedNode.data.shapeHeight) - parseInt(node.data.shapeHeight)) / 2) + 120;
                            outputCurrentConnector = 2;
                            if (newlyCreatedNode.data.typeIndex == 1) {//IF-IF
                                inputDir = "up";
                                outputDir = "right";
                                srcIndex = 1;
                                nodey = parseInt(node.y()) + 120;
                            }
                        }
                    }

                    var tmpOutputPos;
                    var tmpInputPos;
                    for (var i = 0 ; i < connectorPositions.length; i++) {
                        if (connectorPositions[i].typeIndex == newlyCreatedNode.data.typeIndex && connectorPositions[i].index == srcIndex) {
                            tmpInputPos = { x: parseInt(connectorPositions[i].x), y: parseInt(connectorPositions[i].y) };
                        }
                    }

                    for (var i = 0 ; i < connectorPositions.length; i++) {
                        if (connectorPositions[i].typeIndex == node.data.typeIndex && connectorPositions[i].index == destIndex) {
                            tmpOutputPos = { x: parseInt(connectorPositions[i].x), y: parseInt(connectorPositions[i].y) };
                        }
                    }

                    $scope.chartViewModel.createNewConnection(node.outputConnectors[node.data.numChild - 1],
                        $scope.chartViewModel.GetNode($scope.chartViewModel.GetNumNodes() - 1).inputConnectors[0], false,
                        tmpInputPos.x, tmpInputPos.y, tmpOutputPos.x, tmpOutputPos.y, tmpOutputPos.x, tmpOutputPos.y, outputCurrentConnector, "", "",
                        inputDir, outputDir, $scope.chartViewModel);
                    if (nodex < 20)
                        nodex = 20;
                    newlyCreatedNode.data.x = $scope.RoundNumbers(nodex);
                    newlyCreatedNode.data.y = $scope.RoundNumbers(nodey);

                    $scope.chartViewModel.SetConnectionCaption($scope.chartViewModel.connections[$scope.chartViewModel.connections.length - 1], mycaption);

                    var realheight = parseInt(divDrop.style.height.replace("px", ""));
                    var realwidth = parseInt(divDrop.style.width.replace("px", ""));
                    $scope.chartViewModel.FindMinWidthHeight();

                    if (minHeight + 40 > parseInt(realheight)) {
                        divSettings.style.display = "none";
                        divAddIcon.style.display = "none";
                        divAddDialog.style.display = "none";

                        var h = (minHeight + 40);
                        if (h <= maxFlowchartHeight + initialHeight) {
                            divDrop.style.height = (h) + "px";
                            $("#divChart").height((h));
                            var svg = document.getElementsByTagName("svg");
                            svg[0].setAttribute("height", h);
                            svg[0].style.height = (h) + "px";
                        }
                    }

                    if (minWidth > realwidth) {
                        divSettings.style.display = "none";
                        divAddIcon.style.display = "none";
                        divAddDialog.style.display = "none";
                        divDrop.style.width = (minWidth) + "px";
                        var w = minWidth;
                        $("#divChart").width((w));
                        $("#divFlowChartParent").width((w));
                        $("#divFlowChartParentTitle").width((w));
                        var svg = document.getElementsByTagName("svg");
                        svg[0].setAttribute("width", w);
                        svg[0].style.width = (w) + "px";
                        CenterDrop();
                    }
                    PasteArrayReplaceCount++;
                    var UndoObject = {
                        action: "addconnection", sourceNodeID: $scope.chartViewModel.connections[$scope.chartViewModel.connections.length - 1].data.source.nodeID, x: 0, y: 0,
                        id: $scope.chartViewModel.connections[$scope.chartViewModel.connections.length - 1].data.source.nodeID, srcNodeGUID: node.data.GUID, dstNodeGUID: newlyCreatedNode.data.GUID,
                        destNodeID: $scope.chartViewModel.connections[$scope.chartViewModel.connections.length - 1].data.dest.nodeID, ix: tmpInputPos.x, iy: tmpInputPos.y,
                        o1x: tmpOutputPos.x, o2x: tmpOutputPos.x, o1y: tmpOutputPos.y, o2y: tmpOutputPos.y, onumber: outputCurrentConnector, inputDir: inputDir, outputDir: outputDir
                    };
                    $scope.chartViewModel.AddToUndoList(UndoObject);
                    RedoList.length = 0;

                    UndoList[UndoList.length - 1].flwWidthAfter = divDrop.style.width;
                    UndoList[UndoList.length - 1].flwHeightAfter = divDrop.style.height;

                }
            } catch (ex) {
                //alert(ex.message + " , " + ex.number);
            }

            try {
                if (parseInt(divDrop.style.width.replace("px", "")) >= initialWidth) {
                    //object inserted near the border? englarge screen to cover the newly inserted object

                    var bottomY = -10000;
                    var rightX = -10000;
                    for (var k = 0; k < $scope.chartViewModel.GetNumNodes(); k++) {

                        if ($scope.chartViewModel.GetNode(k).data.y > bottomY && $scope.chartViewModel.GetNodeVisible(k) && $scope.chartViewModel.GetNode(k).data.typeIndex != 6) {
                            bottomY = $scope.chartViewModel.GetNode(k).data.y;
                        }

                        if ($scope.chartViewModel.GetNode(k).data.x > rightX && $scope.chartViewModel.GetNodeVisible(k) && $scope.chartViewModel.GetNode(k).data.typeIndex != 6) {
                            rightX = $scope.chartViewModel.GetNode(k).data.x;
                        }
                    }

                    var realheight = parseInt(divDrop.style.height.replace("px", ""));
                    var realwidth = parseInt(divDrop.style.width.replace("px", ""));
                    $scope.chartViewModel.FindMinWidthHeight();
                    if (minHeight > realheight) {
                        divSettings.style.display = "none";
                        divAddIcon.style.display = "none";
                        divAddDialog.style.display = "none";
                        divDrop.style.height = (realheight) + "px";
                        var h = realheight;
                        divDrop.style.height = (h) + "px";
                        $("#divChart").height((h));
                        var svg = document.getElementsByTagName("svg");
                        svg[0].setAttribute("height", h);
                        svg[0].style.height = (h) + "px";
                    }

                    if (minWidth > realwidth) {
                        divSettings.style.display = "none";
                        divAddIcon.style.display = "none";
                        divAddDialog.style.display = "none";
                        divDrop.style.width = (minWidth) + "px";
                        var w = minWidth;
                        $("#divChart").width((w));
                        $("#divFlowChartParent").width((w));
                        $("#divFlowChartParentTitle").width((w));
                        var svg = document.getElementsByTagName("svg");
                        svg[0].setAttribute("width", w);
                        svg[0].style.width = (w) + "px";
                        CenterDrop();
                    }

                    UndoList[UndoList.length - 1].flwWidthAfter = divDrop.style.width;
                    UndoList[UndoList.length - 1].flwHeightAfter = divDrop.style.height;
                    //
                    return;
                }
                var w = 100;
                var h = 150;
                if (index - 1 == 5) {
                    w = initialWidth;
                    h = initialHeight;
                    bigMode = true;
                    imgResize.style.display = "";
                    divTools.style.display = "";
                    //btnToXAML.style.display = "";
                    btnToCSharp.style.display = "";
                    //btnFromCSharp.style.display = "";
                    btnSave.style.display = "";
                    btnVariables.style.display = btnArguments.style.display = "";
                    btnImports.style.display = "";

                } else {
                    sh -= 60;
                    divDrop.style.width = sw + "px";
                    divDrop.style.height = sh + "px";
                    var svg = document.getElementsByTagName("svg");
                    svg[0].setAttribute("height", sh);
                    svg[0].style.height = sh + "px";
                    svg[0].setAttribute("width", sw);
                    svg[0].style.width = sw + "px";
                    svg[0].style.position = "relative";
                    svg[0].style.left = "-" + sw + "px";
                    svg[0].style.top = "30px";
                    svg[0].style.backgroundColor = "transparent";
                    divChart.style.backgroundColor = "transparent";
                    divDrop.style.backgroundColor = "transparent";
                    $scope.chartViewModel.GetNode(0).data.hideconnectors = true;
                    return;
                }
                divDrop.style.width = w + "px";
                divDrop.style.height = h + "px";
                divChart.style.width = w + "px";
                divChart.style.height = h + "px";
                var svg = document.getElementsByTagName("svg");
                svg[0].setAttribute("height", h);
                svg[0].style.height = h + "px";
                svg[0].setAttribute("width", w);
                svg[0].style.width = w + "px";
                var newnode = $scope.chartViewModel.GetNodeById(0);

                newnode.data.x = 0;
                newnode.data.y = 0;
                CenterDrop();
                divDrop.style.top = "0px";
                //add (initial) start node
                var newNodeDataModel = {
                    name: "Example Node 1",
                    id: $scope.chartViewModel.GetNumNodes(),
                    GUID: nextGUID,
                    x: $scope.RoundNumbers((divDrop.clientWidth / 2) - ($scope.chartViewModel.GetShapeWidthList(8 - 1) / 2) + 1),
                    y: 50,
                    width: 300,
                    parent: parent,
                    collapseable: true,
                    collapsed: true,
                    collapseWidth: 100,
                    collapseHeight: 150,
                    iconWidth: 24,
                    iconHeight: 24,
                    iconX: 3,
                    typeIndex: 8,
                    iconY: 3,
                    shapeWidth: $scope.chartViewModel.GetShapeWidthList(8 - 1),
                    shapeHeight: $scope.chartViewModel.GetShapeHeightList(8 - 1),
                    z: 0,
                    visible: true,
                    rootNodeID: currentRootNodeID,
                    rootNodeIDForeach: "",
                    ForeachItemName: "item",
                    ForeachListName: "list",
                    currentActivityNumber: currentActivityNumber,
                    numChild: 0,
                    collapseIconX: 236,
                    collapseIconY: 0,
                    title: $scope.chartViewModel.GetName(8 - 1),
                    titleX: 20,
                    titleY: 20,
                    backColor: $scope.chartViewModel.GetBackColor(8 - 1),
                    origBackColor: $scope.chartViewModel.GetBackColor(8 - 1),
                    iconCollapsedWidth: 50,
                    iconCollapsedHeight: 50,
                    iconCollapsedX: 25,
                    mouseOver: false,
                    iconCollapsedY: 50,
                    collapserVisible: true,
                    collapserMouseOver: false,
                    titleVisible: true,
                    icon: $scope.chartViewModel.GetIcons(8 - 1),
                    inputcount: 0,
                    outputcount: 1,
                    showSettings: true,
                    settingsX: 80,
                    ShowHover: false,
                    settingsY: 130,
                    clickX: 5,
                    covered: false,
                    hideconnectors: false,
                    connectorclicked: false,
                    dragcovered: false,
                    clickY: 130,
                    defaultPosition: true,
                    nextInputConnector: 0,
                    parentIDs: [],
                    childrenGUIDs: [],
                    childrenGUIDsTEMP: [],
                    connected: false,
                    outputConnectors: [
                        {
                            name: "o1",
                            x: 115 - 95 + 15,
                            y: 150,
                            numConnection: 0,
                            connected: false,
                            connectorCircleRadius: 5,
                            caption: caps[0],
                        },
                    ],
                };
                nextGUID++;
                $scope.chartViewModel.addNode(newNodeDataModel);

                //add (initial) start node
            } catch (ex) {

            }
        } catch (ex) {
            //alert(ex.message + " , " + ex.number);
        }

    };

    $scope.islock = true;
    $scope.isunlock = false;
    $scope.lockTitle = "Lock Items";
    $scope.ToggleLock = function() {
        if ($scope.islock) {
            $scope.islock = false;
            $scope.isunlock = true;
            $scope.lockTitle = "UnLock Items";
        } else {
            $scope.islock = true;
            $scope.isunlock = false;
            $scope.lockTitle = "Lock Items";
        }
        $scope.chartViewModel.Lock = !$scope.chartViewModel.Lock;
    };

    //
    // Delete selected nodes and connections.
    //
    $scope.deleteSelected = function() {

        $scope.chartViewModel.deleteSelected();
    };

    //
    // Create the view-model for the chart and attach to the scope.
    //
    $scope.chartViewModel = new flowchart.ChartViewModel(chartDataModel);
});
