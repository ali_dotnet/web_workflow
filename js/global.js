﻿//defintion of global variables
var nextGUID = 0;
var nextVarGUID = 0;
var nextArgGUID = 0;
var TempDeletedObjectList = new Array();
var TempDeletedLineList = new Array();
var currentRootNodeID = 0;
var currentActivityNumber = 0;
var nodeRootIDList = new Array();

var ForeachActive = false;
var SelectedForEachNode = null;
var FlowchartIDIndexDictionary = new Array();
FlowchartIDIndexDictionary.push({ id: 0, index: 0 });

var maxFlowchartHeight = 10000;
var maxFlowchartWidth = 10000;

var importedLibraries = [];//this array will hold imported libraries (namespaces)

var destConnectorUpX;
var destConnectorUpY;

var currentActiveNodeForConnectionSourceID = -1;
var connectorWiseConnectionMode = false;
var LineSwitchConnectionIndex = -1;
var GhostPathSrc = { x: -1000, y: -1000 };
var srcConnectorIndex = -1;
var spriteCancel = false;
var moveSprites = [];
var movingSpriteIds = [];
var ClipboarIDs = new Array();
var ClipboardOffsets = new Array();
var ClipboardParent = new Array();
var ClipboardConnection = new Array();
var tmpCorrectPathPoint = null;
var LineSwitchMode = "";
var SelectedPointIndex = -1;
var ResizeOperationActive = false;
var ChainedPointIndex = -1;
var SelectedConnectionIndex = -1;
var SelectedConnectionPoint;
var ClipboardConnectionOffset = 0;
var moveDeltaX = 0;
var moveDeltaY = 0;
var PathMap = new Array();
var PointList = [];
var titleChangeCurrentElement = 0;
var dropIndex = -1;
var dropConnectorData = null;
var program_init = false;
var dropX = 0;
var dropY = 0;
var flowChartWidth = new Array();
var flowChartHeight = new Array();
var flowChartTitle = new Array();
var XMLdata = "";
var XMLdata2 = "";
var mainRefIds = new Array();
var refIds = new Array();
var refIDInfo = "";
var Variables = new Array();
var Arguments = new Array();
var References = new Array();
var tmpVariables = new Array();
var currentFlowchartIDForVariables = 0;
var widthChangeValue;
var heightChangeValue;
var beforeNodeIndex = -1;
var afterNodeIndex = -1;
var beforeNode;
var afterNode;
var noFlowchartMode = false;
var totalvarindex = 0;
var CorrectFlowChartListForVariables = new Array();
var CorrectFlowChartValuesForVariables = new Array();
CorrectFlowChartListForVariables.push("Flowchart");
CorrectFlowChartValuesForVariables.push("0");

var nodeInputOutputConnector = new Array();

var connectorPositions = new Array();

nodeInputOutputConnector.push({ inputx: 20, inputy: 0, inputdir: "up", output1x: -15, output1y: 62.5, output2x: 62.5, output2y: 70, output1dir: "left", output2dir: "right" });//if
nodeInputOutputConnector.push({
    inputx: 105, inputy: 0, output1x: 105, output1y: 90, output2x: 0, output2y: 0, input2x: 240, input2y: 40, input3x: -30, input3y: 40,//assign
    inputdir: "up", output1dir: "down", input2dir: "right", input3dir: "left"
});
nodeInputOutputConnector.push({ inputx: 15, inputy: 0, output1x: 15, output1y: 100, output2x: 0, output2y: 0, inputdir: "up", output1dir: "down" });
nodeInputOutputConnector.push({ inputx: 15, inputy: 0, output1x: 15, output1y: 100, output2x: 0, output2y: 0, inputdir: "up", output1dir: "down" });
nodeInputOutputConnector.push({ inputx: 15, inputy: 0, output1x: 15, output1y: 100, output2x: 0, output2y: 0, inputdir: "up", output1dir: "down" });
nodeInputOutputConnector.push({ inputx: 35, inputy: 0, output1x: 35, output1y: 150, output2x: 0, output2y: 0, inputdir: "up", output1dir: "down" });
nodeInputOutputConnector.push({
    inputx: 85, inputy: 0, output1x: 85, output1y: 80, output2x: 0, output2y: 0, input2x: 200, input2y: 40, input3x: -30, input3y: 40,
    inputdir: "up", output1dir: "down", input2dir: "right", input3dir: "left"
});
nodeInputOutputConnector.push({ inputx: 15, inputy: 0, inputdir:"up", output1x: 15, output1y: 85, output2x: 0, output2y: 0, output1dir: "down" });//start

nodeInputOutputConnector.push({
    inputx: 105, inputy: 0, output1x: 105, output1y: 90, output2x: 0, output2y: 0, input2x: 240, input2y: 40, input3x: -30, input3y: 40,//value editor
    inputdir: "up", output1dir: "down", input2dir: "right", input3dir: "left"
});

nodeInputOutputConnector.push({//connector
    inputx: 105, inputy: 0, output1x: 105, output1y: 90, output2x: 0, output2y: 0, input2x: 240, input2y: 40, input3x: -30, input3y: 40,//value editor
    inputdir: "up", output1dir: "down", input2dir: "right", input3dir: "left"
});

nodeInputOutputConnector.push({//collector
    inputx: 105, inputy: 0, output1x: 105, output1y: 90, output2x: 0, output2y: 0, input2x: 240, input2y: 40, input3x: -30, input3y: 40,//value editor
    inputdir: "up", output1dir: "down", input2dir: "right", input3dir: "left"
});

nodeInputOutputConnector.push({//workflow
    inputx: 105, inputy: 0, output1x: 105, output1y: 90, output2x: 0, output2y: 0, input2x: 240, input2y: 40, input3x: -30, input3y: 40,//value editor
    inputdir: "up", output1dir: "down", input2dir: "right", input3dir: "left"
});
nodeInputOutputConnector.push({
    inputx: 105, inputy: 0, output1x: 105, output1y: 90, output2x: 0, output2y: 0, input2x: 240, input2y: 40, input3x: -30, input3y: 40,//foreach
    inputdir: "up", output1dir: "down", input2dir: "right", input3dir: "left"
});

//if
connectorPositions.push({ x: 20, y: 0, index: 0, typeIndex: 1, rx: 93, ry: -15 });
connectorPositions.push({ x: 50, y: 0, index: 1, typeIndex: 1, rx: 93, ry: -15 });
connectorPositions.push({ x: 80, y: 0, index: 2, typeIndex: 1, rx: 93, ry: -15 });
connectorPositions.push({ x: 50, y: 120, index: 3, typeIndex: 1, rx: 93, ry: -15 });
connectorPositions.push({ x: 0, y: 70, index: 4, typeIndex: 1, rx: -15, ry: 58 });
connectorPositions.push({ x: 100, y: 70, index: 5, typeIndex: 1, rx: 115, ry: 58 });

//assign
connectorPositions.push({ x: 150, y: 0, index: 0, typeIndex: 2, rx: 147, ry: 0 });
connectorPositions.push({ x: 300, y: 50, index: 1, typeIndex: 2, rx: 316, ry: 63 });
connectorPositions.push({ x: 150, y: 100, index: 2, typeIndex: 2, rx: 147, ry: 132 });
connectorPositions.push({ x: 0, y: 50, index: 3, typeIndex: 2, rx: -15, ry: 63 });

//loop
connectorPositions.push({ x: 15, y: 0, index: 0, typeIndex: 3 });
connectorPositions.push({ x: 60, y: 57.5, index: 1, typeIndex: 3 });
connectorPositions.push({ x: 15, y: 110, index: 2, typeIndex: 3 });
connectorPositions.push({ x: -30, y: 57.5, index: 3, typeIndex: 3 });

//showform
connectorPositions.push({ x: 15, y: 0, index: 0, typeIndex: 4 });
connectorPositions.push({ x: 60, y: 57.5, index: 1, typeIndex: 4 });
connectorPositions.push({ x: 15, y: 110, index: 2, typeIndex: 4 });
connectorPositions.push({ x: -30, y: 57.5, index: 3, typeIndex: 4 });

//webservice
connectorPositions.push({ x: 15, y: 0, index: 0, typeIndex: 5 });
connectorPositions.push({ x: 60, y: 57.5, index: 1, typeIndex: 5 });
connectorPositions.push({ x: 15, y: 110, index: 2, typeIndex: 5 });
connectorPositions.push({ x: -30, y: 57.5, index: 3, typeIndex: 5 });

//flowchart
connectorPositions.push({ x: 100, y: 0, index: 0, typeIndex: 7, rx: 99, ry: 0 });
connectorPositions.push({ x: 200, y: 25, index: 1, typeIndex: 7, rx: 215, ry: 42 });
connectorPositions.push({ x: 100, y: 50, index: 2, typeIndex: 7, rx: 99, ry: 80 });
connectorPositions.push({ x: 0, y: 25, index: 3, typeIndex: 7, rx: -15, ry: 42 });//-25

//start
connectorPositions.push({ x: 50, y: 0, index: 0, typeIndex: 8, rx: 54, ry: -15 });
connectorPositions.push({ x: 100, y: 50, index: 1, typeIndex: 8, rx: 125, ry: 48 });
connectorPositions.push({ x: 50, y: 100, index: 2, typeIndex: 8, rx: 54, ry: 116 });
connectorPositions.push({ x: 0, y: 50, index: 3, typeIndex: 8, rx: -15, ry: 48 });

//valueEditor
connectorPositions.push({ x: 90, y: 0, index: 0, typeIndex: 9, rx: 93, ry: -15 });
connectorPositions.push({ x: 185, y: 30, index: 1, typeIndex: 9, rx: 196, ry: 33 });
connectorPositions.push({ x: 90, y: 65, index: 2, typeIndex: 9, rx: 93, ry: 76 });
connectorPositions.push({ x: 0, y: 30, index: 3, typeIndex: 9, rx: -15, ry: 33 });

//connectorActivity
connectorPositions.push({ x: 70, y: 0, index: 0, typeIndex: 10, rx: 63, ry: -15 });
connectorPositions.push({ x: 140, y: 75, index: 1, typeIndex: 10, rx: 150, ry: 72 });
connectorPositions.push({ x: 70, y: 150, index: 2, typeIndex: 10, rx: 63, ry: 161 });
connectorPositions.push({ x: 0, y: 75, index: 3, typeIndex: 10, rx: -15, ry: 72 });

//collectionActivity
connectorPositions.push({ x: 70, y: 0, index: 0, typeIndex: 11, rx: 68, ry: -15 });
connectorPositions.push({ x: 140, y: 70, index: 1, typeIndex: 11, rx: 150, ry: 72 });
connectorPositions.push({ x: 70, y: 140, index: 2, typeIndex: 11, rx: 68, ry: 156 });
connectorPositions.push({ x: 0, y: 70, index: 3, typeIndex: 11, rx: -15, ry: 72 });

//workflowActivity
connectorPositions.push({ x: 90, y: 0, index: 0, typeIndex: 12, rx: 90, ry: -15 });
connectorPositions.push({ x: 180, y: 70, index: 1, typeIndex: 12, rx: 195, ry: 70 });
connectorPositions.push({ x: 90, y: 140, index: 2, typeIndex: 12, rx: 90, ry: 156 });
connectorPositions.push({ x: 0, y: 70, index: 3, typeIndex: 12, rx: -15, ry: 70 });

//foreach
connectorPositions.push({ x: 120, y: 0, index: 0, typeIndex: 13, rx: 99, ry: 0 });
connectorPositions.push({ x: 240, y: 30, index: 1, typeIndex: 13, rx: 215, ry: 42 });
connectorPositions.push({ x: 120, y: 60, index: 2, typeIndex: 13, rx: 99, ry: 80 });
connectorPositions.push({ x: 0, y: 30, index: 3, typeIndex: 13, rx: -15, ry: 42 });//-25

//initialize references
References.push("System");
References.push("System.Collections.Generic");
References.push("System.Linq");
References.push("System.Text");
References.push("System.Activities");
References.push("System.Collections");
References.push("Newtonsoft.Json.Linq");
References.push("WF.Web.Core.Common");
References.push("System.Net");

var numSupportedFlowcharts = 100;
var flowChartHasStart = new Array();
var initialWidth = 600;
var initialHeight = 600;
var minWidth = initialWidth;
var minHeight = initialHeight;
var mouseX;
var mouseY;
var currentDropIndex = -1;
var xmlReference = 0;

var UndoList = new Array();
var RedoList = new Array();

var currentEditAssignNode1 = -1;
var currentEditAssignNode2 = -1;
var currentEditIfCondition = -1;
var dragcovered = "false";
var FlowChartTitleChangerVisible = false;
var FlowchartTitles = "";
var FlowchartSizes = "";
flowChartHasStart.push(true);
var flowChartIDStart = "";
var lastFlowChartIndex = 1;

var ClassNames = new Array();
ClassNames.push("if");
ClassNames.push("assign");
ClassNames.push("loop");
ClassNames.push("showform");
ClassNames.push("webservice");
ClassNames.push("flowchart");
ClassNames.push("flowchart2");
ClassNames.push("start");
ClassNames.push("keyvalue");
ClassNames.push("connector");
ClassNames.push("collection");
ClassNames.push("workflow");
ClassNames.push("foreach");
var tmpUndoX;
var tmpUndoY;

var bigMode = false;
var snapX = 10;
var snapY = 10;

function StateSuggestions() {
    this.states = [
    ];
}

function tmpMouseUp(evt) {
    try
    {
        for (var k = 0 ; k < moveSprites.length; k++) {
            divDrop.removeChild(moveSprites[k]);
        }
        moveSprites.length = 0;
        moveDeltaX = 0;
        moveDeltaY = 0;
        spriteCancel = true;
    }
    catch (ex) {
        //alert(ex.message);
    }

};

function tmpMouseMove(evt) {
    try {
        mouseX = -1;
        mouseY = -1;

        var screenx = 0;
        var screeny = 0;

        if (evt.type == 'touchstart' || evt.type == 'touchmove' || evt.type == 'touchend' || evt.type == 'touchcancel') {
            var touch = evt.touches[0] || evt.changedTouches[0];
            screenx = touch.pageX;
            screeny = touch.pageY;
        } else if (evt.type == 'mousedown' || evt.type == 'mouseup' || evt.type == 'mousemove' || evt.type == 'mouseover' || evt.type == 'mouseout' || evt.type == 'mouseenter' || evt.type == 'mouseleave') {
            screenx = evt.screenX;
            screeny = evt.screenY;
        }

        var topOffset = screen.height - window.innerHeight;
        var leftOffset = screen.width - window.innerWidth;
        if (screenx >= divDrop.offsetLeft + leftOffset && screenx <= divDrop.offsetLeft + leftOffset + divDrop.clientWidth &&
            screeny >= divDrop.offsetTop + topOffset && screeny <= divDrop.offsetTop + topOffset + divDrop.clientHeight) {
            mouseX = screenx - divDrop.offsetLeft - leftOffset;
            mouseY = screeny - divDrop.offsetTop - topOffset;
        }
        else {
            mouseX = -1;
            mouseY = -1;
        }

        if (ResizeOperationActive)//resize operation in progress (using the span icon)
        {
            try {
                var finalW = (resizer_startWidth + evt.clientX - resizer_startX);
                var finalH = (resizer_startHeight + evt.clientY - resizer_startY);
                var mw = minWidth + 40;
                var mh = minHeight + 40;
                if (mw < initialWidth)
                    mw = initialWidth;
                if (mh < initialHeight)
                    mh = initialHeight;
                var svg = document.getElementsByTagName("svg");

                if (!(finalW < (mw - 40) || finalW > (parseInt(maxFlowchartWidth) + parseInt(initialWidth)))) {
                    divDrop.style.width = (resizer_startWidth + evt.clientX - resizer_startX) + 'px';
                    widthApplied = false;
                    $("#divChart").width(finalW);
                    $("#divFlowChartParent").width(finalW);
                    $("#divFlowChartParentTitle").width(finalW);
                    svg[0].setAttribute("width", finalW);
                    svg[0].style.width = (finalW) + "px";
                    CenterDrop();
                }

                if (!(finalH > (parseInt(maxFlowchartHeight) + parseInt(initialHeight)) || finalH < (mh))) {//-40
                    divDrop.style.height = (resizer_startHeight + evt.clientY - resizer_startY) + 'px';
                    $("#divChart").height(finalH);
                    svg[0].setAttribute("height", finalH);
                    svg[0].style.height = (finalH) + "px";
                }
            }
            catch (ex) {
                divDebug.innerHTML = ex.message;
            }
        }//end resize operation uing span icon
    }
    catch (ex) {

    }
}

for (var i = 0 ; i < numSupportedFlowcharts; i++) {
    nodeRootIDList.push(0);
    flowChartTitle.push("Flowchart");
    flowChartWidth.push(initialWidth);
    flowChartHeight.push(initialHeight);
}

//from wfglobal.js

function EditWorkflow() {
    lblWorkflowName.disabled = false;
    lblWorkflowName.focus();
}

function WorkflowKeyup() {
    lblWorkflowName.disabled = true;
}

function requestFullScreen() {

    var el = document.body;

    // Supports most browsers and their versions.
    var requestMethod = el.requestFullScreen || el.webkitRequestFullScreen
    || el.mozRequestFullScreen || el.msRequestFullScreen;

    if (requestMethod) {

        // Native full screen.
        requestMethod.call(el);

    } else if (typeof window.ActiveXObject !== "undefined") {

        // Older IE.
        var wscript = new ActiveXObject("WScript.Shell");

        if (wscript !== null) {
            wscript.SendKeys("{F11}");
        }
    }
}

function ShowLoader() {
    if (divJSONLoader.style.display == "block") {
        divJSONLoader.style.display = "none";
        $('.divXamlBack').css('display', 'none');
    } else {
        $('.divXamlBack').css('display', 'block');
        divJSONLoader.style.display = "";
    }
    setTimeout(function () { $('textarea').focus(); }, 100);
}

function ShowJSONDialog() {
    if (divJONSDialog.style.display == "block") {
        divJONSDialog.style.display = "none";
        $('.divXamlBack').css('display', 'none');
    } else {
        $('.divXamlBack').css('display', 'block');
        divJONSDialog.style.display = "";
    }
}

function UpdateVariableCombo() {
    for (var i = 0 ; i < 1000; i++) {
        try {
            var cmb = document.getElementById("cmbAssignVariable" + i);
            var cmb2 = document.getElementById("cmbAssignVariable2-" + i);
            cmb.options.length = 0;
            cmb2.options.length = 0;
            for (var j = 0 ; j < Variables.length; j++) {
                var z = document.createElement("option");
                var t = document.createTextNode(Variables[j].Name);
                z.appendChild(t);
                cmb.appendChild(z);

                var z2 = document.createElement("option");
                var t2 = document.createTextNode(Variables[j].Name);
                z2.appendChild(t2);
                cmb2.appendChild(z2);
            }

            for (var j = 0 ; j < Arguments.length; j++) {
                var z = document.createElement("option");
                var t = document.createTextNode(Arguments[j].Name);
                z.appendChild(t);
                cmb.appendChild(z);

                var z2 = document.createElement("option");
                var t2 = document.createTextNode(Arguments[j].Name);
                z2.appendChild(t2);
                cmb2.appendChild(z2);
            }
        }
        catch (ex) {

        }
    }
}

function CenterDrop() {
    try {
        divDrop.style.position = "relative";
        if (divDrop.childNodes.length > 1) {
            var child = document.getElementsByClassName("ActivityItem");
            if (child != null) {
            }
        }
        divDrop.style.left = (((window.innerWidth - divDrop.clientWidth) / 2) - 80) + "px";
        if ((((window.innerWidth - divDrop.clientWidth) / 2) - 80) < 0) {
            divDrop.style.left = "0px";
        }
    }
    catch (ex) {
        //alert(ex.message);
    }
}

var names = new Array();
var icons = new Array();
var close_mode = false;
var close_mode_add = false;

function TitleKeyup() {
    lblFakeTitle.disabled = true;
    txtTitle.style.display = 'none';
    if (titleChangeCurrentElement == 0) {
        lblFakeTitle.style.display = '';
        var UndoObject = { action: "changetitle", titleBefore: lblFakeTitle.value, titleAfter: lblFakeTitle.value };
        UndoList.push(UndoObject);
        RedoList.length = 0;
    }
    var current_item;
    for (var i = 1 ; i < 16; i++) {
        var name = "lblActivity" + i.toString();
        if (document.getElementById(name).style.display != "none") {
            current_item = i;
        }
    }

    var lblname = "lblActivity" + current_item;
    var sign = "";
    if (current_item != 1) {
        sign = " > ";
    }

    if (titleChangeCurrentElement == 0) {
        document.getElementById(lblname).childNodes[0].childNodes[0].innerHTML = lblFakeTitle.value;
    }
    else {
        flowChartTitle[current_item] = lblFakeTitle.value;
        lblname = "lblActivity" + (current_item + 1);
        document.getElementById(lblname).childNodes[0].childNodes[0].innerHTML = lblFakeTitle.value;
    }

    FlowchartTitles += currentRootNodeID + "=" + lblFakeTitle.value + ",";

    for (var j = 0 ; j < 100; j++) {
        try {
            var cmbname = "cmbScope" + j;
            var realcmb = document.getElementById(cmbname);
            for (var q = 0 ; q < realcmb.options.length; q++) {
                if (realcmb.options[q].value == currentRootNodeID) {
                    realcmb.options[q].text = lblFakeTitle.value;
                }
            }
        }
        catch (ex) {
        }
    }
}

//resize operation with the span icon
var beforeSize;
var resizer_startX, resizer_startY, resizer_startWidth, resizer_startHeight;
function initDrag(e) {
    ResizeOperationActive = true;
    SelectedPointIndex = -1;
    resizer_startX = e.clientX;
    resizer_startY = e.clientY;
    resizer_startWidth = divDrop.clientWidth;
    resizer_startHeight = divDrop.clientHeight;
    document.documentElement.addEventListener('mouseup', stopDrag, false);

    beforeSize = { w: divDrop.style.width.replace("px", ""), h: divDrop.style.height.replace("px", "") };
}
var x = 0;

function stopDrag(e) {
    ResizeOperationActive = false;
    document.documentElement.removeEventListener('mouseup', stopDrag, false);
    var sizeAfter = { w: divDrop.style.width.replace("px", ""), h: divDrop.style.height.replace("px", "") };
    var UndoObject = { action: "dragresize", sizeBefore: beforeSize, sizeAfter: sizeAfter };
    UndoList.push(UndoObject);
    RedoList.length = 0;
}

//end resize operation with span icon

function allowDrop(x) {
    x.preventDefault();
}

var imgResizeDrag = document.getElementById('imgResize');
imgResizeDrag.addEventListener("touchmove", imgResizeDragOver, false);
imgResizeDrag.addEventListener("touchstart", imgResizeDragStart, false);
imgResizeDrag.addEventListener("touchend", imgResizeDragEnd, false);

function imgResizeDragStart(evt) {
    try
    {
        var touch = evt.touches[0] || evt.changedTouches[0];
        var screenx = touch.pageX;
        var screeny = touch.pageY;
        ResizeOperationActive = true;
        SelectedPointIndex = -1;
        resizer_startX = screenx;
        resizer_startY = screeny;
        resizer_startWidth = divDrop.clientWidth;
        resizer_startHeight = divDrop.clientHeight;
        evt.preventDefault();
    }
    catch (ex) {
        divDebug.innerHTML = ex.message;
    }
}

function imgResizeDragEnd(evt) {
    ResizeOperationActive = false;
}

function imgResizeDragOver(evt) {
    var touch = evt.touches[0] || evt.changedTouches[0];
    var screenx = touch.pageX;
    var screeny = touch.pageY;
    try {
        var finalW = (resizer_startWidth + screenx - resizer_startX);
        var finalH = (resizer_startHeight + screeny - resizer_startY);
        var mw = minWidth + 40;
        var mh = minHeight + 40;
        if (mw < initialWidth)
            mw = initialWidth;
        if (mh < initialHeight)
            mh = initialHeight;
        var svg = document.getElementsByTagName("svg");
        if (!(finalW < (mw - 40) || finalW > (parseInt(maxFlowchartWidth) + parseInt(initialWidth)))) {
            divDrop.style.width = (resizer_startWidth + screenx - resizer_startX) + 'px';
            widthApplied = false;
            $("#divChart").width(finalW);
            $("#divFlowChartParent").width(finalW);
            $("#divFlowChartParentTitle").width(finalW);
            svg[0].setAttribute("width", finalW);
            svg[0].style.width = (finalW) + "px";
            CenterDrop();
        }

        if (!(finalH > (parseInt(maxFlowchartHeight) + parseInt(initialHeight)) || finalH < (mh))) {//-40
            divDrop.style.height = (resizer_startHeight + screeny - resizer_startY) + 'px';
            $("#divChart").height(finalH);
            svg[0].setAttribute("height", finalH);
            svg[0].style.height = (finalH) + "px";
        }
        evt.preventDefault();
    }
    catch (ex) {
        divDebug.innerHTML = ex.message;
    }

}

setTimeout(function () {
    var elDrag = document.getElementById('divLeftToolbarX');

    elDrag.addEventListener("touchstart", DragStart, false);
    elDrag.addEventListener("touchend", DragEnd, false);
    elDrag.addEventListener("touchmove", DragOver, false);


}, 1000);

function DragOver(evt) {
    SetDragCovered();
}

function DragStart(evt) {
    try {
        Body.style.cursor = "url(content/cursor/" + evt.target.getAttribute("index") + ".png), auto";
        dropIndex = evt.target.getAttribute("index");
        dropConnectorData = evt.target.getAttribute("iteminfo");
        if (program_init && dropIndex == 6)//start node selected after program has been initialized?
        {
            dropIndex = -1;
        }
    }
    catch (ex) {
        alert(ex.message);
    }
}

function DragEnd(evt) {
    try
    {
        document.body.style.cursor = "default";
        var topOffset = window.screenY + (window.outerHeight - window.innerHeight);
        var leftOffset = window.screenX;
        if (topOffset < 0)
            topOffset = 0;
        if (leftOffset < 0)
            leftOffset = 0;
        var screenx = 0;
        var screeny = 0;
        if (evt.type == 'touchstart' || evt.type == 'touchmove' || evt.type == 'touchend' || evt.type == 'touchcancel') {
            var touch = evt.touches[0] || evt.changedTouches[0];
            screenx = touch.pageX;
            screeny = touch.pageY;
        } else if (evt.type == 'mousedown' || evt.type == 'mouseup' || evt.type == 'mousemove' || evt.type == 'mouseover'
            || evt.type == 'mouseout' || evt.type == 'mouseenter' || evt.type == 'mouseleave' || evt.type == "dragend") {
            screenx = evt.screenX;
            screeny = evt.screenY;
        }

        if (screenx >= divDrop.offsetLeft + leftOffset && screenx <= divDrop.offsetLeft + leftOffset + divDrop.clientWidth &&
            screeny >= divDrop.offsetTop + topOffset && screeny <= divDrop.offsetTop + topOffset + divDrop.clientHeight) {
            dropX = screenx - divDrop.offsetLeft - leftOffset;
            dropY = screeny - divDrop.offsetTop - topOffset;
            DragInsert();
        }
    }
    catch (ex) {
        divDebug.innerHTML = ex.message;
    }
}

$("#divMouseCapture")
  .mouseup(function () {

  })
  .mousedown(function () {

  });

var varSortDirection = [];
var argSortDirection = [];
varSortDirection.push(true);
varSortDirection.push(true);
varSortDirection.push(true);
varSortDirection.push(true);

argSortDirection.push(true);
argSortDirection.push(true);
argSortDirection.push(true);
argSortDirection.push(true);

function DoSort(tableName, col) {
    var dir = true;
    if (tableName == "tbl") {
        dir = varSortDirection[col];
        var cssClass = "";
        if (dir)
            document.getElementById("imgVarSort" + col).className = "fa fa-caret-down pull-right";
        else
            document.getElementById("imgVarSort" + col).className = "fa fa-caret-up pull-right";
        varSortDirection[col] = !varSortDirection[col];
    }
    else {
        dir = argSortDirection[col];
        var cssClass = "";
        if (dir)
            document.getElementById("imgArgSort" + col).className = "fa fa-caret-down pull-right";
        else
            document.getElementById("imgArgSort" + col).className = "fa fa-caret-up pull-right";
        argSortDirection[col] = !argSortDirection[col];
    }
    SortTables(tableName, col, dir);
}

function SortTables(tbl, col, dir) {
    try {
        var tableData = document.getElementById(tbl);
        var rowData = tableData.getElementsByTagName('tr');
        for (var i = 0; i < rowData.length - 2; i++) {
            for (var j = 0; j < rowData.length - (i + 2) ; j++) {
                var str1;
                var str2;
                if (col == 0 || col == 3)//name/default
                {
                    str1 = $('#' + tbl).find("tr")[j].getElementsByTagName("td")[col].children[0].value;
                    str2 = $('#' + tbl).find("tr")[j + 1].getElementsByTagName("td")[col].children[0].value;
                }
                else {
                    var cmb1 = $('#' + tbl).find("tr")[j].getElementsByTagName("td")[col].children[0];
                    var cmb2 = $('#' + tbl).find("tr")[j + 1].getElementsByTagName("td")[col].children[0];
                    str1 = cmb1.options[cmb1.selectedIndex].text;
                    str2 = cmb2.options[cmb2.selectedIndex].text;
                }
                var n = str1.localeCompare(str2);
                var performsort = false;
                if ((dir && n == 1) || (!dir && n == -1))
                    performsort = true;
                if (performsort)
                    tableData.insertBefore(rowData.item(j + 1), rowData.item(j));
            }
        }
    }
    catch (ex) {
        alert(ex.message);
    }
}
