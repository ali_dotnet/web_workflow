﻿require.config({
    baseUrl: 'app'
});

require(
    [
        'app',
        'services/cryptoService',
        'services/formsService',
        'services/languageService',
        'services/starterService',
        'services/utilsService',
        'services/userInfoService',
        'services/themMgrService',
        'services/notificationService',
        'services/wfExecService',
        'controllers/main/navbarController'
    ],
    function () {
        
    });
