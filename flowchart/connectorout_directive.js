﻿app.directive('connectorout', function () {
    return {
        restrict: 'ECA',
        templateUrl: "flowchart/connectorout_template.html",
        scope: {
        },

        //
        // Controller for the flowchart directive.
        // Having a separate controller is better for unit testing, otherwise
        // it is painful to unit test a directive without instantiating the DOM 
        // (which is possible, just not ideal).
        //
        controller: 'connectoroutController',
    };
})


//
// Controller for the flowchart directive.
// Having a separate controller is better for unit testing, otherwise
// it is painful to unit test a directive without instantiating the DOM 
// (which is possible, just not ideal).
//
.controller('connectoroutController', ['$scope', 'dragging', '$element', function connectoroutController($scope, dragging, $element) {
    $scope.linerange = [100, 200, 300];
    var controller = this;

    this.document = document;
    //
    // Wrap jQuery so it can easily be  mocked for testing.
    //
    this.jQuery = function (element) {
        return $(element);
    }

}])
;
