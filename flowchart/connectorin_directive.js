﻿app.directive('connectorin', function () {
    return {
        restrict: 'ECA',
        templateUrl: "flowchart/connectorin_template.html",
        scope: {         },

        //
        // Controller for the flowchart directive.
        // Having a separate controller is better for unit testing, otherwise
        // it is painful to unit test a directive without instantiating the DOM 
        // (which is possible, just not ideal).
        //
        controller: 'connectorinController',
    };
})


//
// Controller for the flowchart directive.
// Having a separate controller is better for unit testing, otherwise
// it is painful to unit test a directive without instantiating the DOM 
// (which is possible, just not ideal).
//
.controller('connectorinController', ['$scope', 'dragging', '$element', '$attrs', function connectorinController($scope, dragging, $element,$attrs) {
    $scope.linerange = [100, 200, 300];
    var controller = this;

    this.document = document;
    $scope.tmp = $attrs.x;
    //
    // Wrap jQuery so it can easily be  mocked for testing.
    //
    this.jQuery = function (element) {
        return $(element);
    }

}])
;
