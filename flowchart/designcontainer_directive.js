﻿//
// Flowchart module.
//
angular.module('designcontainer', ['dragging'] )

//
// Directive that generates the rendered chart from the data model.
//
.directive('designcontainer', function () {
  return {
  	restrict: 'E',
  	templateUrl: "flowchart/designcontainer_template.html",
  	replace: true,
  	scope: {
  		chart: "=chart",
  	},

  	//
  	// Controller for the flowchart directive.
  	// Having a separate controller is better for unit testing, otherwise
  	// it is painful to unit test a directive without instantiating the DOM 
  	// (which is possible, just not ideal).
  	//
  	controller: 'designcontainerController',
  };
})

//
// Controller for the flowchart directive.
// Having a separate controller is better for unit testing, otherwise
// it is painful to unit test a directive without instantiating the DOM 
// (which is possible, just not ideal).
//
.controller('designcontainerController', ['$scope', '$rootScope', 'dragging', '$element', function designcontainerController($scope, $rootScope, dragging, $element) {
    $scope.linerange = [100, 200, 300];
    var controller = this;

    //
    // Reference to the document and jQuery, can be overridden for testting.
    //
    this.document = document;

    //
    // Wrap jQuery so it can easily be  mocked for testing.
    //
    this.jQuery = function (element) {
        return $(element);
    }

    //
    // Init data-model variables.
    //

    $scope.draggingConnection = false;
    $scope.collapseable = true;
    $scope.collapsed = false;
    $scope.connectorSize = 4;
    $scope.dragSelecting = false;
    $scope.rectX = 0;
    $scope.rectY = 0;
    $scope.rectWidth = 0;
    $scope.rectHeight = 0;

    //$scope.dragSelectionRect = {
    //    x: 0,
    //    y: 0,
    //    width: 0,
    //   height: 0,
    //};
    //touch device

    //try {
        setTimeout(function () {
            var svgContainerDrag = document.getElementById('svgContainer');
            svgContainerDrag.addEventListener("touchstart", svgContainerDragStart, false);//window
            svgContainerDrag.addEventListener("touchend", svgContainerDragEnd, false);//window
            svgContainerDrag.addEventListener("touchmove", svgContainerDragOver, false);//window
        }, 1000);

        function svgContainerDragStart(evt) {
            //if (btnTouchScroll.innerHTML == "Scroll Off")
            //    evt.preventDefault();
            if (moveSprites.length > 0)
                return;
            try {
                $scope.dragSelecting = true;
                var touch = evt.touches[0] || evt.changedTouches[0];
                screenx = touch.pageX;
                screeny = touch.pageY;

                //if (!$scope.dragSelecting) {
            //    divDebug.innerHTML = divDrop.offsetLeft + "," + leftOffset; //$("#divDrop").position().left;
                    //var sp = controller.translateCoordinates(parseInt(screenx) - (parseInt($("#divDrop").position().left) + 30),
                    //    parseInt(screeny) - (parseInt($("#divDrop").position().top) + 30), evt);
                //var sp = controller.translateCoordinates(parseInt(screenx), parseInt(screeny), evt);
                
                var topOffset = window.screenY + (window.outerHeight - window.innerHeight);
                var leftOffset = window.screenX;
                if (topOffset < 0)
                    topOffset = 0;
                if (leftOffset < 0)
                    leftOffset = 0;

                var sp = { x: screenx - divDrop.offsetLeft - leftOffset + divContainer.scrollLeft, y: screeny - divDrop.offsetTop - topOffset + divContainer.scrollTop };

                    touchStartX.innerHTML = sp.x;
                    touchStartY.innerHTML = sp.y;
                    //TouchdragSelectionStartPoint = sp;
                    //$scope.dragSelectionRect = {
                    //    x: sp.x,
                    //    y: sp.y,
                    //    width: 0,
                    //    height: 0,
                    //};
                //}
            }
            catch (ex) {
                divDebug.innerHTML = ex.message;
            }
        }

        var startX = -1;
        var startY = -1;
        var TouchdragSelectionStartPoint;
        function svgContainerDragEnd(evt) {
            //if (btnTouchScroll.innerHTML == "Scroll Off")
            //    evt.preventDefault();
            if (moveSprites.length > 0)
                return;

            //$scope.dragSelecting = false;
            $scope.chart.applySelectionRect($scope.dragSelectionRect);
            //delete $scope.dragSelectionStartPoint;
            //delete $scope.dragSelectionRect;
        }

        function svgContainerDragOver(evt) {
            if (btnTouchScroll.innerHTML == "Scroll Off")
                evt.preventDefault();

            if (moveSprites.length > 0)
                return;
            try {
                var x;
                var y;
                var touch = evt.touches[0] || evt.changedTouches[0];
                x = touch.pageX;
                y = touch.pageY;
               // divDebug.innerHTML = $("#divDrop").position().left;
                //var curPoint = controller.translateCoordinates(parseInt(x) - (parseInt($("#divDrop").position().left) + 30),
                //    parseInt(y) - (parseInt($("#divDrop").position().top) + 30), evt);
                //var curPoint = controller.translateCoordinates(parseInt(x), parseInt(y), evt);
                var topOffset = window.screenY + (window.outerHeight - window.innerHeight);
                var leftOffset = window.screenX;
                if (topOffset < 0)
                    topOffset = 0;
                if (leftOffset < 0)
                    leftOffset = 0;

                var curPoint = { x: x - divDrop.offsetLeft - leftOffset + divContainer.scrollLeft, y: y - divDrop.offsetTop - topOffset + divContainer.scrollTop };

                //divDebug.innerHTML = curPoint.x + "," + divDrop.offsetLeft;
                $scope.dragSelectionRect = {
                    x: curPoint.x > touchStartX.innerHTML ? touchStartX.innerHTML : curPoint.x,
                    y: curPoint.y > touchStartY.innerHTML ? touchStartY.innerHTML : curPoint.y,
                    width: curPoint.x > touchStartX.innerHTML ? curPoint.x - touchStartX.innerHTML : touchStartX.innerHTML - curPoint.x,
                    height: curPoint.y > touchStartY.innerHTML ? curPoint.y - touchStartY.innerHTML : touchStartY.innerHTML - curPoint.y,
                };
                //$scope.rectX = curPoint.x > touchStartX.innerHTML ? touchStartX.innerHTML : curPoint.x;
                //$scope.rectY = curPoint.y > touchStartY.innerHTML ? touchStartY.innerHTML : curPoint.y;
                //$scope.rectWidth = curPoint.x > touchStartX.innerHTML ? curPoint.x - touchStartX.innerHTML : touchStartX.innerHTML - curPoint.x;
                //$scope.rectHeight = curPoint.y > touchStartY.innerHTML ? curPoint.y - touchStartY.innerHTML : touchStartY.innerHTML - curPoint.y;
                //divDebug.innerHTML += curPoint.x;//+= curPoint.x + "," + touchStartX.innerHTML + ";";
                //divDebug.innerHTML = $scope.dragSelectionRect.x + "," + $scope.dragSelectionRect.y + "," + $scope.dragSelectionRect.width + "," + $scope.dragSelectionRect.height + ";";
                //evt.preventDefault();
            }
            catch (ex) {
                divDebug.innerHTML = ex.message;
            }
        }
    //}
    //catch (ex) {
    //    alert(ex.message);
    //}

    //end touch device

    //
    // Reference to the connection, connector or node that the mouse is currently over.
    //
    $scope.mouseOverConnector = null;
    $scope.mouseOverConnection = null;
    $scope.mouseOverNode = null;

    $scope.FindMinDistance = function (src, dest1, dest2, dest3) {
        var dest;
        var dist1, dist2, dist3;
        dist1 = Math.sqrt(Math.pow(src.x - dest1.x, 2) + Math.pow(src.y - dest1.y, 2));
        dist2 = Math.sqrt(Math.pow(src.x - dest2.x, 2) + Math.pow(src.y - dest2.y, 2));
        dist3 = Math.sqrt(Math.pow(src.x - dest3.x, 2) + Math.pow(src.y - dest3.y, 2));

        if (dist1 < dist2) {
            if (dist3 < dist1) {
                dest = dest3;
            }
            else {
                dest = dest1;
            }
        }
        else {
            if (dist3 < dist2) {
                dest = dest3;
            }
            else {
                dest = dest2;
            }
        }

        return dest;
    };

    $scope.InputConnectorPosition = function (typeIndex, src, inputid) {
        var dir = "";
        try {
            if (nodeInputOutputConnector[typeIndex - 1].input2x != null) {//we have multiple inputs? test src/dest distance value for all of them and use the nearest dest (how to find the distance? lalal...)
                var tmpNode = $scope.chart.GetNodeById(inputid);
                var p3 = { x: 1000000, y: 1000000 };
                if (nodeInputOutputConnector[typeIndex - 1].input3x != null) {
                    p3 = { x: nodeInputOutputConnector[typeIndex - 1].input3x, y: nodeInputOutputConnector[typeIndex - 1].input3y };
                }

                var dest = $scope.FindMinDistance(src, {
                    x: parseInt(tmpNode.data.x) + parseInt(nodeInputOutputConnector[typeIndex - 1].inputx),
                    y: parseInt(tmpNode.data.y) + parseInt(nodeInputOutputConnector[typeIndex - 1].inputy)
                },
                    {
                        x: parseInt(tmpNode.data.x) + parseInt(nodeInputOutputConnector[typeIndex - 1].input2x), y:
                          parseInt(tmpNode.data.y) + parseInt(nodeInputOutputConnector[typeIndex - 1].input2y)
                    },
{
    x: parseInt(tmpNode.data.x) + parseInt(p3.x), y: parseInt(tmpNode.data.y) + parseInt(p3.y)
});
                if (dest.x == parseInt(tmpNode.data.x) + parseInt(nodeInputOutputConnector[typeIndex - 1].inputx) &&
                    dest.y == parseInt(tmpNode.data.y) + parseInt(nodeInputOutputConnector[typeIndex - 1].inputy))
                    dir = nodeInputOutputConnector[typeIndex - 1].inputdir;
                else if (dest.x == parseInt(tmpNode.data.x) + parseInt(nodeInputOutputConnector[typeIndex - 1].input2x) &&
                    dest.y == parseInt(tmpNode.data.y) + parseInt(nodeInputOutputConnector[typeIndex - 1].input2y))
                    dir = nodeInputOutputConnector[typeIndex - 1].input2dir;
                else if (dest.x == parseInt(tmpNode.data.x) + parseInt(nodeInputOutputConnector[typeIndex - 1].input3x) &&
                    dest.y == parseInt(tmpNode.data.y) + parseInt(nodeInputOutputConnector[typeIndex - 1].input3y))
                    dir = nodeInputOutputConnector[typeIndex - 1].input3dir;

                return { x: parseInt(dest.x) - parseInt(tmpNode.data.x), y: parseInt(dest.y) - parseInt(tmpNode.data.y), dir: dir };
            }
            else {
                dir = nodeInputOutputConnector[typeIndex - 1].inputdir;
                return { x: nodeInputOutputConnector[typeIndex - 1].inputx, y: nodeInputOutputConnector[typeIndex - 1].inputy, dir: dir };
            }
        }
        catch (ex) {
            //alert(ex.message + " , " + ex.number);
        }
    };

    $scope.OutputConnectorPosition1 = function (typeIndex) {
        var dir = nodeInputOutputConnector[typeIndex - 1].output1dir;
        try {
            return { x: nodeInputOutputConnector[typeIndex - 1].output1x, y: nodeInputOutputConnector[typeIndex - 1].output1y, dir: dir };
        }
        catch (ex) {
            //alert(ex.message + " , " + ex.number);
        }
    };

    $scope.OutputConnectorPosition2 = function (typeIndex) {
        var dir = nodeInputOutputConnector[typeIndex - 1].output2dir;
        try {
            return { x: nodeInputOutputConnector[typeIndex - 1].output2x, y: nodeInputOutputConnector[typeIndex - 1].output2y, dir: dir };
        }
        catch (ex) {

        }
    };

    //
    // The class for connections and connectors.
    //
    this.connectionClass = 'connection';
    this.connectorClass = 'connector';
    this.nodeClass = 'node';

    //
    // Search up the HTML element tree for an element the requested class.
    //
    this.searchUp = function (element, parentClass) {

        //
        // Reached the root.
        //
        if (element == null || element.length == 0) {
            return null;
        }

        // 
        // Check if the element has the class that identifies it as a connector.
        //
        if (hasClassSVG(element, parentClass)) {
            //
            // Found the connector element.
            //
            return element;
        }

        //
        // Recursively search parent elements.
        //
        return this.searchUp(element.parent(), parentClass);
    };

    //
    // Hit test and retreive node and connector that was hit at the specified coordinates.
    //
    this.hitTest = function (clientX, clientY) {

        //
        // Retreive the element the mouse is currently over.
        //
        return this.document.elementFromPoint(clientX, clientY);
    };

    //
    // Hit test and retreive node and connector that was hit at the specified coordinates.
    //
    this.checkForHit = function (mouseOverElement, whichClass) {

        //
        // Find the parent element, if any, that is a connector.
        //
        var hoverElement = this.searchUp(this.jQuery(mouseOverElement), whichClass);
        if (!hoverElement) {
            return null;
        }

        return hoverElement.scope();
    };

    //
    // Translate the coordinates so they are relative to the svg element.
    //
    this.translateCoordinates = function(x, y, evt) {

        var point = { x: x, y: y };
        return point;
    };

    var current_connection_source = -1;
    var current_connection_dest = -1;
    var current_connection_dir = "";
    var middlepoint = 0;
    var tmpcon;
    var tmpmode = 0;

    //
    // Called on mouse up in the chart.
    //

    $scope.mouseUp = function (evt) {
        try
        {
            if (!tmpDragNodeAlreadyConnected)
                tmpDragNode.data.connectorclicked = false;
        }
        catch (ex) {

        }
        $scope.RemoveGhostPath();
        if (LineSwitchConnectionIndex != -1) {
            try
            {
                $scope.chart.connections[LineSwitchConnectionIndex].PathPointArray[0] = $scope.chart.connections[LineSwitchConnectionIndex].OriginalFirstPoint;
                $scope.chart.connections[LineSwitchConnectionIndex].PathPointArray[$scope.chart.connections[LineSwitchConnectionIndex].PathPointArray.length - 1] = $scope.chart.connections[LineSwitchConnectionIndex].OriginalLastPoint;
                LineSwitchConnectionIndex = -1;
                LineSwitchMode = "";
            }
            catch (ex) {
                //alert(ex.message);
            }
        }
        LineSwitchConnectionIndex = -1;
        LineSwitchMode = "";
        if (dropIndex != -1) {
            return;
        }
        if (noFlowchartMode) {
            return;
        }
        SelectedPointIndex = -1;
        ResizeOperationActive = false;
        ChainedPointIndex = -1;
        SelectedConnectionIndex = -1;
        if ($scope.chart.GetCurrentDragIndex() == 6 && $scope.chart.GetNumNodes() != 0) {
            $scope.chart.DragIndex = -1;
            return;
        }

        tmpmode = 0;
        var isRightMB;
        evt = evt || window.event;
        if ("which" in evt)  // Gecko (Firefox), WebKit (Safari/Chrome) & Opera
            isRightMB = evt.which == 3;
        else if ("button" in evt)  // IE, Opera 
            isRightMB = e.button == 2;

        if (isRightMB) {
            $scope.chart.VoidDrag();
        }

        if (middlepoint != 0)
            middlepoint = 0;

        Body.className = "";
        Body.style.cursor = "default";
        //change connection source/dest

        if (current_connection_source != -1) {
            var new_node_id = -1;

            for (var k = 0 ; k < $scope.chart.GetNumNodes() ; k++) {
                if (evt.clientX >= $scope.chart.GetNodeX(k) &&
            evt.clientX <= $scope.chart.GetNodeX(k) + 100
            && evt.pageY >= $scope.chart.GetNodeY(k) &&
            evt.pageY <= $scope.chart.GetNodeY(k) + $scope.chart.GetNodeHeight(k)) {
                    new_node_id = $scope.chart.GetNode(k).data.id;
                }
            }

            if (new_node_id != -1) {
                var main_source = current_connection_source;
                if (current_connection_dir == "source") {
                    current_connection_source = new_node_id;
                }
                else {
                    current_connection_dest = new_node_id;
                }

                var valid = true;
                if (current_connection_dir == "source") {
                    if ($scope.chart.GetNodeById(current_connection_source).data.numChild >= $scope.chart.GetNodeById(current_connection_source).data.outputcount) {
                        valid = false;
                    }
                }

                if (valid) {
                    $scope.chart.deleteSelected();
                    $scope.chart.GetNodeById(main_source).data.numChild--;
                    $scope.chart.GetNodeById(current_connection_source).data.numChild++;
                    $scope.chart.createNewConnection($scope.chart.GetNodeById(current_connection_source).outputConnectors[$scope.chart.GetNodeById(current_connection_source).data.numChild - 1],
                        $scope.chart.GetNodeById(current_connection_dest).inputConnectors[0], false);
                }
                current_connection_source = current_connection_dest = -1;
            }
        }
        //end change connection source/dest

        if ($scope.chart.GetCurrentDragIndex() == -1) {
            return;
        }

        Body.className = "";
        var parent = "";

        for (var k = 1 ; k < $scope.chart.GetNumNodes() ; k++) {
            if (evt.clientX >= parseInt($scope.chart.GetNodeX(k)) && evt.clientX <= parseInt($scope.chart.GetNodeX(k)) + parseInt($scope.chart.GetNodeWidth(k))
                        && evt.clientY >= parseInt($scope.chart.GetNodeY(k)) && evt.clientY <= parseInt($scope.chart.GetNodeY(k)) + parseInt($scope.chart.GetNodeHeight(k)) &&
                                $scope.chart.GetNodeVisible(k)) {
                parent = $scope.chart.GetNode(k).data.id;
            }
        }

        var node;
        var bigInserted = false;
        var nodex;
        var nodey;
        var connected = false;

        //check maximum number of children for a FOREACH node (currently 1)
        if (ForeachActive) {
            //find number of current ForEach node
            var numForeachChildren = 0;
            var foreachFirstChildType = null;
            for (var n = 0 ; n < $scope.chart.nodes.length; n++) {
                if ($scope.chart.nodes[n].data.rootNodeIDForeach == SelectedForEachNode.data.id) {
                    if (foreachFirstChildType == null) {
                        foreachFirstChildType = $scope.chart.nodes[n].data.typeIndex;
                    }
                    numForeachChildren++;
                }
            }
            if (numForeachChildren == 1 && foreachFirstChildType != 1) {
                return;
            }
        }

        if (parent.toString() == "") {

            var topOffset = window.screenY + (window.outerHeight - window.innerHeight);
            var leftOffset = window.screenX;
            if (topOffset < 0)
                topOffset = 0;
            if (leftOffset < 0)
                leftOffset

            var screenx = 0;
            var screeny = 0;

            if (evt.type == 'touchstart' || evt.type == 'touchmove' || evt.type == 'touchend' || evt.type == 'touchcancel') {
                var touch = evt.touches[0] || evt.changedTouches[0];
                screenx = touch.pageX;
                screeny = touch.pageY;
            } else if (evt.type == 'mousedown' || evt.type == 'mouseup' || evt.type == 'mousemove' || evt.type == 'mouseover' || evt.type == 'mouseout' || evt.type == 'mouseenter' || evt.type == 'mouseleave') {
                screenx = evt.screenX;
                screeny = evt.screenY;
            }

            nodex = (screenx - divDrop.offsetLeft - leftOffset) + divContainer.scrollLeft;
            nodey = (screeny - divDrop.offsetTop - topOffset) + divContainer.scrollTop;
        }
        else {
            node = $scope.chart.GetNodeById(parent);
            node.data.connected = true;
            connected = true;
            if (node.data.numChild >= node.data.outputcount) {
                return;
            }



            if (node.data.numChild >= node.data.outputConnectors.length)
                return;

            node.data.numChild++;

            var b = 150;
            if (node.data.outputcount == 2) {
                b = 0;
            }
            if (node.data.numChild == 1) {
                nodex = node.x() + (-150) + (b * node.data.numChild);
            }
            else {
                nodex = node.x() + (150) + (b * node.data.numChild);
            }
            if (nodex < 20)
                nodex = 20;
            nodey = node.y() + 150;
        }
        var ind = $scope.chart.GetCurrentDragIndex() - 1;
        var sw = $scope.chart.GetShapeWidthList(ind);
        var sh = $scope.chart.GetShapeHeightList(ind);
        if (ind == 5 && $scope.chart.GetNumNodes() == 0) {
            sw = initialWidth;
            sh = initialHeight;
        }
        if (ind == 6 || ind == 5) {
            imgResize.style.display = "";
            divTools.style.display = "";
            btnVariables.style.display = btnArguments.style.display = "";
            btnImports.style.display = "";
            //btnToXAML.style.display = "";
            btnToCSharp.style.display = "";
            //btnFromCSharp.style.display = "";
            btnSave.style.display = "";
            currentActivityNumber++;
        }
        $scope.chart.VoidDrag();

        close_mode = false;
        close_mode_add = false;

        if ($scope.chart.GetNumNodes() == 0)//program init?
        {
            program_init = true;
            if (ind != 5) {
                noFlowchartMode = true;
            }
        }

        var finalX = 0;
        var finalY = 0;
        for (var i = 0 ; i < 1000; i++) {
            if (nodex >= i * snapX && nodex < (i + 1) * snapX) {
                finalX = i * snapX;
            }
        }
        for (var i = 2 ; i < 1000; i++) {
            if (nodey >= i * snapY && nodey < (i + 1) * snapY) {
                finalY = i * snapY - (snapY / 2);
            }
        }

        if (noFlowchartMode) {
            finalX = 0;
            if (ind == 0) {
                finalY = 185;
            }
            else if (ind == 1) {
                finalY = 185;
            }
        }

        if (ForeachActive) {
            lblForEachLabel.style.display = "none";
            finalX = (600 - parseInt(sw)) / 2;
            finalY = (600 - parseInt(sh)) / 2;
        }
        $scope.chart.CreateNewNode($scope.chart.GetNumNodes(), finalX, finalY, parent, sw, sh, ind + 1, false, nextGUID);
        if (ForeachActive) {
            divDrop.style.backgroundColor = "white";
            document.getElementsByClassName("ActivityItem")[0].style.backgroundColor = "white";
            if (SelectedForEachNode != null) {
                $scope.chart.nodes[$scope.chart.nodes.length - 1].data.rootNodeIDForeach = SelectedForEachNode.data.id;
            }
        }

        $scope.chart.nodes[$scope.chart.nodes.length - 1].SetConnctorData($scope.chart.GetConnectorItemInformation());
        $scope.chart.FindMinWidthHeight();
        divDrop.style.backgroundColor = "white";
        document.getElementsByClassName("ActivityItem")[0].style.backgroundColor = "white";
        //check minimum height after new node has been added, and increase flowchart height if new item is out of the flowchart
        var realheight = parseInt(divDrop.style.height.replace("px", ""));
        if (minHeight + 40 > realheight && !ForeachActive) {
            divSettings.style.display = "none";
            divAddIcon.style.display = "none";
            divAddDialog.style.display = "none";
            if (minHeight <= maxFlowchartHeight + initialHeight) {
                var h = minHeight + 40;
                divDrop.style.height = h + "px";
                divDrop.style.height = (h) + "px";
                $("#divChart").height((h));
                var svg = document.getElementsByTagName("svg");
                svg[0].setAttribute("height", h);
                svg[0].style.height = (h) + "px";
            }
        }
        //

        nextGUID++;
        if (!program_init) {
            var UndoObject = { action: "addnode", id: $scope.chart.nodes[$scope.chart.nodes.length - 1].data.id, x: nodex, y: nodey, typeIndex: ind + 1, GUID: (nextGUID - 1) };
            $scope.chart.AddToUndoList(UndoObject);
            RedoList.length = 0;
        }

        try{
            if (parent.toString() != "") {
                var mycaption = node.outputConnectors[node.data.numChild - 1].caption();
                $scope.chart.createNewConnection(node.outputConnectors[node.data.numChild - 1], $scope.chart.GetNode($scope.chart.GetNumNodes() - 1).inputConnectors[0],
                    false, 50, 0, 35, 0, 35, outy, 35, 150, 1);
                $scope.chart.SetConnectionCaption($scope.chart.connections[$scope.chart.connections.length - 1], mycaption);
            }
        }
        catch (ex) {
            //alert(ex.message);
        }

        try
        {
            if (parseInt(divDrop.style.width.replace("px", "")) >= initialWidth) {
                //object inserted near the border? englarge screen to cover the newly inserted object

                var bottomY = -10000;
                var rightX = -10000;
                for (var k = 1 ; k < $scope.chart.GetNumNodes() ; k++) {

                    if ($scope.chart.GetNode(k).data.y > bottomY && $scope.chart.GetNodeVisible(k)) {
                        bottomY = $scope.chart.GetNode(k).data.y;
                    }

                    if ($scope.chart.GetNode(k).data.x > rightX && $scope.chart.GetNodeVisible(k)) {
                        rightX = $scope.chart.GetNode(k).data.x;
                    }
                }

                var realwidth = parseInt(divDrop.style.width.replace("px", ""));
                if (parseInt(rightX) + parseInt(sw) > realwidth) {
                    divSettings.style.display = "none";
                    divAddIcon.style.display = "none";
                    divAddDialog.style.display = "none";
                    divDrop.style.width = (minWidth) + "px";
                    var w = minWidth;
                    $("#divChart").width((w));
                    $("#divFlowChartParent").width((w));
                    $("#divFlowChartParentTitle").width((w));
                    var svg = document.getElementsByTagName("svg");
                    svg[0].setAttribute("width", w);
                    svg[0].style.width = (w) + "px";
                    CenterDrop();
                }

                //
                return;
            }

            var w = 100;
            var h = 150;
            try
            {
                if (ind == 5) {
                    $rootScope.ChangeFlowchartIndex();
                    w = initialWidth;
                    h = initialHeight;
                    bigMode = true;
                }
                else {
                    divDrop.style.width = sw + "px";
                    divDrop.style.height = sh + "px";
                    var svg = document.getElementsByTagName("svg");
                    svg[0].setAttribute("height", sh);
                    svg[0].style.height = sh + "px";
                    svg[0].setAttribute("width", sw);
                    svg[0].style.width = sw + "px";
                    svg[0].style.position = "relative";
                    svg[0].style.backgroundColor = "transparent";
                    divChart.style.backgroundColor = "transparent";
                    divChart.style.position = "relative";
                    divChart.style.top = "-200px";
                    divChart.style.width = sw + "px";
                    divChart.style.height = sh + "px";
                    divDrop.style.backgroundColor = "transparent";
                    $('.in').css("display", "none");
                    $('.out1').css("display", "none");
                    return;
                }
            }
            catch (ex) {
                //alert(ex.message + " , " + ex.number);
            }
            divDrop.style.width = w + "px";
            divDrop.style.height = h + "px";
            divChart.style.width = sw + "px";
            divChart.style.height = sh + "px";
            var svg = document.getElementsByTagName("svg");
            svg[0].setAttribute("height", h);
            svg[0].style.height = h + "px";
            svg[0].setAttribute("width", w);
            svg[0].style.width = w + "px";
            var newnode = $scope.chart.GetNodeById(0);

            newnode.data.x = 0;
            newnode.data.y = 0;
            CenterDrop();

            divDrop.style.top = "0px";
            //add (initial) start node

            var newNodeDataModel = {
                name: "Example Node 1",
                id: nextGUID,//$scope.chart.GetNumNodes(),
                GUID: nextGUID,
                x: (divDrop.clientWidth / 2) - ($scope.chart.GetShapeWidthList(8 - 1) / 2) + 1,
                y: 50,
                width: 300,
                parent: parent,
                collapseable: true,
                collapsed: true,
                collapseWidth: 100,
                collapseHeight: 150,
                iconWidth: 24,
                iconHeight: 24,
                iconX: 3,
                typeIndex: 8,
                iconY: 3,
                shapeWidth: $scope.chart.GetShapeWidthList(8 - 1),
                shapeHeight: $scope.chart.GetShapeHeightList(8 - 1),
                z: 0,
                visible: true,
                rootNodeID: currentRootNodeID,
                rootNodeIDForeach: "",
                ForeachItemName: "item",
                ForeachListName: "list",
                currentActivityNumber: currentActivityNumber,
                numChild: 0,
                collapseIconX: 236,
                collapseIconY: 0,
                title: $scope.chart.GetName(8 - 1),
                titleX: 20,
                titleY: 20,
                backColor: $scope.chart.GetBackColor(8 - 1),
                origBackColor: $scope.chart.GetBackColor(8 - 1),
                iconCollapsedWidth: 50,
                iconCollapsedHeight: 50,
                iconCollapsedX: 25,
                mouseOver: false,
                iconCollapsedY: 50,
                collapserVisible: true,
                collapserMouseOver: false,
                titleVisible: true,
                icon: $scope.chart.GetIcons(8 - 1),
                inputcount: 0,
                outputcount: 1,
                showSettings: true,
                settingsX: 80,
                ShowHover: false,
                settingsY: 130,
                clickX: 5,
                clickY: 130,
                defaultPosition: true,
                nextInputConnector: 0,
                parentIDs: [],
                childrenGUIDs: [],
                childrenGUIDsTEMP: [],
                connected: false,
                covered: false,
                hideconnectors: false,
                connectorclicked: false,
                connectordata: { name: 'visualizy', internalType: 'couchbase' },
                activeConnector: "",
                activeConnectorInput: "",
                dicarray: [{ key: "z", value: "1000" }],
                dragcovered: false,
                outputConnectors: [
                    {
                        name: "o1",
                        x: 115 - 95 + 15,
                        y: 150,
                        numConnection: 0,
                        connected: false,
                        connectorCircleRadius: 5,
                        caption: "",
                    },
                ],
            };
            $scope.chart.addNode(newNodeDataModel);
            var UndoObject = { action: "addnode", id: $scope.chart.nodes[$scope.chart.nodes.length - 1].data.id, GUID: nextGUID, typeIndex: 8 };
            nextGUID++;
            $scope.chart.AddToUndoList(UndoObject);
            RedoList.length = 0;

            //add (initial) start node
        }
        catch (ex) {
            //alert(ex.message + " , " + ex.number);
        }

    };

    $scope.keyup = function (evt) {

    };

    $scope.mouseDown = function (evt) {
        $scope.chart.deselectAll();
        if (tmpcon != null) {
            try {
                $scope.AddPoint(tmpcon, 1, evt.clientX, evt.clientY);
                tmpcon = null;
            }
            catch (ex) {
                //alert(ex.message + " , " + ex.number);
            }
        }

        for (var i = 0 ; i < $scope.chart.GetNumNodes() ; i++) {
            $scope.chart.GetNode(i).data.ShowHover = false;
        }
        var startPoint;
        dragging.startDrag(evt, {

            //
            // Commence dragging... setup variables to display the drag selection rect.
            //
            dragStarted: function (x, y) {
                $scope.dragSelecting = true;
                startPoint = controller.translateCoordinates(evt.clientX - ($("#divDrop").position().left + 30),
                    evt.clientY - ($("#divDrop").position().top + 30), evt);
                $scope.dragSelectionStartPoint = startPoint;
                $scope.dragSelectionRect = {
                    x: evt.clientX,
                    y: evt.clientY,
                    width: 0,
                    height: 0,
                };
            },

            //
            // Update the drag selection rect while dragging continues.
            //
            dragging: function (x, y) {
                var curPoint = controller.translateCoordinates(x - ($("#divDrop").position().left + 30),
                    y - ($("#divDrop").position().top + 30), evt);
                $scope.dragSelectionRect = {
                    x: curPoint.x > startPoint.x ? startPoint.x : curPoint.x,
                    y: curPoint.y > startPoint.y ? startPoint.y : curPoint.y,
                    width: curPoint.x > startPoint.x ? curPoint.x - startPoint.x : startPoint.x - curPoint.x,
                    height: curPoint.y > startPoint.y ? curPoint.y - startPoint.y : startPoint.y - curPoint.y,
                };
            },

            //
            // Dragging has ended... select all that are within the drag selection rect.
            //
            dragEnded: function () {
                $scope.dragSelecting = false;
                $scope.chart.applySelectionRect($scope.dragSelectionRect);
                delete $scope.dragSelectionStartPoint;
                delete $scope.dragSelectionRect;
            },
        });
    };

    //this function is responsible for creating the ghost path (the temporary path that is drawn between the source node and mouse cursor before the final path has been created)
    $scope.CreateGhostPath = function (src, dest) {
        var path;
        if (src.x == dest.x && src.y == dest.y)
            return "";
        path = " m " + src.x + " , " + src.y;
        if (dest.x == src.x) {//vertical line
            path += " L " + dest.x + " , " + dest.y;
        }
        else if (src.y == dest.y) {//horizontal line
            path += " L " + dest.x + " , " + dest.y;
        }
        else {
            path += " L " + src.x + " , " + dest.y + " L " + dest.x + " , " + dest.y;
        }

        return path;
    }

    //
    // Called for each mouse move on the svg element.
    //
    $scope.mouseMove = function (evt) {

        var offset_y = 250 - 55 - 50;
        if ($(".mainHeader").css('display') == "none")
            offset_y = 196 - 55 - 50;

        var GhostPathDest = {
            x: parseInt(evt.clientX) - parseInt(divDrop.offsetLeft) + parseInt(divContainer.scrollLeft),
            y: parseInt(evt.clientY) - parseInt(offset_y) + parseInt(divContainer.scrollTop)
        };

        if (GhostPathSrc.x != -1000) {
            $scope.chart.GhostPath = $scope.CreateGhostPath(GhostPathSrc, GhostPathDest);
        }

        try {
            if (SelectedPointIndex != -1 && SelectedConnectionIndex != -1 && !ResizeOperationActive) {
                var point = SelectedConnectionPoint;
                var connection = $scope.chart.connections[SelectedConnectionIndex];
                if (point.index == 0 || point.index == connection.PathPointArray.length - 1) {
                    if (point.index == 0)
                        LineSwitchMode = "src";
                    else
                        LineSwitchMode = "dest";
                    LineSwitchConnectionIndex = SelectedConnectionIndex;

                    if (GhostPathSrc.x == -1000) {
                        if (LineSwitchMode == "dest") {
                            GhostPathSrc = {
                                x: $scope.chart.connections[LineSwitchConnectionIndex].PathPointArray[0].x,
                                y: $scope.chart.connections[LineSwitchConnectionIndex].PathPointArray[0].y
                            };
                        }
                        else {
                            var l = $scope.chart.connections[LineSwitchConnectionIndex].PathPointArray.length;
                            GhostPathSrc = {
                                x: $scope.chart.connections[LineSwitchConnectionIndex].PathPointArray[l - 1].x,
                                y: $scope.chart.connections[LineSwitchConnectionIndex].PathPointArray[l - 1].y
                            };
                        }
                    }

                    var posX = $("#divDrop").offset().left, posY = $("#divDrop").offset().top;
                    var x = evt.pageX - posX;
                    var y = evt.pageY - posY;
                    connection.PathPointArray[point.index] = { x: x, y: y, index: point.index };
                }
                return;
                var outputx;
                var outputy;
                if (connection.sourceOutputID == 1) {
                    outputy = connection.oy1;
                    outputx = connection.ox1;
                }
                else {
                    outputy = connection.oy2;
                    outputx = connection.ox2;
                }

                var inputOffset = { x: 0, y: 0 };
                var outputOffset = { x: 0, y: 0 };
                var inputDir = this.inputDir;
                var outputDir = this.outputDir;

                if (outputDir == "down") {
                    outputOffset.y = 30;
                }
                else if (outputDir == "up") {
                    outputOffset.y = -30;
                }
                else if (outputDir == "left") {
                    outputOffset.x = -30;
                }
                else if (outputDir == "right") {
                    outputOffset.x = 30;
                }

                var srcPoint = {
                    x: $scope.RoundNumbers(parseInt(connection.source.parentNode().x()) + parseInt(outputx) + 15 + outputOffset.x),
                    y: $scope.RoundNumbers(parseInt(connection.source.parentNode().y()) + parseInt(outputy) + outputOffset.y)
                };
                var destPoint = {
                    x: $scope.RoundNumbers(parseInt(connection.dest.parentNode().x()) + parseInt(connection.inputx) + 15 + inputOffset.x),
                    y: $scope.RoundNumbers(parseInt(connection.dest.parentNode().y()) + parseInt(connection.inputy) + inputOffset.y)
                };


                if (inputDir == "up") {
                    inputOffset.y = -30;
                }
                else if (inputDir == "right") {
                    inputOffset.x = 30;
                }
                else if (inputDir == "left") {
                    inputOffset.x = -30;
                }

                if (!connection.customModeFirstNodeAdded) {//insert a point before all existing points
                    //connection.PathPointArray.unshift({ x: srcPoint.x, y: srcPoint.y, index: 0 });
                    //for (var i = 1 ; i < connection.PathPointArray.length; i++) {
                    //    connection.PathPointArray[i].index = i;
                    //}
                    //connection.customModeFirstNodeAdded = true;
                }

                //var posX = $("#divDrop").offset().left, posY = $("#divDrop").offset().top;
                //var x = evt.pageX - posX;
                //var y = evt.pageY - posY;
                //first and last points CANNOT be moved (either directly by user or indirectly via algorithm)
                if (point.index == 0 || point.index == connection.PathPointArray.length - 1) {
                    // return;
                }

                //find neighbor nodes
                if (beforeNodeIndex == -1 && afterNodeIndex == -1) {
                    beforeNodeIndex = point.index - 1;
                    afterNodeIndex = point.index + 1;

                    beforeNode = connection.PathPointArray[beforeNodeIndex];
                    afterNode = connection.PathPointArray[afterNodeIndex];
                    if (beforeNodeIndex == 0)//before node is the first node? so create a new node, and use the newly created node as the before node
                    {
                        //horizontal or vertical?
                        //make the new node half the way between  selected node (active node) and the last node
                        var newX;
                        var newY;
                        if (beforeNode.x == point.x)//vertical
                        {
                            newX = point.x;
                            newY = point.y + ((beforeNode.y - point.y) / 2);
                        }
                        else {//horizontal
                            newX = point.x + ((beforeNode.x - point.x) / 2);
                            newY = point.y;
                        }
                        connection.PathPointArray.splice(1, 0, { x: newX, y: newY });
                        connection.PathPointArray.splice(1, 0, { x: newX, y: newY });

                        for (var j = 1 ; j < connection.PathPointArray.length; j++) {
                            connection.PathPointArray[j].index = j;
                        }
                        beforeNode = connection.PathPointArray[2];
                        beforeNodeIndex = 2;
                        afterNodeIndex += 2;
                    }
                    if (afterNodeIndex == connection.PathPointArray.length - 1) {//after node is the last node? so create a new node and use the newly created node as the after node
                        //horizontal or vertical?
                        //make the new node half the way between  selected node (active node) and the last node
                        var newX;
                        var newY;
                        if (afterNode.x == point.x)//vertical
                        {
                            newX = point.x;
                            newY = point.y + ((afterNode.y - point.y) / 2);
                        }
                        else {//horizontal
                            newX = point.x + ((afterNode.x - point.x) / 2);
                            newY = point.y;
                        }
                        connection.PathPointArray.splice(point.index + 1, 0, { x: newX, y: newY });
                        connection.PathPointArray.splice(point.index + 1, 0, { x: newX, y: newY });
                        for (var j = 1 ; j < connection.PathPointArray.length; j++) {
                            connection.PathPointArray[j].index = j;
                        }
                        afterNode = connection.PathPointArray[afterNodeIndex];
                    }
                }
                var m = 0;
                var a = afterNode;
                var b = beforeNode;

                var beforeChainType;
                var afterChainType;

                if (point.index > 0 && point.index < connection.PathPointArray.length - 1 && ChainedPointIndex == -1)//if the currently selected point is not the first or last node
                {
                    if (connection.PathPointArray[point.index - 1].x == point.x)
                        beforeChainType = "v";
                    if (connection.PathPointArray[point.index - 1].y == point.y)
                        beforeChainType = "h";
                    if (connection.PathPointArray[point.index + 1].x == point.x)
                        afterChainType = "v";
                    if (connection.PathPointArray[point.index + 1].y == point.y)
                        afterChainType = "h";
                    if (connection.PathPointArray[point.index - 1].x == point.x && connection.PathPointArray[point.index - 1].y == point.y && afterChainType == "h") {
                        ChainedPointIndex = point.index - 1;
                    }
                    if (connection.PathPointArray[point.index + 1].x == point.x && connection.PathPointArray[point.index + 1].y == point.y && beforeChainType == "h") {
                        ChainedPointIndex = point.index + 1;
                    }
                }

                if (a.x > b.x && a.y > b.y)//1, 3
                {
                    if (a.x > point.x && point.y > b.y) {
                        a.y = y;
                        b.x = x;
                        m = 1;
                    }
                    else {
                        a.x = x;
                        b.y = y;
                        m = 2;
                    }
                }

                if (b.x > a.x && b.y > a.y)//1, 3
                {
                    if (b.x > point.x && point.y > a.y) {
                        b.y = y;
                        a.x = x;
                        m = 3;
                    }
                    else {
                        b.x = x;
                        a.y = y;
                        m = 4;
                    }
                }

                if (b.y > a.y && a.x > b.x)//2, 4
                {
                    if (a.x > point.x && b.y > point.y) {
                        a.y = y;
                        b.x = x;
                        m = 5;
                    }
                    else {
                        a.x = x;
                        b.y = y;
                        m = 6;
                    }
                }

                if (a.y > b.y && b.x > a.x)//2, 4
                {
                    if (b.x > point.x && a.y > point.y) {
                        b.y = y;
                        a.x = x;
                        m = 7;
                    }
                    else {
                        b.x = x;
                        a.y = y;
                        m = 8;
                    }
                }

                //junctions
                if (ChainedPointIndex == -1) {
                    if (b.x > a.x && b.y == a.y) {
                        if (point.x > a.x) {
                            a.y = y;
                            b.x = x;
                            m = 9;
                        }
                        else {
                            a.x = x;
                            b.y = y;
                            m = 10;
                        }
                    }

                    if (a.y > b.y && a.x == b.x) {
                        if (point.y > b.y) {
                            a.y = y;
                            b.x = x;
                            m = 11;
                        }
                        else {
                            a.x = x;
                            b.y = y;
                            m = 12;
                        }
                    }

                    if (a.x > b.x && a.y == b.y) {
                        if (a.x > point.x) {
                            a.y = y;
                            b.x = x;
                            m = 13;
                        }
                        else {
                            a.x = x;
                            b.y = y;
                            m = 14;
                        }
                    }

                    if (b.y > a.y && b.x == a.x) {
                        if (b.y > point.y) {
                            a.y = y;
                            b.x = x;
                            m = 15;
                        }
                        else {
                            a.x = x;
                            b.y = y;
                            m = 16;
                        }
                    }
                }
                if (ChainedPointIndex == -1) {
                    afterNode = a;
                    beforeNode = b;
                }
                connection.PathPointArray[point.index] = { x: x, y: y, index: point.index };
                point = SelectedConnectionPoint = { x: x, y: y, index: point.index };
                if (ChainedPointIndex != -1) {
                    if (ChainedPointIndex == point.index + 1) {//after chain
                        connection.PathPointArray[point.index - 1].y = point.y;
                        connection.PathPointArray[point.index + 2].y = point.y;
                    }
                    else//before chain
                    {
                        connection.PathPointArray[point.index - 2].y = point.y;
                        connection.PathPointArray[point.index + 1].y = point.y;
                    }
                }

                if (ChainedPointIndex == -1) {
                    connection.PathPointArray[beforeNodeIndex].x = beforeNode.x;
                    connection.PathPointArray[beforeNodeIndex].y = beforeNode.y;

                    connection.PathPointArray[afterNodeIndex].x = afterNode.x;
                    connection.PathPointArray[afterNodeIndex].y = afterNode.y;
                }

                if (ChainedPointIndex != -1) {
                    connection.PathPointArray[ChainedPointIndex].x = point.x;
                    connection.PathPointArray[ChainedPointIndex].y = point.y;
                }

                //make the new path and pass it to the Path function, so it can be displayed
                var newPath = "";

                for (var i = 0 ; i < connection.PathPointArray.length; i++) {
                    if (i == 0) {
                        newPath += " M ";
                    }
                    else {
                        newPath += " L ";
                    }
                    newPath += connection.PathPointArray[i].x + " , " + connection.PathPointArray[i].y;
                }

                connection.customLineData = newPath;
            }
        }
        catch (ex) {
            divDebug.innerHTML = ex.message;
        }

        if (tmpmode != 0) {
            $scope.TmpPath(tmpcon, evt.clientX, evt.clientY, tmpmode);
        }
            var hoverID = -1;
        try {
            var currentNodeIndex = $scope.chart.GetSelectedNodeIndex();
            for (var k = 1 ; k < $scope.chart.GetNumNodes() ; k++) {
                $scope.chart.GetNode(k).data.backColor = "#2ab989";
                $scope.chart.GetNode(k).data.covered = false;
                var ind = $scope.chart.GetNode(k).data.typeIndex - 1;
                var myX = parseInt(evt.clientX) - parseInt(divDrop.offsetLeft) - parseInt(divContainer.scrollLeft);
                var myY = parseInt(evt.clientY) - parseInt(offset_y) + parseInt(divContainer.scrollTop);
                if (myX >= $scope.chart.GetNodeX(k) && myX <= parseInt($scope.chart.GetNodeX(k)) + parseInt($scope.chart.GetShapeWidthList(ind))
            && myY >= $scope.chart.GetNodeY(k) && myY <= parseInt($scope.chart.GetNodeY(k)) + parseInt($scope.chart.GetShapeHeightList(ind)) && $scope.chart.GetNodeVisible(k)) {
                    $scope.chart.GetNode(k).data.covered = true;
                    hoverID = $scope.chart.GetNode(k).data.id;
                }
            }
        }
        catch (ex) {

        }
        if (hoverID != -1) {
            $scope.chart.GetNodeById(hoverID)._hovered = true;
            if ($scope.chart.GetNodeById(hoverID).data.numChild < $scope.chart.GetNodeById(hoverID).data.outputcount) {
                $scope.chart.GetNodeById(hoverID).data.backColor = "#22a2d4";
            }
            else {
                if ($scope.chart.GetCurrentDragIndex() != -1)
                    Body.className = "cursor-nodrop";
            }
        }

        //
        // Clear out all cached mouse over elements.
        //
        $scope.mouseOverConnection = null;
        $scope.mouseOverConnector = null;

        var mouseOverElement = controller.hitTest(evt.clientX, evt.clientY);
        if (mouseOverElement == null) {
            // Mouse isn't over anything, just clear all.
            return;
        }

        if (!$scope.draggingConnection) { // Only allow 'connection mouse over' when not dragging out a connection.

            // Figure out if the mouse is over a connection.
            var scope = controller.checkForHit(mouseOverElement, controller.connectionClass);
            $scope.mouseOverConnection = (scope && scope.connection) ? scope.connection : null;
            if ($scope.mouseOverConnection) {
                // Don't attempt to mouse over anything else.
                return;
            }
        }

        // Figure out if the mouse is over a connector.
        var scope = controller.checkForHit(mouseOverElement, controller.connectorClass);
        $scope.mouseOverConnector = (scope && scope.connector) ? scope.connector : null;
        if ($scope.mouseOverConnector) {
            // Don't attempt to mouse over anything else.
            return;
        }

        // Figure out if the mouse is over a node.
        var scope = controller.checkForHit(mouseOverElement, controller.nodeClass);
    };

    $scope.AddPoint = function (connection, index, x, y) {
        try
        {
            connection.AddPoint(index, x, y);
        }
        catch (ex) {

        }
    };

    $scope.TmpPath = function (connection, x, y, number) {
        connection.MakeTmpPathX(x, number);
        connection.MakeTmpPathY(y, number);
    };

	$scope.connectorMouseEnter = function (evt, node, connector) {
	    connector.data.connectorCircleRadius = 8;
	};

	$scope.connectorMouseLeave = function (evt, node, connector) {
	    connector.data.connectorCircleRadius = 5;
	};

	$scope.nodeSettingsLeave = function (evt, node) {
	    var chart = $scope.chart;
	    node.data.ShowHover = false;
	};

	$scope.nodeMouseOverMove = function (evt, node) {

	};

	$scope.nodeMouseOver = function (evt, node) {
	    var chart = $scope.chart;
	    try{
	        $scope.mouseOverNode = node;
	        node.data.mouseOver = true;
	        if (node.data.collapsed)
	            node.data.collapserVisible = true;
	    }
	    catch (ex) {
	    }


	};

	$scope.collapserMouseOver = function (evt, node) {
	    node.data.collapserMouseOver = true;
	};

	$scope.collapserMouseOut = function (evt, node) {
	    node.data.collapserMouseOver = false;
	};

	$scope.nodeMouseMove = function (evt, node) {

	    try {
	        var currentNodeIndex = $scope.chart.GetSelectedNodeIndex();
	        if (currentNodeIndex == -1)
	            return;
	        var offset_y = 250 - 55 - 50;
	        if ($(".mainHeader").css('display') == "none")
	            offset_y = 196 - 55 - 50;

	        var currentNodeTopLeft = { x: $scope.chart.GetNodeX(currentNodeIndex), y: $scope.chart.GetNodeY(currentNodeIndex) };
	        for (var k = 1 ; k < $scope.chart.GetNumNodes() ; k++) {
	            $scope.chart.GetNode(k).data.covered = false;
	            var ind = $scope.chart.GetNode(k).data.typeIndex - 1;
	            var myX = parseInt(evt.clientX) - parseInt(divDrop.offsetLeft) - parseInt(divContainer.scrollLeft);
	            var myY = parseInt(evt.clientY) - parseInt(offset_y) + parseInt(divContainer.scrollTop);
	            if (myX >= $scope.chart.GetNodeX(k) && myX <= parseInt($scope.chart.GetNodeX(k)) + parseInt($scope.chart.GetShapeWidthList(ind))
            && myY >= $scope.chart.GetNodeY(k) && myY <= parseInt($scope.chart.GetNodeY(k)) + parseInt($scope.chart.GetShapeHeightList(ind)) && $scope.chart.GetNodeVisible(k)) {
	                if (k != currentNodeIndex) {
	                    $scope.chart.GetNode(k).data.covered = true;
	                    if ($scope.chart.GetNode(k).data.numChild == $scope.chart.GetNode(k).data.outputConnectors.length) {
	                        $scope.chart.GetNode(k).data.covered = "nodrop";
	                    }
	                }
	            }
	        }
	    }
	    catch (ex) {
	        //alert(ex.message + " , " + ex.number);
	    }

	};

	$scope.nodeMouseOut = function (evt, node) {
	    var chart = $scope.chart;
	    node.data.mouseOver = false;
	    if (node.data.collapsed)
	        node.data.collapserVisible = false;
	};

	$scope.RemoveGhostPath = function () {
	    GhostPathSrc = { x: -1000, y: -1000 };
	    $scope.chart.GhostPath = "";
	};

	$scope.nodeMouseUp = function (evt, node) {
	    $scope.RemoveGhostPath();

	    if (noFlowchartMode)
	        return;

	    if ($scope.chart.GetCurrentDragIndex() == -1) {
	        return;
	    }

	    Body.className = "";
	    var parent = "";

	    parent = node.data.id;

	    var node;
	    var bigInserted = false;
	    var nodex;
	    var nodey;
	    var connected = false;
	    var ind = $scope.chart.GetCurrentDragIndex() - 1;
	    if (parent.toString() == "") {
	        nodex = evt.clientX - (((divBody.clientWidth - divDrop.clientWidth) / 2) + 50 - divContainer.scrollLeft);
	        nodey = evt.clientY - 85 + divContainer.scrollTop;
	    }
	    else {
	        node = $scope.chart.GetNodeById(parent);
	        node.data.connected = true;
	        connected = true;
	        if (node.data.numChild >= node.data.outputcount) {
	            return;
	        }
	        if (node.data.numChild >= node.data.outputConnectors.length)
	            return;

	        node.data.numChild++;

	        var b = 150;
	        if (node.data.outputcount == 2) {
	            b = 0;
	        }
	        if (node.data.numChild == 1) {
	            nodex = node.x() + (-150) + (b * node.data.numChild);
	        }
	        else {
	            nodex = node.x() + (150) + (b * node.data.numChild);
	        }
	        nodey = node.y() + 150;

	        if (node.data.typeIndex != 1) {//parent item is NOT a FLOWDECISION (IF)
	            nodey = parseInt(node.y()) + parseInt(node.data.shapeHeight) + 70;
	            nodex = parseInt(node.x()) - ((parseInt($scope.chart.GetShapeWidthList(ind)) - parseInt(node.data.shapeWidth)) / 2);
	        }

	        if (nodex < 20)
	            nodex = 20;
	    }

	    var sw = $scope.chart.GetShapeWidthList(ind);
	    var sh = $scope.chart.GetShapeHeightList(ind);

	    if (ind == 5 && $scope.chart.GetNumNodes() == 0) {
	        sw = initialWidth;
	        sh = initialHeight;
	    }

	    if (ind == 6 || ind == 5) {
	        currentActivityNumber++;
	    }
	    var caps = $scope.chart.GetCaptionList(ind).split('_');
	    $scope.chart.VoidDrag();
	    Body.className = "";
	    Body.style.cursor = "default";
	    close_mode = false;
	    close_mode_add = false;
	    $scope.chart.CreateNewNode($scope.chart.GetNumNodes(), nodex, nodey, parent, sw, sh, ind + 1, connected, nextGUID);
	    nextGUID++;

	    var UndoObject = {
	        action: "addnode", id: $scope.chart.nodes[$scope.chart.nodes.length - 1].data.id, GUID: $scope.chart.nodes[$scope.chart.nodes.length - 1].data.GUID,
	        typeIndex: ind + 1
	    };
	    RedoList.length = 0;
	    $scope.chart.AddToUndoList(UndoObject);

	    try {
	        var outy = 150;
	        if (node.data.typeIndex == 8)
	            outy = 120;
	        if (parent.toString() != "") {
	            var mycaption = node.outputConnectors[node.data.numChild - 1].caption();
	            var o1 = $scope.OutputConnectorPosition1(node.data.typeIndex);
	            var o2 = $scope.OutputConnectorPosition2(node.data.typeIndex);
	            var _cap = "";
	            var first_empty_child_index = node.data.numChild;
	            if (node.data.typeIndex == 1) {//it is IF node? so find the FIRST empty output connector.
	                if (!node.outputConnectors[0].data.connected) {
	                    first_empty_child_index = 1;
	                }
	                else {
	                    first_empty_child_index = 2;
	                }
	            }

	            var outputPoint;
	            if (first_empty_child_index == 1) {
	                outputPoint = { x: o1.x, y: o1.y, dir: o1.dir };
	            }
	            else {
	                outputPoint = { x: o2.x, y: o2.y, dir: o2.dir };
	            }
	            var inputPoint = $scope.InputConnectorPosition($scope.chart.GetNode($scope.chart.GetNumNodes() - 1).data.typeIndex,
    { x: parseInt(node.data.x) + parseInt(outputPoint.x), y: parseInt(node.data.y) + parseInt(outputPoint.y) },
    $scope.chart.GetNode($scope.chart.GetNumNodes() - 1).data.id);

	            //
	            var inputDir = "up";
	            var outputDir = "down";
	            var destIndex = 0;
	            var srcIndex = 2;
	            var outputCurrentConnector = node.data.numChild;
	            var newlyCreatedNode = $scope.chart.GetNode($scope.chart.GetNumNodes() - 1);
	            destIndex = 0;
	            if (newlyCreatedNode.data.typeIndex == 1)//destination item is a FlowDecision
	            {
	                destIndex = 1;
	            }
	            inputDir = "up";
	            outputDir = "down";
	            if (node.data.typeIndex != 1) {//parent item is NOT a FLOWDECISION (IF)
	                nodey = node.y() + (parseInt(node.data.shapeHeight)) + 70;
	                nodex = parseInt(node.x()) - ((parseInt(newlyCreatedNode.data.shapeWidth) - parseInt(node.data.shapeWidth)) / 2);
	            }

	            if (node.data.typeIndex == 1) {//parent item is a FLOWDECISION (IF)
	                if (node.data.numChild == 1 || (node.data.outputConnectors[1].connected && node.data.numChild == 2)) {
	                    srcIndex = 4;
	                    inputDir = "up";
	                    outputDir = "left";
	                    destIndex = 0;
	                    nodex = node.x() - (parseInt(newlyCreatedNode.data.shapeWidth)) - 135;
	                    nodey = parseInt(node.y()) - ((parseInt(newlyCreatedNode.data.shapeHeight) - parseInt(node.data.shapeHeight)) / 2) + 120;
	                    outputCurrentConnector = 1;
	                    if (newlyCreatedNode.data.typeIndex == 1)//IF-IF
	                    {
	                        inputDir = "up";
	                        outputDir = "left";
	                        destIndex = 1;
	                        nodey = parseInt(node.y()) + 90;
	                    }
	                }
	                else {
	                    srcIndex = 5;
	                    inputDir = "up";
	                    outputDir = "right";
	                    destIndex = 0;
	                    nodex = parseInt(node.x()) + (parseInt(node.data.shapeWidth)) + 135;
	                    nodey = parseInt(node.y()) - ((parseInt(newlyCreatedNode.data.shapeHeight) - parseInt(node.data.shapeHeight)) / 2) + 120;
	                    outputCurrentConnector = 2;
	                    if (newlyCreatedNode.data.typeIndex == 1)//IF-IF
	                    {
	                        inputDir = "up";
	                        outputDir = "right";
	                        destIndex = 1;
	                        nodey = parseInt(node.y()) + 90;
	                    }
	                }
	            }

	            var tmpOutputPos;
	            var tmpInputPos;
	            for (var i = 0 ; i < connectorPositions.length; i++) {
	                if (connectorPositions[i].typeIndex == node.data.typeIndex && connectorPositions[i].index == srcIndex) {
	                    tmpInputPos = { x: parseInt(connectorPositions[i].x), y: parseInt(connectorPositions[i].y) };
	                }
	            }

	            for (var i = 0 ; i < connectorPositions.length; i++) {
	                if (connectorPositions[i].typeIndex == newlyCreatedNode.data.typeIndex && connectorPositions[i].index == destIndex) {
	                    tmpOutputPos = { x: parseInt(connectorPositions[i].x), y: parseInt(connectorPositions[i].y) };
	                }
	            }
                //
	            $scope.chart.createNewConnection(node.outputConnectors[node.data.numChild - 1],
    $scope.chart.GetNode($scope.chart.GetNumNodes() - 1).inputConnectors[0], false,
        tmpOutputPos.x, tmpOutputPos.y, tmpInputPos.x, tmpInputPos.y, tmpInputPos.x, tmpInputPos.y, outputCurrentConnector, "", "", inputDir, outputDir, $scope.chart);

	            if (nodex < 20)
	                nodex = 20;

	            newlyCreatedNode.data.x = $scope.RoundNumbers(nodex);
	            newlyCreatedNode.data.y = $scope.RoundNumbers(nodey);

	            UndoObject = {
	                action: "addconnection", sourceNodeID: node.data.id, x: 0, y: 0, srcNodeGUID: node.data.GUID,
	                dstNodeGUID: $scope.chart.GetNode($scope.chart.GetNumNodes() - 1).data.GUID,
	                id: $scope.chart.connections[$scope.chart.connections.length - 1].data.source.nodeID,
	                destNodeID: $scope.chart.GetNode($scope.chart.GetNumNodes() - 1).data.id,
	                ix: tmpOutputPos.x, iy: tmpOutputPos.y,
	                o1x: tmpInputPos.x, o2x: tmpInputPos.x, o1y: tmpInputPos.y, o2y: tmpInputPos.y, onumber: outputCurrentConnector, inputDir: inputDir, outputDir: outputDir
	            };

	            $scope.chart.AddToUndoList(UndoObject);
	            RedoList.length = 0;

	            UndoList[UndoList.length - 1].flwWidthAfter = divDrop.style.width;
	            UndoList[UndoList.length - 1].flwHeightAfter = divDrop.style.height;

	            var realheight = parseInt(divDrop.style.height.replace("px", ""));
	            var realwidth = parseInt(divDrop.style.width.replace("px", ""));
	            $scope.chart.FindMinWidthHeight();

	            if (minHeight + 40 > parseInt(realheight)) {
	                divSettings.style.display = "none";
	                divAddIcon.style.display = "none";
	                divAddDialog.style.display = "none";

	                var h = (minHeight + 40);
	                if (h <= maxFlowchartHeight + initialHeight) {
	                    divDrop.style.height = (h) + "px";
	                    $("#divChart").height((h));
	                    var svg = document.getElementsByTagName("svg");
	                    svg[0].setAttribute("height", h);
	                    svg[0].style.height = (h) + "px";
	                }
	            }

	            if (minWidth > realwidth) {
	                divSettings.style.display = "none";
	                divAddIcon.style.display = "none";
	                divAddDialog.style.display = "none";
	                divDrop.style.width = (minWidth) + "px";
	                var w = minWidth;
	                $("#divChart").width((w));
	                $("#divFlowChartParent").width((w));
	                $("#divFlowChartParentTitle").width((w));
	                var svg = document.getElementsByTagName("svg");
	                svg[0].setAttribute("width", w);
	                svg[0].style.width = (w) + "px";
	                CenterDrop();
	            }

	            $scope.chart.SetConnectionCaption($scope.chart.connections[$scope.chart.connections.length - 1], mycaption);
	        }
	    }
	    catch (ex) {
	        //alert(ex.message + " , " + ex.number);
	    }
	};

	var PositionBeforeDrag;

	$rootScope.nodeMouseDown = function (evt, node) {
	    $scope.nodeMouseDown(evt, node);
	};

	var timeout;
	var lastTap = 0;
	var touchInitialNodePosition = [];
	var touchNodeMoveStartPoint;
	nodeDragOBJDragStart = function (evt) {
	    var multipleSelectMode = false;
	    if (btnTouchMultiSelect.innerHTML == "MultiSelect On") {
	        multipleSelectMode = true;
	    }

	    touchNodeDragged = false;
	    $scope.dragSelecting = false;
	    try
	    {
	        var currentTime = new Date().getTime();
	        var tapLength = currentTime - lastTap;
	        clearTimeout(timeout);
	        if (tapLength < 500 && tapLength > 0) {
	            if ($scope.chart.GetNodeById(touchSelectedNodeID).data.typeIndex == 7)//switch root should be applied only for flowchart nodes
	            {
	                $scope.chart.SwitchRoot();
	            }
	            else if ($scope.chart.GetNodeById(touchSelectedNodeID).data.typeIndex == 13)//switch root foreach should be applied only for foreach nodes
	            {
	                $scope.chart.SwitchToForEach();
	            }

	            evt.preventDefault();
	        } else {
	            timeout = setTimeout(function () {
	                clearTimeout(timeout);
	            }, 500);
	        }
	        lastTap = currentTime;
	    }
	    catch (ex) {
	        //alert(ex.message);
	    }

	    if ($scope.chart.Lock)
	        return;
	    try {
	        var touchActiveNode = $scope.chart.GetNodeById(touchSelectedNodeID);

            //finding initial drag position, so that we can always have an offset value (amount of position dragged by user)
	        var touch = evt.touches[0] || evt.changedTouches[0];
	        screenx = touch.pageX;
	        screeny = touch.pageY;

	        var topOffset = window.screenY + (window.outerHeight - window.innerHeight);
	        var leftOffset = window.screenX;
	        if (topOffset < 0)
	            topOffset = 0;
	        if (leftOffset < 0)
	            leftOffset = 0;

	        var sp = { x: screenx - divDrop.offsetLeft - leftOffset + divContainer.scrollLeft, y: screeny - divDrop.offsetTop - topOffset + divContainer.scrollTop };
	        if (sp.x < 0)
	            sp.x = 0;
	        if (sp.y < 0)
	            sp.y = 0;
	        touchNodeMoveStartPoint = { x: sp.x, y: sp.y };
            //----

	        for (var k = 0 ; k < moveSprites.length; k++) {
	            divDrop.removeChild(moveSprites[k]);
	        }
	        moveSprites.length = 0;
	        touchInitialNodePosition.length = 0;
	        try
	        {
	            if ($scope.chart.getSelectedNodes().length != 0) {//if we have several objects selected, then make a for loop and show sprite for each of them
	                for (var j = 0 ; j < $scope.chart.getSelectedNodes().length; j++) {
	                    var tmpSprite = document.createElement("img");
	                    tmpSprite.src = "content/images/" + $scope.chart.getSelectedNodes()[j].data.typeIndex + ".png";
	                    tmpSprite.style.left = $scope.chart.getSelectedNodes()[j].data.x + "px";
	                    tmpSprite.style.top = $scope.chart.getSelectedNodes()[j].data.y + "px";
	                    touchInitialNodePosition.push({ x: $scope.chart.getSelectedNodes()[j].data.x, y: $scope.chart.getSelectedNodes()[j].data.y });
	                    tmpSprite.style.position = "absolute";
	                    moveSprites.push(tmpSprite);
	                    divDrop.appendChild(moveSprites[moveSprites.length - 1]);
	                }
	            }
	            else {//else (no object has been already selected) just show sprite for the currently selected (dragged) node
	                var tmpSprite = document.createElement("img");
	                tmpSprite.src = "content/images/" + touchActiveNode.data.typeIndex + ".png";
	                tmpSprite.style.position = "absolute";
	                tmpSprite.style.left = touchActiveNode.data.x + "px";
	                tmpSprite.style.top = touchActiveNode.data.y + "px";
	                touchInitialNodePosition.push({ x: touchActiveNode.data.x, y: touchActiveNode.data.y });
	                moveSprites.push(tmpSprite);
	                divDrop.appendChild(moveSprites[moveSprites.length - 1]);
	            }
	        }
	        catch (ex) {
	            divDebug.innerHTML = ex.message;
	        }

	        //applying redo/undo capability for touch move
	        var GUIDArray = new Array();
	        var xArray = new Array();
	        var yArray = new Array();

	        GUIDArray.push(touchActiveNode.data.GUID);
	        xArray.push(touchActiveNode.data.x);
	        yArray.push(touchActiveNode.data.y);

            //perform undo/redo operation for the current move action
	        var UndoObject = {
	            action: "nodemove", id: touchActiveNode.data.id, xArray: xArray, yArray: yArray, GUIDArray: GUIDArray,
	            flwWidthBefore: divDrop.style.width, flwHeightBefore: divDrop.style.height
	        };
	        $scope.chart.AddToUndoList(UndoObject);
	        UndoList[UndoList.length - 1].flwWidthAfter = divDrop.style.width;
	        UndoList[UndoList.length - 1].flwHeightAfter = divDrop.style.height;
	        RedoList.length = 0;

	        if (btnTouchMultiSelect.innerHTML != "MultiSelect On") {
	            touchActiveNode.select();
	        }
	        else {
	            try
	            {
	                touchActiveNode._selected = !touchActiveNode._selected;
	            }
	            catch (ex) {
	                //alert(ex.message);
	            }
	        }
	    }
	    catch (ex) {
	        divDebug.innerHTML = ex.message;
	    }
	};
	var touchNodeDragged = false;
	nodeDragOBJDragOver = function (evt) {
	    evt.preventDefault();
	    $scope.dragSelecting = false;
	    touchNodeDragged = true;
	    if ($scope.chart.Lock)
	        return;
	    try {
	        var touchActiveNode = $scope.chart.GetNodeById(touchSelectedNodeID);

	        var touch = evt.touches[0] || evt.changedTouches[0];
	        screenx = touch.pageX;
	        screeny = touch.pageY;

	        var topOffset = window.screenY + (window.outerHeight - window.innerHeight);
	        var leftOffset = window.screenX;
	        if (topOffset < 0)
	            topOffset = 0;
	        if (leftOffset < 0)
	            leftOffset = 0;

	        var sp = { x: screenx - divDrop.offsetLeft - leftOffset + divContainer.scrollLeft, y: screeny - divDrop.offsetTop - topOffset + divContainer.scrollTop };
	        if (sp.x < 0)
	            sp.x = 0;
	        if (sp.y < 0)
	            sp.y = 0;
	        for (var m = 0 ; m < moveSprites.length; m++) {
	            moveSprites[m].style.left = parseInt(touchInitialNodePosition[m].x) + (parseInt(sp.x) - parseInt(touchNodeMoveStartPoint.x)) + "px";
	            moveSprites[m].style.top = parseInt(touchInitialNodePosition[m].y) + (parseInt(sp.y) - parseInt(touchNodeMoveStartPoint.y)) + "px";
	        }
	    }
	    catch (ex) {
	        divDebug.innerHTML = ex.message;
	    }
	};
	nodeDragOBJDragEnd = function (evt) {
	    $scope.dragSelecting = false;
	    if (!touchNodeDragged)
	        return;
	    if ($scope.chart.Lock)
	        return;
	    try
	    {
	        var touchActiveNode = $scope.chart.GetNodeById(touchSelectedNodeID);

	        var touch = evt.touches[0] || evt.changedTouches[0];
	        screenx = touch.pageX;
	        screeny = touch.pageY;

	        var topOffset = window.screenY + (window.outerHeight - window.innerHeight);
	        var leftOffset = window.screenX;
	        if (topOffset < 0)
	            topOffset = 0;
	        if (leftOffset < 0)
	            leftOffset = 0;
	        var sp = { x: screenx - divDrop.offsetLeft - leftOffset + divContainer.scrollLeft, y: screeny - divDrop.offsetTop - topOffset + divContainer.scrollTop };
	        if (sp.x < 0)
	            sp.x = 0;
	        if (sp.y < 0)
	            sp.y = 0;

	        try
	        {
	            if ($scope.chart.getSelectedNodes().length != 0) {//if we have several objects selected, then make a for loop and show sprite for each of them
	                for (var j = 0 ; j < $scope.chart.getSelectedNodes().length; j++) {
	                    $scope.chart.getSelectedNodes()[j].data.x = parseInt(touchInitialNodePosition[j].x) + (parseInt(sp.x) - parseInt(touchNodeMoveStartPoint.x));
	                    $scope.chart.getSelectedNodes()[j].data.y = parseInt(touchInitialNodePosition[j].y) + (parseInt(sp.y) - parseInt(touchNodeMoveStartPoint.y));
	                }
	            }
	            else {
	                touchActiveNode.data.x = parseInt(touchInitialNodePosition[touchInitialNodePosition.length - 1].x) + (parseInt(sp.x) - parseInt(touchNodeMoveStartPoint.x));
	                touchActiveNode.data.y = parseInt(touchInitialNodePosition[touchInitialNodePosition.length - 1].y) + (parseInt(sp.y) - parseInt(touchNodeMoveStartPoint.y));
	            }
	        }
	        catch (ex) {
	            divDebug.innerHTML = ex.message;
	        }

	        touchActiveNode.data.x = parseInt(touchInitialNodePosition[touchInitialNodePosition.length - 1].x) + (parseInt(sp.x) - parseInt(touchNodeMoveStartPoint.x));
	        touchActiveNode.data.y = parseInt(touchInitialNodePosition[touchInitialNodePosition.length - 1].y) + (parseInt(sp.y) - parseInt(touchNodeMoveStartPoint.y));

	        if (moveDeltaX != 0 || moveDeltaY != 0) {//move selected nodes only after user has clicked on the destination (i.e. dragging has been finished)
	            //in this way we can save save a lot of CPU time which would be otherwise wasted for path finding algorithm
	            //chart.updateSelectedNodesLocation($scope.RoundNumbers(moveDeltaX), $scope.RoundNumbers(moveDeltaY), true);
	            for (var k = 0 ; k < moveSprites.length; k++) {
	                divDrop.removeChild(moveSprites[k]);
	            }
	            moveSprites.length = 0;
	        }
	        evt.preventDefault();
	        moveDeltaX = 0;
	        moveDeltaY = 0;

	        try {
	            var bottomY = -10000;
	            var rightX = -10000;
	            for (var k = 0 ; k < $scope.chart.GetNumNodes() ; k++) {
	                if ($scope.chart.GetNode(k).data.y > bottomY) {
	                    bottomY = $scope.chart.GetNode(k).data.y;
	                }

	                if ($scope.chart.GetNode(k).data.x > rightX) {
	                    rightX = $scope.chart.GetNode(k).data.x;
	                }
	            }

	            var realheight = parseInt(divDrop.style.height.replace("px", ""));
	            var realwidth = parseInt(divDrop.style.width.replace("px", ""));
	            $scope.chart.FindMinWidthHeight();

	            if (minHeight + 40 > parseInt(realheight)) {
	                divSettings.style.display = "none";
	                divAddIcon.style.display = "none";
	                divAddDialog.style.display = "none";

	                var h = (minHeight + 40);
	                if (h <= maxFlowchartHeight + initialHeight) {
	                    divDrop.style.height = (h) + "px";
	                    $("#divChart").height((h));
	                    var svg = document.getElementsByTagName("svg");
	                    svg[0].setAttribute("height", h);
	                    svg[0].style.height = (h) + "px";
	                }
	            }

	            if (minWidth + 40 > realwidth) {
	                divSettings.style.display = "none";
	                divAddIcon.style.display = "none";
	                divAddDialog.style.display = "none";
	                var w = parseInt(minWidth) + 40;

	                if (w <= (maxFlowchartWidth + initialWidth)) {
	                    divDrop.style.width = w + "px";
	                    $("#divChart").width((w));
	                    $("#divFlowChartParent").width((w));
	                    $("#divFlowChartParentTitle").width((w));
	                    var svg = document.getElementsByTagName("svg");
	                    svg[0].setAttribute("width", w);
	                    svg[0].style.width = (w) + "px";
	                }
	                CenterDrop();
	            }

	            UndoList[UndoList.length - 1].flwWidthAfter = divDrop.style.width;
	            UndoList[UndoList.length - 1].flwHeightAfter = divDrop.style.height;

	        }
	        catch (ex) {
	            divDebug.innerHTML = ex.message;
	        }

	    }
	    catch (ex) {
	        divDebug.innerHTML = ex.message;
	    }
	};

	var touchSelectedNodeID;

	$scope.nodeMouseDown = function (evt, node) {
	    touchSelectedNodeID = node.data.id;
	    if (node.data.id == 0)
	        return;
 
	    if (node.data.id != "0") {

	    }
	    if (tmpcon != null) {

	    }

		var chart = $scope.chart;
		var lastMouseCoords;
		//if (chart.Lock)
		//    return;
		if (node.data.id != "0") {

		}

	    try
	    {
	        evt.target.addEventListener("touchmove", nodeDragOBJDragOver, false);
	        evt.target.addEventListener("touchstart", nodeDragOBJDragStart, false);
	        evt.target.addEventListener("touchend", nodeDragOBJDragEnd, false);
	    }
	    catch (ex) {
	        divDebug.innerHTML = ex.message;
	    }

		dragging.startDrag(evt, {
            
			//
			// Node dragging has commenced.
			//
		    dragStarted: function (x, y) {
		        if (chart.Lock)
		            return;

		        spriteCancel = false;
		        connectorWiseConnectionMode = false;
		        node.data.defaultPosition = false;
		        lastMouseCoords = controller.translateCoordinates(x, y, evt);
		        currentActiveNodeForConnectionSourceID = node.data.id;
		        try
		        {
		            var GUIDArray = new Array();
		            var newobject = true;
		            var xArray = new Array();
		            var yArray = new Array();
		            for (var j = 0 ; j < chart.getSelectedNodes().length; j++) {
		                if (node == chart.getSelectedNodes()[j]) {
		                    newobject = false;
		                }
		            }
		            if (chart.getSelectedNodes().length != 0 && !newobject) {
		                for (var j = 0 ; j < chart.getSelectedNodes().length; j++) {
		                    GUIDArray.push(chart.getSelectedNodes()[j].data.GUID);
		                    xArray.push(chart.getSelectedNodes()[j].data.x);
		                    yArray.push(chart.getSelectedNodes()[j].data.y);

		                    var tmpSprite = document.createElement("img");
		                    tmpSprite.src = "content/images/" + chart.getSelectedNodes()[j].data.typeIndex + ".png";
		                    tmpSprite.style.position = "absolute";
		                    tmpSprite.style.left = chart.getSelectedNodes()[j].data.x + "px";
		                    tmpSprite.style.top = chart.getSelectedNodes()[j].data.y + "px";
		                    moveSprites.push(tmpSprite);
		                    divDrop.appendChild(moveSprites[moveSprites.length - 1]);
		                }
		            }
		            else {
		                GUIDArray.push(node.data.GUID);
		                xArray.push(node.data.x);
		                yArray.push(node.data.y);

		                var tmpSprite = document.createElement("img");
		                tmpSprite.src = "content/images/" + node.data.typeIndex + ".png";
		                tmpSprite.style.position = "absolute";
		                tmpSprite.style.left = node.data.x + "px";
		                tmpSprite.style.top = node.data.x + "px";
		                moveSprites.push(tmpSprite);
		                divDrop.appendChild(moveSprites[moveSprites.length - 1]);
		            }
		            var UndoObject = {
		                action: "nodemove", id: node.data.id, xArray: xArray, yArray: yArray, GUIDArray: GUIDArray,
		                flwWidthBefore: divDrop.style.width, flwHeightBefore: divDrop.style.height
		            };
		            $scope.chart.AddToUndoList(UndoObject);

		            UndoList[UndoList.length - 1].flwWidthAfter = divDrop.style.width;
		            UndoList[UndoList.length - 1].flwHeightAfter = divDrop.style.height;

		            RedoList.length = 0;
		        }
		        catch (ex) {
		            //alert(ex.message + " , " + ex.number);
		        }
			    PositionBeforeDrag = { x: node.data.x, y: node.data.y };
				//
				// If nothing is selected when dragging starts, 
				// at least select the node we are dragging.
				//
				if (!node.selected()) {
				    chart.deselectAll();
					node.select();
				}
			},
			
			//
			// Dragging selected nodes... update their x,y coordinates.
			//
		    dragging: function (x, y) {
		        if (chart.Lock)
		            return;

		        if (spriteCancel) {
		            return;
		        }
		        var currentNodeIndex = chart.GetSelectedNodeIndex();
		        try {
		            var offset_y = 250 - 55 - 50;
		            if ($(".mainHeader").css('display') == "none")
		                offset_y = 196 - 55 - 50;
		            for (var k = 1 ; k < chart.GetNumNodes() ; k++) {
		                if (k != currentNodeIndex) {
		                    chart.GetNode(k).data.backColor = "#2ab989";
		                    chart.GetNode(k).data.covered = false;
		                    var ind = chart.GetNode(k).data.typeIndex - 1;
		                    var myX = parseInt(x) - parseInt(divDrop.offsetLeft) - parseInt(divContainer.scrollLeft);
		                    var myY = parseInt(y) - parseInt(offset_y) + parseInt(divContainer.scrollTop);
		                    if (myX >= $scope.chart.GetNodeX(k) - 15 && myX <= parseInt($scope.chart.GetNodeX(k)) + parseInt($scope.chart.GetShapeWidthList(ind)) + 15
                        && myY >= $scope.chart.GetNodeY(k) - 15 && myY <= parseInt($scope.chart.GetNodeY(k)) + parseInt($scope.chart.GetShapeHeightList(ind)) + 15 && $scope.chart.GetNodeVisible(k)) {
		                        chart.GetNode(k).data.covered = true;
		                        hoverID = chart.GetNode(k).data.id;
		                        node.data.z = 0;
		                        chart.GetNode(k).data.z = 10;
		                    }
		                }
		            }
		        }
		        catch (ex) {
		            divDebug.innerHTML = ex.message;
		        }

		        var realheight = parseInt(divDrop.style.height.replace("px", ""));
		        var realwidth = parseInt(divDrop.style.width.replace("px", ""));
		        var curCoords = controller.translateCoordinates(x, y, evt);
		        var deltaX = curCoords.x - lastMouseCoords.x;
		        var deltaY = curCoords.y - lastMouseCoords.y;

		        if (currentNodeIndex != -1) {
		            var curnode = chart.GetNode(currentNodeIndex);
                    //check all selected objects while mouse is being dragged on a node, immediately exit the function if any of the selected objects is going out of valid border
		            var selectedobjectarray = new Array();
		            selectedobjectarray.length = 0;
		            for (var i = 1 ; i < chart.GetNumNodes() ; i++) {
		                if (chart.GetNode(i).data.visible && chart.GetNode(i).selected()) {
		                    selectedobjectarray.push(i);
		                }
		            }
		            for (var j = 0 ; j < selectedobjectarray.length; j++) {
		                if (parseInt(chart.GetNodeX(selectedobjectarray[j])) + deltaX < 0 || parseInt(chart.GetNodeY(selectedobjectarray[j])) + deltaY < 30 ||
                            parseInt(chart.GetNodeX(selectedobjectarray[j])) + deltaX + parseInt(chart.GetNode(selectedobjectarray[j]).data.shapeWidth) > realwidth ||
                            parseInt(chart.GetNodeY(selectedobjectarray[j])) + parseInt(chart.GetNode(selectedobjectarray[j]).data.shapeHeight) + deltaY > realheight) {
		                    //return;
		                }
		            }

		            divSettings.style.left = (curnode.data.x + 106 - 32 + (((divBody.clientWidth - divDrop.clientWidth) / 2) + 50) - divContainer.scrollLeft) + "px";
		            divSettings.style.top = (curnode.data.y + 174 - 44 + 85 - divContainer.scrollTop) + "px";

		            divAddIcon.style.left = (curnode.data.x + 106 - 32 - 74 + (((divBody.clientWidth - divDrop.clientWidth) / 2) + 50) - divContainer.scrollLeft) + "px";
		            divAddIcon.style.top = (curnode.data.y + 174 - 44 + 85 - divContainer.scrollTop) + "px";
		            curnode.data.ShowHover = false;
		            if (!spriteCancel) {
		                moveDeltaX += deltaX;
		                moveDeltaY += deltaY;
		            }
		            var selectedNodes = chart.getSelectedNodes();

		            for (var j = 0 ; j < selectedNodes.length; j++) {
		                var movingNode = selectedNodes[j];
		                moveSprites[j].style.left = parseInt(movingNode.data.x) + parseInt(moveDeltaX) + "px";
		                moveSprites[j].style.top = parseInt(movingNode.data.y) + parseInt(moveDeltaY) + "px";
		            }
		            lastMouseCoords = curCoords;

		            var correctX = x - (((divBody.clientWidth - divDrop.clientWidth) / 2) + 50 - divContainer.scrollLeft);
		            var correctY = y - 85 + divContainer.scrollTop;
		            for (var k = 1 ; k < $scope.chart.GetNumNodes() ; k++) {
		                var ind = $scope.chart.GetNode(k).data.typeIndex - 1;
		                if (correctX >= $scope.chart.GetNodeX(k) - 20 &&
                    correctX <= parseInt($scope.chart.GetNodeX(k)) + parseInt($scope.chart.GetShapeWidthList(ind)) + 20
                    && correctY >= parseInt($scope.chart.GetNodeY(k)) - 20 &&
                    correctY <= parseInt($scope.chart.GetNodeY(k)) + parseInt($scope.chart.GetShapeHeightList(ind)) + 20 && $scope.chart.GetNodeVisible(k) && node.data.id != $scope.chart.GetNode(k).data.id) {
		                    $scope.chart.BringToFront(k);
		                }
		            }
		        }
			},
			dragEnded: function (x, y)
			{
			    if (chart.Lock)
			        return;

			    currentActiveNodeForConnectionSourceID = -1;
			    var currentNodeIndex = chart.GetSelectedNodeIndex();
			    if (!connectorWiseConnectionMode) {
			        if (moveDeltaX != 0 || moveDeltaY != 0) {//move selected nodes only after user has clicked on the destination (i.e. dragging has been finished)
			            //in this way we can save save a lot of CPU time which would be otherwise wasted for path finding algorithm
			            chart.updateSelectedNodesLocation($scope.RoundNumbers(moveDeltaX), $scope.RoundNumbers(moveDeltaY), true);
			            for (var k = 0 ; k < moveSprites.length; k++) {
			                divDrop.removeChild(moveSprites[k]);
			            }
			            moveSprites.length = 0;
			        }
			    }
			    moveDeltaX = 0;
			    moveDeltaY = 0;

			    var noderight = parseInt(node.data.x) + parseInt(node.data.shapeWidth);
			    var nodetop = parseInt(node.data.y);
			    var nodebottom = parseInt(node.data.y) + parseInt(node.data.shapeHeight);
			    var nodeleft = parseInt(node.data.x);

			    //detect child
			    var ItIsChild = false;
			    try {
			        var bottomY = -10000;
			        var rightX = -10000;
			        for (var k = 0 ; k < chart.GetNumNodes() ; k++) {

			            if (chart.GetNode(k).data.y > bottomY) {
			                bottomY = chart.GetNode(k).data.y;
			            }

			            if (chart.GetNode(k).data.x > rightX) {
			                rightX = chart.GetNode(k).data.x;
			            }
			        }

			        var realheight = parseInt(divDrop.style.height.replace("px", ""));
			        var realwidth = parseInt(divDrop.style.width.replace("px", ""));
			        $scope.chart.FindMinWidthHeight();

			        if (minHeight + 40 > parseInt(realheight)) {
			            divSettings.style.display = "none";
			            divAddIcon.style.display = "none";
			            divAddDialog.style.display = "none";

			            var h = (minHeight + 40);
			            if (h <= maxFlowchartHeight + initialHeight) {
			                divDrop.style.height = (h) + "px";
			                $("#divChart").height((h));
			                var svg = document.getElementsByTagName("svg");
			                svg[0].setAttribute("height", h);
			                svg[0].style.height = (h) + "px";
			            }
			        }

			        if (minWidth + 40 > realwidth) {
			            divSettings.style.display = "none";
			            divAddIcon.style.display = "none";
			            divAddDialog.style.display = "none";
			            var w = parseInt(minWidth) + 40;

			            if (w <= (maxFlowchartWidth + initialWidth)) {
			                divDrop.style.width = w + "px";
			                $("#divChart").width((w));
			                $("#divFlowChartParent").width((w));
			                $("#divFlowChartParentTitle").width((w));
			                var svg = document.getElementsByTagName("svg");
			                svg[0].setAttribute("width", w);
			                svg[0].style.width = (w) + "px";
			            }
			            CenterDrop();
			        }

			        UndoList[UndoList.length - 1].flwWidthAfter = divDrop.style.width;
			        UndoList[UndoList.length - 1].flwHeightAfter = divDrop.style.height;

			    }
			    catch (ex) {
			        node.data.title = ex.message;
			    }
			},
			//
			// The node wasn't dragged... it was clicked.
			//
			clicked: function () {
				chart.handleNodeClicked(node, evt.ctrlKey);
			},

		});
	};

    $scope.PointIsInRect = function (px, py, rleft, rtop, rright, rbottom) {
        var result = false;
        if (px >= rleft && px <= rright && py >= rtop && py <= rbottom) {
            result = true;
        }

        return result;
    };

	//
	// Handle mousedown on a connection.
    //

    $scope.ConnectionNodeMouseUp = function (evt, connection) {
        $scope.RemoveGhostPath();
        return;
        SelectedPointIndex = -1;
	    SelectedConnectionIndex = -1;
	    beforeNodeIndex = -1;
	    afterNodeIndex = -1;
	    //loop through all connection nodes, and remove redundant nodes
	    var tmpArray = [];
	    tmpArray.push(connection.PathPointArray[0]);//first node certainly would stay
	    for (var i = 1 ; i < connection.PathPointArray.length - 1; i++) {
	        var valid = true;
	        var h = false;
	        var v = false;

	        if (connection.PathPointArray[i - 1].x == connection.PathPointArray[i].x && connection.PathPointArray[i + 1].x == connection.PathPointArray[i].x) {
	            v = true;
	        }
	        if (connection.PathPointArray[i - 1].y == connection.PathPointArray[i].y && connection.PathPointArray[i + 1].y == connection.PathPointArray[i].y) {
	            h = true;
	        }

	        if (h || v) {//before-current-after making a horizontal or vertical line? so current node should be removed
	            valid = false;
	        }
	        if (valid) {
	            tmpArray.push(connection.PathPointArray[i]);
	        }
	    }
	    tmpArray.push(connection.PathPointArray[connection.PathPointArray.length - 1]);//last node would also certainly stay there anyway

	    connection.PathPointArray = tmpArray;
	    for (var i = 0 ; i < connection.PathPointArray.length; i++) {
	        connection.PathPointArray[i].index = i;
	    }

	    var newPath = "";

	    for (var i = 0 ; i < connection.PathPointArray.length; i++) {
	        if (i == 0) {
	            newPath += " M ";
	        }
	        else {
	            newPath += " L ";
	        }
	        newPath += connection.PathPointArray[i].x + " , " + connection.PathPointArray[i].y;
	    }
	    connection.customLineData = newPath;

	    var afterPath;
	    if (connection.customLineData == "")
	        afterPath = "X" + connection.lastPath;
	    else
	        afterPath = "Y" + connection.customLineData;

	    var pArray = jQuery.extend(true, [], connection.PathPointArray);

	    UndoList[UndoList.length - 1].afterPath = afterPath;
	    UndoList[UndoList.length - 1].afterPathPointArray = pArray;
	};

	$scope.ConnectionNodeDrag = function (evt, connection, point) {


	};

	$scope.RoundNumbers = function (number) {
	    var result = number;
	    var x = number % 10;
	    if (x <= 5) {
	        result = number - x;
	    }
	    else {
	        result = number + (10 - x);
	    }
	    return result;
	};

	$scope.ConnectionNodeMouseDown = function (evt, connection, dir, point) {
	    tmpmode = dir;
	    if (dir == 1 || dir == 2) {
	        middlepoint = dir;
	    }
	    SelectedConnectionPoint = point;
	    if (tmpCorrectPathPoint == null) {
	        tmpCorrectPathPoint = jQuery.extend(true, {}, connection.PathPointArray);
	    }

	    SelectedPointIndex = point.index;
	    var beforePath;
	    if (connection.customLineData == "")
	        beforePath = "X" + connection.lastPath;
	    else
	        beforePath = "Y" + connection.customLineData;

	    var pArray = jQuery.extend(true, [], connection.PathPointArray);
	    var UndoObject = {
	        action: "pathConnectorMove", beforePath: beforePath, beforePathPointArray: pArray,
	        PathsrcNodeID: connection.source.parentNode().data.id, PathdestNodeID: connection.dest.parentNode().data.id
	    };
	    UndoList.push(UndoObject);
	    RedoList.length = 0;

	    for (var i = 0 ; i < $scope.chart.connections.length; i++) {
	        if (connection == $scope.chart.connections[i]) {
	            SelectedConnectionIndex = i;
	            break;
	        }
	    }
	    current_connection_source = 0;
	    current_connection_dest = 0;
	    current_connection_dir = dir;
	    var chart = $scope.chart;
	    try
	    {
	        var node = chart.GetNodeById(chart.GetSourceNodeID(connection));
	        current_connection_source = node.data.id;
	        current_connection_dest = chart.GetDestNodeID(connection);
	    }
	    catch (ex) {
	    }
	};
	var tmpPathChange;
	var tmpYOffsetBefore = "";
	var tmpXOffsetBefore = "";
	var tmpYOffsetBefore1 = "";
	var tmpYOffsetBefore2 = "";

	$scope.connectionMouseDown = function (evt, connection) {
	    tmpcon = connection;
	    var offsetX;
	    var offsetY;

	    var inputOffset = { x: 0, y: 0 };
	    var outputOffset = { x: 0, y: 0 };
	    var outputx;
	    var outputy;
	    if (connection.sourceOutputID == 1) {
	        outputy = connection.oy1;
	        outputx = connection.ox1;
	    }
	    else {
	        outputy = connection.oy2;
	        outputx = connection.ox2;
	    }
	    var inputConnectorIndex = 0;
	    var outputConnectorIndex = 0;
	    var inputDir = connection.inputDir;
	    var outputDir = connection.outputDir;

	    if (outputDir == "down") {
	        outputOffset.y = 0;
	        outputConnectorIndex = 2;
	    }
	    else if (outputDir == "up") {
	        outputOffset.y = 0;
	        outputConnectorIndex = 0;
	    }
	    else if (outputDir == "left") {
	        outputOffset.x = 0;
	        outputConnectorIndex = 3;
	    }
	    else if (outputDir == "right") {
	        outputOffset.x = 0;
	        outputConnectorIndex = 1;
	    }

	    if (connection.source.parentNode().data.typeIndex == 1)//source node is IF
	    {
	        if (outputDir == "left")
	            outputConnectorIndex = 4;
	        else if (outputDir == "right")
	            outputConnectorIndex = 5;
	    }

	    var srcPoint = {
	        x: $scope.RoundNumbers(parseInt(connection.source.parentNode().x()) + parseInt(outputx) + outputOffset.x),
	        y: $scope.RoundNumbers(parseInt(connection.source.parentNode().y()) + parseInt(outputy) + outputOffset.y)
	    };

	    if (connection.inputDir == "up") {
	        offsetY = 0;
	        offsetX = 0;
	        inputConnectorIndex = 0;
	    }
	    else if (connection.inputDir == "down") {
	        offsetY = 0;
	        offsetX = 0;
	        inputConnectorIndex = 2;
	    }
	    else if (connection.inputDir == "left") {
	        offsetY = 0;
	        offsetX = 0;
	        inputConnectorIndex = 3;
	    }
	    else if (connection.inputDir == "right") {
	        offsetY = 0;
	        offsetX = 0;
	        inputConnectorIndex = 1;
	    }

	    if (!connection.LineSwitchFirstNodeAdded) {//this action should be done only once

	        try
	        {
	            var srcNode = connection.source.parentNode();
	            var dstNode = connection.dest.parentNode();
	            var connectorPos;
	            for (var i = 0 ; i < connectorPositions.length; i++) {
	                if (connectorPositions[i].typeIndex == srcNode.data.typeIndex && connectorPositions[i].index == outputConnectorIndex) {
	                    connectorPos = { x: parseInt(connectorPositions[i].rx), y: parseInt(connectorPositions[i].ry) };
	                }
	            }
	            if (connection.source.parentNode().data.typeIndex != 1)//if the source node is not IF
	            {
	                //connection.PathPointArray.unshift({ x: parseInt(srcNode.data.x) + parseInt(connectorPos.x), y: parseInt(srcNode.data.y) + parseInt(connectorPos.y), index: 0 });

	                for (var i = 0 ; i < connection.PathPointArray.length; i++) {
	                  //  connection.PathPointArray[i].index = i;
	                }
	            }

	            if (inputDir == "left" || inputDir == "right") {
	                for (var i = 0 ; i < connectorPositions.length; i++) {
	                    if (connectorPositions[i].typeIndex == dstNode.data.typeIndex && connectorPositions[i].index == inputConnectorIndex) {
	                        connectorPos = { x: parseInt(connectorPositions[i].rx), y: parseInt(connectorPositions[i].ry) };
	                    }
	                }
	                //connection.PathPointArray.push({
	                //    x: parseInt(dstNode.data.x) + parseInt(connectorPos.x), y: parseInt(dstNode.data.y) + parseInt(connectorPos.y),
	                //    index: connection.PathPointArray.length
	                //});
	            }
	            connection.LineSwitchFirstNodeAdded = true;//this action should be done only once
	        }
	        catch (ex) {
	            //alert(ex.message + " , " + ex.number);
	        }
	        connection.OriginalFirstPoint = connection.PathPointArray[0];
	        connection.OriginalLastPoint = connection.PathPointArray[connection.PathPointArray.length - 1];
	    }

	    var chart = $scope.chart;
	    dragging.startDrag(evt, {
	        dragStarted: function (x, y) {
	            tmpYOffsetBefore = connection.tmpYOffset;
	            tmpXOffsetBefore = connection.tmpXOffset;
	            tmpYOffsetBefore1 = connection.tmpYOffset1;
	            tmpYOffsetBefore2 = connection.tmpYOffset2;
	        },
	        dragging: function (x, y, evt) {

	        },
	        dragEnded: function () {
	            try {
	                var srcNodeID = connection.source.parentNode().data.id;
	                var destNodeID = connection.dest.parentNode().data.id;
	            }
	            catch (ex) {
	                //alert(ex.message + " , " + ex.number);
	            }
	        }

	    });

	    chart.handleConnectionMouseDown(connection, evt.ctrlKey);
	    // Don't let the chart handle the mouse down.
	    evt.stopPropagation();
	    evt.preventDefault();
	};

    //
    // Handle mouseup on an input connector.
    //



	$scope.connectorMouseUp = function (evt, parentnode, connectorIndex) {
	    $scope.RemoveGhostPath();//remove the red ghost path (temporary path that is displayed only while mouse button is pressed)
	    if (LineSwitchConnectionIndex != -1) {//this variable holds index of active connection (connection which its source/dest is going to be changed dynamically)
	        var oldInputDir, oldOutputDir, newInputDir, newOutputDir, oldSource, oldDest, newSource, newDest, oldInputPos,
            oldOutputPos, newInputPos, newOutputPos;
	        var tmpConnectorIndex;
	        oldInputDir = $scope.chart.connections[LineSwitchConnectionIndex].inputDir;
	        oldOutputDir = $scope.chart.connections[LineSwitchConnectionIndex].outputDir;
	        oldSource = $scope.chart.connections[LineSwitchConnectionIndex].source;
	        oldDest = $scope.chart.connections[LineSwitchConnectionIndex].dest;
	        oldInputPos = { x: $scope.chart.connections[LineSwitchConnectionIndex].inputx, y: $scope.chart.connections[LineSwitchConnectionIndex].inputy };
	        oldOutputPos = {
	            x1: $scope.chart.connections[LineSwitchConnectionIndex].ox1, y1: $scope.chart.connections[LineSwitchConnectionIndex].oy1,
	            x2: $scope.chart.connections[LineSwitchConnectionIndex].ox2, y2: $scope.chart.connections[LineSwitchConnectionIndex].oy2
	        };

	        if (LineSwitchMode == "src") {//line source point is being changed
	            try{
	                //if source node is "IF", number of connections on each branch should be checked, not more than 1 connection is allowed for each branch
	                if (parentnode.data.typeIndex == 1) {
	                    if (connectorIndex == "4") {//left (TRUE)
	                        if (parentnode.data.outputConnectors[0].connected) {
	                            return;
	                        }
	                    }
	                    else {
	                        if (parentnode.data.outputConnectors[1].connected) {
	                            return;
	                        }
	                    }
	                }

	                if ((parentnode.data.numChild < parentnode.data.outputcount || (parentnode.data.numChild == parentnode.data.outputcount &&
                        parentnode.data.GUID==$scope.chart.connections[LineSwitchConnectionIndex].source.parentNode().data.GUID)) &&
                        parentnode.data.GUID != $scope.chart.connections[LineSwitchConnectionIndex].dest.parentNode().data.GUID) {
	                    if (parentnode.data.typeIndex == 1 && (connectorIndex == 0 || connectorIndex == 1 || connectorIndex == 2 || connectorIndex == 3))
	                        return;
	                    //if previous source was a decision and the new one is not, remove the caption
	                    if ($scope.chart.connections[LineSwitchConnectionIndex].source.parentNode().data.typeIndex == 1 && parentnode.data.typeIndex != 1) {
	                        $scope.chart.connections[LineSwitchConnectionIndex]._caption = "";
	                    }

	                    //change ParentIDs, childrenGUIDs array
	                    try {
	                        for (var q = 0 ; q < $scope.chart.connections[LineSwitchConnectionIndex].dest.parentNode().data.parentIDs.length; q++) {//loop through all parentIDs of the destination node
	                            if ($scope.chart.connections[LineSwitchConnectionIndex].dest.parentNode().data.parentIDs[q] == oldSource.parentNode().data.id) {
	                                $scope.chart.connections[LineSwitchConnectionIndex].dest.parentNode().data.parentIDs[q] = parentnode.data.id;//change parentIDs array
	                                break;
	                            }
	                        }

	                        //remove connection from the previous source point
	                        for (var q = $scope.chart.connections[LineSwitchConnectionIndex].source.parentNode().data.childrenGUIDs.length - 1; q >= 0; q--) {//loop through all childrenGUIDs of the destination node
	                            if ($scope.chart.connections[LineSwitchConnectionIndex].source.parentNode().data.childrenGUIDs[q].guid == oldDest.parentNode().data.GUID) {
	                                $scope.chart.connections[LineSwitchConnectionIndex].source.parentNode().data.childrenGUIDs.splice(q, 1);//remove previous childrenGUIDs array item
	                                break;
	                            }
	                        }

	                        //now add new childGUID to the new source item
	                        var soID = 1;
	                        if (connectorIndex == 5)//FALSE branch of IF item
	                            soID = 2;
	                        parentnode.data.childrenGUIDs.push({ guid: oldDest.parentNode().data.GUID, number: soID });
	                    }
	                    catch (ex) {
	                        //alert(ex.message);
	                    }

	                    var outputConnectorIndex = 0;
	                    if ($scope.chart.connections[LineSwitchConnectionIndex].source.parentNode().data.typeIndex == 1)//source node is IF, so we should determine which outputconnector is being released
	                    {
	                        if ($scope.chart.connections[LineSwitchConnectionIndex].outputDir == "right") {
	                            outputConnectorIndex = 1;
	                        }
	                    }
	                    $scope.chart.connections[LineSwitchConnectionIndex].source.parentNode().data.outputConnectors[outputConnectorIndex].connected = false;

	                    $scope.chart.connections[LineSwitchConnectionIndex].source.parentNode().data.numChild--;
	                    $scope.chart.connections[LineSwitchConnectionIndex].source.parentNode().data.activeConnector = "";
	                    if ($scope.chart.connections[LineSwitchConnectionIndex].source.parentNode().data.numChild == 0 &&
                            $scope.chart.connections[LineSwitchConnectionIndex].source.parentNode().data.activeConnectorInput == "")
	                        $scope.chart.connections[LineSwitchConnectionIndex].source.parentNode().data.connected = false;
	                    $scope.chart.connections[LineSwitchConnectionIndex].source.parentNode().data.connectorclicked = false;
	                    parentnode.data.numChild++;
	                    parentnode.data.connected = true;
	                    var newConnectionOutputConnectorIndex = 0;
	                    if ($scope.chart.connections[LineSwitchConnectionIndex].source.parentNode().data.typeIndex == 1 && parentnode.data.typeIndex == 1) {//if both old/new sources are IF node, change output connector index
	                        if (connectorIndex == 5)//newly inserted node is the right node
	                        {
	                            if (outputConnectorIndex == 0) {
	                                newConnectionOutputConnectorIndex = 1;
	                                $scope.chart.connections[LineSwitchConnectionIndex]._caption = "FALSE";
	                            }
	                        }
	                        else {
	                            $scope.chart.connections[LineSwitchConnectionIndex]._caption = "TRUE";
	                        }
	                    }

	                    if (parentnode.data.typeIndex == 1) {//if both old/new sources are IF node, change output connector index
	                        if (connectorIndex == 5)//newly inserted node is the right node
	                        {
	                            if (outputConnectorIndex == 0) {
	                                newConnectionOutputConnectorIndex = 1;
	                                $scope.chart.connections[LineSwitchConnectionIndex]._caption = "FALSE";
	                            }
	                        }
	                        else {
	                            $scope.chart.connections[LineSwitchConnectionIndex]._caption = "TRUE";
	                        }
	                    }

	                    parentnode.data.outputConnectors[newConnectionOutputConnectorIndex].connected = true;

	                    var outputCurrentConnector = parentnode.data.numChild;
	                    $scope.chart.connections[LineSwitchConnectionIndex].source = parentnode.outputConnectors[parentnode.data.numChild - 1];
	                    if (connectorIndex == 0)
	                        $scope.chart.connections[LineSwitchConnectionIndex].outputDir = "up";
	                    else if (connectorIndex == 1)
	                        $scope.chart.connections[LineSwitchConnectionIndex].outputDir = "right";
	                    else if (connectorIndex == 2)
	                        $scope.chart.connections[LineSwitchConnectionIndex].outputDir = "down";
	                    else if (connectorIndex == 3)
	                        $scope.chart.connections[LineSwitchConnectionIndex].outputDir = "left";
	                    else if (connectorIndex == 4)//IF-true
	                        $scope.chart.connections[LineSwitchConnectionIndex].outputDir = "left";
	                    else if (connectorIndex == 5)//IF-false
	                        $scope.chart.connections[LineSwitchConnectionIndex].outputDir = "right";

	                    var tmpOutputPos;
	                    for (var i = 0 ; i < connectorPositions.length; i++) {
	                        if (connectorPositions[i].typeIndex == parentnode.data.typeIndex && connectorPositions[i].index == connectorIndex) {
	                            tmpOutputPos = { x: parseInt(connectorPositions[i].x), y: parseInt(connectorPositions[i].y) };
	                        }
	                    }
	                    $scope.chart.connections[LineSwitchConnectionIndex].sourceOutputID = outputCurrentConnector;
	                    $scope.chart.connections[LineSwitchConnectionIndex].ox1 = tmpOutputPos.x;
	                    $scope.chart.connections[LineSwitchConnectionIndex].oy1 = tmpOutputPos.y;
	                    $scope.chart.connections[LineSwitchConnectionIndex].ox2 = tmpOutputPos.x;
	                    $scope.chart.connections[LineSwitchConnectionIndex].oy2 = tmpOutputPos.y;
	                    $scope.chart.connections[LineSwitchConnectionIndex].LineSwitchFirstNodeAdded = false;
	                }
	            }
	            catch (ex) {

	            }
	        }
	        else {//line destination point is being changed
	            if ((connectorIndex == 4 || connectorIndex == 5) && parentnode.data.typeIndex == 1) {
	                return;
	            }

	            try
	            {
                    //remove connection from the previous dest point
	                for (var q = $scope.chart.connections[LineSwitchConnectionIndex].dest.parentNode().data.parentIDs.length - 1; q >= 0; q--) {//loop through all parentIDs of the destination node
	                    if ($scope.chart.connections[LineSwitchConnectionIndex].dest.parentNode().data.parentIDs[q] == oldSource.parentNode().data.id) {
	                        $scope.chart.connections[LineSwitchConnectionIndex].dest.parentNode().data.parentIDs.splice(q, 1);//remove previous parentIDs array item
	                        break;
	                    }
	                }
                    //now add the parent to parentIDs array of the new destination
	                parentnode.data.parentIDs.push(oldSource.parentNode().data.id);

	                for (var q = 0 ; q < $scope.chart.connections[LineSwitchConnectionIndex].source.parentNode().data.childrenGUIDs.length; q++) {//loop through all childrenGUIDs of the destination node
	                    if ($scope.chart.connections[LineSwitchConnectionIndex].source.parentNode().data.childrenGUIDs[q].guid == oldDest.parentNode().data.GUID) {
	                        $scope.chart.connections[LineSwitchConnectionIndex].source.parentNode().data.childrenGUIDs[q].guid = parentnode.data.GUID;//change childrenGUIDs array
	                        break;
	                    }
	                }
	            }
	            catch (ex) {
	                //alert(ex.message);
	            }

	            //source and dest nodes cannot be the same, also START node can not be used as the destination node of the new connection
	            if ($scope.chart.connections[LineSwitchConnectionIndex].source.parentNode().data.GUID != parentnode.data.GUID && parentnode.data.typeIndex != 8) {
	                var indexToBeDeleted = "";
	                if ($scope.chart.connections[LineSwitchConnectionIndex].inputDir == "up") {
	                    indexToBeDeleted = "0";
	                }
	                else if ($scope.chart.connections[LineSwitchConnectionIndex].inputDir == "right") {
	                    indexToBeDeleted = "1";
	                }
	                else if ($scope.chart.connections[LineSwitchConnectionIndex].inputDir == "down") {
	                    indexToBeDeleted = "2";
	                }
	                else if ($scope.chart.connections[LineSwitchConnectionIndex].inputDir == "left") {
	                    indexToBeDeleted = "3";
	                }
	                tmpConnectorIndex = indexToBeDeleted;
	                $scope.chart.connections[LineSwitchConnectionIndex].dest.parentNode().data.activeConnectorInput =
                        $scope.chart.connections[LineSwitchConnectionIndex].dest.parentNode().data.activeConnectorInput.replace(indexToBeDeleted, "");
	                if ($scope.chart.connections[LineSwitchConnectionIndex].dest.parentNode().data.numChild == 0 &&
                        $scope.chart.connections[LineSwitchConnectionIndex].dest.parentNode().data.activeConnectorInput == "")
	                    $scope.chart.connections[LineSwitchConnectionIndex].dest.parentNode().data.connected = false;

	                $scope.chart.connections[LineSwitchConnectionIndex].dest = parentnode.inputConnectors[parentnode.data.nextInputConnector];
	                var tmpInputPos;
	                for (var i = 0 ; i < connectorPositions.length; i++) {
	                    if (connectorPositions[i].typeIndex == parentnode.data.typeIndex && connectorPositions[i].index == connectorIndex) {
	                        tmpInputPos = { x: parseInt(connectorPositions[i].x), y: parseInt(connectorPositions[i].y) };
	                    }
	                }

	                $scope.chart.connections[LineSwitchConnectionIndex].inputx = tmpInputPos.x;
	                $scope.chart.connections[LineSwitchConnectionIndex].inputy = tmpInputPos.y;
	                if (connectorIndex == 0)
	                    $scope.chart.connections[LineSwitchConnectionIndex].inputDir = "up";
	                else if (connectorIndex == 1)
	                    $scope.chart.connections[LineSwitchConnectionIndex].inputDir = "right";
	                else if (connectorIndex == 2)
	                    $scope.chart.connections[LineSwitchConnectionIndex].inputDir = "down";
	                else if (connectorIndex == 3)
	                    $scope.chart.connections[LineSwitchConnectionIndex].inputDir = "left";
	                parentnode.data.activeConnectorInput += connectorIndex;
	                parentnode.data.connected = true;
	                if (parentnode.data.typeIndex == 1)//if the new destination is a DECISION node
	                {
	                    if (connectorIndex == 0)
	                        $scope.chart.connections[LineSwitchConnectionIndex].inputDir = "up";
	                    else if (connectorIndex == 1)
	                        $scope.chart.connections[LineSwitchConnectionIndex].inputDir = "up";
	                    else if (connectorIndex == 2)
	                        $scope.chart.connections[LineSwitchConnectionIndex].inputDir = "up";
	                    else if (connectorIndex == 3)
	                        $scope.chart.connections[LineSwitchConnectionIndex].inputDir = "down";
	                }
	            }
	        }

	        newInputDir = $scope.chart.connections[LineSwitchConnectionIndex].inputDir;
	        newOutputDir = $scope.chart.connections[LineSwitchConnectionIndex].outputDir;
	        newSource = $scope.chart.connections[LineSwitchConnectionIndex].source;
	        newDest = $scope.chart.connections[LineSwitchConnectionIndex].dest;
	        newInputPos = { x: $scope.chart.connections[LineSwitchConnectionIndex].inputx, y: $scope.chart.connections[LineSwitchConnectionIndex].inputy };
	        newOutputPos = {
	            x1: $scope.chart.connections[LineSwitchConnectionIndex].ox1, y1: $scope.chart.connections[LineSwitchConnectionIndex].oy1,
	            x2: $scope.chart.connections[LineSwitchConnectionIndex].ox2, y2: $scope.chart.connections[LineSwitchConnectionIndex].oy2
	        };

	        var UndoObject = {
	            action: "lineswitch", connectionIndex: LineSwitchConnectionIndex, switchMode: LineSwitchMode, oldInputDir: oldInputDir, oldOutputDir: oldOutputDir,
	            newInputDir: newInputDir, newOutputDir: newOutputDir,
	            oldSource: oldSource, oldDest: oldDest, newSource: newSource, newDest: newDest, oldInputPos: oldInputPos,
	            oldOutputPos: oldOutputPos, newInputPos: newInputPos, newOutputPos: newOutputPos, oldconnectorIndex: tmpConnectorIndex, newconnectorIndex: connectorIndex
	        };
	        $scope.chart.AddToUndoList(UndoObject);
	        RedoList.length = 0;

	        LineSwitchConnectionIndex = -1;
	        LineSwitchMode = "";

	        return;
	    }
	    if (parentnode.data.typeIndex == 1 && (connectorIndex == 0 || connectorIndex == 1 || connectorIndex == 2 || connectorIndex == 3))
	        return;

	    if (currentActiveNodeForConnectionSourceID == -1)
	        return;

	    try
	    {
	        var node = $scope.chart.GetNodeById(currentActiveNodeForConnectionSourceID);
	        if (node.data.GUID == parentnode.data.GUID)//source and dest node should be DIFFERENT in a connection
	            return;
	        if (node.data.typeIndex == 8)//start node cannot be added to any node!!!
	            return;
	        var inputDir = "up";
	        var outputDir = "down";
	        var destIndex = 0;
	        var srcIndex = connectorIndex;
	        var nodex;
	        var nodey;

	        if (parentnode.data.numChild >= parentnode.data.outputcount) {
	            return;
	        }

	        if (parentnode.data.typeIndex == 1) {
	            if (parentnode.data.outputConnectors[0].connected && connectorIndex == 4)
	                return;
	            //do nothing if user is trying to make another line starting from an already existing FALSE branch of an IF node
	            if (parentnode.data.outputConnectors[1].connected && connectorIndex == 5)
	                return;
	        }

	        parentnode.data.numChild++;
	        var outputCurrentConnector = parentnode.data.numChild;
	        if (connectorIndex == 0) {
	            if (node.data.typeIndex != 1)
	                destIndex = 2;
	            else
	                destIndex = 3;
	            inputDir = "down";
	            outputDir = "up";
	            if (parentnode.data.typeIndex != 1) {//parent item is NOT a FLOWDECISION (IF)
	                nodey = parentnode.y() - 70 - parseInt(node.data.shapeHeight);
	                nodex = parseInt(parentnode.x()) - ((parseInt(node.data.shapeWidth) - parseInt(parentnode.data.shapeWidth)) / 2);
	            }
	        }
	        else if (connectorIndex == 1) {
	            if (node.data.typeIndex != 1) {
	                destIndex = 3;
	                inputDir = "left";
	            }
	            else {
	                destIndex = 1;
	                inputDir = "up";
	            }

	            outputDir = "right";
	            if (parentnode.data.typeIndex != 1) {//parent item is NOT a FLOWDECISION (IF)
	                nodex = parseInt(parentnode.x()) + (parseInt(parentnode.data.shapeWidth)) + 70;
	                nodey = parseInt(parentnode.y()) - ((parseInt(parentnode.data.shapeHeight) - parseInt(parentnode.data.shapeHeight)) / 2);
	            }
	        }
	        else if (connectorIndex == 2) {
	            destIndex = 0;
	            if (node.data.typeIndex == 1)//destination item is a FlowDecision
	            {
	                destIndex = 1;
	            }
	            inputDir = "up";
	            outputDir = "down";
	            if (parentnode.data.typeIndex != 1) {//parent item is NOT a FLOWDECISION (IF)
	                nodey = parentnode.y() + (parseInt(parentnode.data.shapeHeight)) + 70;
	                nodex = parseInt(parentnode.x()) - ((parseInt(node.data.shapeWidth) - parseInt(parentnode.data.shapeWidth)) / 2);
	            }
	        }
	        else if (connectorIndex == 3) {
	            destIndex = 1;
	            if (node.data.typeIndex != 1)
	                inputDir = "right";
	            else
	                inputDir = "up";
	            outputDir = "left";
	            if (parentnode.data.typeIndex != 1) {//parent item is NOT a FLOWDECISION (IF)
	                nodex = parentnode.x() - (parseInt(node.data.shapeWidth)) - 70;
	                nodey = parseInt(parentnode.y()) - ((parseInt(node.data.shapeHeight) - parseInt(parentnode.data.shapeHeight)) / 2);
	            }
	        }
	        else if (connectorIndex == 4)//true
	        {
	            destIndex = 1;
	            inputDir = "right";
	            outputDir = "left";
	            nodex = parentnode.x() - (parseInt(node.data.shapeWidth)) - 135;
	            nodey = parseInt(parentnode.y()) - ((parseInt(node.data.shapeHeight) - parseInt(parentnode.data.shapeHeight)) / 2);
	            outputCurrentConnector = 1;
	        }
	        else if (connectorIndex == 5)//false
	        {
	            outputCurrentConnector = 2;
	            destIndex = 3;
	            inputDir = "left";
	            outputDir = "right";
	            nodex = parseInt(parentnode.x()) + (parseInt(parentnode.data.shapeWidth)) + 135;
	            nodey = parseInt(parentnode.y()) - ((parseInt(node.data.shapeHeight) - parseInt(parentnode.data.shapeHeight)) / 2);
	        }

	        if (nodex < 20)
	            nodex = 20;

	        var o1 = $scope.OutputConnectorPosition1(parentnode.data.typeIndex);
	        var o2 = $scope.OutputConnectorPosition2(parentnode.data.typeIndex);

	        parentnode.data.connected = true;
	        node.data.connected = true;
	        var outputPoint;
	        if (parentnode.data.numChild == 1) {
	            outputPoint = { x: o1.x, y: o1.y };
	        }
	        else {
	            outputPoint = { x: o2.x, y: o2.y };
	        }

	        var inputPoint = $scope.InputConnectorPosition(node.data.typeIndex,
                { x: parseInt(parentnode.data.x) + parseInt(outputPoint.x), y: parseInt(parentnode.data.x) + parseInt(outputPoint.x) },
            node.data.id);

	        var ic = {
	            x: inputPoint.x,
	            y: inputPoint.y
	        };

	        var tmpOutputPos;
	        var tmpInputPos;
	        for (var i = 0 ; i < connectorPositions.length; i++) {
	            if (connectorPositions[i].typeIndex == parentnode.data.typeIndex && connectorPositions[i].index == srcIndex) {
	                tmpInputPos = { x: parseInt(connectorPositions[i].x), y: parseInt(connectorPositions[i].y) };
	            }
	        }

	        for (var i = 0 ; i < connectorPositions.length; i++) {
	            if (connectorPositions[i].typeIndex == node.data.typeIndex && connectorPositions[i].index == destIndex) {
	                tmpOutputPos = { x: parseInt(connectorPositions[i].x), y: parseInt(connectorPositions[i].y) };
	            }
	        }
	        //d (dragging item on connector)
	        $scope.chart.createNewConnection(parentnode.outputConnectors[parentnode.data.numChild - 1], node.inputConnectors[0], false,
        tmpOutputPos.x, tmpOutputPos.y, tmpInputPos.x, tmpInputPos.y, tmpInputPos.x, tmpInputPos.y, outputCurrentConnector, "", "", inputDir, outputDir, $scope.chart);
	        currentActiveNodeForConnectionSourceID = -1;
	        node.data.x = $scope.RoundNumbers(nodex);
	        node.data.y = $scope.RoundNumbers(nodey);
	        connectorWiseConnectionMode = true;

	        var UndoObject = {
	            action: "addconnection", sourceNodeID: $scope.chart.connections[$scope.chart.connections.length - 1].data.source.nodeID, x: 0, y: 0,
	            id: $scope.chart.connections[$scope.chart.connections.length - 1].data.source.nodeID, srcNodeGUID: parentnode.data.GUID, dstNodeGUID: node.data.GUID,
	            destNodeID: $scope.chart.connections[$scope.chart.connections.length - 1].data.dest.nodeID, ix: tmpOutputPos.x, iy: tmpOutputPos.y,
	            o1x: tmpInputPos.x, o2x: tmpInputPos.x, o1y: tmpInputPos.y, o2y: tmpInputPos.y, onumber: outputCurrentConnector, inputDir: inputDir, outputDir: outputDir
	        };
	        $scope.chart.AddToUndoList(UndoObject);
	        RedoList.length = 0;

	        UndoList[UndoList.length - 1].flwWidthAfter = divDrop.style.width;
	        UndoList[UndoList.length - 1].flwHeightAfter = divDrop.style.height;

	        var realheight = parseInt(divDrop.style.height.replace("px", ""));
	        var realwidth = parseInt(divDrop.style.width.replace("px", ""));
	        $scope.chart.FindMinWidthHeight();

	        if (minHeight + 40 > parseInt(realheight)) {
	            divSettings.style.display = "none";
	            divAddIcon.style.display = "none";
	            divAddDialog.style.display = "none";

	            var h = (minHeight + 40);
	            if (h <= maxFlowchartHeight + initialHeight) {
	                divDrop.style.height = (h) + "px";
	                $("#divChart").height((h));
	                var svg = document.getElementsByTagName("svg");
	                svg[0].setAttribute("height", h);
	                svg[0].style.height = (h) + "px";
	            }
	        }

	        if (minWidth > realwidth) {
	            divSettings.style.display = "none";
	            divAddIcon.style.display = "none";
	            divAddDialog.style.display = "none";
	            divDrop.style.width = (minWidth) + "px";
	            var w = minWidth;
	            $("#divChart").width((w));
	            $("#divFlowChartParent").width((w));
	            $("#divFlowChartParentTitle").width((w));
	            var svg = document.getElementsByTagName("svg");
	            svg[0].setAttribute("width", w);
	            svg[0].style.width = (w) + "px";
	            CenterDrop();
	        }


	    }
	    catch (ex) {
	        //alert(ex.message + " , " + ex.number);
	    }
	};

    //
	// Handle mousedown on an input connector.
    //
	var invalidDrag = false;
	var tmpDragNode;
	var tmpDragNodeAlreadyConnected = false;

	connectorDragOver = function (evt) {

	}

	connectorDragStart = function (evt) {
	    invalidDrag = false;
	    if (touchConnectionSourceNode.data.typeIndex == 1 && (touchconnectorIndex == 0 || touchconnectorIndex == 1 || touchconnectorIndex == 2 || touchconnectorIndex == 3)) {
	        invalidDrag = true;
	        return;
	    }
	    srcConnectorIndex = connectorIndex;
	    //tmpDragNodeAlreadyConnected = touchConnectionSourceNode.data.connectorclicked;
	    touchConnectionSourceNode.data.connectorclicked = true;
	}

	connectorDragEnd = function (evt) {
	    if ($scope.mouseOverNode && $scope.mouseOverNode != touchConnectionSourceNode && !invalidDrag) {
	        try {
	            if ($scope.mouseOverNode.inputConnectors.length > 0 && touchConnectionSourceNode.data.numChild < touchConnectionSourceNode.data.outputConnectors.length) {
	                //do nothing if user is trying to make another line starting from an already existing TRUE branch of an IF node
	                if (touchConnectionSourceNode.data.typeIndex == 1) {
	                    if (touchConnectionSourceNode.data.outputConnectors[0].connected && srcConnectorIndex == 4)
	                        return;
	                    //do nothing if user is trying to make another line starting from an already existing FALSE branch of an IF node
	                    if (touchConnectionSourceNode.data.outputConnectors[1].connected && srcConnectorIndex == 5)
	                        return;
	                }

	                var o1 = $scope.OutputConnectorPosition1(touchConnectionSourceNode.data.typeIndex);
	                var o2 = $scope.OutputConnectorPosition2(touchConnectionSourceNode.data.typeIndex);
	                touchConnectionSourceNode.data.connected = true;
	                touchConnectionSourceNode.data.numChild++;
	                $scope.mouseOverNode.data.connected = true;
	                var tmpInputPos;
	                var outputDir;
	                if (touchConnectionSourceNode.data.typeIndex != 1) {
	                    if (srcConnectorIndex == 0)
	                        outputDir = "up";
	                    else if (srcConnectorIndex == 1)
	                        outputDir = "right";
	                    else if (srcConnectorIndex == 2)
	                        outputDir = "down";
	                    else if (srcConnectorIndex == 3)
	                        outputDir = "left";
	                }
	                else {
	                    if (srcConnectorIndex == 4) {
	                        outputDir = "left";
	                    }
	                    else if (srcConnectorIndex == 5) {
	                        outputDir = "right";
	                    }
	                }

	                var tmpOutputPos;
	                for (var i = 0 ; i < connectorPositions.length; i++) {
	                    if (connectorPositions[i].typeIndex == touchConnectionSourceNode.data.typeIndex && connectorPositions[i].index == srcConnectorIndex) {
	                        tmpInputPos = { x: parseInt(connectorPositions[i].x), y: parseInt(connectorPositions[i].y) };
	                    }
	                }
	                var destConnectorIndex = 0;
	                var inputDir;

	                if ($scope.mouseOverNode.data.typeIndex != 1)//items other than if
	                {
	                    if (destConnectorIndex == 0)
	                        inputDir = "up";
	                    else if (destConnectorIndex == 1)
	                        inputDir = "right";
	                    else if (destConnectorIndex == 2)
	                        inputDir = "down";
	                    else if (destConnectorIndex == 3)
	                        inputDir = "left";
	                }
	                else {
	                    if (destConnectorIndex == 0 || destConnectorIndex == 1 || destConnectorIndex == 2)
	                        inputDir = "up";
	                    else if (destConnectorIndex == 3)
	                        inputDir = "down";
	                }

	                for (var i = 0 ; i < connectorPositions.length; i++) {
	                    if (connectorPositions[i].typeIndex == $scope.mouseOverNode.data.typeIndex && connectorPositions[i].index == destConnectorIndex) {
	                        tmpOutputPos = { x: parseInt(connectorPositions[i].x), y: parseInt(connectorPositions[i].y) };
	                    }
	                }

	                $scope.mouseOverNode.inputConnectors[$scope.mouseOverNode.data.nextInputConnector].data.connected = true;
	                $scope.chart.createNewConnection(touchConnectionSourceNode.outputConnectors[0], $scope.mouseOverNode.inputConnectors[$scope.mouseOverNode.data.nextInputConnector], true,
                        tmpOutputPos.x, tmpOutputPos.y, tmpInputPos.x, tmpInputPos.y, tmpInputPos.x, tmpInputPos.y, parseInt(touchconnectorIndex) - 4 + 1,
                        "", "", inputDir, outputDir, $scope.chart);
	            }
	        }//end try
	        catch (ex) {
	            divDebug.innerHTML = ex.message;
	        }
	    }
	}

	var touchConnectionSourceNode;
	var touchconnectorIndex;

	$scope.connectorMouseDown = function (evt, node, connector, connectorIndex, isInputConnector) {
	    try
	    {
	        //
	        // Initiate dragging out of a connection.
	        //
	        touchconnectorIndex = connectorIndex;
	        touchConnectionSourceNode = node;
	        try {
	            evt.target.addEventListener("touchmove", connectorDragOver, false);
	            evt.target.addEventListener("touchstart", connectorDragStart, false);
	            evt.target.addEventListener("touchend", connectorDragEnd, false);
	        }
	        catch (ex) {
	            divDebug.innerHTML = ex.message;
	        }

	        dragging.startDrag(evt, {
	            //
	            // Called when the mouse has moved greater than the threshold distance
	            // and dragging has commenced.
	            //
	            dragStarted: function (x, y) {
	                invalidDrag = false;
	                if (node.data.typeIndex == 1 && (connectorIndex == 0 || connectorIndex == 1 || connectorIndex == 2 || connectorIndex == 3)) {
	                    invalidDrag = true;
	                    return;
	                }
	                srcConnectorIndex = connectorIndex;
	                tmpDragNodeAlreadyConnected = node.data.connectorclicked;
	                node.data.connectorclicked = true;
	                tmpDragNode = node;
	                var curCoords = controller.translateCoordinates(x, y, evt);
	                var tmpx = evt.clientX - (((divBody.clientWidth - divDrop.clientWidth) / 2) + 50 - divContainer.scrollLeft);
	                var tmpy = evt.clientY - 85 + divContainer.scrollTop;

	                var offset_y = 250 - 55 - 50;
	                if ($(".mainHeader").css('display') == "none")
	                    offset_y = 196 - 55 - 50;

	                GhostPathSrc = {
	                    x: parseInt(evt.clientX) - parseInt(divDrop.offsetLeft) + parseInt(divContainer.scrollLeft),
	                    y: parseInt(evt.clientY) - parseInt(offset_y) + parseInt(divContainer.scrollTop)
	                };
	                $scope.draggingConnection = true;
	                $scope.dragPoint1 = flowchart.computeConnectorPos(node, connectorIndex, isInputConnector);
	                $scope.dragPoint2 = {
	                    x: tmpx,
	                    y: tmpy
	                };
	                $scope.dragTangent1 = flowchart.computeConnectionSourceTangent($scope.dragPoint1, $scope.dragPoint2);
	                $scope.dragTangent2 = flowchart.computeConnectionDestTangent($scope.dragPoint1, $scope.dragPoint2);
	            },

	            //
	            // Called on mousemove while dragging out a connection.
	            //
	            dragging: function (x, y, evt) {
	                var startCoords = controller.translateCoordinates(x, y, evt);
	                var tmpx = evt.clientX - ($("#divDrop").position().left + 30);
	                var tmpy = evt.clientY - ($("#divDrop").position().top + 30);
	                $scope.dragPoint1 = flowchart.computeConnectorPos(node, connectorIndex, isInputConnector);
	                $scope.dragPoint2 = {
	                    x: tmpx,
	                    y: tmpy
	                };
	                destConnectorUpX = tmpx;
	                destConnectorUpY = tmpy;
	                $scope.dragTangent1 = flowchart.computeConnectionSourceTangent($scope.dragPoint1, $scope.dragPoint2);
	                $scope.dragTangent2 = flowchart.computeConnectionDestTangent($scope.dragPoint1, $scope.dragPoint2);
	            },

	            //
	            // Clean up when dragging has finished.
	            //
	            dragEnded: function () {
	                if ($scope.mouseOverNode && $scope.mouseOverNode != node && !invalidDrag){
	                    //
	                    // Dragging has ended...
	                    // The mouse is over a valid connector...
	                    // Create a new connection.
	                    //
	                    try {
	                        if ($scope.mouseOverNode.inputConnectors.length > 0 && node.data.numChild < node.data.outputConnectors.length) {
	                            //do nothing if user is trying to make another line starting from an already existing TRUE branch of an IF node
	                            if (node.data.typeIndex == 1) {
	                                if (node.data.outputConnectors[0].connected && srcConnectorIndex == 4)
	                                    return;
	                                //do nothing if user is trying to make another line starting from an already existing FALSE branch of an IF node
	                                if (node.data.outputConnectors[1].connected && srcConnectorIndex == 5)
	                                    return;
	                            }

	                            var o1 = $scope.OutputConnectorPosition1(node.data.typeIndex);
	                            var o2 = $scope.OutputConnectorPosition2(node.data.typeIndex);
	                            node.data.connected = true;
	                            node.data.numChild++;
	                            $scope.mouseOverNode.data.connected = true;
	                            var tmpInputPos;
	                            var outputDir;
	                            if (node.data.typeIndex != 1) {
	                                if (srcConnectorIndex == 0)
	                                    outputDir = "up";
	                                else if (srcConnectorIndex == 1)
	                                    outputDir = "right";
	                                else if (srcConnectorIndex == 2)
	                                    outputDir = "down";
	                                else if (srcConnectorIndex == 3)
	                                    outputDir = "left";
	                            }
	                            else {
	                                if (srcConnectorIndex == 4) {
	                                    outputDir = "left";
	                                }
	                                else if (srcConnectorIndex == 5) {
	                                    outputDir = "right";
	                                }
	                            }

	                            var tmpOutputPos;
	                            for (var i = 0 ; i < connectorPositions.length; i++) {
	                                if (connectorPositions[i].typeIndex == node.data.typeIndex && connectorPositions[i].index == srcConnectorIndex) {
	                                    tmpInputPos = { x: parseInt(connectorPositions[i].x), y: parseInt(connectorPositions[i].y) };
	                                }
	                            }
	                            var destConnectorIndex = 0;
	                            var inputDir;

	                            //find distance between cursor position & 4 connectors of the destination node, then select the minimum value in order to find the tatget connector
	                            var distance = [];

	                            var destConnectorArray = [];
	                            for (var i = 0 ; i < connectorPositions.length; i++) {
	                                if (connectorPositions[i].typeIndex == $scope.mouseOverNode.data.typeIndex) {
	                                    destConnectorArray.push({
	                                        x: parseInt($scope.mouseOverNode.data.x) + parseInt(connectorPositions[i].x),
	                                        y: parseInt($scope.mouseOverNode.data.y) + parseInt(connectorPositions[i].y)
	                                    });
	                                }
	                            }

	                            for (var i = 0 ; i < 4 ; i++) {
	                                distance.push(Math.sqrt(Math.pow(destConnectorUpX - destConnectorArray[i].x, 2) + Math.pow(destConnectorUpY - destConnectorArray[i].y, 2)));
	                            }
	                            var min = distance[0];

	                            for (var i = 1; i < distance.length; i++) {
	                                if (distance[i] < min) {
	                                    destConnectorIndex = i;
	                                    min = distance[i];
	                                }
	                            }

	                            if ($scope.mouseOverNode.data.typeIndex != 1)//items other than if
	                            {
	                                if (destConnectorIndex == 0)
	                                    inputDir = "up";
	                                else if (destConnectorIndex == 1)
	                                    inputDir = "right";
	                                else if (destConnectorIndex == 2)
	                                    inputDir = "down";
	                                else if (destConnectorIndex == 3)
	                                    inputDir = "left";
	                            }
	                            else {
	                                if (destConnectorIndex == 0 || destConnectorIndex == 1 || destConnectorIndex == 2)
	                                    inputDir = "up";
	                                else if (destConnectorIndex == 3)
	                                    inputDir = "down";
	                            }

	                            for (var i = 0 ; i < connectorPositions.length; i++) {
	                                if (connectorPositions[i].typeIndex == $scope.mouseOverNode.data.typeIndex && connectorPositions[i].index == destConnectorIndex) {
	                                    tmpOutputPos = { x: parseInt(connectorPositions[i].x), y: parseInt(connectorPositions[i].y) };
	                                }
	                            }

	                            var inputPoint = $scope.InputConnectorPosition($scope.mouseOverNode.data.typeIndex,
                                    { x: parseInt(node.data.x) + parseInt(o1.x), y: parseInt(node.data.x) + parseInt(o1.y) },
                        $scope.mouseOverNode.data.id);
	                            var ic = {
	                                x: inputPoint.x,
	                                y: inputPoint.y
	                            };

	                            $scope.mouseOverNode.inputConnectors[$scope.mouseOverNode.data.nextInputConnector].data.connected = true;

	                            //try {
	                            //e (connector to connector)
	                            $scope.chart.createNewConnection(node.outputConnectors[0], $scope.mouseOverNode.inputConnectors[$scope.mouseOverNode.data.nextInputConnector], true,
                                    tmpOutputPos.x, tmpOutputPos.y, tmpInputPos.x, tmpInputPos.y, tmpInputPos.x, tmpInputPos.y, parseInt(connectorIndex) - 4 + 1,
                                    "", "", inputDir, outputDir, $scope.chart);

	                            var UndoObject = {
	                                action: "addconnection", sourceNodeID: $scope.chart.connections[$scope.chart.connections.length - 1].data.source.nodeID, x: 0, y: 0,
	                                id: $scope.chart.connections[$scope.chart.connections.length - 1].data.source.nodeID, srcNodeGUID: node.data.GUID, dstNodeGUID: $scope.mouseOverNode.data.GUID,
	                                destNodeID: $scope.chart.connections[$scope.chart.connections.length - 1].data.dest.nodeID,
	                                ix: tmpOutputPos.x, iy: tmpOutputPos.y, o1x: tmpInputPos.x, o2x: tmpInputPos.x, o1y: tmpInputPos.y,
	                                o2y: tmpInputPos.y, onumber: (parseInt(connectorIndex) - 4 + 1), inputDir: inputDir, outputDir: outputDir
	                            };
	                            //}
	                            //catch (ex) {
	                            //    alert(ex.message + " , " + ex.number);
	                            //}

	                            $scope.chart.AddToUndoList(UndoObject);
	                            RedoList.length = 0;
	                            $scope.mouseOverNode.inputConnectors[$scope.mouseOverNode.data.nextInputConnector].data.numConnection++;
	                            if ($scope.mouseOverNode.data.nextInputConnector < $scope.mouseOverNode.inputConnectors.length - 1) {
	                                $scope.mouseOverNode.data.nextInputConnector++;
	                            }
	                            else {
	                                $scope.mouseOverNode.data.nextInputConnector = 0;
	                            }
	                        }
	                    }
	                    catch (ex) {
	                        //alert(ex.message + " , " + ex.number);
	                    }
	                }

	                $scope.draggingConnection = false;
	                delete $scope.dragPoint1;
	                delete $scope.dragTangent1;
	                delete $scope.dragPoint2;
	                delete $scope.dragTangent2;
	            },

	        });

	    }
	    catch (ex) {
	        //alert(ex.message + " , " + ex.number);
	    }



	};
}])
;
