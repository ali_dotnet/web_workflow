
//
// Global accessor.
//
var flowchart = {

};

// Module.
(function () {

	//
	// Width of a node.
	//
	flowchart.defaultNodeWidth = 300;
	var nodeArray = [];
	//
	// Amount of space reserved for displaying the node's name.
	//
	flowchart.nodeNameHeight = 40;

	//
	// Height of a connector in a node.
	//
	flowchart.connectorHeight = 35;

	//
	// Compute the Y coordinate of a connector, given its index.
	//
	flowchart.computeConnectorY = function (connectorIndex) {
		return flowchart.nodeNameHeight + (connectorIndex * flowchart.connectorHeight);
	}

	//
	// Compute the position of a connector in the graph.
	//
	flowchart.computeConnectorPos = function (node, connectorIndex, inputConnector) {
	    return {
	        x: node.x() + (node.width() / 2),
	        y: node.y() + node.height() + 15 /*flowchart.computeConnectorY(connectorIndex)*/,
	    };
	};

	//
	// View model for a connector.
	//
	flowchart.ConnectorViewModel = function (connectorDataModel, x, y, parentNode) {

		this.data = connectorDataModel;
		this._parentNode = parentNode;
		this._x = x;
		this._y = y;

		//
		// The name of the connector.
		//
		this.name = function () {
			return this.data.name;
		}

		//
		// X coordinate of the connector.
		//
		this.x = function () {

		    return this.data.x;
		};

		this.MakeNode = function () {
		    alert($scope.chartViewModel);
		};

		this.connectorCircleRadius = function () {
		    return this.data.connectorCircleRadius;
		};

		this.caption = function () {
		    return this.data.caption;
		};

		this.SetCaption = function (caption) {
		    this.data.caption = caption;
		};

		//
		// Y coordinate of the connector.
		//
		this.y = function () { 
			return this.data.y;
		};

		//
		// The parent node that the connector is attached to.
		//
		this.parentNode = function () {
			return this._parentNode;
		};
	};

	//
	// Create view model for a list of data models.
	//
	var createConnectorsViewModel = function (connectorDataModels, x, parentNode) {
		var viewModels = [];

		if (connectorDataModels) {
			for (var i = 0; i < connectorDataModels.length; ++i) {
				var connectorViewModel = 
					new flowchart.ConnectorViewModel(connectorDataModels[i], x, flowchart.computeConnectorY(i), parentNode);
				viewModels.push(connectorViewModel);
			}
		}

		return viewModels;
	};

	//
	// View model for a node.
	//
	flowchart.NodeViewModel = function (nodeDataModel) {

		this.data = nodeDataModel;

		// set the default width value of the node
		if (!this.data.width || this.data.width < 0) {
			this.data.width = flowchart.defaultNodeWidth;
		}
		this.inputConnectors = createConnectorsViewModel(this.data.inputConnectors, 0, this);
		this.outputConnectors = createConnectorsViewModel(this.data.outputConnectors, this.data.width, this);

		// Set to true when the node is selected.
		this._selected = false;
		this._hovered = false;

		//
		// Name of the node.
		//
		this.name = function () {
			return this.data.name || "";
		};

		//
		// X coordinate of the node.
	    //

		this.NodesPlanePosition = function () {

		};

		this.x = function () { 
			return this.data.x;
		};

		//
		// Y coordinate of the node.
		//
		this.y = function () {
			return this.data.y;
		};

		this.opacity = function () {
		    return this.data.opacity;
		};

		this.GetNodeInputConnectorX = function (index) {

		    return 0;
		};

		this.GetNodeInputConnectorY = function (index) {
		    return 20;
		};

		this.ConnectorInX = function () {

		    return "12";
		}

		//
		// Width of the node.
		//
		this.width = function () {
		    if (this.data.collapsed)
		        return this.data.collapseWidth;
		    else
		        return this.data.width;
		}

		this.ShowGrid = function () {
		    return this.data.ShowGrid;
		}


		this.ShowInfo = function () {
		    return this.data.ShowInfo;
		}

		this.Lock = function () {
		    return this.data.Lock;
		}

		this.parent = function () {
		    return this.data.parent;
		}

		this.backColor = function () {
		    return this.data.backColor;
		}

		this.collapseable = function () {
		    return this.data.collapseable;
		}

		this.collapsed = function () {
		    return this.data.collapsed;
		}

		this.inputcount = function () {
		    return this.data.inputcount;
		}

		this.collapseWidth = function () {
		    return this.data.collapseWidth;
		}

		this.collapseHeight = function() {
		    return this.data.collapseHeight;
		}

		this.icon = function () {
		    return this.data.icon;
		}

		this.collapserVisible = function () {
		    return this.data.collapserVisible;
		}

		this.numChild = function () {
		    return this.data.numChild;
		}

		this.outputcount = function () {
		    return this.data.outputcount;
		}

		this.rootNodeID = function () {
		    return this.data.rootNodeID;
		}

		this.visible = function () {
		    return this.data.visible;
		}

		this.z = function () {
		    return this.data.z;
		}

		this.currentActivityNumber = function () {
		    return this.data.currentActivityNumber;
		}

		this.shapeWidth = function () {
		    return this.data.shapeWidth;
		}

		this.shapeHeight = function () {
		    return this.data.shapeHeight;
		}

		this.iconWidth = function () {
		    if (!this.data.collapsed)
		        return this.data.iconWidth;
		    else
		        return this.data.iconCollapsedWidth;
		}

		this.iconHeight = function () {
		    if (!this.data.collapsed)
		        return this.data.iconHeight;
		    else
		        return this.data.iconCollapsedHeight;
		}

		this.collapserMouseOver = function () {
		    return this.data.collapserMouseOver;
		}

		this.titleVisible = function () {
		    return this.data.titleVisible;
		}

		this.collapseIconX = function () {
		    if (!this.data.collapsed)
		        return this.data.collapseIconX;
		    else
		        return this.data.collapseWidth - 24;
		}

		this.connectorclicked = function () {
		    return this.data.connectorclicked;
		}

		this.SetConnctorData = function (item) {
		    this.data.connectordata = item;
		}

		this.connectordata = function () {
		    return this.data.connectordata;
		}

		this.activeConnector = function () {
		    return this.data.activeConnector;
		}

		this.dicarray = function () {
		    return this.data.dicarray;
		}

		this.activeConnectorInput = function () {
		    return this.data.activeConnectorInput;
		}

		this.flwtitle = function () {
		    var title = "Flowchart";
		    var parts = FlowchartTitles.split(',');
		    for (var i = 0 ; i < parts.length; i++) {
		        if (parts[i] != "") {
		            var cells = parts[i].split('=');
		            if (cells[0] == this.data.id) {
		                title = cells[1];
		            }
		        }
		    }
		    return title;
		}

		this.dragcovered = function () {
		    return this.data.dragcovered;
		}

		this.hideconnectors = function () {
		    return this.data.hideconnectors;
		}

		this.covered = function () {
		    return this.data.covered;
		}

		this.connected = function () {
		    return this.data.connected;
		}

		this.iconX = function () {
		    if (!this.data.collapsed)
		        return this.data.iconX;
		    else
		        return this.data.iconCollapsedX;
		}

		this.showSettings = function () {
		    return this.data.showSettings;
		}

		this.clickX = function () {
		    return this.data.clickX;
		}

		this.clickY = function () {
		    return this.data.clickY;
		}

		this.settingsX = function () {
		    return this.data.settingsX;
		}

		this.settingsY = function () {
		    return this.data.settingsY;
		}

		this.defaultPosition = function () {
		    return this.data.defaultPosition;
		}


		this.settingDialogVisible = function () {
		    return this.data.settingDialogVisible;
		}

		this.iconY = function () {
		    if (!this.data.collapsed)
		        return this.data.iconY;
		    else
		        return this.data.iconCollapsedY;
		}

		this.collapseIconY = function () {
		    return this.data.collapseIconY;
		}

		this.index = function () {
		    return this.data.typeIndex;
		}

		this.title = function () {
		    return this.data.title;
		}
        
		this.nextInputConnector = function () {
		    return this.data.nextInputConnector;
		}

		this.iconCollapsedWidth = function () {
		    return this.data.iconCollapsedWidth;
		}

		this.iconCollapsedHeight = function () {
		    return this.data.iconCollapsedHeight;
		}

		this.titleX = function () {
		    if (!this.data.collapsed)
		        return this.data.titleX;
		    else
		        return 70;
		}

		this.titleY = function () {
		    if (!this.data.collapsed)
		        return this.data.titleY;
		    else
		        return this.data.collapseHeight - 26;
		}

		this.mouseOver = function () {
		    return this.data.mouseOver;
		}

		this.ShowHover = function () {
		    return this.data.ShowHover;
		}

	    //
		// Height of the node.
		//
		this.height = function () {
		    //var numConnectors =
		    //	Math.max(
		    //		this.inputConnectors.length, 
		    //		this.outputConnectors.length);
		    //return flowchart.computeConnectorY(numConnectors);
		    var ht = 0;
		    if (this.data.collapsed)
		        ht = this.data.collapseHeight;
		    else
		        ht = 170;
		    return ht;
		}

		//
		// Select the node.
		//
		this.select = function () {
			this._selected = true;
		};

		//
		// Deselect the node.
		//
		this.deselect = function () {
		    if (btnTouchMultiSelect.innerHTML == "MultiSelect On") {
		        return;
		    }
		    this.data.backColor = this.data.origBackColor;
		    divSettings.style.display = "none";
		    divAddIcon.style.display = "none";
		    divAddDialog.style.display = "none";
		    divAddDialog.style.opacity = "0.0";
		    divAddDialog.style.filter = 'alpha(opacity=0)';
		    divAddDialog.style.minHeight = "0px";
		    divAddDialog.style.minWidth = "0px";

		    divSettingDialog.style.display = "none";
		    divSettingDialog.style.opacity = "0.0";
		    divSettingDialog.style.filter = 'alpha(opacity=0)';
		    divSettingDialog.style.minHeight = "0px";
		    divSettingDialog.style.minWidth = "0px";
		    this._selected = false;
		    imgDelete.style.display = "none";
		    close_mode = false;
		    close_mode_add = false;
		};

		//
		// Toggle the selection state of the node.
		//
		this.toggleSelected = function () {
		    this._selected = !this._selected;
		    if (this._selected) {
		        this.data.backColor = "#ff0000";
		    }
		    else {
		        divSettings.style.display = "none";
		        divSettingDialog.style.display = "none";
		        divSettingDialog.style.opacity = "0.0";
		        divSettingDialog.style.filter = 'alpha(opacity=0)';
		        divSettingDialog.style.minHeight = "0px";
		        divSettingDialog.style.minWidth = "0px";


		        divAddIcon.style.display = "none";
		        divAddDialog.style.display = "none";
		        divAddDialog.style.opacity = "0.0";
		        divAddDialog.style.filter = 'alpha(opacity=0)';
		        divAddDialog.style.minHeight = "0px";
		        divAddDialog.style.minWidth = "0px";

		        this.data.backColor = "#2ab989";
		        close_mode = false;
		        close_mode_add = false;
		    }
		};

		//
		// Returns true if the node is selected.
		//

		this.nodeid = function () {
		    return this.data.id;
		};


		this.selected = function () {
			return this._selected;
		};

		this.hovered = function () {
		    return this._hovered;
		};

		//
		// Internal function to add a connector.
		this._addConnector = function (connectorDataModel, x, connectorsDataModel, connectorsViewModel) {
			var connectorViewModel = 
				new flowchart.ConnectorViewModel(connectorDataModel, x, 
						flowchart.computeConnectorY(connectorsViewModel.length), this);

			connectorsDataModel.push(connectorDataModel);

			// Add to node's view model.
			connectorsViewModel.push(connectorViewModel);
		}

		//
		// Add an input connector to the node.
		//
		this.addInputConnector = function (connectorDataModel) {

			if (!this.data.inputConnectors) {
				this.data.inputConnectors = [];
			}
			this._addConnector(connectorDataModel, 0, this.data.inputConnectors, this.inputConnectors);
		};

		//
		// Add an ouput connector to the node.
		//
		this.addOutputConnector = function (connectorDataModel) {

			if (!this.data.outputConnectors) {
				this.data.outputConnectors = [];
			}
			this._addConnector(connectorDataModel, this.data.width, this.data.outputConnectors, this.outputConnectors);
		};
	};

	// 
	// Wrap the nodes data-model in a view-model.
	//
	var createNodesViewModel = function (nodesDataModel) {
		var nodesViewModel = [];

		if (nodesDataModel) {
			for (var i = 0; i < nodesDataModel.length; ++i) {
			    nodeArray.push(new flowchart.NodeViewModel(nodesDataModel[i]));
			    nodesViewModel.push(new flowchart.NodeViewModel(nodesDataModel[i]));
			}
		}

		return nodesViewModel;
	};

	//
	// View model for a connection.
	//
	flowchart.ConnectionViewModel = function (connectionDataModel, sourceConnector, destConnector, rootnodeid, inx, iny, outx1, outy1, outx2, outy2, sourceOutputID, cap, xamlRootNode,
        inputDir, outputDir, chartmodel) {
	    this.chartmodel = chartmodel;
	    this.data = connectionDataModel;
	    this.source = sourceConnector;
	    this.dest = destConnector;
	    this._caption = cap;
	    this.inputx = inx;
	    this.inputy = iny;
	    this.ox1 = outx1;
	    this.oy1 = outy1;
	    this.ox2 = outx2;
	    this.oy2 = outy2;
	    this.sourceOutputID = sourceOutputID;
	    this.lastSrcPos = { x: -1000, y: -1000 };
	    this.lastPath;
	    this.LineSwitchFirstNodeAdded = false;
	    this.customModeFirstNodeAdded = false;
	    this.customModeLastNodeAdded = false;
	    this.customModeFirstStepCreated = false;
	    this.customLineData = "";
	    this.PathPointArray = [];
	    this.PathTwoFirstPoints = [];
	    this.OriginalFirstPoint;
	    this.OriginalLastPoint;
	    this.lastDestPos;
	    this.pathLineArray = [];
	    this.inputDir = inputDir;
	    this.tooltipXPos;
	    this.tooltipYPos;
	    this.outputDir = outputDir;
	    this.LastCorrectPath;

	    // Set to true when the connection is selected.
	    this._selected = false;
	    this._visible = true;
	    if (xamlRootNode != null) {//a XAML file is being loaded (so the current connection which is created should be INVISIBLE, as it is not in the CURRENT FLOWCHART)
	        if (xamlRootNode != "") {
	            if (xamlRootNode > 0) {
	                this._visible = false;
	            }
	        }
	    }
	    this._rootNodeID = rootnodeid;
	    this.rootNodeIDForeach = "";
	    if (ForeachActive) {
	        this.rootNodeIDForeach = SelectedForEachNode.data.id;
	    }
	    if (xamlRootNode != null) {
	        if (xamlRootNode != "") {
	            this._rootNodeID = xamlRootNode;
	        }
	    }
	    this.custom = 0;
	    this.PathPoint;
	    this.PathPoint2;

	    this.tmpYOffset = 0;
	    this.tmpYOffset1 = this.dest.parentNode().y() + this.inputy - 40;
	    this.tmpYOffset2 = this.source.parentNode().y() + this.oy1 + 50;
	    try
	    {
	        this.source.parentNode().data.outputConnectors[sourceOutputID - 1].connected = true;
	    }
	    catch (ex) {

	    }

	    this.pathmode = "single";
	    this.dualcurrentside = "left";

	    this.CurrentYOffset = 0;
	    this.CurrentYOffset1 = 0;
	    this.CurrentYOffset2 = 0;

	    this.CurrentXOffset = 0;

	    this.tmpPath = false;
	    this.tmpPath2 = false;
	    this.tmpPathStepX = 0;
	    this.tmpPathStepW = 0;
	    this.tmpPathStepH = 0;
	    this.PathXArray = new Array();
	    this.PathYArray = new Array();

	    this.PathXArrayOrig = new Array();
	    this.PathYArrayOrig = new Array();

	    this.pathTmpX = new Array();
	    this.pathTmpY = new Array();

	    this.pathTmpX2 = new Array();
	    this.pathTmpY2 = new Array();

	    this.pathTmpX3 = new Array();
	    this.pathTmpY3 = new Array();

	    this.pathMilestoneIndex = new Array();
	    this.tmpX = 0;
	    this.tmpY = 0;
	    this.tmpX2 = 0;
	    this.tmpY2 = 0;
	    this.dest.parentNode().data.parent = this.source.parentNode().data.id;
	    this.dest.parentNode().data.parentIDs.push(this.source.parentNode().data.id);
	    this.source.parentNode().data.childrenGUIDs.push({ guid: this.dest.parentNode().data.GUID, number: sourceOutputID });
	    this.pathMilestoneIndex.push(1);
	    this.pathMilestoneIndex.push(2);

	    this.tmpXOffset = (this.source.parentNode().x() + this.inputx + 15) + (((this.dest.parentNode().x() + this.inputx + 15) - (this.source.parentNode().x() + this.ox1 + 15)) / 2);
	    if (this.source.parentNode().x() == this.dest.parentNode().x())
	        this.tmpXOffset += 70;

	    this.GetNumPathPoints = function () {
	        return this.PathXArray.length;
	    };

	    this.ComputePath = function () {
	        //default path
	        var outputx;
	        var outputy;
	        if (this.sourceOutputID == 1) {
	            outputy = this.oy1;
	            outputx = this.ox1;
	        }
	        else {
	            outputy = this.oy2;
	            outputx = this.ox2;
	        }
	        this.PathXArray.length = 0;
	        this.PathYArray.length = 0;
	        var dy = this.dest.parentNode().y() + this.inputy - 4;
	        var sy = this.source.parentNode().y() + outputy + 30;

	        this.CurrentYOffset = this.source.parentNode().y() + outputy + 30 + ((dy - sy) / 2);
	        this.PathXArray.push(this.source.parentNode().x() + outputx + 15);
	        var heightdistance = parseInt(this.dest.parentNode().y()) - parseInt(this.source.parentNode().y()) - parseInt(outputy) + 30 - 100;

	        if (this.tmpYOffset < -1 * (heightdistance / 2) && this.tmpYOffset < 0)
	            this.tmpYOffset = -1 * (heightdistance / 2);
	        if (this.tmpYOffset > heightdistance / 2 && this.tmpYOffset > 0)
	            this.tmpYOffset = heightdistance / 2;

	        this.PathYArray.push(this.source.parentNode().y() + outputy + 0 + ((dy - sy) / 2) + this.tmpYOffset);//0 was previously 30

	        this.PathXArray.push(this.dest.parentNode().x() + this.inputx + 15);
	        this.PathYArray.push(this.source.parentNode().y() + outputy + 0 + ((dy - sy) / 2) + this.tmpYOffset);//0 was previously 30

	        this.PathXArray.push(this.dest.parentNode().x() + this.inputx + 15);
	        this.PathYArray.push(dy);

	        this.PathXArrayOrig.length = 0;
	        this.PathYArrayOrig.length = 0;
	        this.PathXArrayOrig.push(this.source.parentNode().x() + outputx + 15);
	        this.PathYArrayOrig.push(this.source.parentNode().y() + outputy + 30);

	        this.PathXArrayOrig.push(this.source.parentNode().x() + outputx + 15);
	        this.PathYArrayOrig.push(this.source.parentNode().y() + outputy + 30 + ((dy - sy) / 2));

	        this.PathXArrayOrig.push(this.dest.parentNode().x() + this.inputx + 15);
	        this.PathYArrayOrig.push(this.source.parentNode().y() + outputy + 30 + ((dy - sy) / 2));

	        this.PathXArrayOrig.push(this.dest.parentNode().x() + this.inputx + 15);
	        this.PathYArrayOrig.push(dy);

	    };

	    this.visible = function () {
	        return this._visible;
	    };

	    this.rootNodeID = function () {
	        return this._rootNodeID;
	    };

	    this.AddPoint = function (index, x, y) {
	        //loop through all current points, find distance between click point and each point, then find the 2 nearest points to the click point
	        tmpDistance = new Array();
	        modes = new Array();
	        var centerPointList = new Array();
	        for (var i = 0 ; i < this.PathXArrayOrig.length - 1; i++) {
	            //horizontal
	            if (this.PathYArrayOrig[i] == this.PathYArrayOrig[i + 1]) {
	                if (this.PathXArrayOrig[i] < this.PathXArrayOrig[i + 1]) {
	                    centerPointList.push({ x: this.PathXArrayOrig[i] + ((this.PathXArrayOrig[i + 1] - this.PathXArrayOrig[i]) / 2), y: this.PathYArrayOrig[i] });
	                }
	                else {
	                    centerPointList.push({ x: this.PathXArrayOrig[i + 1] + ((this.PathXArrayOrig[i] - this.PathXArrayOrig[i + 1]) / 2), y: this.PathYArrayOrig[i] });
	                }
	                modes.push("H");
	            }
	                //vertical
	            else {
	                if (this.PathYArrayOrig[i] < this.PathYArrayOrig[i + 1]) {
	                    centerPointList.push({ x: this.PathXArrayOrig[i], y: this.PathYArrayOrig[i] + ((this.PathYArrayOrig[i + 1] - this.PathYArrayOrig[i]) / 2) });
	                }
	                else {
	                    centerPointList.push({ x: this.PathXArrayOrig[i], y: this.PathYArrayOrig[i + 1] + ((this.PathYArrayOrig[i] - this.PathYArrayOrig[i - 1]) / 2) });
	                }
	                modes.push("V");
	            }
	        }

	        for (var i = 0 ; i < centerPointList.length; i++) {
	            tmpDistance.push(Math.sqrt(Math.pow(x - centerPointList[i].x, 2) + Math.pow(y - centerPointList[i].y, 2)));
	        }

	        var index = 0;
	        var minDistance = 10000000;
	        for (var i = 0 ; i < tmpDistance.length; i++) {
	            if (tmpDistance[i] < minDistance) {
	                minDistance = tmpDistance[i];
	                index = i;
	            }
	        }

	        //make a step
	        for (var i = 0 ; i < this.PathXArrayOrig.length - 1; i++) {
	            if (index == i) {
	                if (modes[i] == "H") {
	                    if (i == 0) {
	                        this.pathTmpX.push(x - 25);
	                        this.pathTmpY.push(this.PathYArrayOrig[i]);

	                        this.pathTmpX.push(x - 25);
	                        this.pathTmpY.push(y);

	                        this.pathTmpX.push(x + 25);
	                        this.pathTmpY.push(y);

	                        this.pathTmpX.push(x + 25);
	                        this.pathTmpY.push(this.PathYArrayOrig[i]);
	                    }
	                    else if (i == 1) {
	                        this.pathTmpX2.push(x - 25);
	                        this.pathTmpY2.push(this.PathYArrayOrig[i]);

	                        this.pathTmpX2.push(x - 25);
	                        this.pathTmpY2.push(y);

	                        this.pathTmpX2.push(x + 25);
	                        this.pathTmpY2.push(y);

	                        this.pathTmpX2.push(x + 25);
	                        this.pathTmpY2.push(this.PathYArrayOrig[i]);
	                    }
	                    else if (i == 2) {
	                        this.pathTmpX3.push(x - 25);
	                        this.pathTmpY3.push(this.PathYArrayOrig[i]);

	                        this.pathTmpX3.push(x - 25);
	                        this.pathTmpY3.push(y);

	                        this.pathTmpX3.push(x + 25);
	                        this.pathTmpY3.push(y);

	                        this.pathTmpX3.push(x + 25);
	                        this.pathTmpY3.push(this.PathYArrayOrig[i]);
	                    }

	                }
	                else {
	                    if (i == 0) {
	                        this.pathTmpX.push(this.PathXArrayOrig[i]);
	                        this.pathTmpY.push(y - 25);

	                        this.pathTmpX.push(x);
	                        this.pathTmpY.push(y - 25);

	                        this.pathTmpX.push(x);
	                        this.pathTmpY.push(y + 25);


	                        this.pathTmpX.push(this.PathXArrayOrig[i]);
	                        this.pathTmpY.push(y + 25);
	                    }
	                    else if (i == 1) {
	                        this.pathTmpX2.push(this.PathXArrayOrig[i]);
	                        this.pathTmpY2.push(y - 25);

	                        this.pathTmpX2.push(x);
	                        this.pathTmpY2.push(y - 25);

	                        this.pathTmpX2.push(x);
	                        this.pathTmpY2.push(y + 25);


	                        this.pathTmpX2.push(this.PathXArrayOrig[i]);
	                        this.pathTmpY2.push(y + 25);
	                    }
	                    else if (i == 2) {
	                        this.pathTmpX3.push(this.PathXArrayOrig[i]);
	                        this.pathTmpY3.push(y - 25);

	                        this.pathTmpX3.push(x);
	                        this.pathTmpY3.push(y - 25);

	                        this.pathTmpX3.push(x);
	                        this.pathTmpY3.push(y + 25);


	                        this.pathTmpX3.push(this.PathXArrayOrig[i]);
	                        this.pathTmpY3.push(y + 25);
	                    }
	                }
	            }
	        }
	    };

	    this.PathNumNodes = function () {
	        return nodeArray.length;
	    };

	    this.PathNodeX = function (index) {
	        return nodeArray[index].x();
	    };

	    this.PathNodeY = function (index) {
	        return nodeArray[index].y();
	    };

	    this.PathNodeWidth = function (index) {
	        return nodeArray[index].width();
	    };

	    this.PathNodeHeight = function (index) {
	        return 150;
	    };

	    this.Test = function () {
	        return nodeArray[0].x() + "," + nodeArray[1].x();
	    };

	    this.a = function () {
	        return true;
	    };

	    this.ConnectionNodes = [];

	    this.sourceCoordX = function () { 
	        return this.source.parentNode().x() + this.source.x() + 15;
	    };

	    this.pathMiddlePoint1X = function () {
	        return this.PathTmp1X() + 0;
	        //(this.PathTmp1X() - srcX)
	    };

	    this.pathMiddlePoint1Y = function () {
	        //return this.sourceCoordY() + (this.distanceY() / 2);
	        return this.PathTmp1Y();
	    };

	    this.pathMiddlePoint2X = function () {
	        return this.PathTmp2X();
	    };

	    this.pathMiddlePoint2Y = function () {
	        //return this.sourceCoordY() + (this.distanceY() / 2);
	        return this.PathTmp2Y();
	    };

	    this.sourceCoordY = function () { 
	        return this.source.parentNode().y() + this.source.y() + 30;
	    };

	    this.sourceCoord = function () {
	        return {
	            x: this.sourceCoordX(),
	            y: this.sourceCoordY()
	        };
	    }

	    this.sourceTangentX = function () { 
	        return flowchart.computeConnectionSourceTangentX(this.sourceCoord(), this.destCoord());
	    };

	    this.sourceTangentY = function () { 
	        return flowchart.computeConnectionSourceTangentY(this.sourceCoord(), this.destCoord());
	    };

	    this.tmpArray = function () {
	        var ar = new Array();
	        ar.push(1);
            alert(ar.length);
	        return ar;
	    };

	    this.destCoordX = function () {
	        try
	        {
	            return this.dest.parentNode().x() + this.inputx + 15;
	        }
	        catch (ex) {
	            //alert(ex.message + " , " + ex.number);
	        }
	    };

	    this.distanceX = function () {
	        return this.destCoordX() - this.sourceCoordX();
	    };

	    this.distanceY = function () {
	        return (this.destCoordY() - this.sourceCoordY());
	    };

	    this.ArrowStartX = function () {
	        return this.destCoordX() - 5;
	    };

	    this.ArrowStartY = function () {
	        return this.destCoordY() - 15;
	    };

	    this.PathCircleX = function () {
	        return this.sourceCoordX();
	    }

	    this.PathCircleY = function () {
	        return this.sourceCoordY() + (this.distanceY() / 2);
	    }

	    this.Text = "Caption";
	    this.Points = [];

	    this.ToolTipWidth = function () {
	        return (this.Text.length * 5);
	    }

	    this.PathTmpStepX = function () {
	        return this.tmpPathStepX;
	    };

	    this.PathTmpStepW = function () {
	        return this.tmpPathStepW;
	    };

	    this.PathTmpStepH = function () {
	        return this.tmpPathStepH;
	    };

	    this.PathTmp1X = function () {
	        if (!this.tmpPath)
	            return this.sourceCoordX();
	        else
	            return this.tmpX;
	    };

	    this.PathTmp1Y = function () {
	        if (!this.tmpPath)
	            return this.sourceCoordY() + (this.distanceY() / 2);
	        else {

	            return this.tmpY;
	        }
	    };

	    this.PathTmp2X = function () {
	        if (!this.tmpPath2)
	            return this.destCoordX();
	        else {
	            return this.tmpX2;
	        }
	    };

	    this.PathTmp2Y = function () {
	        if (!this.tmpPath2)
	            return this.sourceCoordY() + (this.distanceY() / 2);
	        else {

	            return this.tmpY2;
	        }
	    };

	    this.MakeTmpPathX = function (x, number) {
	        if (number == 1)
	            this.tmpPath = true;
	        else
	            this.tmpPath2 = true;
	        if (number == 1) {
	            this.tmpX = x;
	        }
	        else {
	            this.tmpX2 = x;
	        }
	    };

	    this.MakeTmpPathY = function (y, number) {
	        this.tmpPath = true;
	        if (number == 1) {
	            this.tmpY = y;
	        }
	        else {
	            this.tmpY2 = y;
	        }
	    };

	    this.GetPathPoint2 = function () {
	        return this.PathPoint2;
	    };

	    this.GetPathPoint = function () {
	        return this.PathPoint;
	    };

	    this.Path = function () {
	        this.PathPoint = "";
	        this.PathPoint2 = "";
	        this.Points.length = 0;
	        var srcX = this.sourceCoordX();
	        var srcY = this.sourceCoordY();
	        var destX = this.destCoordX();
	        var destY = this.destCoordY();
	        var outputx;
	        var outputy;

	        if (this.sourceOutputID == 1) {
	            outputy = this.oy1;
	            outputx = this.ox1;
	        }
	        else {
	            outputy = this.oy2;
	            outputx = this.ox2;
	        }
	        var inputOffset = { x: 0, y: 0 };
	        var outputOffset = { x: 0, y: 0 };

	        var path = "";//"m " + (this.source.parentNode().x() + outputx + 15) + "," + (this.source.parentNode().y() + outputy);
	        this.PathPoint += (parseInt(this.source.parentNode().x()) + parseInt(outputx) + 15) + "," + (parseInt(this.source.parentNode().y()) + parseInt(outputy)) + " ";
	        try {
	            //our path is composed of 3 lines, although one (or two) of these lines may be omitted in some special cases (line length may become 0), position and length of each line is
	            //determined based on position of source and destination nodes
	            var inputDir = this.inputDir;
	            var outputDir = this.outputDir;
	            var pathPointOutputOffset = { x: 0, y: 0 };
	            var pathPointOutputOffsetLast = { x: 0, y: 0 };

	            if (inputDir == "up") {
	                inputOffset.y = -30;//-30
	                pathPointOutputOffsetLast.y = -4;
	            }
	            else if (inputDir == "right") {
	                inputOffset.x = 30;//30
	                pathPointOutputOffsetLast.x = 4;
	            }
	            else if (inputDir == "left") {
	                inputOffset.x = -30;//-30
	                pathPointOutputOffsetLast.x = -4;
	            }
	            else if (inputDir == "down") {
	                inputOffset.y = 30;//30
	                pathPointOutputOffsetLast.y = 4;
	            }
	            if (outputDir == "down") {
	                outputOffset.y = 30;//30
	                pathPointOutputOffset.y = 4;
	            }
	            else if (outputDir == "left") {
	                outputOffset.x = -30;
	                pathPointOutputOffset.x = -4;
	            }
	            else if (outputDir == "right") {
	                outputOffset.x = 30;
	                pathPointOutputOffset.x = 4;
	            }
	            else if (outputDir == "up") {
	                outputOffset.y = -30;//-30
	                pathPointOutputOffset.y = -4;
	            }

	            var srcPointMain = {
	                x: this.RoundNumbers(parseInt(this.source.parentNode().x()) + parseInt(outputx)),
	                y: this.RoundNumbers(parseInt(this.source.parentNode().y()) + parseInt(outputy))
	            };

	            var srcPoint = {
	                x: this.RoundNumbers(parseInt(this.source.parentNode().x()) + parseInt(outputx) + parseInt(outputOffset.x)),
	                y: this.RoundNumbers(parseInt(this.source.parentNode().y()) + parseInt(outputy) + parseInt(outputOffset.y))
	            };

	            if (this.inputx == undefined || this.inputx == null || this.inputx == 'undefined')
	                this.inputx = 0;
	            if (this.inputy == undefined || this.inputy == null || this.inputy == 'undefined')
	                this.inputy = 0;

	            var destPoint = {
	                x: this.RoundNumbers(parseInt(this.dest.parentNode().x()) + parseInt(this.inputx) + parseInt(inputOffset.x)),
	                y: this.RoundNumbers(parseInt(this.dest.parentNode().y()) + parseInt(this.inputy) + parseInt(inputOffset.y))
	            };

	            if (destPoint.x < 0)
	                destPoint.x = 0;
	            if (srcPoint.x < 0)
	                srcPoint.x = 0;
	            if (destPoint.y < 0)
	                destPoint.y = 0;
	            if (srcPoint.y < 0)
	                srcPoint.y = 0;
	            if (srcPoint.x > parseInt(divDrop.style.width.replace("px", "")))
	                srcPoint.x = parseInt(divDrop.style.width.replace("px", ""));
	            if (destPoint.x > parseInt(divDrop.style.width.replace("px", "")))
	                destPoint.x = parseInt(divDrop.style.width.replace("px", ""));
	            if (srcPoint.y > parseInt(divDrop.style.height.replace("px", "")))
	                srcPoint.y = parseInt(divDrop.style.height.replace("px", ""));
	            if (destPoint.y > parseInt(divDrop.style.height.replace("px", "")))
	                destPoint.y = parseInt(divDrop.style.height.replace("px", ""));
	            var sameSourceDestPoint = false;
	            if (this.lastSrcPos.x != srcPoint.x || this.lastSrcPos.y != srcPoint.y || this.lastDestPos.x != destPoint.x || this.lastDestPos.y != destPoint.y) {
	                this.lastSrcPos = srcPoint;
	                this.lastDestPos = destPoint;
	                this.LineSwitchFirstNodeAdded = false;
	                this.PathPointArray.length = 0;
	                this.PathTwoFirstPoints.length = 0;
	                this.PathPointArray.push({
	                    x: parseInt(srcPoint.x) - parseInt(outputOffset.x) + parseInt(pathPointOutputOffset.x),
	                    y: parseInt(srcPoint.y) - parseInt(outputOffset.y) + parseInt(pathPointOutputOffset.y), index: 0
	                });

	                this.PathPointArray.push({
	                    x: parseInt(destPoint.x) - parseInt(inputOffset.x) + parseInt(pathPointOutputOffsetLast.x),
	                    y: parseInt(destPoint.y) - parseInt(inputOffset.y) + parseInt(pathPointOutputOffsetLast.y), index: 1
	                });
	                if (srcPoint.x == destPoint.x && srcPoint.y == destPoint.y) {
	                    sameSourceDestPoint = true;
	                }
	                else {
	                    this.MakePath(srcPoint, destPoint);
	                    sameSourceDestPoint = false;
	                }
	                this.customLineData = "";
	                SelectedPointIndex = -1;
	                ChainedPointIndex = -1;
	            }

	            if (this.customLineData == "") {
	                var coincidenceLength = 0;
	                var destLengthOffset = 0;
	                //correct the path (detect any possible coincidency and remove it)
	                if (!sameSourceDestPoint) {
	                    try {
	                        if (this.PathTwoFirstPoints[0].x == this.PathTwoFirstPoints[1].x)//vertical
	                        {
	                            if (outputOffset.y != 0) {
	                                if (this.PathTwoFirstPoints[0].x == srcPointMain.x) {
	                                    if (parseInt(this.PathTwoFirstPoints[0].y) >= parseInt(srcPointMain.y) &&
                                            parseInt(this.PathTwoFirstPoints[0].y) <= parseInt(srcPointMain.y) + parseInt(outputOffset.y) &&
                                            parseInt(this.PathTwoFirstPoints[1].y) >= parseInt(srcPointMain.y) &&
                                            parseInt(this.PathTwoFirstPoints[1].y) <= parseInt(srcPointMain.y) + parseInt(outputOffset.y)
                                            ) {
	                                        coincidenceLength = Math.abs(parseInt(this.PathTwoFirstPoints[1].y) - parseInt(this.PathTwoFirstPoints[0].y));
	                                    }
	                                    if (parseInt(this.PathTwoFirstPoints[0].y) <= parseInt(srcPointMain.y) &&
            parseInt(this.PathTwoFirstPoints[0].y) >= parseInt(srcPointMain.y) + parseInt(outputOffset.y) &&
            parseInt(this.PathTwoFirstPoints[1].y) <= parseInt(srcPointMain.y) &&
            parseInt(this.PathTwoFirstPoints[1].y) >= parseInt(srcPointMain.y) + parseInt(outputOffset.y)
            ) {
	                                        coincidenceLength = Math.abs(parseInt(this.PathTwoFirstPoints[1].y) - parseInt(this.PathTwoFirstPoints[0].y));
	                                    }
	                                }
	                            }
	                        }

	                        if (inputDir == "left" || inputDir == "right") {//horizontal
	                            if (inputOffset.x != 0) {
	                                if (this.PathTwoFirstPoints[this.PathTwoFirstPoints.length - 1].y == destPoint.y) {
	                                    var lastpointx = this.PathTwoFirstPoints[this.PathTwoFirstPoints.length - 1].x;
	                                    var lastpoint2x = this.PathTwoFirstPoints[this.PathTwoFirstPoints.length - 2].x;
	                                    if (parseInt(lastpointx) >= parseInt(destPoint.x) &&
                                            parseInt(lastpointx) <= parseInt(destPoint.x) - parseInt(inputOffset.x) &&
                                            parseInt(lastpoint2x) >= parseInt(destPoint.x) &&
                                            parseInt(lastpoint2x) <= parseInt(destPoint.x) - parseInt(inputOffset.x)) {
	                                        destLengthOffset = -1 * Math.abs(parseInt(lastpointx) - parseInt(lastpoint2x));//left
	                                    }
	                                    if (parseInt(lastpointx) <= parseInt(destPoint.x) &&
                                            parseInt(lastpointx) >= parseInt(destPoint.x) - parseInt(inputOffset.x) &&
                                            parseInt(lastpoint2x) <= parseInt(destPoint.x) &&
                                            parseInt(lastpoint2x) >= parseInt(destPoint.x) - parseInt(inputOffset.x)) {
	                                        destLengthOffset = Math.abs(parseInt(lastpointx) - parseInt(lastpoint2x));//right
	                                    }
	                                }
	                            }
	                        }
	                    }
	                    catch (ex) {

	                    }
	                }

	                if (outputOffset.x != 0) {
	                    outputOffset.x -= coincidenceLength;
	                }
	                if (outputOffset.y != 0) {
	                    if (outputOffset.y > 0)
	                        outputOffset.y -= coincidenceLength;
	                    else
	                        outputOffset.y += coincidenceLength;
	                }
	                path = " m " + srcPointMain.x + " , " + srcPointMain.y + " l " + outputOffset.x + " , " + outputOffset.y;
	                path += this.lastPath;
	                if (coincidenceLength != 0) {
	                    path = path.replace(" M " + srcPoint.x + " , " + srcPoint.y + " ",
                            " M " + (parseInt(srcPointMain.x) + parseInt(outputOffset.x)) + " , " + (parseInt(srcPointMain.y) + parseInt(outputOffset.y)) + " ");
	                }
	                if (destLengthOffset != 0) {
	                    path = path.replace(" L " + this.PathTwoFirstPoints[this.PathTwoFirstPoints.length - 1].x + " , " + destPoint.y, "");
	                }

	                this.customModeFirstNodeAdded = false;
	                this.customModeFirstStepCreated = false;
	                this.customModeLastNodeAdded = false;
	                beforeNodeIndex = -1;
	                afterNodeIndex = -1;
	            }
	            else {

	            }
	        }
	        catch (ex) {
	            //alert(ex.message + " , " + ex.number);
	        }
	        path = path.replace("undefined", "");
	        //down arrow

	        if (this.customLineData == "") {
	            path += " M " + (parseInt(destPoint.x) - parseInt(destLengthOffset)) + "," + (parseInt(destPoint.y) + 0);
	            if (inputDir == "up") {
	                path += " l 0 , " + (-1 * inputOffset.y);
	                path += " l 0 , -10";
	                path += " l " + "-3" + ", " + "0";
	                path += " l " + "3" + ", " + "10";
	                path += " l " + "3" + ", " + "-10";
	                path += " l " + "-6" + ", " + "0";
	            }
	                //left arrow
	            else if (inputDir == "right") {
	                path += " l " + (-1 * (parseInt(inputOffset.x) - parseInt(destLengthOffset))) + " , " + "0";
	                path += " l 15 , 0";
	                path += " l " + "0" + ", " + "-3";
	                path += " l " + "-10" + ", " + "3";
	                path += " l " + "10" + ", " + "3";
	                path += " l " + "0" + ", " + "-6";
	            }
	                //right arrow
	            else if (inputDir == "left") {
	                path += " l " + (-1 * (parseInt(inputOffset.x) - parseInt(destLengthOffset))) + " , " + "0";
	                path += " l -10 , 0";
	                path += " l " + "0" + ", " + "-3";
	                path += " l " + "10" + ", " + "3";
	                path += " l " + "-10" + ", " + "3";
	                path += " l " + "0" + ", " + "-6";
	            }
	                //up arrow
	            else if (inputDir == "down") {
	                path += " M " + destPoint.x + " , " + destPoint.y;
	                path += " l 0 , " + (-1 * inputOffset.y);
	                path += " l 0 , 10";
	                path += " l " + "-3" + ", " + "0";
	                path += " l " + "3" + ", " + "-10";
	                path += " l " + "3" + ", " + "10";
	                path += " l " + "-6" + ", " + "0";
	            }
	        }

	        if (this.sourceOutputID == 2) {
	            this.PathPoint2 = this.PathPoint;
	        }

	        return path;
	    };

	    //path finding algorithm starts from here
 
	    this.UpdatePathMap = function (makeVoid) {
	        PathMap.length = 0;
	        var width = divDrop.style.width.replace("px", "");
	        var height = divDrop.style.height.replace("px", "");
	        var stepX = 10;
	        var stepY = 10;
	        for (var i = 0 ; i <= parseInt(width) + 10; i += stepX) {
	            var tmpArray = new Array();
	            if (makeVoid == null) {
	                tmpArray.push("x");
	                tmpArray.push("x");
	                tmpArray.push("x");
	                tmpArray.push("x");
	            }
	            else {
	                tmpArray.push("_");
	                tmpArray.push("_");
	                tmpArray.push("_");
	                tmpArray.push("_");
	            }
	            for (var j = 40 ; j <= parseInt(height) + 10; j += stepY) {
	                if (this.PositionIsCovered(i, j) && makeVoid == null) {
	                    tmpArray.push("x");
	                }
	                else {
	                    tmpArray.push("_");
	                }
	            }
	            PathMap.push(tmpArray);
	        }
	        return PathMap;
	    };

	    var AStargrid = [];

	    this.MakeNewPath = function (src, dest) {
	        AStargrid.length = 0;
	        var width = divDrop.style.width.replace("px", "");
	        var height = divDrop.style.height.replace("px", "");
	        var stepX = 10;
	        var stepY = 10;

	        //CoverLookup.length = 0;
	        //var margin = 0;
	        //for (var i = 1 ; i < this.chartmodel.nodes.length; i++) {
	        //    var node = this.chartmodel.nodes[i];
	        //    if (node.visible()) {
	        //        var left = parseInt(node.data.x) - parseInt(margin);
	        //        var top = parseInt(node.data.y) - parseInt(margin);
	        //        var right = parseInt(node.data.x) + parseInt(node.data.shapeWidth) + parseInt(margin);
	        //        var bottom = parseInt(node.data.y) + parseInt(node.data.shapeHeight) + parseInt(margin);
	        //        if (x >= left && x <= right && y >= top && y <= bottom) {
	        //            //return true;

	        //        }
	        //    }
	        //}

	        for (var i = 0 ; i <= parseInt(height) + 10; i += stepY) {
	            var tmpArray = new Array();
	            //tmpArray.push("1");
	            //tmpArray.push("1");
	            //tmpArray.push("1");
	            //tmpArray.push("1");
	            for (var j = 0 ; j <= parseInt(width) + 10; j += stepX) {
	                if (this.PositionIsCovered(j, i)) {
	                //if (CoverLookup[i][j]) {
	                    tmpArray.push("1");
	                }
	                else {
	                    tmpArray.push("0");
	                }
	            }
	            AStargrid.push(tmpArray);
	        }
	        var graph = new Graph(AStargrid);
	        var roundx = this.RoundNumbers(dest.x / 10);
	        var roundy = this.RoundNumbers(dest.y / 10);
	        var start = graph.nodes[this.RoundNumbers(src.y) / 10][this.RoundNumbers(src.x) / 10];
	        var end = graph.nodes[this.RoundNumbers(dest.y) / 10][this.RoundNumbers(dest.x) / 10];
	        var tmppath = astar.search(graph.nodes, start, end, false);
	        if (tmppath.length == 0) {
	            tmppath = astar.search(graph.nodes, start, end, true);
	        }
	        var newpath = [];
	        if (tmppath.length != 0) {
	            if (tmppath[0].x != src.x || tmppath[0].y != src.y) {
	                tmppath.unshift({ x: src.y / 10, y: src.x / 10 });
	            }
	        }
	        for (var i = 0 ; i < tmppath.length; i++) {
	            newpath.push({ x: tmppath[i].y * 10, y: tmppath[i].x * 10 });
	        }

	        return newpath;
	    };

	    this.RoundNumbers = function (number) {
	        var result = number;
	        var x = number % 10;
	        if (x <= 5) {
	            result = number - x;
	        }
	        else {
	            result = number + (10 - x);
	        }
	        return result;
	    };

	    //start A*
	    var astar = {
	        init: function (grid) {
	            for (var x = 0; x < grid.length; x++) {
	                for (var y = 0; y < grid[x].length; y++) {
	                    var node = grid[x][y];
	                    node.f
	                    node.f = 0;
	                    node.g = 0;
	                    node.h = 0;
	                    node.visited = false;
	                    node.closed = false;
	                    node.debug = "";
	                    node.parent = null;
	                }
	            }
	        },
	        search: function (grid, start, end, nullmode) {
	            astar.init(grid);
	            heuristic = astar.manhattan;
	            var openList = [];
	            openList.push(start);

	            var openHeap = new BinaryHeap(function (node) { return node.f; });
	            openHeap.push(start);
	            while (openHeap.size() > 0) {

	                // Grab the lowest f(x) to process next.  Heap keeps this sorted for us.
	                var currentNode = openHeap.pop();

	                // End case -- result has been found, return the traced path
	                if (currentNode == end) {
	                    var curr = currentNode;
	                    var ret = [];
	                    while (curr.parent) {
	                        ret.push(curr);
	                        curr = curr.parent;
	                    }
	                    return ret.reverse();
	                }

	                // Normal case -- move currentNode from open to closed, process each of its neighbors
	                currentNode.closed = true;

	                var neighbors = astar.neighbors(grid, currentNode);
	                for (var i = 0; i < neighbors.length; i++) {
	                    var neighbor = neighbors[i];
	                    if (!nullmode) {
	                        if (neighbor.closed || ((neighbor.isWall()) && neighbor != end)) {
	                            // not a valid node to process, skip to next neighbor
	                            continue;
	                        }
	                    }
	                    else {
	                        if (neighbor.closed) {
	                            // not a valid node to process, skip to next neighbor
	                            continue;
	                        }
	                    }

	                    // g score is the shortest distance from start to current node, we need to check if
	                    //   the path we have arrived at this neighbor is the shortest one we have seen yet
	                    // 1 is the distance from a node to it's neighbor.  This could be variable for weighted paths.
	                    var gScore = currentNode.g + 1;
	                    var gScoreIsBest = false;
	                    var beenVisited = neighbor.visited;

	                    if (!beenVisited || gScore < neighbor.g) {

	                        // Found an optimal (so far) path to this node.  Take score for node to see how good it is.				    
	                        neighbor.visited = true;
	                        neighbor.parent = currentNode;
	                        neighbor.h = neighbor.h || (2 * heuristic(neighbor.pos, end.pos));
	                        neighbor.g = gScore;
	                        neighbor.f = neighbor.g + neighbor.h;

	                        if (!beenVisited) {
	                            // Pushing to heap will put it in proper place based on the 'f' value.
	                            openHeap.push(neighbor);
	                        }
	                        else {
	                            // Already seen the node, but since it has been rescored we need to reorder it in the heap
	                            openHeap.rescoreElement(neighbor);
	                        }
	                    }
	                }
	            }
	            // No result was found -- empty array signifies failure to find path
	            return [];
	        },
	        manhattan: function (pos0, pos1) {

	            var d1 = Math.abs(pos1.x - pos0.x);
	            var d2 = Math.abs(pos1.y - pos0.y);
	            return d1 + d2;
	        },
	        neighbors: function (grid, node) {
	            var ret = [];
	            var x = node.x;
	            var y = node.y;

	            if (grid[x - 1] && grid[x - 1][y]) {
	                ret.push(grid[x - 1][y]);
	            }
	            if (grid[x + 1] && grid[x + 1][y]) {
	                ret.push(grid[x + 1][y]);
	            }
	            if (grid[x][y - 1] && grid[x][y - 1]) {
	                ret.push(grid[x][y - 1]);
	            }
	            if (grid[x][y + 1] && grid[x][y + 1]) {
	                ret.push(grid[x][y + 1]);
	            }

	            return ret;
	        }
	    };
        ///////////////////

	    if (!Array.prototype.indexOf) {
	        Array.prototype.indexOf = function (elt /*, from*/) {
	            var len = this.length;

	            var from = Number(arguments[1]) || 0;
	            from = (from < 0)
                     ? Math.ceil(from)
                     : Math.floor(from);
	            if (from < 0)
	                from += len;

	            for (; from < len; from++) {
	                if (from in this &&
                        this[from] === elt)
	                    return from;
	            }
	            return -1;
	        };
	    }

	    Array.prototype.remove = function (from, to) {
	        var rest = this.slice((to || from) + 1 || this.length);
	        this.length = from < 0 ? this.length + from : from;
	        return this.push.apply(this, rest);
	    };

	    var GraphNodeType = { OPEN: 0, WALL: 1 };
	    function Graph(grid) {
	        this.elements = grid;
	        this.nodes = [];

	        for (var x = 0; x < grid.length; x++) {
	            var row = grid[x];
	            this.nodes[x] = [];
	            for (var y = 0; y < row.length; y++) {
	                this.nodes[x].push(new GraphNode(x, y, row[y]));
	            }
	        }
	    }
	    Graph.prototype.toString = function () {
	        var graphString = "\n";
	        var nodes = this.nodes;
	        for (var x = 0; x < nodes.length; x++) {
	            var rowDebug = "";
	            var row = nodes[x];
	            for (var y = 0; y < row.length; y++) {
	                rowDebug += row[y].type + " ";
	            }
	            graphString = graphString + rowDebug + "\n";
	        }
	        return graphString;
	    };

	    function GraphNode(x, y, type) {
	        this.data = {};
	        this.x = x;
	        this.y = y;
	        this.pos = { x: x, y: y };
	        this.type = type;
	    }
	    GraphNode.prototype.toString = function () {
	        return "[" + this.x + " " + this.y + "]";
	    };
	    GraphNode.prototype.isWall = function () {
	        return this.type == GraphNodeType.WALL;
	    };

	    function BinaryHeap(scoreFunction) {
	        this.content = [];
	        this.scoreFunction = scoreFunction;
	    }

	    BinaryHeap.prototype = {
	        push: function (element) {
	            // Add the new element to the end of the array.
	            this.content.push(element);
	            // Allow it to sink down.
	            this.sinkDown(this.content.length - 1);
	        },

	        pop: function () {
	            // Store the first element so we can return it later.
	            var result = this.content[0];
	            // Get the element at the end of the array.
	            var end = this.content.pop();
	            // If there are any elements left, put the end element at the
	            // start, and let it bubble up.
	            if (this.content.length > 0) {
	                this.content[0] = end;
	                this.bubbleUp(0);
	            }
	            return result;
	        },
	        remove: function (node) {
	            var len = this.content.length;
	            // To remove a value, we must search through the array to find
	            // it.
	            for (var i = 0; i < len; i++) {
	                if (this.content[i] == node) {
	                    // When it is found, the process seen in 'pop' is repeated
	                    // to fill up the hole.
	                    var end = this.content.pop();
	                    if (i != len - 1) {
	                        this.content[i] = end;
	                        if (this.scoreFunction(end) < this.scoreFunction(node))
	                            this.sinkDown(i);
	                        else
	                            this.bubbleUp(i);
	                    }
	                    return;
	                }
	            }
	            throw new Error("Node not found.");
	        },

	        size: function () {
	            return this.content.length;
	        },

	        rescoreElement: function (node) {
	            this.sinkDown(this.content.indexOf(node));
	        },
	        sinkDown: function (n) {
	            // Fetch the element that has to be sunk.
	            var element = this.content[n];
	            // When at 0, an element can not sink any further.
	            while (n > 0) {
	                // Compute the parent element's index, and fetch it.
	                var parentN = Math.floor((n + 1) / 2) - 1,
                        parent = this.content[parentN];
	                // Swap the elements if the parent is greater.
	                if (this.scoreFunction(element) < this.scoreFunction(parent)) {
	                    this.content[parentN] = element;
	                    this.content[n] = parent;
	                    // Update 'n' to continue at the new position.
	                    n = parentN;
	                }
	                    // Found a parent that is less, no need to sink any further.
	                else {
	                    break;
	                }
	            }
	        },

	        bubbleUp: function (n) {
	            // Look up the target element and its score.
	            var length = this.content.length,
                    element = this.content[n],
                    elemScore = this.scoreFunction(element);

	            while (true) {
	                // Compute the indices of the child elements.
	                var child2N = (n + 1) * 2, child1N = child2N - 1;
	                // This is used to store the new position of the element,
	                // if any.
	                var swap = null;
	                // If the first child exists (is inside the array)...
	                if (child1N < length) {
	                    // Look it up and compute its score.
	                    var child1 = this.content[child1N],
                            child1Score = this.scoreFunction(child1);
	                    // If the score is less than our element's, we need to swap.
	                    if (child1Score < elemScore)
	                        swap = child1N;
	                }
	                // Do the same checks for the other child.
	                if (child2N < length) {
	                    var child2 = this.content[child2N],
                            child2Score = this.scoreFunction(child2);
	                    if (child2Score < (swap == null ? elemScore : child1Score))
	                        swap = child2N;
	                }

	                // If the element needs to be moved, swap it, and continue.
	                if (swap != null) {
	                    this.content[n] = this.content[swap];
	                    this.content[swap] = element;
	                    n = swap;
	                }
	                    // Otherwise, we are done.
	                else {
	                    break;
	                }
	            }
	        }
	    };


        //end A*

	    this.CreateActualSVGPath = function (points) {
	        if (points.length == 0)
	            return;
	        //take src and dest points as the first and last points of our SVG line, then loop through all points (except src/dest) and for each point compare the previous and next points,
	        //if both of these compared points have the same orientation (horizontal or vertical), then pass away and go to the next point the list
	        var line = "";

	        line = " M " + points[0].x + " , " + points[0].y;
	        var pointInfo = [];
	        pointInfo.push({ x: points[0].x, y: points[0].y, valid: true });
	        this.PathTwoFirstPoints.push({ x: points[0].x, y: points[0].y  });
	        //this.PathPointArray.push({ x: points[0].x, y: points[0].y, index: 0 });

	        var maxLen = -1;
	        if (points.length > 2) {
	            for (var i = 1 ; i < points.length - 1; i++) {
	                var pointBefore = points[i - 1];
	                var pointAfter = points[i + 1];

	                var h = false;
	                var v = false;
	                if (pointBefore.y == pointAfter.y) {
	                    h = true;
	                }
	                if (pointBefore.x == pointAfter.x) {
	                    v = true;
	                }

	                if (!h && !v) {
	                    pointInfo.push({ x: points[i].x, y: points[i].y, valid: true });
	                    this.PathTwoFirstPoints.push({ x: points[i].x, y: points[i].y });
	                    //this.PathPointArray.push({ x: points[i].x, y: points[i].y, index: this.PathPointArray.length });
	                }
	            }
	        }

	        var middleX;
	        var middleY;

	        for (var i = 1 ; i < pointInfo.length; i++) {
	            if (pointInfo[i].valid) {
	                line += " L " + pointInfo[i].x + " , " + pointInfo[i].y;
	            }
	        }

	        line += " L " + points[points.length - 1].x + " , " + points[points.length - 1].y;
	        pointInfo.push({ x: points[points.length - 1].x, y: points[points.length - 1].y });
	        this.PathTwoFirstPoints.push({ x: points[points.length - 1].x, y: points[points.length - 1].y });
	        //this.PathPointArray.push({ x: points[points.length - 1].x, y: points[points.length - 1].y, index: this.PathPointArray.length });

	        for (var i = 1 ; i < pointInfo.length; i++) {
	            var len = Math.abs(pointInfo[i].x - pointInfo[i - 1].x) + Math.abs(pointInfo[i].y - pointInfo[i - 1].y);
	            if (len > maxLen) {
	                maxLen = len;
	                middleX = Math.min(pointInfo[i].x, pointInfo[i - 1].x) + (Math.abs(pointInfo[i].x - pointInfo[i - 1].x)) / 2;
	                middleY = Math.min(pointInfo[i].y, pointInfo[i - 1].y) + (Math.abs(pointInfo[i].y - pointInfo[i - 1].y)) / 2;
	            }
	        }
	        this.tooltipXPos = middleX;
	        this.tooltipYPos = middleY;
	        this.lastPath = line;
	    };

	    this.Heuristic = function (A, B) {
	        return Math.abs(A.x - B.x) + Math.abs(A.y - B.y);
	    };
	    var finalNewPath = [];
	    this.MakePath = function (src, dest, voidMode) {
	        finalNewPath = this.MakeNewPath(src, dest);
	        this.CreateActualSVGPath(finalNewPath);
	    };

	    var CoverLookup = [];
	    this.PositionIsCovered = function (x, y) {
	        var result = false;
	        //return true if passed position is covered by a node
	        try
	        {
	            var margin = 20;
	            for (var i = 1 ; i < this.chartmodel.nodes.length; i++) {
	                var node = this.chartmodel.nodes[i];
	                if (node.visible()) {
	                    var left = parseInt(node.data.x) - parseInt(margin);
	                    var top = parseInt(node.data.y) - parseInt(margin);
	                    var right = parseInt(node.data.x) + parseInt(node.data.shapeWidth) + parseInt(margin);
	                    var bottom = parseInt(node.data.y) + parseInt(node.data.shapeHeight) + parseInt(margin);
	                    if (x >= left && x <= right && y >= top && y <= bottom) {
	                        return true;
	                    }
	                }
	            }
	        }
	        catch (ex) {
	            divDebug.innerHTML = ex.message;
	        }

	        return result;
	    };


        //end path finding

	    this.FindQuarter = function (src, dest) {
	        var quarter = 1;

	        var deg2rad = Math.PI/180;
	        var rad2deg = 180/Math.PI;

	        var angle = Math.atan((dest.y - src.y) / (dest.x - src.x)) * rad2deg;

	        if (angle >= 0 && dest.y > src.y) {
	            quarter = 4;
	        }
	        else if (angle < 0 && dest.y > src.y) {
	            quarter = 3;
	        }
	        else if (angle >= 0 && dest.y < src.y) {
	            quarter = 2;
	        }
	        else if (angle < 0 && dest.y < src.y) {
	            quarter = 1;
	        }

	        return quarter;
	    };

	    this.PathCaption = function () {
	        return this._caption;
	    };

	    this.PathTooltipX = function () {
	        return this.tooltipXPos;
	    };

	    this.PathTooltipY = function () {
	        return this.tooltipYPos;
	    };

		this.NewPathX = function () {
		    if (this.distanceY() >= 0)
		        return -1000000;
		    else
		        return this.sourceCoordX() + (this.distanceX() / 2);
		};

		this.NewPathY = function () {
		    if (this.distanceY() >= 0)
		        return -1000000;
		    else
		        return this.sourceCoordY();
		};

		this.path4Y = function () {
		    if (this.distanceY() >= 0)
		        return this.distanceY() / 2;
		    else
		        return 0;
		};

		this.path5X = function () {
		        return 0;
		};

		this.path5Y = function () {
		    if (this.distanceY() >= 0)
		        return 0;
		    else
		        return 20;
		};

		this.destCoordY = function () { 
		    return this.dest.parentNode().y() + this.inputy;
		};

		this.destCoord = function () {
			return {
				x: this.destCoordX(),
				y: this.destCoordY()
			};
		}

		this.destTangentX = function () { 
			return flowchart.computeConnectionDestTangentX(this.sourceCoord(), this.destCoord());
		};

		this.destTangentY = function () { 
			return flowchart.computeConnectionDestTangentY(this.sourceCoord(), this.destCoord());
		};

		//
		// Select the connection.
		//
		this.select = function () {
		    this._selected = true;
		};

		this.CustomMode = function (mode) {
		    this.custom = mode;
		};

		//
		// Deselect the connection.
		//
		this.deselect = function () {
		    this._selected = false;
		    imgDelete.style.display = "none";
		};

		//
		// Toggle the selection state of the connection.
	    //

		this.GetSourceNodeID = function () {
		    return this.source.parentNode().data.id;
		};

		this.GetDestNodeID = function () {
		    return this.dest.parentNode().data.id;
		};

		this.toggleSelected = function () {
			this._selected = !this._selected;
		};

		//
		// Returns true if the connection is selected.
	    //



		this.selected = function () {
		    return this._selected;
		};
	};

	//
	// Helper function.
	//
	var computeConnectionTangentOffset = function (pt1, pt2) {

		return (pt2.x - pt1.x) / 2;	
	}

	//
	// Compute the tangent for the bezier curve.
	//
	flowchart.computeConnectionSourceTangentX = function (pt1, pt2) {

		return pt1.x + computeConnectionTangentOffset(pt1, pt2);
	};

	//
	// Compute the tangent for the bezier curve.
	//
	flowchart.computeConnectionSourceTangentY = function (pt1, pt2) {

		return pt1.y;
	};

	//
	// Compute the tangent for the bezier curve.
	//
	flowchart.computeConnectionSourceTangent = function(pt1, pt2) {
		return {
			x: flowchart.computeConnectionSourceTangentX(pt1, pt2),
			y: flowchart.computeConnectionSourceTangentY(pt1, pt2),
		};
	};

	//
	// Compute the tangent for the bezier curve.
    //

	flowchart.GetPointInfo = function () {
	    return 10;
	};

	flowchart.computeConnectionDestTangentX = function (pt1, pt2) {

		return pt2.x - computeConnectionTangentOffset(pt1, pt2);
	};

	//
	// Compute the tangent for the bezier curve.
	//
	flowchart.computeConnectionDestTangentY = function (pt1, pt2) {

		return pt2.y;
	};

	//
	// Compute the tangent for the bezier curve.
	//
	flowchart.computeConnectionDestTangent = function(pt1, pt2) {
		return {
			x: flowchart.computeConnectionDestTangentX(pt1, pt2),
			y: flowchart.computeConnectionDestTangentY(pt1, pt2),
		};
	};

	//
	// View model for the chart.
	//
	flowchart.ChartViewModel = function (chartDataModel) {
	    this.names = new Array();
	    this.icons = new Array();
	    this.settingsHTML = new Array();
	    this.outputCountList = new Array();
	    this.backColorList = new Array();
	    this.captionList = new Array();
	    this.ShapeWidthList = new Array();
	    this.ShapeHeightList = new Array();
	    this.GhostPath = " ";
	    //
	    //
	    // Find a specific node within the chart.
	    //
	    this.findNode = function (nodeID) {

	        for (var i = 0; i < this.nodes.length; ++i) {
	            var node = this.nodes[i];
	            if (node.data.id == nodeID) {
	                return node;
	            }
	        }

	        throw new Error("Failed to find node " + nodeID);
	    };

	    //
	    // Find a specific input connector within the chart.
	    //
	    this.findInputConnector = function (nodeID, connectorIndex) {

	        var node = this.findNode(nodeID);

	        if (!node.inputConnectors || node.inputConnectors.length <= connectorIndex) {
	            throw new Error("Node " + nodeID + " has invalid input connectors.");
	        }

	        return node.inputConnectors[connectorIndex];
	    };

	    //
	    // Find a specific output connector within the chart.
	    //
	    this.findOutputConnector = function (nodeID, connectorIndex) {

	        var node = this.findNode(nodeID);

	        if (!node.outputConnectors || node.outputConnectors.length <= connectorIndex) {
	            throw new Error("Node " + nodeID + " has invalid output connectors.");
	        }

	        return node.outputConnectors[connectorIndex];
	    };

	    //
	    // Create a view model for connection from the data model.
	    //
	    this._createConnectionViewModel = function(connectionDataModel) {
	        var Points = [];
	        Points.push("0");
	        Points.push("1");
	        Points.push("2");
	        var sourceConnector = this.findOutputConnector(connectionDataModel.source.nodeID, connectionDataModel.source.connectorIndex);
	        var destConnector = this.findInputConnector(connectionDataModel.dest.nodeID, connectionDataModel.dest.connectorIndex);			
	        return new flowchart.ConnectionViewModel(connectionDataModel, sourceConnector, destConnector);
	    }

	    // 
	    // Wrap the connections data-model in a view-model.
	    //
	    this._createConnectionsViewModel = function (connectionsDataModel) {

	        var connectionsViewModel = [];
	        var Points = [];
	        Points.push("0");
	        Points.push("1");
	        Points.push("2");

	        if (connectionsDataModel) {
	            for (var i = 0; i < connectionsDataModel.length; ++i) {
	                connectionsViewModel.push(this._createConnectionViewModel(connectionsDataModel[i]));
	            }
	        }

	        return connectionsViewModel;
	    };

	    // Reference to the underlying data.
	    this.data = chartDataModel;

	    // Create a view-model for nodes.
	    this.nodes = createNodesViewModel(this.data.nodes);

	    this.ArgumentName = new Array();
	    this.ArgumentType = new Array();
	    this.ArgumentDirection = new Array();
	    this.UndoIndex = 0;
	    this.ShowGrid = false;

	    this.ShowInfo = true;
	    this.Lock = false;
	    this.XLines = [];
	    this.YLines = [];
	    for (var i = 0 ; i < 1000; i++) {
	        this.XLines.push(i * 25);
	        this.YLines.push(i * 25);
	    }

	    // Create a view-model for connections.
	    this.connections = this._createConnectionsViewModel(this.data.connections);

	    //
	    // Create a view model for a new connection.
	    //
	    this.createNewConnection = function (startConnector, endConnector, mode, inputx, inputy, outputx1, outputy1, outputx2, outputy2, outputCurrentConnector, cap, specialRootID, inputDir, outputDir,
            chartmodel) {
	        if (endConnector == undefined) {
	            return;
	        }
	        try {
	            if (inputDir == null) {
	                inputDir = "up";
	            }
	            if (outputDir == null) {
	                outputDir = "down";
	            }

	            var connectionsDataModel = this.data.connections;
	            if (!connectionsDataModel) {
	                connectionsDataModel = this.data.connections = [];
	            }

	            var connectionsViewModel = this.connections;
	            if (!connectionsViewModel) {
	                connectionsViewModel = this.connections = [];
	            }

	            var startNode = startConnector.parentNode();
	            if (startNode.data.typeIndex == 1) {
	                if (outputCurrentConnector == 1)
	                    cap = "TRUE";
	                else
	                    cap = "FALSE";
	            }
	            var startConnectorIndex = startNode.outputConnectors.indexOf(startConnector);
	            var startConnectorType = 'output';
	            if (startConnectorIndex == -1) {
	                startConnectorIndex = startNode.inputConnectors.indexOf(startConnector);
	                startConnectorType = 'input';
	                if (startConnectorIndex == -1) {
	                    throw new Error("Failed to find source connector within either inputConnectors or outputConnectors of source node.");
	                }
	            }

	            var endNode = endConnector.parentNode();
	            endNode.data.childNumberOfParent = outputCurrentConnector;
	            var endConnectorIndex = endNode.inputConnectors.indexOf(endConnector);
	            var endConnectorType = 'input';
	            if (endConnectorIndex == -1) {
	                endConnectorIndex = endNode.outputConnectors.indexOf(endConnector);
	                endConnectorType = 'output';
	                if (endConnectorIndex == -1) {
	                    throw new Error("Failed to find dest connector within inputConnectors or outputConnectors of dest node.");
	                }
	            }

	            if (startConnectorType == endConnectorType) {
	                throw new Error("Failed to create connection. Only output to input connections are allowed.")
	            }

	            if (startNode == endNode) {
	                throw new Error("Failed to create connection. Cannot link a node with itself.")
	            }

	            var startNode = {
	                nodeID: startNode.data.id,
                    GUID: startNode.data.GUID,
	                connectorIndex: startConnectorIndex,
	            }

	            var endNode = {
	                nodeID: endNode.data.id,
                    GUID: endNode.data.GUID,
	                connectorIndex: endConnectorIndex,
	            }

	            var connectionDataModel = {
	                source: startConnectorType == 'output' ? startNode : endNode,
	                dest: startConnectorType == 'output' ? endNode : startNode,
	            };
	            connectionsDataModel.push(connectionDataModel);

	            var outputConnector = startConnectorType == 'output' ? startConnector : endConnector;
	            var inputConnector = startConnectorType == 'output' ? endConnector : startConnector;
	            var connectionViewModel = new flowchart.ConnectionViewModel(connectionDataModel, outputConnector, inputConnector, currentRootNodeID, inputx, inputy,
                    outputx1, outputy1, outputx2, outputy2, outputCurrentConnector, cap, specialRootID, inputDir, outputDir, chartmodel);
	            connectionsViewModel.push(connectionViewModel);
	            if (outputDir == "up") {
	                startConnector.parentNode().data.activeConnector = "0";
	            }
	            else if (outputDir == "down") {
	                startConnector.parentNode().data.activeConnector = "2";
	            }
	            else if (outputDir == "left") {
	                startConnector.parentNode().data.activeConnector = "3";
	            }
	            else if (outputDir == "right") {
	                startConnector.parentNode().data.activeConnector = "1";
	            }

	            if (inputDir == "up") {
	                endConnector.parentNode().data.activeConnectorInput += "0";
	            }
	            else if (inputDir == "down") {
	                endConnector.parentNode().data.activeConnectorInput += "2";
	            }
	            else if (inputDir == "left") {
	                endConnector.parentNode().data.activeConnectorInput += "3";
	            }
	            else if (inputDir == "right") {
	                endConnector.parentNode().data.activeConnectorInput += "1";
	            }
	        }
	        catch (ex) {
	            //alert(ex.message + " , " + ex.number);
	        }
	    };

	    //
	    // Add a node to the view model.
	    //

	    this.AlignTop = function () {
	        var topY = 50000;
	        for (var i = 0 ; i < this.nodes.length; i++) {
	            if (this.nodes[i]._selected) {
	                if (topY > this.nodes[i].data.y) {
	                    topY = this.nodes[i].data.y;
	                }
	            }
	        }
	        for (var i = 0 ; i < this.nodes.length; i++) {
	            if (this.nodes[i]._selected) {
	                this.nodes[i].data.y = topY;
	            }
	        }
	    };

	    this.AlignRight = function () {

	    };

	    this.VoidDrag = function () {
	        this.DragIndex = -1;
	    };

	    this.ToolClick = function () {
	        this.DragIndex = 1;
	    };

	    this.ToolClick = function (index, item) {
	        this.DragIndex = index;
	        if (item != null) {
	            this.tmpConnectorItem = item;
	        }
	    };

	    this.Caption1Change = function () {
	        var id = -1;
	        for (var i = 0 ; i < this.nodes.length; i++) {
	            if (this.nodes[i]._selected) {
	                id = this.nodes[i].data.id;
	            }
	        }
	        if (id != -1) {
	            this.GetNodeById(id).data.title = txtDisplayName.value;
	        }
	    };

	    this.DisplayNameChange = function () {
	        var id = -1;
	        for (var i = 0 ; i < this.nodes.length; i++) {
	            if (this.nodes[i]._selected) {
	                id = this.nodes[i].data.id;
	            }
	        }
	        if (id != -1) {
	            this.GetNodeById(id).data.title = txtDisplayName.value;
	        }
	    };

	    this.ChangeRoot = function (index) {
	        var ret = false;
	        $('#imgEraser').css("display", "none");
	        currentRootNodeID = index;
	        for (var i = 0 ; i < this.nodes.length; i++) {//hide all nodes
	            this.nodes[i].data.visible = false;
	            //only show nodes that are child of current rootNodeID
	            if (this.nodes[i].data.rootNodeID == currentRootNodeID || this.nodes[i].data.id == 0) {
	                this.nodes[i].data.visible = true;
	                ret = true;
	                if (this.nodes[i].data.id != 0 && this.nodes[i].data.typeIndex != 8) {
	                    $('#imgEraser').css("display", "");
	                }
	            }
	        }
	        for (var i = 0 ; i < this.connections.length; i++) {
	            this.connections[i]._visible = false;
	            if (this.connections[i].rootNodeID() == currentRootNodeID) {
	                this.connections[i]._visible = true;
	            }
	        }
	        return ret;
	    };

	    this.ClearAll = function () {
//clear all nodes and connections (except start node) in each flowchart
	        for (var i = 0 ; i < this.nodes.length; i++) {
	            if (this.nodes[i].data.typeIndex != 6 && this.nodes[i].data.typeIndex != 8 && this.nodes[i].data.rootNodeID == currentRootNodeID) {
	                this.nodes[i].select();
	            }
	        }
	        this.DeleteSelectedObjects();
	    }

	    this.DeleteSelectedObjects = function () {
	            this.deleteSelected();

	        imgDelete.style.display = "none";
	        divSettings.style.display = "none";
	        divAddIcon.style.display = "none";
	        divAddDialog.style.display = "none";
	        foreachif = false;
	    };

	    this.AlignLeft = function () {
	        var leftX = 50000;
	        for (var i = 0 ; i < this.nodes.length; i++) {
	            if (this.nodes[i]._selected) {
	                if (leftX > this.nodes[i].data.x) {
	                    leftX = this.nodes[i].data.x;
	                }
	            }
	        }
	        for (var i = 0 ; i < this.nodes.length; i++) {
	            if (this.nodes[i]._selected) {
	                this.nodes[i].data.x = leftX;
	            }
	        }
	    };

	    this.AlignBottom = function () {

	    };

	    this.addNode = function (nodeDataModel) {
	        if (!this.data.nodes) {
	            this.data.nodes = [];
	        }



	        // 
	        // Update the data model.
	        //
	        this.data.nodes.push(nodeDataModel);

	        // 
	        // Update the view model.
	        //
	        nodeArray.push(new flowchart.NodeViewModel(nodeDataModel));
	        this.nodes.push(new flowchart.NodeViewModel(nodeDataModel));		
	    }

	    //
	    // Select all nodes and connections in the chart.
	    //
	    this.selectAll = function () {

	        var nodes = this.nodes;
	        for (var i = 0; i < nodes.length; ++i) {
	            var node = nodes[i];
	            node.select();
	        }

	        var connections = this.connections;
	        for (var i = 0; i < connections.length; ++i) {
	            var connection = connections[i];
	            connection.select();
	        }			
	    }

	    //
	    // Deselect all nodes and connections in the chart.
	    //
	    this.deselectAll = function () {
	        var nodes = this.nodes;
	        for (var i = 0; i < nodes.length; ++i) {
	            var node = nodes[i];
	            node.deselect();
	        }

	        var connections = this.connections;
	        for (var i = 0; i < connections.length; ++i) {
	            var connection = connections[i];
	            connection.deselect();
	        }
	    };

	    //
	    // Update the location of the node and its connectors
	    //
	    this.updateSelectedNodesLocation = function (deltaX, deltaY, relative) {
	        var selectedNodes = this.getSelectedNodes();

	        var noderight = -1000;
	        var nodebottom = -1000;
	        var nodeleft = 10000;
	        var nodetop = 10000;

	        for (var i = 0; i < selectedNodes.length; i++) {
	            if (parseInt(selectedNodes[i].data.x) + parseInt(deltaX) + parseInt(selectedNodes[i].data.shapeWidth) > noderight) {
	                noderight = parseInt(selectedNodes[i].data.x) + parseInt(deltaX) + parseInt(selectedNodes[i].data.shapeWidth);
	            }

	            if (parseInt(selectedNodes[i].data.x) + parseInt(deltaX) < nodeleft) {
	                nodeleft = parseInt(selectedNodes[i].data.x) + parseInt(deltaX);
	            }

	            if (parseInt(selectedNodes[i].data.y) + parseInt(deltaY) + parseInt(selectedNodes[i].data.shapeHeight) > nodebottom) {
	                nodebottom = parseInt(selectedNodes[i].data.y) + parseInt(deltaY) + parseInt(selectedNodes[i].data.shapeHeight);
	            }

	            if (parseInt(selectedNodes[i].data.y) + parseInt(deltaY) < nodetop) {
	                nodetop = parseInt(selectedNodes[i].data.y) + parseInt(deltaY);
	            }
	        }

	        if (noderight > (maxFlowchartWidth + initialWidth) || nodebottom > (maxFlowchartHeight + initialHeight) || nodeleft < 20 || nodetop < 30) {
	            return;
	        }

	        for (var i = 0; i < selectedNodes.length; ++i) {
	            var node = selectedNodes[i];
	            if (relative) {
	                node.data.x += deltaX;
	                node.data.y += deltaY;
	            }
	            else {
	                node.data.x = deltaX;
	                node.data.y = deltaY;
	            }
	        }
	    };

	    var SelectedFlowChartNode = null;

	    this.FindFlowchartSize = function (nodeID, jsonmode) {
	        var size = initialWidth + ";" + initialHeight;
	        var parts = FlowchartSizes.split(',');
	        var flowchartNotFound = true;
	        for (var i = 0 ; i < parts.length; i++) {
	            if (parts[i] != "") {
	                var cells = parts[i].split('=');
	                if (cells[0] == nodeID) {
	                    size = cells[1];
	                    flowchartNotFound = false;
	                }
	            }
	        }
	        if (flowchartNotFound && jsonmode != null) {
	            size = divDrop.style.width.replace("px", "") + ";" + divDrop.style.height.replace("px", "");
	        }
	        return size;
	    }

	    this.FindFlowchartTitle = function (nodeID) {
	        var title = "Flowchart";
	        var parts = FlowchartTitles.split(',');
	        for (var i = 0 ; i < parts.length; i++) {
	            if (parts[i] != "") {
	                var cells = parts[i].split('=');
	                if (cells[0] == nodeID) {
	                    title = cells[1];
	                }
	            }
	        }
	        if (this.GetNodeById(nodeID).data.typeIndex == 13)//foreach
	        {
	            title = "Foreach";
	        }
	        return title;
	    }

	    this.FlowChartHasStart = function (nodeID) {
	        var hasStart = false;
	        var rows = flowChartIDStart.split(',');
	        for (var i = 0 ; i < rows.length; i++) {
	            if (rows[i] != "") {
	                if (rows[i] == nodeID) {
	                    hasStart = true;
	                }
	            }
	        }
	        return hasStart;
	    }

	    this.IsDescendant = function (child, parent) {
	        var result = false;
	        var childNode = this.GetNodeById(child);
	        //look recursively into childNode rootNodeID property, check wether passed parent is a valid holder for the child or not
	        //create an array from all ascendants of parent

	        try{
	            if (childNode.data.rootNodeID == parent) {
	                return true;
	            }
	            if (this.GetNodeById(childNode.data.rootNodeID).data.rootNodeID == parent) {
	                return true;
	            }
	            if (this.GetNodeById(this.GetNodeById(childNode.data.rootNodeID).data.rootNodeID).data.rootNodeID == parent) {
	                return true;
	            }
	            if (this.GetNodeById(this.GetNodeById(this.GetNodeById(childNode.data.rootNodeID).data.rootNodeID).data.rootNodeID).data.rootNodeID == parent) {
	                return true;
	            }
	            if (this.GetNodeById(this.GetNodeById(this.GetNodeById(this.GetNodeById(childNode.data.rootNodeID).data.rootNodeID).data.rootNodeID).data.rootNodeID).data.rootNodeID == parent) {
	                return true;
	            }
	        }
	        catch (ex) {
	            return false;
	        }

	        return result;
	    };

	    this.FindCorrectScopeFlowChartItems = function (currentRootID) {
	        CorrectFlowChartListForVariables.length = 0;
	        CorrectFlowChartValuesForVariables.length = 0;

	        CorrectFlowChartListForVariables.push(this.FindFlowchartTitle(0));
	        CorrectFlowChartValuesForVariables.push("0");
	        for (var i = 0 ; i < this.nodes.length; i++) {
	            if (this.nodes[i].data.typeIndex == 7)//flowchart?
	            {
	                if (this.IsDescendant(currentRootID, this.nodes[i].data.id) || currentRootID == this.nodes[i].data.id) {
	                    CorrectFlowChartListForVariables.push(this.FindFlowchartTitle(this.nodes[i].data.id));
	                    CorrectFlowChartValuesForVariables.push(this.nodes[i].data.id);
	                }
	            }
	        }
	    };

	    this.FilterVariables = function (currentRootID) {
	        try {
	            var scopeArray = new Array();
	            //first reset the variables table
	            currentFlowchartIDForVariables = currentRootID;
	            var table = document.getElementById('tbl');//initially clear variables table
	            for (var i = table.getElementsByTagName("tr").length - 1 ; i >= 0; i--) {
	                table.deleteRow(i);
	            }

	            //then add variables belonging to the current (and parent) flowchart(s)
	            var numVars = 0;
	            totalvarindex = 0;
	            for (var i = 0 ; i < Variables.length; i++) {
	                if (Variables[i].ScopeID == currentRootID || this.IsDescendant(currentRootID, Variables[i].ScopeID)) {
	                    scopeArray.push(Variables[i].ScopeID);
	                    ApplyVariableFilter(Variables[i]);
	                    numVars++;
	                }
	            }

	            CorrectFlowChartListForVariables.length = 0;
	            CorrectFlowChartValuesForVariables.length = 0;

	            CorrectFlowChartListForVariables.push(this.FindFlowchartTitle(0));
	            CorrectFlowChartValuesForVariables.push("0");
	            for (var i = 0 ; i < this.nodes.length; i++) {
	                if (this.nodes[i].data.typeIndex == 7)//flowchart?
	                {
	                    if (this.IsDescendant(currentRootID, this.nodes[i].data.id) || currentRootID == this.nodes[i].data.id) {
	                        CorrectFlowChartListForVariables.push(this.FindFlowchartTitle(this.nodes[i].data.id));
	                        CorrectFlowChartValuesForVariables.push(this.nodes[i].data.id);
	                    }
	                }
	            }

	            //update existing scope combo boxes based on values retrieved in the above line
	            var tmpIndex = 0;
	            for (var j = 0 ; j < 100; j++) {
	                try {
	                    var cmbname = "cmbScope" + j;
	                    document.getElementById(cmbname).options.length = 0;
	                    for (var i = 0 ; i < CorrectFlowChartListForVariables.length; i++) {//loop through flowcharts
	                        var z = document.createElement("option");
	                        z.setAttribute("value", CorrectFlowChartValuesForVariables[i]);
	                        var t = document.createTextNode(CorrectFlowChartListForVariables[i]);
	                        z.appendChild(t);
	                        document.getElementById(cmbname).appendChild(z);
	                    }

	                    for (var k = 0 ; k < document.getElementById(cmbname).options.length; k++) {
	                        if (document.getElementById(cmbname).options[k].value == scopeArray[tmpIndex]) {
	                            document.getElementById(cmbname).selectedIndex = k;
	                        }
	                    }
	                    tmpIndex++;
	                }
	                catch (ex) {
	                }
	            }

	            //after corresponding variables of the CURRENT flowchart have been loaded (i.e. variables not belonging to the current flowchart have been removed from the list)
	            //try to bring variables of the current flowchart to the top of the list

	            var tableData = document.getElementById('tbl');
	            var rowData = tableData.getElementsByTagName('tr');

	            //loop through variables belonging to the current flowchart
	            try
	            {
	                for (var i = 0; i < rowData.length; i++) {
	                    var cmb = $('#tbl').find("tr")[i].getElementsByTagName("td")[2].children[0];
	                    if (cmb[cmb.selectedIndex].value == currentRootID) {
	                        tableData.insertBefore(rowData.item(i), rowData.item(0));
	                    }
	                }
	            }
	            catch (ex) {

	            }

	            ApplyVariableFilter();//add an empty row (only if the current flowchart doesn't have any variables
	        }
	        catch (ex) {
	            //alert(ex.message + " , " + ex.number);
	        }
	    }

	    this.UpdateTempObjects = function () {

	        for (var i = 1 ; i < this.nodes.length ; i++) {
	            var tmpnode = this.nodes[i];
	            if (this.nodes[i].data.visible && this.nodes[i].data.typeIndex != 8) {
	                TempDeletedObjectList.push({ typeindex: this.nodes[i].data.typeIndex, x: this.nodes[i].data.x, y: this.nodes[i].data.y, parent: this.nodes[i].data.parent });
	                tmpnode.select();
	            }
	        }
	        for (var i = 0 ; i < 10; i++) {
	            TempDeletedLineList.push({ srcID: 0, destID: 0, p1x: 0, p1y: 0, p2x: 0, p2y: 0, p3x: 0, p3y: 0, p4x: 0, p4y: 0, p5x: 0, p5y: 0, p6x: 0, p6y: 0 });
	        }
	    }

	    this.ManageFlowchartIDIndexDictionary = function (id) {
	        var index = 0;
	        for (var i = 0 ; i < FlowchartIDIndexDictionary.length; i++) {
	            if (FlowchartIDIndexDictionary[i].id == id) {
	                return FlowchartIDIndexDictionary[i].index;
	            }
	        }
	        index = parseInt(FlowchartIDIndexDictionary[FlowchartIDIndexDictionary.length - 1].index) + 1;
	        FlowchartIDIndexDictionary.push({ id: id, index: index });
	        return index;
	    }

	    this.RoundNumbers = function (number) {
	        var result = number;
	        var x = number % 10;
	        if (x <= 5) {
	            result = number - x;
	        }
	        else {
	            result = number + (10 - x);
	        }
	        return result;
	    };

	    this.SwitchToForEach = function (dstflw, norecord) {
	        ForeachActive = true;
	        try
	        {
	            UndoList.length = 0;
	            RedoList.length = 0;
	            divForeachBar.style.display = "";
	            imgResize.style.display = "none";
	            $('#divHierarchy').css("display", "none");
	            $('#imgEraser').css("display", "none");
	            if (SelectedFlowChartNode == null)
	                return;
	            var node = SelectedFlowChartNode;
	            var previouscurrentRootNodeID = currentRootNodeID;
	            FlowchartSizes += currentRootNodeID + "=" + divDrop.style.width.replace("px", "") + ";" + divDrop.style.height.replace("px", "") + ",";
	            if (dstflw == null)
	                currentRootNodeID = node.data.id;
	            else
	                currentRootNodeID = dstflw;
	            var nonodefound = true;
	            for (var i = 0 ; i < this.nodes.length; i++) {//hide all nodes
	                this.nodes[i].data.visible = false;
	                //only show nodes that are child of current rootNodeID
	                if (this.nodes[i].data.rootNodeIDForeach == currentRootNodeID || this.nodes[i].data.id == 0) {
	                    this.nodes[i].data.visible = true;
	                    if (i != 0)
	                        nonodefound = false;
	                    if (this.nodes[i].data.id != 0 && this.nodes[i].data.typeIndex != 8) {
	                        $('#imgEraser').css("display", "");
	                    }
	                }
	            }

	            if (!nonodefound) {
	                lblForEachLabel.style.display = "none";
	            }
	            else {
	                lblForEachLabel.style.display = "";
	            }
	            var current_scene = 0;
	            for (var i = 1 ; i < (numSupportedFlowcharts + 1) ; i++) {
	                var name = "lblActivity" + i.toString();
	                if (document.getElementById(name).style.display != "none") {
	                    current_scene = i - 1;
	                }
	            }

	            flowChartTitle[current_scene] = lblFakeTitle.value;
	            flowChartWidth[current_scene] = divDrop.style.width.replace("px", "");
	            flowChartHeight[current_scene] = divDrop.style.height.replace("px", "");
	            for (var i = 0 ; i < this.connections.length; i++) {
	                this.connections[i]._visible = false;//by default all connections should be invisible
	                if (this.connections[i].rootNodeIDForeach == currentRootNodeID) {//only show connections that belong to this foreach node
	                    this.connections[i]._visible = true;
	                }
	            }
	            //initial width & height for new flowchart
	            lblFakeTitle.value = "ForEach";
	            $("#lblSwitchAlert").html("Drilling down to " + lblFakeTitle.value);
	            $("#divSwitchAlert").fadeIn(1500, function () {
	                $("#divSwitchAlert").fadeOut(1500);
	            });

	            var w = 600;
	            var h = 600;

	            this.FilterVariables(currentRootNodeID);

	            divDrop.style.height = h + "px";
	            $("#divChart").height(h);
	            var svg = document.getElementsByTagName("svg");
	            svg[0].setAttribute("height", h);
	            svg[0].style.height = (h) + "px";

	            divDrop.style.width = w + "px";
	            $("#divChart").width(w);
	            svg[0].setAttribute("width", w);
	            document.getElementById('divFlowChartParent').style.width = (w + "px");
	            document.getElementById('divFlowChartParentTitle').style.width = (w + "px");
	            svg[0].style.width = (w) + "px";

	            CenterDrop();
	            //end

	            divAddIcon.style.display = "none";
	            divSettings.style.display = "none";
	            var name = "lblActivity" + node.data.currentActivityNumber;

	            document.getElementById(name).style.display = "";

	            var correctIndex = 0;
	            for (var i = 1 ; i < (numSupportedFlowcharts + 1) ; i++) {
	                var name = "lblActivity" + i.toString();
	                if (document.getElementById(name).style.display != "none") {
	                    correctIndex = name.replace("lblActivity", "");
	                }
	            }

	            if (dstflw == null)
	                nodeRootIDList[this.ManageFlowchartIDIndexDictionary(node.data.id)] = node.data.id;
	            else
	                nodeRootIDList[currentRootNodeID] = dstflw;

	            //only last 5 labels should be visible
	            var num_visible_labels = 0;
	            for (var i = 1 ; i < (numSupportedFlowcharts + 1) ; i++) {
	                var name = "lblActivity" + i.toString();
	                if (document.getElementById(name).style.display != "none") {
	                    num_visible_labels++;
	                }
	            }

	            for (var i = 1 ; i < (numSupportedFlowcharts + 1) ; i++) {
	                var name = "lblActivity" + i.toString();
	                document.getElementById(name).style.display = "none";
	            }
	            var min = current_scene + 2 - 4;
	            if (min < 1)
	                min = 1;


	            for (var i = min ; i <= current_scene + 2; i++) {
	                var name = "lblActivity" + i.toString();
	                document.getElementById(name).style.display = "";
	            }

	            //add start node
	            var real_activity_index;
	            for (var q = 1 ; q < (numSupportedFlowcharts + 1) ; q++) {
	                var n = "lblActivity" + q;
	                if (document.getElementById(n).style.display == "") {
	                    real_activity_index = q - 1;
	                }
	            }
	            if (norecord == null) {
	                var UndoObject = { action: "flowchartdrilldown", dstFlowchart: currentRootNodeID + ";" + real_activity_index, srcFlowchart: previouscurrentRootNodeID + ";" + real_activity_index };
	                //RedoList.length = 0;
	                //this.AddToUndoList(UndoObject);
	            }

	            document.getElementById("lblActivity" + (real_activity_index + 1)).childNodes[0].childNodes[0].innerHTML = this.FindFlowchartTitle(currentRootNodeID);
	            var tmpFunction = "VirtualSetRoot('" + currentRootNodeID + ";" + real_activity_index + ";" + "foreach" + "')";
	            document.getElementById("lblActivity" + (real_activity_index + 1)).setAttribute("onclick", tmpFunction);

	            //find min Height & min Width
	            this.FindMinWidthHeight();
	            txtTitle.style.display = 'none';
	            lblFakeTitle.style.display = '';
	            this.deselectAll();

	            this.Lock = true;
	            setTimeout(function () {
	                document.getElementById('txtForeachItemName').value = SelectedFlowChartNode.data.ForeachItemName;
	                document.getElementById('txtForeachListName').value = SelectedFlowChartNode.data.ForeachListName;
	            }, 500);


	            //setTimeout(function () {
	            //    divDrop.style.width = "300px";
	            //    divChart.style.width = "300px";
	            //    divDrop.style.height = "210px";
	            //    divChart.style.height = "210px";
	            //    divFlowChartParent.style.width = "300px";
	            //    divFlowChartParentTitle.style.width = "300px";
	            //}, 10000);
	        }
	        catch (ex) {
	            //alert(ex.message);
	        }
	    };

	    this.SwitchRoot = function (dstflw, norecord) {
	        try{
	            if (ForeachActive) {
	                this.GetNodeById(currentRootNodeID).data.ForeachItemName = document.getElementById('txtForeachItemName').value;
	                this.GetNodeById(currentRootNodeID).data.ForeachListName = document.getElementById('txtForeachListName').value;
	            }
	        }
	        catch (ex) {
	            console.log(ex.message);
	        }

	        this.Lock = false;
	        ForeachActive = false;
	        divForeachBar.style.display = "none";
	        UndoList.length = 0;
	        RedoList.length = 0;

	        $('#divHierarchy').css("display", "none");
	        $('#imgEraser').css("display", "none");
	        if (SelectedFlowChartNode == null)
	            return;
	        var node = SelectedFlowChartNode;
	        var previouscurrentRootNodeID = currentRootNodeID;
	        FlowchartSizes += currentRootNodeID + "=" + divDrop.style.width.replace("px", "") + ";" + divDrop.style.height.replace("px", "") + ",";
	        if (dstflw == null)
	            currentRootNodeID = node.data.id;
	        else
	            currentRootNodeID = dstflw;
	        for (var i = 0 ; i < this.nodes.length; i++) {//hide all nodes
	            this.nodes[i].data.visible = false;
	            //only show nodes that are child of current rootNodeID (do NOT show foreach items)
	            if ((this.nodes[i].data.rootNodeID == currentRootNodeID && this.nodes[i].data.rootNodeIDForeach == "") || this.nodes[i].data.id == 0) {
	                this.nodes[i].data.visible = true;
	                if (this.nodes[i].data.id != 0 && this.nodes[i].data.typeIndex != 8) {
	                    $('#imgEraser').css("display", "");
	                }
	            }
	        }
	        var current_scene = 0;
	        for (var i = 1 ; i < (numSupportedFlowcharts + 1) ; i++) {
	            var name = "lblActivity" + i.toString();
	            if (document.getElementById(name).style.display != "none") {
	                current_scene = i - 1;
	            }
	        }

	        flowChartTitle[current_scene] = lblFakeTitle.value;
	        flowChartWidth[current_scene] = divDrop.style.width.replace("px", "");
	        flowChartHeight[current_scene] = divDrop.style.height.replace("px", "");
	        for (var i = 0 ; i < this.connections.length; i++) {
	            this.connections[i]._visible = false;
	            if (this.connections[i].rootNodeID() == currentRootNodeID && this.connections[i].rootNodeIDForeach == "") {
	                this.connections[i]._visible = true;
	            }
	        }
	        //initial width & height for new flowchart
	        lblFakeTitle.value = this.FindFlowchartTitle(currentRootNodeID);
	        $("#lblSwitchAlert").html("Drilling down to " + lblFakeTitle.value);
	        $("#divSwitchAlert").fadeIn(1500, function () {
	            $("#divSwitchAlert").fadeOut(1500);
	        });

	        var sizes = this.FindFlowchartSize(currentRootNodeID).split(';');
	        var w = flowChartWidth[node.data.currentActivityNumber - 1];
	        var h = flowChartHeight[node.data.currentActivityNumber - 1];
	        w = sizes[0];
	        h = sizes[1];

	        this.FilterVariables(currentRootNodeID);

	        divDrop.style.height = h + "px";
	        $("#divChart").height(h);
	        var svg = document.getElementsByTagName("svg");
	        svg[0].setAttribute("height", h);
	        svg[0].style.height = (h) + "px";

	        divDrop.style.width = w + "px";
	        $("#divChart").width(w);
	        svg[0].setAttribute("width", w);
	        document.getElementById('divFlowChartParent').style.width = (w + "px");
	        document.getElementById('divFlowChartParentTitle').style.width = (w + "px");
	        svg[0].style.width = (w) + "px";

	        CenterDrop();
	        //end

	        divAddIcon.style.display = "none";
	        divSettings.style.display = "none";
	        var name = "lblActivity" + node.data.currentActivityNumber;

	        document.getElementById(name).style.display = "";

	        var correctIndex = 0;
	        for (var i = 1 ; i < (numSupportedFlowcharts + 1) ; i++) {
	            var name = "lblActivity" + i.toString();
	            if (document.getElementById(name).style.display != "none") {
	                correctIndex = name.replace("lblActivity", "");
	            }
	        }

	        if (dstflw == null)
	            nodeRootIDList[this.ManageFlowchartIDIndexDictionary(node.data.id)] = node.data.id;
	        else
	            nodeRootIDList[currentRootNodeID] = dstflw;

	        //only last 5 labels should be visible
	        var num_visible_labels = 0;
	        for (var i = 1 ; i < (numSupportedFlowcharts + 1) ; i++) {
	            var name = "lblActivity" + i.toString();
	            if (document.getElementById(name).style.display != "none") {
	                num_visible_labels++;
	            }
	        }

	        for (var i = 1 ; i < (numSupportedFlowcharts + 1) ; i++) {
	            var name = "lblActivity" + i.toString();
	            document.getElementById(name).style.display = "none";
	        }
	        var min = current_scene + 2 - 4;
	        if (min < 1)
	            min = 1;


	        for (var i = min ; i <= current_scene + 2; i++) {
	            var name = "lblActivity" + i.toString();
	            document.getElementById(name).style.display = "";
	        }

	        //add start node
	        var real_activity_index;
	        for (var q = 1 ; q < (numSupportedFlowcharts + 1) ; q++) {
	            var n = "lblActivity" + q;
	            if (document.getElementById(n).style.display == "") {
	                real_activity_index = q - 1;
	            }
	        }
	        if (norecord == null) {
	            var UndoObject = { action: "flowchartdrilldown", dstFlowchart: currentRootNodeID + ";" + real_activity_index, srcFlowchart: previouscurrentRootNodeID + ";" + real_activity_index };
	            //RedoList.length = 0;
	            //this.AddToUndoList(UndoObject);
	        }

	        document.getElementById("lblActivity" + (real_activity_index + 1)).childNodes[0].childNodes[0].innerHTML = this.FindFlowchartTitle(currentRootNodeID);
	        var tmpFunction = "VirtualSetRoot('" + currentRootNodeID + ";" + real_activity_index + "')";
	        document.getElementById("lblActivity" + (real_activity_index + 1)).setAttribute("onclick", tmpFunction);

	        if (!this.FlowChartHasStart(currentRootNodeID)) {
	            try {
	                var newNodeDataModel = {
	                    name: "Example Node 1",
	                    id: nextGUID, //this.nodes.length,
	                    GUID: nextGUID,
	                    x: this.RoundNumbers(parseInt(divDrop.clientWidth / 2) - parseInt(this.GetShapeWidthList(8 - 1) / 2) + 1),
	                    y: 50,
	                    width: 300,
	                    parent: 0,
	                    collapseable: true,
	                    collapsed: true,
	                    collapseWidth: 100,
	                    shapeWidth: this.GetShapeWidthList(8 - 1),
	                    shapeHeight: this.GetShapeHeightList(8 - 1),
	                    z: 0,
	                    visible: true,
	                    rootNodeID: currentRootNodeID,
	                    rootNodeIDForeach: "",
	                    ForeachItemName: "item",
	                    ForeachListName: "list",
	                    currentActivityNumber: currentActivityNumber,
	                    collapseHeight: 150,
	                    iconWidth: 24,
	                    iconHeight: 24,
	                    iconX: 3,
	                    iconY: 3,
	                    typeIndex: 8,
	                    numChild: 0,
	                    collapseIconX: 236,
	                    collapseIconY: 0,
	                    title: this.names[8 - 1],
	                    titleX: 20,
	                    titleY: 20,
	                    backColor: this.GetBackColor(8 - 1),
	                    origBackColor: this.GetBackColor(8 - 1),
	                    iconCollapsedWidth: 50,
	                    iconCollapsedHeight: 50,
	                    iconCollapsedX: 25,
	                    mouseOver: false,
	                    iconCollapsedY: 70,
	                    collapserVisible: true,
	                    collapserMouseOver: false,
	                    titleVisible: true,
	                    icon: this.icons[8 - 1],
	                    inputcount: 0,
	                    outputcount: 1,
	                    showSettings: true,
	                    settingsX: 80,
	                    ShowHover: false,
	                    settingsY: 130,
	                    clickX: 5,
	                    clickY: 130,
	                    defaultPosition: true,
	                    nextInputConnector: 0,
	                    connected: false,
	                    covered: false,
	                    hideconnectors: false,
	                    connectorclicked: false,
	                    parentIDs: [],
	                    childrenGUIDs: [],
	                    childrenGUIDsTEMP: [],
	                    connectordata: { name: 'visualizy', internalType: 'couchbase' },
	                    activeConnector: "",
	                    activeConnectorInput: "",
	                    dicarray: [{ key: "z", value: "1000" }],
	                    dragcovered: false,
	                    outputConnectors: [
                            {
                                name: "o1",
                                x: 115 - 95 + 15,
                                y: 150,
                                numConnection: 0,
                                connected: false,
                                connectorCircleRadius: 5,
                                caption: "",
                            },
	                    ],

	                };

	                this.addNode(newNodeDataModel);
	                nextGUID++;
	            }
	            catch (ex) {

	            }
	            flowChartIDStart += currentRootNodeID + ",";
	        }
	        //find min Height & min Width
	        this.FindMinWidthHeight();
	        txtTitle.style.display = 'none';
	        lblFakeTitle.style.display = '';
	        this.deselectAll();
	    };

	    this.FindMinWidthHeight = function () {
	        var bottomY = -10000;
	        var rightX = -10000;
	        for (var k = 1 ; k < this.nodes.length ; k++) {
	            if (parseInt(this.nodes[k].data.y) + parseInt(this.nodes[k].data.shapeHeight) > bottomY && this.nodes[k].data.visible) {
	                bottomY = parseInt(this.nodes[k].data.y) + parseInt(this.nodes[k].data.shapeHeight);
	            }
	            if (parseInt(this.nodes[k].data.x) + parseInt(this.nodes[k].data.shapeWidth) > rightX && this.nodes[k].data.visible) {
	                rightX = parseInt(this.nodes[k].data.x) + parseInt(this.nodes[k].data.shapeWidth);
	            }
	        }
	        minWidth = rightX;
	        if (minWidth < initialWidth)
	            minWidth = initialWidth;
	        minHeight = bottomY;
	        if (minHeight < initialHeight)
	            minHeight = initialHeight;
	    };

	    var HierarchyText = "";
	    this.handleNodeClicked = function (node, ctrlKey) {
	        var changeView = false;
            //switch to flowchart layer
	        if (node.data.typeIndex == 7 || node.data.typeIndex == 13) {//flowchart or foreach
	            try
	            {
	                if (node.data.typeIndex == 13) {
	                    SelectedForEachNode = node;
	                }
	            }
	            catch (ex) {
	                //alert(ex.message);
	            }

	            SelectedFlowChartNode = node;
	        }
	        if (this.Lock && !ForeachActive)
	            return;

	        if (ctrlKey) {
	            node.toggleSelected();
	        }
	        else {
	            this.deselectAll();
	            node.data.backColor = "#ff0000";
	            if (node.data.id != 0) {

	                try {
	                    divSettingsContainer.innerHTML = this.GetSettingsHTML(node.data.typeIndex - 1);
	                }
	                catch (ex) {
	                }
	            }

	            if (node.data.numChild == node.data.outputcount) {
	                imgAddIcon.style.display = "none";
	            }
	            else {
	            }

	            HierarchyText = "";
	            this.AddParentToHierarchy(node);
	            divSettingDialog.style.display = "none";

	            try {
	                txtDisplayName.value = node.data.title;
	            }
	            catch (ex) {

	            }

	            try
                {
	                txtCaption1.value = node.data.outputConnectors[0].caption;
	                txtCaption2.value = node.data.outputConnectors[1].caption;
	            }
	            catch (ex) {
	            }
	            close_mode = false;
	            close_mode_add = false;
	            divSettingDialog.style.opacity = "0.0";
	            divSettingDialog.style.filter = 'alpha(opacity=0)';
	            divSettingDialog.style.minHeight = "0px";
	            divSettingDialog.style.minWidth = "0px";

	            divAddDialog.style.opacity = "0.0";
	            divAddDialog.style.filter = 'alpha(opacity=0)';
	            divAddDialog.style.minHeight = "0px";
	            divAddDialog.style.minWidth = "0px";

	            divSettings.style.left = node.data.x + (80 + (((divBody.clientWidth - divDrop.clientWidth) / 2) + 50) - divContainer.scrollLeft) + "px";
	            divSettings.style.top = node.data.y + (130 + 85 - divContainer.scrollTop) + "px";

	            divAddIcon.style.left = node.data.x + (6 + (((divBody.clientWidth - divDrop.clientWidth) / 2) + 50) - divContainer.scrollLeft) + "px";
	            divAddIcon.style.top = node.data.y + (130 + 85 - divContainer.scrollTop) + "px";
	            node.select();
	        }

	        // Move node to the end of the list so it is rendered after all the other.
	        // This is the way Z-order is done in SVG.

	        var nodeIndex = this.nodes.indexOf(node);
	        if (nodeIndex == -1) {
	            throw new Error("Failed to find node in view model!");
	        }
	        this.nodes.splice(nodeIndex, 1);
	        this.nodes.push(node);
	        if (changeView) {
	            divAddIcon.style.display = "none";
	            divSettings.style.display = "none";
	        }
	    };

	    //
	    // Handle mouse down on a connection.
	    //

	    this.AddParentToHierarchy = function (node) {
	        try {
	            var arrow = "";
	            if (HierarchyText != "") {
	                arrow = " > ";
	            }
	            HierarchyText = "<span style='cursor:hand; cursor:pointer;' >" + node.data.title + "</span>" + arrow + "<span style='cursor:hand; cursor:pointer;' >" + HierarchyText + "</span>";
	            if (node.data.parent.toString() != "") {

	            }

	            if (node.data.parent.toString() != "") {
	                var node2 = this.GetNodeById(node.data.parent);
	                this.AddParentToHierarchy(node2);
	            }
	        }
	        catch (ex) {

	        }
	    };

	    this.GetSourceNodeID = function (connection) {
	        return connection.GetSourceNodeID();
	    };

	    this.GetDestNodeID = function (connection) {
	        return connection.GetDestNodeID();
	    };

	    this.handleConnectionMouseDown = function (connection, ctrlKey) {
	        if (ctrlKey) {
	            connection.toggleSelected();
	        }
	        else {
	            this.deselectAll();
	            connection.select();
	        }
	    };

	    //
	    // Delete all nodes and connections that are selected.
	    //
	    var foreachif = false;
	    this.deleteSelected = function (DoNotPushInUndo) {
	        if (!foreachif) {
	            if (ForeachActive) {
	                for (var nodeIndex = 0; nodeIndex < this.nodes.length; ++nodeIndex) {
	                    var node = this.nodes[nodeIndex];
	                    if (node.data.typeIndex == 1 && node.selected()) {
	                        console.clear();
	                        console.log('all');
	                        foreachif = true;
	                        this.ClearAll();
	                    }
	                }
	            }
	        }

	        try
	        {
	            $('#imgEraser').css("display", "none");
	            divSettings.style.display = "none";
	            divAddIcon.style.display = "none";
	            divAddDialog.style.display = "none";

	            for (var nodeIndex = 0; nodeIndex < this.nodes.length; ++nodeIndex) {
	                var node = this.nodes[nodeIndex];
	                if ((nodeIndex == 0 && node.selected()) || (node.data.typeIndex == 8 && node.selected())) {
	                    return;
	                }
	            }

	            var newNodeViewModels = [];
	            var newNodeDataModels = [];

	            var deletedNodeIds = [];

	            //
	            // Sort nodes into:
	            //		nodes to keep and 
	            //		nodes to delete.
	            //

	            for (var nodeIndex = 0; nodeIndex < this.nodes.length; ++nodeIndex) {
	                var node = this.nodes[nodeIndex];
	                if (!node.selected()) {
	                    // Only retain non-selected nodes.
	                    newNodeViewModels.push(node);
	                    newNodeDataModels.push(node.data);
	                }
	                else {
	                    // Keep track of nodes that were deleted, so their connections can also
	                    // be deleted.
	                    deletedNodeIds.push(node.data.GUID);//id
	                    //deletedNodeIds.push(node.data.id);
	                    if (DoNotPushInUndo == null) {
	                        var UndoObject = { action: "deletenode", parent: node.data.parent, id: node.data.id, typeIndex: node.data.typeIndex, x: node.data.x, y: node.data.y, GUID: node.data.GUID };
	                        RedoList.length = 0;
	                        this.AddToUndoList(UndoObject);
	                    }
	                }
	            }

	            if (ForeachActive)//we are in a for each loop?
	                lblForEachLabel.style.display = "";//display the guidance label after user has deleted the object


	            var newConnectionViewModels = [];
	            var newConnectionDataModels = [];

	            //
	            // Remove connections that are selected.
	            // Also remove connections for nodes that have been deleted.
	            //
	            for (var connectionIndex = 0; connectionIndex < this.connections.length; ++connectionIndex) {

	                var connection = this.connections[connectionIndex];
	                if (!connection.selected() &&
                        deletedNodeIds.indexOf(connection.source.parentNode().data.GUID) === -1 &&//source.nodeID
                      deletedNodeIds.indexOf(connection.dest.parentNode().data.GUID) === -1) {//dest.nodeID

	                    //deletedNodeIds.indexOf(connection.data.source.nodeID) === -1 &&//source.nodeID
	                    //deletedNodeIds.indexOf(connection.data.dest.nodeID) === -1) {//dest.nodeID

	                    //
	                    // The nodes this connection is attached to, where not deleted,
	                    // so keep the connection.
	                    //
	                    newConnectionViewModels.push(connection);
	                    newConnectionDataModels.push(connection.data);
	                }
	                else {
	                    try
	                    {
	                        connection.source.parentNode().data.numChild--;
	                        connection.source.parentNode().data.activeConnector = "";
	                        var outputConnectorIndex = 0;
	                        if (connection.source.parentNode().data.typeIndex == 1)//source node is IF, so we should determine which outputconnector is being released
	                        {
	                            if (connection.outputDir == "right") {
	                                outputConnectorIndex = 1;
	                            }
	                        }
	                        connection.source.parentNode().data.outputConnectors[outputConnectorIndex].connected = false;
	                        var indexToBeDeleted = "";
	                        if (connection.inputDir == "up") {
	                            indexToBeDeleted = "0";
	                        }
	                        else if (connection.inputDir == "right") {
	                            indexToBeDeleted = "1";
	                        }
	                        else if (connection.inputDir == "down") {
	                            indexToBeDeleted = "2";
	                        }
	                        else if (connection.inputDir == "left") {
	                            indexToBeDeleted = "3";
	                        }

	                        for (var q = connection.source.parentNode().data.childrenGUIDs.length - 1; q >= 0; q--) {//loop through source item possible children
	                            if (connection.source.parentNode().data.childrenGUIDs[q].guid == connection.dest.parentNode().data.GUID) {//remove any child(ren) that its destination item has been deleted
	                                connection.source.parentNode().data.childrenGUIDs.splice(q, 1);
	                            }
	                        }

	                        for (var q = connection.dest.parentNode().data.parentIDs.length - 1 ; q >= 0; q--)//loop through all parentIDs of the destination node
	                        {
	                            //remove current source ID from parentIDs array (this array holds all parent IDs)
	                            if (connection.dest.parentNode().data.parentIDs[q] == connection.source.parentNode().data.id) {
	                                connection.dest.parentNode().data.parentIDs.splice(q, 1);
	                            }
	                        }

	                        connection.dest.parentNode().data.activeConnectorInput = connection.dest.parentNode().data.activeConnectorInput.replace(indexToBeDeleted, "");
	                        if (connection.dest.parentNode().data.numChild == 0 && connection.dest.parentNode().data.activeConnectorInput == "")
	                            connection.dest.parentNode().data.connected = false;
	                        if (connection.source.parentNode().data.numChild == 0 && connection.source.parentNode().data.activeConnectorInput == "")
	                            connection.source.parentNode().data.connected = false;
	                        connection.source.parentNode().data.connectorclicked = false;
	                        connection.dest.parentNode().data.connectorclicked = false;
	                        if (DoNotPushInUndo == null) {
	                            var UndoObject = {
	                                action: "deleteconnection", srcNodeGUID: connection.source.parentNode().data.GUID, dstNodeGUID: connection.dest.parentNode().data.GUID,
	                                ix: connection.inputx, iy: connection.inputy, o1x: connection.ox1, o1y: connection.oy1, o2x: connection.ox2, o2y: connection.oy2, onumber: connection.sourceOutputID,
	                                inputDir: connection.inputDir, outputDir: connection.outputDir
	                            };
	                            RedoList.length = 0;
	                            this.AddToUndoList(UndoObject);
	                        }
	                    }
	                    catch (ex) {
	                        //alert(ex.message + " , " + ex.number);
	                    }
	                }
	            }

	            //
	            // Update nodes and connections.
	            //
	            this.nodes = newNodeViewModels;
	            this.data.nodes = newNodeDataModels;
	            this.connections = newConnectionViewModels;
	            this.data.connections = newConnectionDataModels;

	            for (var i = 0 ; i < this.nodes.length; i++) {//is there any other node remaining in active (currently visible) workflow? if not, then the erase icon should remain hidden
	                if (this.nodes[i].visible() && this.nodes[i].data.id != 0 && this.nodes[i].data.typeIndex != 8) {
	                    $('#imgEraser').css("display", "");
	                }
	            }
	        }
	        catch (ex) {
	            //alert(ex.message + " , " + ex.number);
	        }
	    };

	    //
	    // Select nodes and connections that fall within the selection rect.
	    //
	    this.applySelectionRect = function (selectionRect) {
	        try
	        {
	            if (btnTouchMultiSelect.innerHTML != "MultiSelect On") {
	                this.deselectAll();
	            }

	            for (var i = 1; i < this.nodes.length; ++i) {
	                if (this.nodes[i].data.visible) {
	                    var node = this.nodes[i];
	                    var LTcorner = false;
	                    var RTcorner = false;
	                    var LBcorner = false;
	                    var RBcorner = false;


	                    if (node.x() >= selectionRect.x && node.x() <= parseInt(selectionRect.x) + parseInt(selectionRect.width) && node.y() >= selectionRect.y && node.y() <= parseInt(selectionRect.y) + parseInt(selectionRect.height))
	                        LTcorner = true;
	                    if (parseInt(node.x()) + parseInt(node.width()) >= selectionRect.x && parseInt(node.x()) + parseInt(node.width()) <= parseInt(selectionRect.x) + parseInt(selectionRect.width) && node.y() >= selectionRect.y && node.y() <= parseInt(selectionRect.y) + parseInt(selectionRect.height))
	                        RTcorner = true;
	                    if (node.x() >= selectionRect.x && node.x() <= parseInt(selectionRect.x) + parseInt(selectionRect.width) &&
                            parseInt(node.y()) + parseInt(node.height()) >= selectionRect.y && parseInt(node.y()) + parseInt(node.height()) <= parseInt(selectionRect.y) + parseInt(selectionRect.height))
	                        LBcorner = true;
	                    if (parseInt(node.x()) + parseInt(node.width()) >= selectionRect.x && parseInt(node.x()) + parseInt(node.width()) <= parseInt(selectionRect.x) + parseInt(selectionRect.width) &&
                            parseInt(node.y()) + parseInt(node.height()) >= selectionRect.y && parseInt(node.y()) + parseInt(node.height()) <= parseInt(selectionRect.y) + parseInt(selectionRect.height))
	                        RBcorner = true;


	                    var numCollision = 0;
	                    if (LTcorner)
	                        numCollision++;
	                    if (RTcorner)
	                        numCollision++;
	                    if (LBcorner)
	                        numCollision++;
	                    if (RBcorner)
	                        numCollision++;
	                    if (numCollision >= 2) {
	                        node.select();
	                    }
	                }
	            }

	            for (var i = 0; i < this.connections.length; ++i) {
	                var connection = this.connections[i];
	                if (connection.source.parentNode().selected() &&
                        connection.dest.parentNode().selected())
	                {
	                    // Select the connection if both its parent nodes are selected.
	                    connection.select();
	                }
	            }
	        }
	        catch (ex) {
	            //alert(ex.message);
	        }
	    };

	    this.CreateNewNode = function (id, x, y, parent, shapeWidth, shapeHeight, index, connected, GUID, _rootnodeid) {
	        var caps = [];
	        x = this.RoundNumbers(x);
	        y = this.RoundNumbers(y);

	        var vis = true;
	        var finalRootNodeID = currentRootNodeID;
	        if (_rootnodeid != null) {
	            finalRootNodeID = _rootnodeid;
	            if (_rootnodeid > 0)
	                vis = false;
	        }

	        if (index != 8 && index != 6) {
	            $('#liRedo').css("display", "");
	            $('#liUndo').css("display", "");
	            $('#imgEraser').css("display", "");
	        }

	        if (index != 8) {
	            try
	            {
	                caps = this.GetCaptionList(index).split('_');
	            }
	            catch (ex) {
	                caps = [''];
	            }
	        }
	        else
	            caps = [''];
	        var newNodeDataModel = {
	            name: "Example Node 1",
	            id: GUID,//id
	            GUID: GUID,
	            x: x,
	            y: y,
	            width: 300,
	            parent: parent,
	            collapseable: true,
	            collapsed: true,
	            collapseWidth: 100,
	            collapseHeight: 150,
	            iconWidth: 24,
	            iconHeight: 24,
	            iconX: 3,
	            typeIndex: index,
	            iconY: 3,
	            shapeWidth: shapeWidth,
	            shapeHeight: shapeHeight,
	            z: 0,
	            visible: vis,
	            rootNodeID: finalRootNodeID,
	            rootNodeIDForeach: "",
	            ForeachItemName: "item",
	            ForeachListName: "list",
	            currentActivityNumber: currentActivityNumber,
	            numChild: 0,
	            collapseIconX: 236,
	            condition: "",
	            collapseIconY: 0,
	            title: this.GetName(index - 1),
	            titleX: 20,
	            titleY: 20,
	            backColor: "white",
	            origBackColor: "white",
	            iconCollapsedWidth: 50,
	            iconCollapsedHeight: 50,
	            iconCollapsedX: 25,
	            mouseOver: false,
	            iconCollapsedY: 50,
	            collapserVisible: true,
	            collapserMouseOver: false,
	            titleVisible: true,
	            icon: this.GetIcons(index - 1),
	            inputcount: 1,
	            outputcount: this.GetOutputCount(index - 1),
	            showSettings: true,
	            settingsX: 80,
	            ShowHover: false,
	            settingsY: 130,
	            clickX: 5,
	            clickY: 130,
	            covered: false,
	            hideconnectors: false,
	            connectorclicked: false,
	            connectordata: { name: 'visualizy', internalType: 'couchbase' },
	            activeConnector: "",
	            activeConnectorInput: "",
	            dicarray: [{ key: "z", value: "1000" }],
	            dragcovered: false,
	            defaultPosition: true,
	            AssignOperator1: "",
	            AssignOperator2: "",
	            IfCondition: "",
	            nextInputConnector: 0,
	            connected: connected,
	            childNumberOfParent: 1,
	            parentIDs: [],
	            childrenGUIDs: [],
	            childrenGUIDsTEMP: [],
	            XAMLReferenceID: "",
	            moveSpriteImage: "/content/images/" + index + ".png",
	            inputConnectors: [
                    {
                        name: "i1",
                        x: 130 - 95,
                        y: 0,
                        numConnection: 0,
                        connected: false,
                        connectorCircleRadius: 5,
                    },

	            ],
	            outputConnectors: [
                    {
                        name: "o1",
                        x: 115 - 95,
                        y: shapeHeight - 30,
                        numConnection: 0,
                        connected: false,
                        connectorCircleRadius: 5,
                        caption: caps[0],
                    },
	            ],
	        };

	        if (this.GetOutputCount(index - 1) == 2) {
	            newNodeDataModel.outputConnectors.push({
	                name: "output2",
	                x: 115 - 95,
	                y: shapeHeight - 30,
	                numConnection: 0,
	                connected: false,
	                connectorCircleRadius: 5,
	                caption: caps[0],
	            });
	        }

            //

	        this.addNode(newNodeDataModel);
	    };

	    this.AddToUndoList = function (action) {
	        UndoList.push(action);
	    };

	    this.AddToRedoList = function (action) {
	        RedoList.push(action);
	    };

	    this.GetNodeTitle = function (index) {
	        return this.nodes[index].data.title;
	    };

	    this.GetNodeID = function (index) {
	        return this.nodes[index].data.id;
	    };

	    this.GetNodeParent = function (index) {
	        return this.nodes[index].data.parent;
	    };

	    this.GetNodeCaption = function (index) {
	        return this.connections[index].data.caption;
	    };

	    this.GetNodeX = function (index) {
	        return this.nodes[index].data.x;
	    };

	    this.GetNodeY = function (index) {
	        return this.nodes[index].data.y;
	    };

	    this.GetSelectedNodeID = function () {
	        var index = -1;
	        for (var i = 0; i < this.nodes.length; ++i) {
	            var node = this.nodes[i];
	            if (node.selected() && node.visible()) {
	                {
	                    index = node.data.id;
	                    break;
	                }
	            }
	        }
	        return index;
	    };

	    this.GetSelectedNodeIndex = function()
	    {
	        var index = -1;
	        for (var i = 0; i < this.nodes.length; ++i) {
	            var node = this.nodes[i];
	            if (node.selected()) {
	                {
	                    index = i;
	                    break;
	                }
	            }
	        }
	        return index;
	    };

	    this.GetNodeZ = function (index) {
	        return this.nodes[index].data.z;
	    };

	    this.GetNodeWidth = function (index) {
		    return this.nodes[index].data.shapeWidth;
		};

	    this.GetNodeVisible = function (index) {
	        return this.nodes[index].data.visible;
	    };

		this.GetNodeHeight = function (index) {
		    return this.nodes[index].data.shapeHeight;
		};

		this.ChangePosition = function (index, x, y) {
		    var cango = true;
		    for (var i = 1 ; i < this.nodes.length; i++) {
		        if (i != index && this.nodes[i].visible()) {
		            var noderight = parseInt(this.nodes[index].data.x) + parseInt(this.nodes[index].data.shapeWidth) + parseInt(x);
		            var nodetop = parseInt(this.nodes[index].data.y) + parseInt(y);
		            var nodebottom = parseInt(this.nodes[index].data.y) + parseInt(this.nodes[index].data.shapeHeight) + parseInt(y);
		            var nodeleft = parseInt(this.nodes[index].data.x) + parseInt(x);

		            var rectleft = parseInt(this.nodes[i].data.x);
		            var rectright = parseInt(this.nodes[i].data.x) + parseInt(this.nodes[i].data.shapeWidth);
		            var recttop = parseInt(this.nodes[i].data.y);
		            var rectbottom = parseInt(this.nodes[i].data.y) + parseInt(this.nodes[i].data.shapeHeight);

		            if (this.PointIsInRect(nodeleft, nodetop, rectleft, recttop, rectright, rectbottom) || this.PointIsInRect(noderight, nodetop, rectleft, recttop, rectright, rectbottom) ||
                        this.PointIsInRect(nodeleft, nodebottom, rectleft, recttop, rectright, rectbottom) || this.PointIsInRect(noderight, nodebottom, rectleft, recttop, rectright, rectbottom) ||
                        noderight > (maxFlowchartWidth + initialWidth) || nodebottom > (maxFlowchartHeight + initialHeight) || nodeleft < 20) {
		                cango = false;
		                break;
		            }
		        }
		    }

		    if (cango) {
		        this.nodes[index].data.x += x;
		        this.nodes[index].data.y += y;
		        this.FindMinWidthHeight();
		    }
		};

		this.PointIsInRect = function (px, py, rleft, rtop, rright, rbottom) {
		    var result = false;
		    if (px >= rleft && px <= rright && py >= rtop && py <= rbottom) {
		        result = true;
		    }

		    return result;
		};

		this.GetNode = function (index) {
		    return this.nodes[index];
		};

		this.GetNodeByGUID = function (guid) {
		    var index = -1;
		    for (var i = 0; i < this.nodes.length; ++i) {
		        var node = this.nodes[i];
		        if (node.data.GUID == guid) {
		            {
		                index = i;
		                break;
		            }
		        }
		    }
		    if (index != -1) {
		        return this.nodes[index];
		    }
		};

		this.GetNodeById = function (id) {
		    var index = -1;
		    for (var i = 0; i < this.nodes.length; ++i) {
		        var node = this.nodes[i];
		        if (node.data.id == id) {
		            {
		                index = i;
		                break;
		            }
		        }
		    }
		    if (index != -1) {
		        return this.nodes[index];
		    }
		};

		this.SetConnectionCaption = function (connection, text) {
		    try
		    {
		        connection.Text = text;
		    }
		    catch (ex) {
		        //alert(ex.message + " , " + ex.number);
		    }
		};

		this.DragIndex = -1;
		this.tmpConnectorItem;

		this.GetName = function (index) {
		    return this.names[index];
		};

		this.GetSettingsHTML = function (index) {
		    return this.settingsHTML[index];
		};

		this.GetOutputCount = function (index) {
		    return this.outputCountList[index];
		};

		this.GetBackColor = function (index) {
		    return this.backColorList[index];
		};

		this.GetCaptionList = function (index) {
		    return this.captionList[index];
		};

		this.GetShapeWidthList = function (index) {
		    return this.ShapeWidthList[index];
		};

		this.GetShapeHeightList = function (index) {
		    return this.ShapeHeightList[index];
		};

		this.GetIcons = function (index) {
		    return this.icons[index];
		};

		this.GetConnectorItemInformation = function () {
		    return this.tmpConnectorItem;
		};

		this.GetCurrentDragIndex = function () {
		    return this.DragIndex;
		};

		this.SendToBack = function (index) {
		    var min_z = 10000;
		    for (var i = 1 ; i < this.nodes.length; i++) {
		        if (this.nodes[i].data.z < min_z) {
		            min_z = this.nodes[i].data.z;
		        }
		    }
		    this.nodes[index].data.z = parseInt(min_z) - 1;
		};

		this.BringToFront = function (index) {
		    var max_z = -1000;
		    for (var i = 0 ; i < this.nodes.length; i++) {
		        if (this.nodes[i].data.z > max_z) {
		            max_z = this.nodes[i].data.z;
		        }
		    }
		    this.nodes[index].data.z = parseInt(max_z) + 1;
		};

		this.GetNumNodes = function () {
		    return this.nodes.length;
		};

		//
		// Get the array of nodes that are currently selected.
		//
		this.getSelectedNodes = function () {
			var selectedNodes = [];

			for (var i = 0; i < this.nodes.length; ++i) {
				var node = this.nodes[i];
				if (node.selected()) {
					selectedNodes.push(node);
				}
			}

			return selectedNodes;
		};

		//
		// Get the array of connections that are currently selected.
		//
		this.getSelectedConnections = function () {
			var selectedConnections = [];

			for (var i = 0; i < this.connections.length; ++i) {
				var connection = this.connections[i];
				if (connection.selected()) {
					selectedConnections.push(connection);
				}
			}

			return selectedConnections;
		};
		

	};

})();
