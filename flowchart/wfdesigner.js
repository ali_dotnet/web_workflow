﻿//
// Directive that generates the rendered chart from the data model.
//
app.directive('wfdesigner', function () {
  return {
  	restrict: 'ECA',
  	templateUrl: "flowchart/wfdesigner.html"
  };
})

.controller('controlController', ['$scope', 'dragging', '$element', function controlController($scope, dragging, $element) {
    $scope.linerange = [100, 200, 300];
    var controller = this;

    //
    // Reference to the document and jQuery, can be overridden for testting.
    //
    this.document = document;

    //
    // Wrap jQuery so it can easily be  mocked for testing.
    //
    this.jQuery = function (element) {
        return $(element);
    }
}])
;
