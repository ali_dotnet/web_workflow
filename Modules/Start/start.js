﻿//
// Directive that generates the rendered chart from the data model.
//
app.directive('start', function () {
  return {
  	restrict: 'ECA',
  	templateUrl: "Modules/start/start.html",
      controller: 'startController'
  };
})

.controller('startController', ['$scope', 'dragging', '$element', '$attrs', function startController($scope, dragging, $element, $attrs) {
    $scope.linerange = [100, 200, 300];
    var controller = this;

    $scope.dragcovered = function () {
        return $attrs.dragcovered;
    };

    $scope.MouseOver = function () {
        $scope._ShowConnector = true;
        $scope.digest();
    };

    $scope.MouseLeave = function () {
        $scope._ShowConnector = false;
        $scope.$digest();
    };

    $scope.ShowConnector = function () {
        var show = false;
        if ($scope._ShowConnector)// || $scope.dragcovered())
            show = true;
        return show;
    };

    $scope.selected = function () {
        var css = "";
        if ($attrs.selected == "true")
            css = "selected";
        else
            css = "normal";
        if ($scope.Covered() == "true")
            css = "hovered";
        return css;
    };

    $scope.GetConnectorPosition = function (type) {
        try {
            var divElement = angular.element($element[0].querySelector(type));
            return {x: divElement.prop('offsetLeft'), y: divElement.prop('offsetTop') };
        }
        catch (ex) {
            alert(ex.message);
        }
    };

    $scope.activeConnector = function () {
        return $attrs.activeconnector;
    };

    $scope.activeConnectorInput = function () {
        return $attrs.activeconnectorinput;
    };

    $scope.ShowOutputConnector = function (index) {
        //var invalidConnector = false;
        //if ($scope.activeConnector() != "") {
        //    if ($scope.activeConnector() != index) {
        //        invalidConnector = true;
        //    }
        //}

        var invalidConnector = false;
        var i = false;
        if ($scope.activeConnectorInput() != "") {
            if ($scope.activeConnectorInput().indexOf(index) != -1) {
                i = true;
            }
        }
        if ($scope.activeConnector() != "") {
            if ($scope.activeConnector() != index) {
                if (!i) {
                    invalidConnector = true;
                }
            }
        }

        var show = false;
        var con = "false";
        if ($scope.Connected() == "true")
            con = "true";
        var cov = "false";
        if ($scope.Covered() == "true")
            cov = "true";
        var hov = "false";
        if ($scope.ShowConnector() == true)
            hov = "true";
        var conclick = "false";
        if ($scope.OutputConnectorClick() == "true")
            conclick = "true";
        //if ((con == "true" || hov == "true" || cov == "true" || conclick == "true") && !invalidConnector) {
        if ((hov == "true" || cov == "true")) {
            show = true;
        }
        return show;
    };

    $scope.OutputConnectorClick = function () {
        return $attrs.connectorclicked;
    };

    $scope.Connected = function () {
        return $attrs.connected;
    };

    $scope.Covered = function () {
        return $attrs.covered;
    };

    this.document = document;
    $scope._ShowConnector = false;
    $scope._Connected = "X";

    $scope.myClass = "normal";

    //
    // Reference to the document and jQuery, can be overridden for testting.
    //

    //
    // Wrap jQuery so it can easily be  mocked for testing.
    //
    this.jQuery = function (element) {
        return $(element);
    }
}])
;
