app.directive('keyvalueeditor', function ($compile) {
    return {
        require: "",
        restrict: "ECA",
        //scope: {
        //    name: '@name',
        //    typeInput: "@typeinput",
        //    index: "@index"
        //},
        templateUrl: "Modules/keyValue/keyValueEditor.html",
        link: function (scope, element, attrs, controller) {
            var startX = 0, startY = 0, x = 0, y = 0;
            element.on('mousedown', function (event) {
                if (!scope.rightBox) {
                    // Prevent default dragging of selected content
                    event.preventDefault();
                    startX = event.pageX - x;
                    startY = event.pageY - y;
                    $document.on('mousemove', mousemove);
                    $document.on('mouseup', mouseup);
                }
            });

            function mousemove(event) {
                y = event.pageY - startY;
                x = event.pageX - startX;
                element.css({
                    top: y + 'px',
                    left: x + 'px'
                });
            }

            function mouseup() {
                $document.off('mousemove', mousemove);
                $document.off('mouseup', mouseup);
            }
        },
        controller: "keyvalueeditorController"

    }

});
app.controller('keyvalueeditorController', ['$scope', '$timeout', '$element', '$attrs', function ($scope, $timeout, $element, $attrs) {

    //#region property
    $scope.dicName = ['Translate', 'User Dictionary', 'Bio Dic', 'Dic1', 'ConvertJson']
    //#endregion property
    $scope.rightBox = false;
    $scope.editKeyValue = function (e) {
        $scope.rightBox = true;
        e.stopPropagation();
    }
    $scope.lblArray = [];
    $scope.setWidth = function () {
        var max = 0;
        $scope.lblArray = [];
        for (var i = 0; i < $scope.propertiesData.length; i++) {
            var width = $(".inputLabel" + $scope.propertiesData[i].key).innerWidth();
            $scope.lblArray.push(width);
        }
        max = $scope.lblArray[0];
        for (var i = 0; i < $scope.lblArray.length; i++) {
            if ($scope.lblArray[i] >= max) {
                max = $scope.lblArray[i];
            }
        }
        for (var i = 0; i < $scope.propertiesData.length; i++) {
            $(".inputLabel" + $scope.propertiesData[i].key).css("width", max).css("text-overflow", "ellipsis").css("overflow", "hidden").css("white-space", "nowrap");
        }

    }
    $scope.test = function (e) {
        e.stopPropagation();
    }

    $scope.dic = [{ key: "", value: "" }];
    $scope.addKey_value = function () {
        var addState = true;
        var keyList = [];
        for (var i = 0; i < $scope.dic.length; i++) {
            if (!$scope.dic[i].key) { addState = false };
            keyList.push($scope.dic[i].key);
        }
        if (!(keyList.length === new Set(keyList).size)) {
            addState = false;
        }
        if (addState) {
            $scope.dic.push({ key: "", value: "" });
        }
    }
    $scope.showEditKeyValue = function () {
        $timeout(function () {
            for (var i = 0; i < $scope.dic.length; i++) {
                $('#prp' + $scope.dic[i].key).attr('checked', true);
            }
        }, 100);
        e.stopPropagation();
    }
    $scope.editPrp = function (item, e) {
        var chk = $('#prp' + item.key).prop('checked');
        if (e.target.localName != 'input') { chk = !chk; $('#prp' + item.key).prop('checked', chk) };
        if (!chk) {
            // remove from property list
            var index = null;
            for (var i = 0; i < $scope.dic.length; i++) {
                if ($scope.dic[i].key == item.key) {
                    index = i;
                    break;
                }
            }
            if (index >= 0) {
                $scope.dic.splice(index, 1);
            }
        } else {
            // insert to property list
            $scope.dic.push(item);
        }
    }
    $scope.closeRightBox = function () {
        $scope.rightBox = false;
    }

    $scope.linerange = [100, 200, 300];
    var controller = this;

    this.document = document;

    $scope.activeConnector = function () {
        return $attrs.activeconnector;
    };

    $scope.activeConnectorInput = function () {
        return $attrs.activeConnectorInput;
    };

    var tmpValue1 = "";
    var tmpValue2 = "";

    $scope.nodeid = function () {
        return $attrs.nodeid;
    };

    $scope.GetFocus1 = function () {
        currentEditAssignNode1 = $scope.nodeid();
    };

    $scope.LostFocus1 = function () {
        currentEditAssignNode1 = -1;
    };

    $scope.GetFocus2 = function () {
        currentEditAssignNode2 = $scope.nodeid();
    };

    $scope.LostFocus2 = function () {
        currentEditAssignNode2 = -1;
    };

    $scope.MouseOver = function () {
        $scope._ShowConnector = true;
        $scope.digest();
    };

    $scope.ShowInputConnector = function () {
        var show = false;

        var conclick = "false";
        if ($scope.OutputConnectorClick() == "true")
            conclick = "true";

        if ($scope.ShowConnector() || $scope.Connected() == "true" || conclick == "true")
            show = true;

        return show;
    };

    $scope.selected = function () {
        var css = "";
        if ($attrs.selected == "true")
            css = "keyvalueEditorSelected";
        else
            css = "keyvalueEditor";
        //if ($scope.Covered() == "true")
        //    css = "hovered";

        return css;
    };

    $scope.GetConnectorPosition = function (type) {
        try {
            var divElement = angular.element($element[0].querySelector(type));
            return { x: divElement.prop('offsetLeft'), y: divElement.prop('offsetTop') };
        }
        catch (ex) {
            alert(ex.message);
        }
    };

    $scope.MouseLeave = function () {
        $scope._ShowConnector = false;
        $scope.$digest();
    };

    $scope.hideconnectors = function () {
        return $attrs.hideconnectors;
    };

    $scope.ShowConnector = function () {
        if ($scope.hideconnectors() == "false") {
            return $scope._ShowConnector;
        }
        else {
            return false;
        }
    };

    $scope.Connected = function () {
        return $attrs.connected;
    };

    $scope.activeConnectorInput = function () {
        return $attrs.activeconnectorinput;
    };

    $scope.ShowOutputConnector = function (index) {
        var invalidConnector = false;
        var i = false;
        if ($scope.activeConnectorInput() != "") {
            if ($scope.activeConnectorInput().indexOf(index) != -1) {
                i = true;
            }
        }
        if ($scope.activeConnector() != "") {
            if ($scope.activeConnector() != index) {
                if (!i) {
                  //  invalidConnector = true;
                }
            }
        }
        var show = false;
        var con = "false";
        if ($scope.Connected() == "true")
            con = "true";
        var cov = "false";
        if ($scope.Covered() == "true")
            cov = "true";
        var hov = "false";
        if ($scope.ShowConnector() == true)
            hov = "true";
        var conclick = "false";
        if ($scope.OutputConnectorClick() == "true")
            conclick = "true";
        //if ((con == "true" || hov == "true" || cov == "true" || conclick == "true") && !invalidConnector) {
        if ((hov == "true" || cov == "true")) {
            show = true;
        }

        return show;
    };

    $scope.OutputConnectorClick = function () {
        return $attrs.connectorclicked;
    };

    $scope.Covered = function () {
        return $attrs.covered;
    };

    this.document = document;
    $scope._ShowConnector = false;
    $scope._Connected = "X";

    //
    // Wrap jQuery so it can easily be  mocked for testing.
    //
    this.jQuery = function (element) {
        return $(element);
    }

}]);