﻿//
// Directive that generates the rendered chart from the data model.
//
app.directive('showform', function () {
  return {
  	restrict: 'ECA',
  	templateUrl: "Modules/showform/showform.html",
  	controller: 'showformController'
  };
})

.controller('showformController', ['$scope', 'dragging', '$element', '$attrs', function showformController($scope, dragging, $element, $attrs) {
    $scope.linerange = [100, 200, 300];
    var controller = this;

    $scope.MouseOver = function () {
        $scope._ShowConnector = true;
        $scope.digest();
    };

    $scope.selected = function () {
        var css = "";
        if ($attrs.selected == "true")
            css = "selected";
        else
            css = "normal";
        if ($scope.Covered() == "true")
            css = "hovered";
        //if ($scope.Covered() == "nodrop")
        //    css = "invalidDrop";

        return css;
    };

    $scope.ShowInputConnector = function () {
        var show = false;

        var conclick = "false";
        if ($scope.OutputConnectorClick() == "true")
            conclick = "true";

        if ($scope.ShowConnector() || $scope.Connected() == "true" || conclick == "true")
            show = true;

        return show;
    };

    $scope.MouseLeave = function () {
        $scope._ShowConnector = false;
        $scope.$digest();
    };

    $scope.hideconnectors = function () {
        return $attrs.hideconnectors;
    };

    $scope.ShowConnector = function () {
        if ($scope.hideconnectors() == "false") {
            return $scope._ShowConnector;
        }
        else {
            return false;
        }
    };

    $scope.Connected = function () {
        return $attrs.connected;
    };

    this.document = document;
    $scope._ShowConnector = false;
    $scope._Connected = "X";

    $scope.GetConnectorPosition = function (type) {
        try {
            var divElement = angular.element($element[0].querySelector(type));
            return { x: divElement.prop('offsetLeft'), y: divElement.prop('offsetTop') };
        }
        catch (ex) {
            alert(ex.message);
        }
    };

    //
    // Reference to the document and jQuery, can be overridden for testting.
    //
    this.document = document;

    $scope.ShowOutputConnector = function () {
        var show = false;
        var con = "false";
        try
        {
            if ($scope.Connected() == "true")
                con = "true";
            var cov = "false";
            if ($scope.Covered() == "true")
                cov = "true";
            var hov = "false";
            if ($scope.ShowConnector() == true)
                hov = "true";
            var conclick = "false";
            if ($scope.OutputConnectorClick() == "true")
                conclick = "true";
            if (con == "true" || hov == "true" || cov == "true" || conclick == "true") {
                show = true;
            }
        }
        catch (ex) {

        }
        return show;
    };

    $scope.OutputConnectorClick = function () {
        return $attrs.connectorclicked;
    };

    $scope.Covered = function () {
        return $attrs.covered;
    };

    //
    // Wrap jQuery so it can easily be  mocked for testing.
    //
    this.jQuery = function (element) {
        return $(element);
    }
}])
;
