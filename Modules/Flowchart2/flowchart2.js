﻿app.directive('flowchart2', function () {
    return {
        restrict: 'ECA',
        templateUrl: "Modules/flowchart2/flowchart2.html",
        controller: 'flowchart2Controller'
    };
})

.controller('flowchart2Controller', ['$scope', 'dragging', '$element', '$attrs', function flowchart2Controller($scope, dragging, $element, $attrs) {
    $scope.linerange = [100, 200, 300];
    var controller = this;

    //
    // Reference to the document and jQuery, can be overridden for testing.
    //

    $scope.ChangeTitle = function (evt) {
        try
        {
            evt.stopPropagation();
            FlowChartTitleChangerVisible = true;
            $scope.show_label = false;
            angular.element(document.querySelector('.myinput')).focus();
        }
        catch (ex) {
            alert(ex.message);
        }
    };

    $scope.nodeid = function () {
        return $attrs.nodeid;
    };

    $scope.selected = function () {
        var css = "";
        if ($attrs.selected == "true")
            css = "selected";
        else
            css = "normal";
        if ($scope.Covered() == "true")
            css = "hovered";
        //if ($scope.Covered() == "nodrop")
            //css = "invalidDrop";

        return css;
    };

    $scope.ShowInputConnector = function () {
        var show = false;

        var conclick = "false";
        if ($scope.OutputConnectorClick() == "true")
            conclick = "true";

        if ($scope.ShowConnector() || $scope.Connected() == "true" || conclick == "true")
            show = true;

        return show;
    };

    $scope.GetKey = function (evt) {
        if (evt.keyCode == 13) {
            var UndoObject = { action: "changetitleNode", titleBefore: $scope.GetTitle(), titleAfter: evt.target.value, id: $scope.nodeid() };
            UndoList.push(UndoObject);
            RedoList.length = 0;

            $scope.Title = evt.target.value;
            $scope.show_label = true;
            FlowChartTitleChangerVisible = false;
            flowChartTitle[currentActivityNumber - 1] = $scope.Title;
            FlowchartTitles += $scope.nodeid() + "=" + evt.target.value + ",";
        }
    };

    $scope.GetTitle = function () {
        return $attrs.flwtitle;
    };

    $scope.ShowChanger = function () {
        return $scope.show_changer;
    };

    $scope.ShowLabel = function () {
        return $scope.show_label;
    };

    $scope.GetConnectorPosition = function (type) {
        try {
            var divElement = angular.element($element[0].querySelector(type));
            return { x: divElement.prop('offsetLeft'), y: divElement.prop('offsetTop') };
        }
        catch (ex) {
            alert(ex.message);
        }
    };

    $scope.MouseOver = function () {
        $scope._ShowConnector = true;
        $scope.digest();
    };

    $scope.activeConnector = function () {
        return $attrs.activeconnector;
    };

    $scope.activeConnectorInput = function () {
        return $attrs.activeconnectorinput;
    };

    $scope.ShowOutputConnector = function (index) {
        var invalidConnector = false;
        var i = false;
        if ($scope.activeConnectorInput() != "") {
            if ($scope.activeConnectorInput().indexOf(index) != -1) {
                i = true;
            }
        }
        if ($scope.activeConnector() != "") {
            if ($scope.activeConnector() != index) {
                if (!i) {
                    //invalidConnector = true;//true
                }
            }
        }
        var show = false;
        var con = "false";
        if ($scope.Connected() == "true")
            con = "true";
        var cov = "false";
        if ($scope.Covered() == "true")
            cov = "true";
        var hov = "false";
        if ($scope.ShowConnector() == true)
            hov = "true";
        var conclick = "false";
        if ($scope.OutputConnectorClick() == "true")
            conclick = "true";
        //if ((con == "true" || hov == "true" || cov == "true" || conclick == "true") && !invalidConnector) {
        if ((hov == "true" || cov == "true")) {
            show = true;
        }
        return show;
    };

    $scope.Covered = function () {
        return $attrs.covered;
    };

    $scope.OutputConnectorClick = function () {
        return $attrs.connectorclicked;
    };

    $scope.MouseLeave = function () {
        $scope._ShowConnector = false;
        $scope.$digest();
    };

    $scope.hideconnectors = function () {
        return $attrs.hideconnectors;
    };

    $scope.ShowConnector = function () {
        if ($scope.hideconnectors() == "false") {
            return $scope._ShowConnector;
        }
        else {
            return false;
        }
    };

    $scope.Connected = function () {
        return $attrs.connected;
    };

    this.document = document;
    $scope._ShowConnector = false;
    $scope._Connected = "X";

    this.document = document;
    var title = "Flowchart";
    $scope.show_label = true;
    $scope.Title = "Flowchart";
    $scope.show_changer = true;
    //
    // Wrap jQuery so it can easily be  mocked for testing.
    //
    this.jQuery = function (element) {
        return $(element);
    }

}])
;
