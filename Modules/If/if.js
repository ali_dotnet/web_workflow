﻿//
// Directive that generates the rendered chart from the data model.
//
app.directive('if', function () {
  return {
  	restrict: 'ECA',
  	templateUrl: "Modules/if/if.html",
  	link: function (scope, element, attrs, controller) {
  	    try {
  	        setTimeout(function () {
  	            if (document.getElementById(scope.nodecndid()).value.trim() == "")
  	                scope.conditionEmpty = true;
  	            else
  	                scope.conditionEmpty = false;

  	        }, 500);
  	    }
  	    catch (ex) {
  	        //alert(ex.message);
  	    }
  	},
  	controller: 'ifController',
  };
})

.controller('ifController', ['$scope', 'dragging', '$element', '$attrs', function ifController($scope, dragging, $element,$attrs) {

    var controller = this;
    $scope.rightBox = false;
    $scope.conditionEmpty = true;
    //
    // Reference to the document and jQuery, can be overridden for testting.
    //
    $scope.mouseUp = function (evt) {

    };

    $scope.nodecndid = function () {
        return "txtIfCondition" + $attrs.nodeid;
    };



    var tmpValue = "";

    $scope.MouseOver = function () {
        $scope._ShowConnector = true;
        $scope.digest();
    };

    $scope.ShowInputConnector = function () {
        var show = false;

        var conclick = "false";
        if ($scope.OutputConnectorClick() == "true")
            conclick = "true";

        if ($scope.ShowConnector() || $scope.Connected() == "true" || conclick == "true")
            show = true;

        return show;
    };

    $scope.selected = function () {
        var css = "";
        if ($attrs.selected == "true")
            css = "Ifselected";
        else
            css = "Ifnormal";
        //if ($scope.Covered() == "true")
        //    css = "hovered";
        //if ($scope.Covered() == "nodrop")
        //    css = "invalidDrop";

        return css;
    };

    $scope.nodeid = function () {
        return $attrs.nodeid;
    };

    $scope.GetConnectorPosition = function (type) {
        try {
            var divElement = angular.element($element[0].querySelector(type));
            return { x: divElement.prop('offsetLeft'), y: divElement.prop('offsetTop') };
        }
        catch (ex) {
            alert(ex.message);
        }
    };

    $scope.MouseLeave = function () {
        $scope._ShowConnector = false;
        $scope.$digest();
    };

    $scope.hideconnectors = function () {
        return $attrs.hideconnectors;
    };

    $scope.ShowConnector = function () {
        if ($scope.hideconnectors() == "false") {
            return $scope._ShowConnector;
        }
        else {
            return false;
        }
    };

    $scope.Connected = function () {
        return $attrs.connected;
    };

    $scope.ShowOutputConnector = function () {
        var show = false;
        var con = "false";
        if ($scope.Connected() == "true")
            con = "true";
        var cov = "false";
        if ($scope.Covered() == "true")
            cov = "true";
        var hov = "false";
        if ($scope.ShowConnector() == true)
            hov = "true";
        var conclick = "false";
        if ($scope.OutputConnectorClick() == "true")
            conclick = "true";
        //if (con == "true" || hov == "true" || cov == "true" || conclick == "true") {
        if ((hov == "true")){// || cov == "true")) {
            show = true;
        }
        return show;
    };

    $scope.OutputConnectorClick = function () {
        return $attrs.connectorclicked;
    };

    $scope.ConditionKeyUp = function (evt) {
        if (document.getElementById($scope.nodecndid()).value.trim() == "")
            $scope.conditionEmpty = true;
        else
            $scope.conditionEmpty = false;
    };

    $scope.editCondition = function (e) {
        $scope.rightBox = true;
        e.stopPropagation();
    }

    $scope.closeRightBox = function () {
        $scope.rightBox = false;
    }

    $scope.Covered = function () {
        return $attrs.covered;
    };

    this.document = document;
    $scope._ShowConnector = false;
    $scope._Connected = "X";

    //
    // Wrap jQuery so it can easily be  mocked for testing.
    //
    this.jQuery = function (element) {
        return $(element);
    }
}])
;
