﻿angular.module('app').service('commonActivityService', function () {

    var commonsActivity = [
        //{
        //    name: "Start",
        //    className: "startActivity",
        //    index: 6
        //},
        //{
        //    name: "End",
        //    className: "endActivity",
        //    index: 20
        //},
        {
            name: "If",
            className: "decisionActivity",
            index: 1
        },
        {
            name: "Assign",
            className: "AssignActivity",
            index: 2
        },
        {
            name: "Flowchart",
            className: "startActivity",
            index: 6//7
        },
        {
            name: "KeyValue Editor",
            className: "startActivity",
            index: 9
        },
        {
            name: "ForEach",
            className: "startActivity",
            index: 13
        }
    ];

    this.getCommonActivities = function () {
        return commonsActivity;
    }

    this.ChangeFlowchartIndex = function () {
        commonsActivity[2].index = 7;
    }

    this.hideStart = function () {
        commonsActivity[0].visible = false;
    }

})