﻿angular.module('app').service('toolbarService', function (wfExecService) {

    //#region connector
    this.getConnectors = function (username, projectId, onSuccess, onError) {
        var arg = {
            "internalType": "",
            "userName": username,
            "projectID": projectId,
        };
        wfExecService.RunWFWithResult("2d5ac76d-c97e-4e02-94d4-d01e582d88be", arg, onSuccess, onError);
    }
    //#endregion connector

    //#region collection
    this.getCollections= function (username, projectId, onSuccess, onError) {
        var arg = {
            "userName": username,
            "projectID": projectId,
        };
        wfExecService.RunWFWithResult("f4a9c3c8-6f6c-476a-b6f2-6095b7da0729", arg, onSuccess, onError);
    }
    //#endregion collection

    //#region logic
    this.getLogics= function (username, projectId, onSuccess, onError) {
        var arg = {
            "userName": username,
            "projectID": projectId,
        };

        wfExecService.RunWFWithResult("c276f7c9-2d20-42bf-8d9f-bc75c879f597", arg, onSuccess, onError);
    }
    //#endregion logic

    //#region common
    this.getCommons= function (username, projectId, onSuccess, onError) {
        var arg = {
            "userName": username,
            "projectID": projectId,
        };

        wfExecService.RunWFWithResult("", arg, onSuccess, onError);
    }
    //#endregion common

});