﻿'use strict';
angular.module('app').factory('wfExecService', function ($http) {
    var factory = function () { };

    factory.RunWFWithResult = function (wfTaskDefID, args, onsuccess, onerror) {
        //var d = {
        //    token: "SRzO5+KboLWwY6raPKKk6xW66lkfASQbYKK7IFMnrLZ7WduYPljDIo0T0hlhACNzrgkgig2zObmnHHaN0wqdl3XX8lJb0tvACCOsQyRL36emiOtm+scIMRUK9id3tASrR70bIO8NZWp1zwD0NaUdbZmh+tsMXqE8+VpeGK8+Iu8=",
        //    wfid: wfTaskDefID,
        //    args: JSON.stringify(args)
        //};

        var headers = {
            'Content-type': 'application/json',
            'Token': 'SRzO5+KboLWwY6raPKKk6xW66lkfASQbYKK7IFMnrLZ7WduYPljDIo0T0hlhACNzrgkgig2zObmnHHaN0wqdl3XX8lJb0tvACCOsQyRL36emiOtm+scIMRUK9id3tASrR70bIO8NZWp1zwD0NaUdbZmh+tsMXqE8+VpeGK8+Iu8=',
            'Wfid': wfTaskDefID
        }
        //
        $http.post("http://13.69.148.176:8098/api/actions", args, { headers: headers }).then(function success(response) { onsuccess(response.data) }, function error(d) { onerror(d.data) });
        //url:"http://13.69.148.176:8098/api/actions",
        //type: "POST",
        //data: args,
        //contentType: "application/json;charset=UTF-8",
        //dataType: "json",
        //Token: "SRzO5+KboLWwY6raPKKk6xW66lkfASQbYKK7IFMnrLZ7WduYPljDIo0T0hlhACNzrgkgig2zObmnHHaN0wqdl3XX8lJb0tvACCOsQyRL36emiOtm+scIMRUK9id3tASrR70bIO8NZWp1zwD0NaUdbZmh+tsMXqE8+VpeGK8+Iu8=",
        //Wfid: wfTaskDefID,
        //error: function (data) {
        //    onerror(data);
        //},
        //success: function (data) {
        //var d = angular.fromJson(data);
        //    onsuccess(data);
        //}
        //});
    }
    //#endregion
    return factory;
});
//});