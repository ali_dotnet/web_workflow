﻿angular.module('app').controller('leftToolbarController', function ($scope, $rootScope,toolbarService) {
    var self = this;
    var connectors = [
        {
            name: "User",
            imgUrl: "content/images/connector/oracle.png",
        },
        {
            name: "User",
            imgUrl: "content/images/connector/couchbase.png",
        },
        {
            name: "User",
            imgUrl: "content/images/connector/excel.png",
        },
        {
            name: "User",
            imgUrl: "content/images/connector/restApi.png",
        },
        {
            name: "User",
            imgUrl: "content/images/connector/sqlservericon.png",
        },
        {
            name: "User",
            imgUrl: "content/images/connector/webservice.png",
        },

    ];
    self.collections = [
       {
           name: "User",
           imgUrl: "content/images/connector/oracle.png",
       },
       {
           name: "Role",
           imgUrl: "content/images/connector/couchbase.png",
       },
       {
           name: "Member",
           imgUrl: "content/images/connector/excel.png",
       },
       {
           name: "Contact",
           imgUrl: "content/images/connector/restApi.png",
       },
       {
           name: "Books",
           imgUrl: "content/images/connector/sqlservericon.png",
       },
       {
           name: "Sales",
           imgUrl: "content/images/connector/webservice.png",
       },

    ];
    //#region property
    self.connectorData = false;
    self.collectionData = false;
    self.logicData = false;
    self.commonData = false;

    self.toollbarSpinner = true;
    self.toollbarDataContent = false;

    self.isOpenToolbarcontent = false;
    self.toollbarData = "";
    //#endregion proprty

    //#region method
    var chk = true;
    self.showActivityData = function (name) {
        switch (name) {
            case "connector":
                self.collectionData = false;
                self.logicData = false;
                self.connectorData = true;
                self.commonData = false;
                openToolbarContent('CONNECTORS');
                //self.getConnectorsData();
                break;
            case "collection":
                self.connectorData = false;
                self.collectionData = true;
                self.logicData = false;
                self.commonData = false;
                openToolbarContent('COLLECTIONS');
                break;
            case "logic":
                self.connectorData = false;
                self.collectionData = false;
                self.logicData = true;
                self.commonData = false;
                openToolbarContent('LOGICS');
                break;
            case "common":
                self.connectorData = false;
                self.collectionData = false;
                self.logicData = false;
                self.commonData = true;
                openToolbarContent('COMMONS');
                break;
            default:
        }
    };
    self.isOpenToolbarcontent = false;
    self.isCloseToolbarcontent = true;
    var openToolbarContent = function (name) {
        var left= $(".leftCmp").css('left');
        if (left == "-320px" && self.isCloseToolbarcontent && !self.isOpenToolbarcontent) {
            $(".leftCmp").animate({ "left": 50 }, "fast");
            divContainer.style.width = (parseInt(window.innerWidth) - 320) + "px";
            $(".content").animate({ "margin-left": 320 }, "slow");
            self.isOpenToolbarcontent = true;
            self.isCloseToolbarcontent = false;
            var activity = name.toLowerCase();
            switch (activity) {
                case "connectors":
                    $rootScope.$broadcast('toolbarContent:getConnectors', { name: "connector" });
                    break;
                case "collections":
                    $rootScope.$broadcast('toolbarContent:getCollection', { name: "collection" });
                    break;
                case "logics":
                    $rootScope.$broadcast('toolbarContent:getLogic', { name: "logic" });
                    break;
                case "commons":
                    $rootScope.$broadcast('toolbarContent:getCommon', { name: "common" });
                    break;
                default:
            }
        } else if (left == "50px" && !self.isCloseToolbarcontent && self.isOpenToolbarcontent) {
            var activityclose = name.toLowerCase();
            if (name == self.toollbarData) {
                $(".leftCmp").animate({ "left": "-320px" }, "fast");
                divContainer.style.width = (parseInt(window.innerWidth)) + "px";
                $(".content").animate({ "margin-left": 0 }, "fast");
                self.isOpenToolbarcontent = false;
                self.isCloseToolbarcontent = true;;
                switch (activityclose) {
                case "connectors":
                    $rootScope.$broadcast('toolbarContent:getConnectors', { name: "connector", closeAction: true });
                    break;
                case "collections":
                    $rootScope.$broadcast('toolbarContent:getCollection', { name: "collection", closeAction: true });
                    break;
                case "logics":
                    $rootScope.$broadcast('toolbarContent:getLogic', { name: "logic", closeAction: true });
                    break;
                case "commons":
                    $rootScope.$broadcast('toolbarContent:getCommon', { name: "common", closeAction: true });
                    break;
                default:
                }
            } else {
                switch (activityclose) {
                    case "connectors":
                        $rootScope.$broadcast('toolbarContent:getConnectors', { name: "connector" });
                        break;
                    case "collections":
                        $rootScope.$broadcast('toolbarContent:getCollection', { name: "collection" });
                        break;
                    case "logics":
                        $rootScope.$broadcast('toolbarContent:getLogic', { name: "logic" });
                        break;
                    case "commons":
                        $rootScope.$broadcast('toolbarContent:getCommon', { name: "common" });
                        break;
                    default:
                }
            }
        }
        self.toollbarData = name;

    }
    $scope.$on('tooltipController:onCloseTooltip', function () {
        self.isOpenToolbarcontent = false;
        self.isCloseToolbarcontent = true;;
    })
    //#endregion method

    //#region other

    
    self.init = function () {
    }
    self.init();
    //#endregion other
});