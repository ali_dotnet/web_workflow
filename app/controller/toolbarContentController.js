﻿angular.module('app').controller('toolbarContentController', function ($scope, $rootScope, $compile, toolbarService, commonActivityService) {
    var self = this;
       //#region property
    self.connectorData = false;
    self.collectionData = false;
    self.logicData = false;
    self.commonData = false;

    self.toollbarSpinner = true;
    self.toollbarDataContent = false;

    self.isOpenToolbarcontent = false;
    self.toollbarData = "";
     
    self.connectors = [];
    self.collections = [];
    self.logics = [];
    self.commons= [];
    //#endregion proprty

    //#region method
    var showActivityData = function (name) {
        switch (name) {
            case "connector":
                self.collectionData = false;
                self.logicData = false;
                self.connectorData = true;
                self.commonData = false;
                self.toollbarData = "CONNECTORS";
                break;
            case "collection":
                self.connectorData = false;
                self.collectionData = true;
                self.logicData = false;
                self.commonData = false;
                self.toollbarData = "COLLECTIONS";
                break;
            case "logic":
                self.connectorData = false;
                self.collectionData = false;
                self.logicData = true;
                self.commonData = false;
                self.toollbarData = "LOGICS";
                break;
            case "common":
                self.connectorData = false;
                self.collectionData = false;
                self.logicData = false;
                self.commonData = true;
                self.toollbarData = "COMMONS";
                self.toollbarSpinner = false;
                self.toollbarDataContent = true;
               // $rootScope.$broadcast('ShowCommonActivity');
                break;
            default:
        }
        angular.element('.toolbarcontentHeader').css('box-shadow', 'none');
    };
    self.refreshToolbar=function() {
        if(self.connectorData){self.getConnectorsData();}
        else if(self.collectionData){ self.getCollectionsData()}
        else if(self.connectorData){self.getLogicsData()}
    }
    // connectors
    $scope.$on('toolbarContent:getConnectors', function (event, obj) {
        if (!obj.closeAction) {
            showActivityData(obj.name);
            if (!self.connectors.length) {
                self.getConnectorsData();
            }
        }
    });
    self.getConnectorsData = function () {
        self.toollbarSpinner = true;
        self.toollbarDataContent = false;
        toolbarService.getConnectors("fakhreddin_rezgol@yahoo.com", "bb01770a-414f-453f-ab80-a2eefa3e59ec", self.getConnectorsDataOnSuccess, self.getConnectorsDataOnError);
    }
    self.getConnectorsDataOnSuccess = function (data) {
        self.toollbarSpinner = false;
        self.toollbarDataContent = true;
        self.connectors = data;
        $scope.$apply();
    }
    self.getConnectorsDataOnError = function () {
        self.toollbarSpinner = false;
        self.toollbarDataContent = true;
      //  self.activityData = connectors;
        $scope.$apply();
    }
    self.showConnectorActivies = function (index) {
        if ($('#showActivityIcon' + index).hasClass('fa-angle-double-down')) {
            $('#connectorBox' + index).animate({ 'min-height': 200 }, 'fast');
            $('#showActivityIcon' + index).removeClass('fa-angle-double-down').addClass('fa-angle-double-up');
            $('#showActivityIcon' + index).attr('title', 'Hidden ' + self.activityData[index].name + ' Activity');
        } else {
            $('#connectorBox' + index).animate({ 'min-height': 95 }, 'fast');
            $('#showActivityIcon' + index).removeClass('fa-angle-double-up').addClass('fa-angle-double-down');
            $('#showActivityIcon' + index).attr('title', 'Show ' + self.activityData[index].name + ' Activity');
        }
        //$('.dataRow_content').css('display', 'none');

        //self.connectorActivity = self.connectors[index].activities;
    }
    // collections
    $scope.$on('toolbarContent:getCollection', function (event, obj) {
        if (!obj.closeAction) {
            showActivityData(obj.name);
            if (!self.collections.length) {
                self.getCollectionsData();
            }
        }
    });
    self.getCollectionsData = function () {
        self.toollbarSpinner = true;
        self.toollbarDataContent = false;
        toolbarService.getCollections("fakhreddin_rezgol@yahoo.com", "bb01770a-414f-453f-ab80-a2eefa3e59ec", self.getCollectionsDataOnSuccess, self.getCollectionsDataOnError);
    }
    self.getCollectionsDataOnSuccess = function (data) {
        self.toollbarSpinner = false;
        self.toollbarDataContent = true;
        self.collections = data;
        $scope.$apply();
    }
    self.getCollectionsDataOnError = function () {
        self.toollbarSpinner = false;
        self.toollbarDataContent = true;
        //self.activityData = collections;
        $scope.$apply();
    }
    // logics
    $scope.$on('toolbarContent:getLogic', function (event, obj) {
        if (!obj.closeAction) {
            showActivityData(obj.name);
            if (!self.logics.length) {
                self.getLogicsData();
            }
        }
    });
    self.getLogicsData = function () {
        self.toollbarSpinner = true;
        self.toollbarDataContent = false;
        toolbarService.getLogics("fakhreddin_rezgol@yahoo.com", "bb01770a-414f-453f-ab80-a2eefa3e59ec", self.getLogicsDataOnSuccess, self.getLogicsDataOnError);
    }
    self.getLogicsDataOnSuccess = function (data) {
        self.toollbarSpinner = false;
        self.toollbarDataContent = true;
        self.logics = data;
        $scope.$apply();

    }
    self.getLogicsDataOnError = function () {
        self.toollbarSpinner = false;
        self.toollbarDataContent = false;
        $scope.$apply();
    }
    // commons
    $scope.$on('toolbarContent:getCommon', function (event, obj) {
        showActivityData(obj.name);
        self.getCommonData();
    });
    self.getCommonData = function () {
        self.toollbarSpinner = false;
        self.toollbarDataContent = true;
        // toolbarService.getCommons("", "", self.getCommonDataOnSuccess, self.getCommonDataOnError);
        self.commons = commonActivityService.getCommonActivities();
    }
    self.getCommonDataOnSuccess = function (data) {
        self.toollbarSpinner = false;
        self.toollbarDataContent = true;
        $scope.commonData = data;
        $scope.$apply();
    }
    self.getCommonDataOnError = function () {
        self.toollbarSpinner = false;
        self.toollbarDataContent = false;
        $scope.$apply();
    }
    //#endregion method

    //#region other

    self.toolClick = function (index, item) {
        Body.style.cursor = "url(content/cursor/" + index + ".png), auto";
        $rootScope.ApplyClick(index, item);
        commonActivityService.hideStart();
    }
    self.showScrollbar = function () {
        $('.leftBox').addClass('leftBoxHover');
    }
    self.init = function () {
    }
    self.init();
    //#endregion other
});