﻿app.service('workflowActivityService', function() {
    this.getWorkflowProperties= function() {
        return [
          {
              isPrimaryKey: true,
              key: 'Guid',
              value: '',
              type: 'number'

          },
          {
              key: 'Name',
              value: '',
              type: 'string',
              isPrimaryKey: false
          },
          {
              key: 'LastName',
              value: '',
              type: 'string',
              isPrimaryKey: false

          },
          {
              key: 'Port',
              value: '',
              type: 'number',
              isPrimaryKey: false
          },
          {
              key: 'IPAddress',
              value: '',
              type: 'number',
              isPrimaryKey: false
          }
        ];
    }
});
app.directive('workflowActivity', ['$document',  function ($document, service) {
        return {
            restrict: "ECA",
            //scope: {
            //    workflowName: "=selectedworkflow"
            //},
            //replace: true,
            //transclude: false, // if i have nested directive , copy parent element 
            templateUrl: "app/views/directives/workflowActivity.html",
            link: function (scope, element, attrs, controller) {
                scope.init(JSON.parse(attrs.wfdata));
            },
            controller:"workflowActivityController",
        }
    }
]);
app.controller('workflowActivityController', ['$scope', '$timeout', 'workflowActivityService', '$element', '$attrs', function ($scope, $timeout, service, $element, $attrs) {
    //#region property
    $scope.rightBox = false;
    $scope.rightBoxHeaderName = "";
    $scope.propertiesData = [];
    $scope.dataSpinner = false;
    $scope.propertiesContent = false;
    //#endregion property

    $scope.init = function(wf) {
        $scope.workflowActivityName = wf.name;
        $scope.workflowActivityDescription = wf.description;
    }
    $scope.showRightBox = function() {
        $scope.rightBox = true;
        $scope.dataSpinner = true;
        $scope.propertiesContent = false;
    }
    $scope.showWorkflowProperties = function(e) {
        if(!$scope.propertiesContent) {
            $scope.activitySettingsContent = false;
            $scope.showRightBox();
            $scope.rightBoxHeaderName = $scope.workflowActivityName + " Properties";
            $timeout(function () {
                $scope.dataSpinner = false;
                $scope.propertiesContent = true;
                $scope.propertiesData = service.getWorkflowProperties();
            }, 500);
            $timeout(function () {
                $scope.setWidth();
            }, 650);
        }
        e.stopPropagation();
    }

    $scope.lblArray = [];
    $scope.setWidth = function () {
        var max = 0;
        $scope.lblArray = [];
        for (var i = 0; i < $scope.propertiesData.length; i++) {
            var width = $(".inputLabel" + $scope.propertiesData[i].key).innerWidth();
            $scope.lblArray.push(width);
        }
        max = $scope.lblArray[0];
        for (var i = 0; i < $scope.lblArray.length; i++) {
            if ($scope.lblArray[i] >= max) {
                max = $scope.lblArray[i];
            }
        }
        for (var i = 0; i < $scope.propertiesData.length; i++) {
            $(".inputLabel" + $scope.propertiesData[i].key).css("width", max).css("text-overflow", "ellipsis").css("overflow", "hidden").css("white-space", "nowrap");
        }

    }
    $scope.showActivitySettings=function(e){
        if (!$scope.activitySettingsContent) {
            $scope.settings = false;
            $scope.entity = false;
            $scope.actions = false;
            $scope.rightBox = true;
            $scope.rightBox = true;
            $scope.propertiesContent = false;
            $scope.entityContent = false;
            $scope.actionsContent = false;
            $scope.rightBoxContent = true;
            $scope.activitySettingsContent = true;
            $scope.rightBoxHeaderName = $scope.workflowActivityName + " Settings";
        }
        e.stopPropagation();
    }
    $scope.txtDisplay={};
    $scope.activitySetting= {
        displayName:"Unnamed Activity"
    }
    $scope.displayNameTitle=$scope.activitySetting.displayName;
    //if ($scope.displayNameTitle=='Unnamed Activity') {
    //    $scope.displayName = false;
    //}else{
    //    $scope.displayName = true;
    //}
    $scope.displayName = true;
    $scope.hideActivityTitle= function() {
        $scope.displayName=false;
        //$('.activityTitle').fadeOut();
    }
    $scope.showActivityTitle = function () {
        if(!$scope.rightBox) {
            //$scope.rightBox = false;
            $scope.editProperty = false;
            $scope.displayName = true;
            $scope.propertiesContent = false;
            $scope.activitySettingsContent = false;
        }
        $scope._ShowConnector = false;
        $scope.$digest();
        //$('.activityTitle').fadeIn();
    }
    $scope.closeRightBox = function(event) {
        $scope.rightBox=false;
        $scope.propertiesContent=false;
        $scope.activitySettingsContent=false;
    }
    $scope.saveSettings=function(obj) {
        if(!obj.name)obj.name="Unnamed Activity";
        $scope.displayNameTitle=obj.name;
        // e.stopPropagation();
    }

    $scope.linerange = [100, 200, 300];
    var controller = this;

    this.document = document;

    $scope.nodecmbid = function () {
        return "cmbAssignVariable" + $attrs.nodeid;
    };

    $scope.nodetxtid = function () {
        return "txtAssignVariable" + $attrs.nodeid;
    };

    $scope.activeConnector = function () {
        return $attrs.activeconnector;
    };

    $scope.activeConnectorInput = function () {
        return $attrs.activeConnectorInput;
    };

    $scope.nodecmbid2 = function () {
        return "cmbAssignVariable2-" + $attrs.nodeid;
    };

    $scope.nodetxtid2 = function () {
        return "txtAssignVariable2-" + $attrs.nodeid;
    };

    $scope.VariableComboChange = function () {
        var cmb = document.getElementById($scope.nodecmbid());
        var txt = document.getElementById($scope.nodetxtid());
        txt.value = cmb.options[cmb.selectedIndex].text;
        cmb.style.display = "none";
    };

    $scope.VariableComboChange2 = function () {
        var cmb = document.getElementById($scope.nodecmbid2());
        var txt = document.getElementById($scope.nodetxtid2());
        txt.value = cmb.options[cmb.selectedIndex].text;
        cmb.style.display = "none";
    };

    $scope.VariableKeyUp2 = function (evt) {
        var UndoObject = { action: "assignTextChange2", assignTextBefore: tmpValue2, assignTextAfter: document.getElementById($scope.nodetxtid2()).value, id: $scope.nodeid() };
        UndoList.push(UndoObject);
        RedoList.length = 0;

        //first hide all other visible combos (except the current one)
        for (var i = 1 ; i < 1000; i++) {
            try {
                document.getElementById("cmbAssignVariable" + i).style.display = "none";
                document.getElementById("cmbAssignVariable2-" + i).style.display = "none";
            }
            catch (ex) {

            }
        }
        var ch = document.getElementById($scope.nodetxtid2()).value;
        if (document.getElementById($scope.nodecmbid2()).options[0].text == "") {
            UpdateVariableCombo();
        }
        var comboIndex = -1;
        var cmb = document.getElementById($scope.nodecmbid2());
        for (var i = 0 ; i < cmb.options.length; i++) {
            if (cmb.options[i].text.substring(0, ch.length) == ch) {
                comboIndex = i;
                break;
            }
        }
        if (comboIndex != -1) {//is anything found? if no matching items is found, then combo should not be displayed at all
            cmb.selectedIndex = comboIndex;
            cmb.style.display = "";
        }
    };

    $scope.VariableKeyDown = function (evt) {
        tmpValue1 = document.getElementById($scope.nodetxtid()).value;
    };

    $scope.VariableKeyDown2 = function (evt) {
        tmpValue2 = document.getElementById($scope.nodetxtid2()).value;
    };

    var tmpValue1 = "";
    var tmpValue2 = "";

    $scope.VariableKeyUp = function (evt) {

        var UndoObject = { action: "assignTextChange1", assignTextBefore: tmpValue1, assignTextAfter: document.getElementById($scope.nodetxtid()).value, id: $scope.nodeid() };
        UndoList.push(UndoObject);
        RedoList.length = 0;

        //first hide all other visible combos (except the current one)
        for (var i = 1 ; i < 1000; i++) {
            try {
                document.getElementById("cmbAssignVariable" + i).style.display = "none";
                document.getElementById("cmbAssignVariable2-" + i).style.display = "none";
            }
            catch (ex) {

            }
        }
        var ch = document.getElementById($scope.nodetxtid()).value;
        if (document.getElementById($scope.nodecmbid()).options[0].text == "") {
            UpdateVariableCombo();
        }

        var comboIndex = -1;
        var cmb = document.getElementById($scope.nodecmbid());
        for (var i = 0 ; i < cmb.options.length; i++) {
            if (cmb.options[i].text.substring(0, ch.length) == ch) {
                comboIndex = i;
                break;
            }
        }

        if (comboIndex != -1) {//is anything found? if no matching items is found, then combo should not be displayed at all
            cmb.selectedIndex = comboIndex;
            cmb.style.display = "";
        }
    };

    $scope.AssignComboChanged = function (obj) {
        try {
            var combo = document.getElementById('cmb10');
            alert(combo.selectedIndex);
        }
        catch (ex) {
            alert(ex.message);
        }
    };

    $scope.nodeid = function () {
        return $attrs.nodeid;
    };

    $scope.GetFocus1 = function () {
        currentEditAssignNode1 = $scope.nodeid();
    };

    $scope.LostFocus1 = function () {
        currentEditAssignNode1 = -1;
    };


    $scope.GetFocus2 = function () {
        currentEditAssignNode2 = $scope.nodeid();
    };

    $scope.LostFocus2 = function () {
        currentEditAssignNode2 = -1;
    };

    $scope.MouseOver = function () {
        $scope._ShowConnector = true;
        $scope.digest();
    };

    $scope.ShowInputConnector = function () {
        var show = false;

        var conclick = "false";
        if ($scope.OutputConnectorClick() == "true")
            conclick = "true";

        if ($scope.ShowConnector() || $scope.Connected() == "true" || conclick == "true")
            show = true;

        return show;
    };

    $scope.selected = function () {
        var css = "";
        if ($attrs.selected == "true")
            css = "selected";
        else
            css = "normal";
        if ($scope.Covered() == "true")
            css = "hovered";
        //if ($scope.Covered() == "nodrop")
        //    css = "invalidDrop";

        return css;
    };

    $scope.GetConnectorPosition = function (type) {
        try {
            var divElement = angular.element($element[0].querySelector(type));
            return { x: divElement.prop('offsetLeft'), y: divElement.prop('offsetTop') };
        }
        catch (ex) {
            alert(ex.message);
        }
    };

    $scope.MouseLeave = function () {
        $scope._ShowConnector = false;
        $scope.$digest();
    };

    $scope.hideconnectors = function () {
        return $attrs.hideconnectors;
    };

    $scope.ShowConnector = function () {
        if ($scope.hideconnectors() == "false") {
            return $scope._ShowConnector;
        }
        else {
            return false;
        }
    };

    $scope.Connected = function () {
        return $attrs.connected;
    };

    $scope.activeConnectorInput = function () {
        return $attrs.activeconnectorinput;
    };

    $scope.ShowOutputConnector = function (index) {
        var invalidConnector = false;
        var i = false;
        if ($scope.activeConnectorInput() != "") {
            if ($scope.activeConnectorInput().indexOf(index) != -1) {
                i = true;
            }
        }
        if ($scope.activeConnector() != "") {
            if ($scope.activeConnector() != index) {
                if (!i) {
                    //invalidConnector = true;
                }
            }
        }
        var show = false;
        var con = "false";
        if ($scope.Connected() == "true")
            con = "true";
        var cov = "false";
        if ($scope.Covered() == "true")
            cov = "true";
        var hov = "false";
        if ($scope.ShowConnector() == true)
            hov = "true";
        var conclick = "false";
        if ($scope.OutputConnectorClick() == "true")
            conclick = "true";
        //if ((con == "true" || hov == "true" || cov == "true" || conclick == "true") && !invalidConnector) {
        if ((hov == "true" || cov == "true")) {
            show = true;
        }
        return show;
    };

    $scope.OutputConnectorClick = function () {
        return $attrs.connectorclicked;
    };

    $scope.Covered = function () {
        return $attrs.covered;
    };

    this.document = document;
    $scope._ShowConnector = false;
    $scope._Connected = "X";

    //
    // Wrap jQuery so it can easily be  mocked for testing.
    //
    this.jQuery = function (element) {
        return $(element);
    }

}]);