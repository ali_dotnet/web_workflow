﻿app.directive('activityoutput', [
    '$document', function($document) {
        return {
            restrict: "EA",
            scope: {
                top: "@top",
                right: "@right",
                bottom: "@bottom",
                left: "@left",
            },
            replace: true,
            transclude: false, // if i have nested directive , copy parent element 
            template: '<div class="activityInput activityOutPut"><div class="activityInput_circle outPut"></div></div>',
            link: function (scope, element, attrs, controller) {
                element.css({
                    top: scope.top + "px",
                    right: scope.right + "px",
                    bottom: scope.bottom + "px",
                    left: scope.left + "px",
                });
            },
            controller:"activityoutputController"
        }
    }
]);
app.controller('activityoutputController',['$scope', '$timeout', function ($scope, $timeout){

}])