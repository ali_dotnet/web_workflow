/**
 * Created by s.padyab on 5/18/2016.
 */
app.directive('valueeditor',function ($compile) {
    return {
        require: "",
        restrict: "EA",
        scope: {
            name: '@name',
            typeInput: "@typeinput",
            index: "@index"
        },
        replace: true,
        //template: '<input  class="form-control" placeholder="value1" >',
        link: function (scope, element, attr) {
            var tabIndex = parseInt(scope.index);
            switch (scope.typeInput) {
                case "number":
                    var inputNumber = '<div class="input-group input-group-sm animated fadeIn col-xs-12" >'
                        + '<span class="input-group-addon inputLabel{{name}} tabEnter" id="basic-addon1">{{name}}</span>'
                        + '<input type="text"tabindex="tabIndex" class="form-control input-sm" data-ng-keydown="numberOnly($event)" placeholder="value" aria-describedby="basic-addon1">'
                        + '</div>';
                    var number = $compile(inputNumber)(scope);
                    element.html(number);
                    break;
                case "string":
                    var inputString = '<div class="input-group input-group-sm animated fadeIn col-xs-12">'
                        + '<span class="input-group-addon inputLabel{{name}}">{{name}}</span>'
                        + '<input type="text"tabindex="tabIndex" class="form-control input-sm" data-ng-keydown="stringOnly($event)" placeholder="value">'
                        + '</div>';
                    var string = $compile(inputString)(scope);
                    element.html(string);
                    break;
                case "boolean":
                    var inputCheckBox = '<div class="input-group input-group-sm animated fadeIn col-xs-12">'
                        + '<span class="input-group-addon inputLabel{{name}}" id="basic-addon1">{{name}}</span>'
                        + '<span class="input-group-addon " style="width: 45px;border-right: 0;background: white"><input type="checkbox" tabindex="tabIndex" style="position: absolute;top: 8px;"/></span>'
                        + '<span class="input-group-addon form-control"style="background: white"></span>'
                        + '</div>';
                    var checkbox = $compile(inputCheckBox)(scope);
                    element.html(checkbox);
                    break;
                case "dictionary":
                    var dic = '<div class="input-group input-group-sm col-xs-12">'
                        + '<span class="input-group-addon inputLabel{{name}}">{{name}}</span>'
                        + '<select class="form-control" tabindex="tabIndex"><option value="1">myDictionary</option><option>Dic1</option><option value="2">Trenaslation</option></select></div>';
                    //var dic = '<div class="input-group input-group-sm animated fadeIn col-xs-12">'
                    //    + '<span class="input-group-addon inputLabel{{name}}">{{name}}</span>'
                    //    + '<select class="form-control" tabindex="tabIndex">' + +'<option>Dic1</option>' +
                    //        //+'<option value="1">"myDictionary"</option>' + +'<option value="2">Trenaslation</option>' + +'<option value="">name_lname</option>' + +'<option value="">getDictionary</option>' +
                    //    +'</select>' + +'</div>';
                    var dictionary = $compile(dic)(scope);
                    element.html(dictionary);
                    break;
                default:
            }
        },
        controller: "valueeditorController"

    }

});
app.controller('valueeditorController',['$scope', '$timeout', function ($scope, $timeout) {
    $scope.numberOnly = function (e) {
        // Allow: backspace, delete, tab, escape, enter and .
        if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110, 190]) !== -1 ||
                // Allow: Ctrl+A, Command+A
            (e.keyCode == 65 && ( e.ctrlKey === true || e.metaKey === true ) ) ||
                // Allow: home, end, left, right, down, up
            (e.keyCode >= 35 && e.keyCode <= 40)) {
            // let it happen, don't do anything
            return;
        }
        // Ensure that it is a number and stop the keypress
        if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
            e.preventDefault();
        }
    }
    $scope.stringOnly = function (e) {
        var regex = new RegExp("^[a-zA-Z]+$");
        var str = String.fromCharCode(!e.charCode ? e.which : e.charCode);
        if (regex.test(str) || e.which == 8 || e.which == 9) {
            return true;
        }

        e.preventDefault();
        return false;
    }
}]);