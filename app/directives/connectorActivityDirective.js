﻿app.service('connectorActivityService', function () {

    this.getProperties = function (onSuccess, onError) {
        return [
            {
                isPrimaryKey: true,
                key: 'Guid',
                value: '',
                type: 'number'

            },
            {
                key: 'Name',
                value: '',
                type: 'string',
                isPrimaryKey: false
            },
            {
                key: 'LastName',
                value: '',
                type: 'string',
                isPrimaryKey: false

            },
            {
                key: 'Values',
                value: '',
                type: 'dictionary',
                isPrimaryKey: false

            },
            {
                key: 'Port',
                value: '',
                type: 'number',
                isPrimaryKey: false
            },
            {
                key: 'IPAddress',
                value: '',
                type: 'number',
                isPrimaryKey: false
            },
            {
                key: 'IsUnique',
                value: '',
                type: 'boolean',
                isPrimaryKey: false
            }
        ];
    }
    this.getProceduresProperties = function () {
        return [];
    }
    this.getQueryProperties = function () {
        return [];
    }
    this.getTablesEntity = function (onSuccess, onError) {
        return [
            { name: 'User' }, { name: 'Roles' }, { name: 'Permissions' }, { name: 'Authentication' }, { name: 'Contact' }
        ];
    }
    this.getTablesEntityActions = function (onSuccess, onError) {
        return [
            {
                name: 'Add', description: "Insert to connector"
            },
            {
                name: 'Delete', description: "delete from connector"
            },
            {
                name: 'Update',
                description: "update record from connector"
            },
            {
                name: 'Read All Data', description: "read all data from connector"
            },
            {
                name: 'Read By Id', description: "read by id from connector"
            },
            {
                name: 'Max of Column',
                description: "max of column record from connector"
            },
            {
                name: 'Min of Column', description: "max of column record from connector"
            },
            {
                name: 'Count', description: "count of records"
            },
            {
                name: 'Average',
                description: "average of column"
            }
        ];
    }
    this.getViewsEntity = function (onSuccess, onError) {
        return [
            { name: 'User' }, { name: 'Roles' }, { name: 'Permissions' }, { name: 'Authentication' }, { name: 'Contact' }, { name: 'Contact' }
        ];
    }
    this.getViewsEntityActions = function (onSuccess, onError) {
        return [
            { name: 'Read All Data', description: "read all data from connector" }, {
                name: 'Read By Id',
                description: "read by id from connector"
            },
            { name: 'Max of Column', description: "max of column record from connector" }, { name: 'Min of Column', description: "max of column record from connector" }, {
                name: 'Count',
                description: "count of records"
            }
        ];
    }

    this.getProceduresEntity = function (onSuccess, onError) {
        return [
            { name: 'User' }, { name: 'Roles' }, { name: 'Permissions' }, { name: 'Authentication' }, { name: 'Contact' }, { name: 'Contact' }, { name: 'Contact' }
        ];
    }
    this.getProceduresEntityActions = function (onSuccess, onError) {
        return [
            { name: 'Execute SP', description: "execute stored procedures" }
        ];
    }
    this.getQuireiesEntity = function (onSuccess, onError) {
        return [
            { name: 'User' }, { name: 'Roles' }, { name: 'Permissions' }, { name: 'Authentication' }, { name: 'Contact' }, { name: 'Contact' }, { name: 'Contact' }, { name: 'Contact' }
        ];
    }
    this.getQuireiesEntityActions = function (onSuccess, onError) {
        return [
            { name: 'Execute Query', description: "execute query " }, { name: 'Lazy Load', description: "load data like paging  " },
            {
                name: 'Count',
                description: "count of records"
            }
        ];
    }

});
app.directive('connectorActivity', ['$document', '$timeout', function ($document, $timeout) {
    return {
        restrict: "ECA",
       // replace: true,
     //   transclude: false, // if i have nested directive , copy parent element 
        templateUrl: "app/views/directives/connectorActivity.html",
        link: function (scope, element, attrs, controller) {
            scope.init(JSON.parse(attrs.connectordata));
        },
        controller: "connectorActivityController",

    }
}
]);
app.controller('connectorActivityController', ['$scope', '$timeout', '$element', '$attrs', 'connectorActivityService', function ($scope, $timeout, $element, $attrs, connectorActivityService) {
    //#region property
    $scope.entityName = "Entity Name";
    $scope.actionName = "Select Action";
    $scope.actionDescription = "";

    $scope.rightBox = false;
    $scope.settings = false;
    $scope.entity = false;
    $scope.actions = false;
    $scope.rightBoxHeaderName = "";
    $scope.rightBoxSpinner = false;
    $scope.rightBoxContent = false;

    $scope.propertiesContent = false;
    $scope.tablesContent = false;
    $scope.actionsContent = false;

    $scope.propertiesData = [];
    $scope.tablesData = [];
    $scope.viewsData = [];
    $scope.prcData = [];
    $scope.qryData = [];
    $scope.actionsData = [];

    $scope.tblActionsData = [];
    $scope.viewsActionsData = [];
    $scope.prcActionsData = [];
    $scope.qryActionsData = [];

    $scope.properties = [];

    $scope.editProperty = false;
    //#endregion property

    $scope.init = function (connector) {
        $scope.connectorActivityName = connector.name;
        $scope.iconType = connector.internalType;
    }
    $scope.showRightBox = function () {
        $scope.rightBox = true;
    }
    $scope.closeRightBox = function (event) {
        $scope.rightBox = false;
        $scope.propertiesContent = false;
        $scope.activitySettingsContent = false;
        $scope.actionsContent = false;
        $scope.entityContent = false;
        $scope.editProperty = false;
        //if($scope.rightBox && (event.target.title=='Connector Activity' || !event.target.title) && event.target.title!='Action Properties' ){
        //    $scope.rightBox=false;
        //}
    }
    var filterProperty = function () {
        $scope.propertiesData = [];
        if ($scope.actionName != "Execute SP" || $scope.actionName != "Execute Query") {
            var data = connectorActivityService.getProperties($scope.getPropertiesOnSuccess, $scope.getPropertiesOnError);
            if (data.length) {
                if ($scope.actionName == "Add" || $scope.actionName == "Update") {
                    $scope.propertiesData = data;
                } else if ($scope.actionName == "Delete" || $scope.actionName == "Read By Id") {
                    for (var i = 0; i < data.length; i++) {
                        if (data[i].isPrimaryKey) {
                            $scope.propertiesData.push(data[i]);
                        }
                    }
                    if (!$scope.propertiesData.length) {
                        $scope.propertiesData = data;
                    }
                } else if ($scope.actionName == "Read All Data") {
                    $scope.propertiesData.push({ isPrimaryKey: false, key: "PageSize", value: "", type: "number" });
                } else if ($scope.actionName == "Min of Column" || $scope.actionName == "Max of Column" || $scope.actionName == "Average") {
                    $scope.propertiesData = [];
                } else if ($scope.actionName == "Count") {
                    $scope.propertiesData = [];
                } else if ($scope.actionName == "Lazy Load") {
                    $scope.propertiesData.push({ isPrimaryKey: false, key: "PageSize", value: "", type: "number" });
                }
            }
        } else {
            if ($scope.actionName == "Execute SP") {
                $scope.getProceduerProperties();
            } else if ($scope.actionName == "Execute Query") {
                $scope.getProceduerProperties();
            }
        }
    }
    $scope.showActionProperties = function (e) {
        $scope.closeRightBox();
        $scope.showActivityTitle();
        //$scope.editProperty = false;
        //$scope.propertiesContent = false;
        //$scope.activitySettingsContent = false;
        //$scope.displayName = true;
        //$scope.actionsContent = false;
        //$scope.entityContent = false;
        //$scope.propertiesContent = true;
        if ($scope.entityName != 'Entity Name' && !$scope.propertiesContent) {
            $scope.properties = connectorActivityService.getProperties($scope.getPropertiesOnSuccess, $scope.getPropertiesOnError);
            $scope.rightBoxHeaderName = $scope.connectorActivityName + " Properties";
            $scope.settings = true;
            $scope.entity = false;
            $scope.actions = false;

            filterProperty();
            $timeout(function () {
                $scope.setWidth();
            }, 100);
            $scope.rightBoxContent = true;
            $scope.propertiesContent = true;
            $scope.rightBox = true;
        }
        e.stopPropagation();
    }
    $scope.getPropertiesOnSuccess = function (data) {
        $scope.rightBoxSpinner = false;
        $scope.rightBoxContent = true;
        $scope.propertiesContent = true;
        $scope.tablesContent = false;
        $scope.actionsContent = false;

    }
    $scope.getPropertiesOnError = function (error) {
        $scope.rightBoxSpinner = false;
        $scope.rightBoxContent = false;
        $scope.propertiesContent = false;
        $scope.tablesContent = false;
        $scope.actionsContent = false;
    }

    $scope.showEntities = function (e) {
        if (!$scope.entityContent) {
            $scope.settings = false;
            $scope.entity = true;
            $scope.actions = false;
            $scope.showRightBox();
            $scope.propertiesContent = false;
            $scope.entityContent = true;
            $scope.activitySettingsContent = false;
            $scope.actionsContent = false;
            $scope.rightBoxHeaderName = $scope.connectorActivityName + " Tables";
            $scope.loadTablesEntity();
        }
        e.stopPropagation();
    }
    $scope.loadTablesEntity = function () {
        $scope.rightBoxHeaderName = $scope.connectorActivityName + " Tables";
        $scope.rightBoxSpinner = false;
        $scope.rightBoxContent = true;
        $scope.tbl = true;
        $scope.views = false;
        $scope.prc = false;
        $scope.qry = false;
        $scope.tablesData = connectorActivityService.getTablesEntity($scope.getTablesOnSuccess, $scope.getTablesOnError);
        $('.entityBtn').removeClass('selectedEntity');
        $('.tbl').addClass('selectedEntity');
    }
    $scope.loadViewsEntity = function () {
        $scope.rightBoxHeaderName = $scope.connectorActivityName + " Views";
        $scope.rightBoxSpinner = false;
        $scope.rightBoxContent = true;
        $scope.tbl = false;
        $scope.views = true;
        $scope.prc = false;
        $scope.qry = false;
        $scope.viewsData = connectorActivityService.getViewsEntity($scope.getTablesOnSuccess, $scope.getTablesOnError);
        $('.entityBtn').removeClass('selectedEntity');
        $('.view').addClass('selectedEntity');
    }
    $scope.loadProceduresEntity = function () {
        $scope.rightBoxHeaderName = $scope.connectorActivityName + " Procedures";
        $scope.rightBoxSpinner = false;
        $scope.rightBoxContent = true;
        $scope.tbl = false;
        $scope.views = false;
        $scope.prc = true;
        $scope.qry = false;
        $scope.prcData = connectorActivityService.getProceduresEntity($scope.getTablesOnSuccess, $scope.getTablesOnError);
        $('.entityBtn').removeClass('selectedEntity');
        $('.prc').addClass('selectedEntity');
    }
    $scope.getProceduerProperties = function () {

    }
    $scope.loadQueriesEntity = function () {
        $scope.rightBoxHeaderName = $scope.connectorActivityName + " Queries";
        $scope.rightBoxSpinner = false;
        $scope.rightBoxContent = true;
        $scope.tbl = false;
        $scope.views = false;
        $scope.prc = false;
        $scope.qry = true;
        $scope.qryData = connectorActivityService.getQuireiesEntity($scope.getTablesOnSuccess, $scope.getTablesOnError);
        $('.entityBtn').removeClass('selectedEntity');
        $('.qur').addClass('selectedEntity');
    }
    $scope.getQueryProperties = function () {

    }
    $scope.getTablesOnSuccess = function (data) {
        $scope.rightBoxSpinner = false;
        $scope.rightBoxContent = true;
        $scope.propertiesContent = false;
        $scope.tablesContent = true;
        $scope.actionsContent = false;
    }
    $scope.getTablesOnError = function (error) {
        $scope.rightBoxSpinner = false;
        $scope.rightBoxContent = false;
        $scope.propertiesContent = false;
        $scope.tablesContent = false;
        $scope.actionsContent = false;
    }
    $scope.showConnectorActions = function (e) {
        if (!$scope.actionsContent) {
            $scope.settings = false;
            $scope.entity = false;
            $scope.actions = true;
            $scope.showRightBox();
            $scope.rightBoxSpinner = true;
            $scope.rightBoxContent = false;
            $scope.propertiesContent = false;
            $scope.entityContent = false;
            $scope.actionsContent = false;
            $scope.activitySettingsContent = false;
            $scope.actionDescription = "";
            $timeout(function () {
                if ($scope.tbl) {
                    $scope.tblActionsData = connectorActivityService.getTablesEntityActions($scope.getActionsOnSuccess, $scope.getActionsOnError);
                } else if ($scope.views) {
                    $scope.viewsActionsData = connectorActivityService.getViewsEntityActions($scope.getActionsOnSuccess, $scope.getActionsOnError);
                } else if ($scope.prc) {
                    $scope.prcActionsData = connectorActivityService.getProceduresEntityActions($scope.getActionsOnSuccess, $scope.getActionsOnError);
                } else if ($scope.qry) {
                    $scope.qryActionsData = connectorActivityService.getQuireiesEntityActions($scope.getActionsOnSuccess, $scope.getActionsOnError);
                } else {
                    $scope.tblActionsData = [];
                    $scope.viewsActionsData = [];
                    $scope.prcActionsData = [];
                    $scope.qryActionsData = [];
                }
                $scope.rightBoxSpinner = false;
                $scope.rightBoxContent = true;
                $scope.actionsContent = true;
            }, 500);
            $scope.rightBoxHeaderName = $scope.connectorActivityName + " Actions";
        }
        e.stopPropagation();
    }
    $scope.getActionsOnSuccess = function (data) {
        $scope.rightBoxSpinner = false;
        $scope.rightBoxContent = true;
        $scope.propertiesContent = false;
        $scope.tablesContent = false;
        $scope.actionsContent = true;
    }
    $scope.getActionsOnError = function (error) {
        $scope.rightBoxSpinner = false;
        $scope.rightBoxContent = false;
        $scope.propertiesContent = false;
        $scope.tablesContent = false;
        $scope.actionsContent = false;
    }

    $scope.radioCheck = false;
    $scope.selectEntityType = function (entity, index) {
        $scope.entityName = entity.name;
        //$('.tblRd').attr('checked', true);
        $("#tblRadioBtn" + index).prop('checked', true);
    }
    $scope.selectAction = function (action) {
        $scope.actionName = action.name;
        $scope.closeRightBox();
    }

    $scope.showActionDescription = function (desc) {
        $scope.actionDescription = desc;
    }
    $scope.lblArray = [];
    $scope.setWidth = function () {
        var max = 0;
        $scope.lblArray = [];
        for (var i = 0; i < $scope.propertiesData.length; i++) {
            var width = $(".inputLabel" + $scope.propertiesData[i].key).innerWidth();
            $scope.lblArray.push(width);
        }
        max = $scope.lblArray[0];
        for (var i = 0; i < $scope.lblArray.length; i++) {
            if ($scope.lblArray[i] >= max) {
                max = $scope.lblArray[i];
            }
        }
        for (var i = 0; i < $scope.propertiesData.length; i++) {
            $(".inputLabel" + $scope.propertiesData[i].key).css("width", max).css("text-overflow", "ellipsis").css("overflow", "hidden").css("white-space", "nowrap");
        }

    }
    var arr = ['Read All Data', 'Count', 'Execute SP', 'Execute Query', 'Lazy Load'];
    $scope.showEditProperty = function (e) {

        if (arr.indexOf($scope.actionName) < 0) {
            $scope.editProperty = true;
            // $('.editPropertyConn').css('display', 'block');
            $timeout(function () {
                if ($scope.propertiesData.length) {
                    for (var i = 0; i < $scope.propertiesData.length; i++) {
                        $('#prp' + $scope.propertiesData[i].key).attr('checked', true);
                    }
                }
            }, 100);
        }
        e.stopPropagation();
    }
    $scope.editPrp = function (item, e) {
        var chk = $('#prp' + item.key).prop('checked');
        if (e.target.localName != 'input') { chk = !chk; $('#prp' + item.key).prop('checked', chk) };
        if (!chk) {
            // remove from property list
            var index = null;
            for (var i = 0; i < $scope.propertiesData.length; i++) {
                if ($scope.propertiesData[i].key == item.key) {
                    index = i;
                    break;
                }
            }
            if (index >= 0) {
                $scope.propertiesData.splice(index, 1);
            }
        } else {
            // insert to property list
            $scope.propertiesData.push(item);
            if ($scope.propertiesData.length > 1) {
                $timeout(function () {
                    $scope.setWidth();
                }, 100);
            }
        }
    }
    $scope.showActivitySettings = function (e) {
        if (!$scope.activitySettingsContent) {
            $scope.settings = false;
            $scope.entity = false;
            $scope.actions = false;
            $scope.rightBox = true;
            $scope.rightBox = true;
            $scope.propertiesContent = false;
            $scope.entityContent = false;
            $scope.actionsContent = false;
            $scope.rightBoxContent = true;
            $scope.activitySettingsContent = true;
            $scope.rightBoxHeaderName = $scope.connectorActivityName + " Settings";
        }
        e.stopPropagation();
    }
    $scope.txtDisplay = {};
    $scope.activitySetting = {
        displayName: "Unnamed Activity"
    }
    $scope.displayNameTitle = $scope.activitySetting.displayName;
    //if ($scope.displayNameTitle=='Unnamed Activity') {
    //    $scope.displayName = false;
    //}else{
    //    $scope.displayName = true;
    //}
    $scope.displayName = true;
    $scope.hideActivityTitle = function () {
        $scope.displayName = false;
        //$('.activityTitle').fadeOut();
    }
    $scope.showActivityTitle = function () {
        if (!$scope.rightBox) {
            $scope.rightBox = false;
            $scope.editProperty = false;
            $scope.displayName = true;
            $scope.propertiesContent = false;
            $scope.activitySettingsContent = false;
            $scope.actionsContent = false;
            $scope.entityContent = false;
        }
        $scope._ShowConnector = false;
        $scope.$digest();
        //$('.activityTitle').fadeIn();
    }

    $scope.saveSettings = function (obj) {
        if (!obj.name) obj.name = "Unnamed Activity";
        $scope.displayNameTitle = obj.name;
        // e.stopPropagation();
    }

    $scope.linerange = [100, 200, 300];
    var controller = this;

    this.document = document;

    $scope.nodecmbid = function () {
        return "cmbAssignVariable" + $attrs.nodeid;
    };

    $scope.nodetxtid = function () {
        return "txtAssignVariable" + $attrs.nodeid;
    };

    $scope.activeConnector = function () {
        return $attrs.activeconnector;
    };

    $scope.activeConnectorInput = function () {
        return $attrs.activeConnectorInput;
    };

    $scope.nodecmbid2 = function () {
        return "cmbAssignVariable2-" + $attrs.nodeid;
    };

    $scope.nodetxtid2 = function () {
        return "txtAssignVariable2-" + $attrs.nodeid;
    };

    $scope.VariableComboChange = function () {
        var cmb = document.getElementById($scope.nodecmbid());
        var txt = document.getElementById($scope.nodetxtid());
        txt.value = cmb.options[cmb.selectedIndex].text;
        cmb.style.display = "none";
    };

    $scope.VariableComboChange2 = function () {
        var cmb = document.getElementById($scope.nodecmbid2());
        var txt = document.getElementById($scope.nodetxtid2());
        txt.value = cmb.options[cmb.selectedIndex].text;
        cmb.style.display = "none";
    };

    $scope.VariableKeyUp2 = function (evt) {
        var UndoObject = { action: "assignTextChange2", assignTextBefore: tmpValue2, assignTextAfter: document.getElementById($scope.nodetxtid2()).value, id: $scope.nodeid() };
        UndoList.push(UndoObject);
        RedoList.length = 0;

        //first hide all other visible combos (except the current one)
        for (var i = 1 ; i < 1000; i++) {
            try {
                document.getElementById("cmbAssignVariable" + i).style.display = "none";
                document.getElementById("cmbAssignVariable2-" + i).style.display = "none";
            }
            catch (ex) {

            }
        }
        var ch = document.getElementById($scope.nodetxtid2()).value;
        if (document.getElementById($scope.nodecmbid2()).options[0].text == "") {
            UpdateVariableCombo();
        }
        var comboIndex = -1;
        var cmb = document.getElementById($scope.nodecmbid2());
        for (var i = 0 ; i < cmb.options.length; i++) {
            if (cmb.options[i].text.substring(0, ch.length) == ch) {
                comboIndex = i;
                break;
            }
        }
        if (comboIndex != -1) {//is anything found? if no matching items is found, then combo should not be displayed at all
            cmb.selectedIndex = comboIndex;
            cmb.style.display = "";
        }
    };

    $scope.VariableKeyDown = function (evt) {
        tmpValue1 = document.getElementById($scope.nodetxtid()).value;
    };

    $scope.VariableKeyDown2 = function (evt) {
        tmpValue2 = document.getElementById($scope.nodetxtid2()).value;
    };

    var tmpValue1 = "";
    var tmpValue2 = "";

    $scope.VariableKeyUp = function (evt) {

        var UndoObject = { action: "assignTextChange1", assignTextBefore: tmpValue1, assignTextAfter: document.getElementById($scope.nodetxtid()).value, id: $scope.nodeid() };
        UndoList.push(UndoObject);
        RedoList.length = 0;

        //first hide all other visible combos (except the current one)
        for (var i = 1 ; i < 1000; i++) {
            try {
                document.getElementById("cmbAssignVariable" + i).style.display = "none";
                document.getElementById("cmbAssignVariable2-" + i).style.display = "none";
            }
            catch (ex) {

            }
        }
        var ch = document.getElementById($scope.nodetxtid()).value;
        if (document.getElementById($scope.nodecmbid()).options[0].text == "") {
            UpdateVariableCombo();
        }

        var comboIndex = -1;
        var cmb = document.getElementById($scope.nodecmbid());
        for (var i = 0 ; i < cmb.options.length; i++) {
            if (cmb.options[i].text.substring(0, ch.length) == ch) {
                comboIndex = i;
                break;
            }
        }

        if (comboIndex != -1) {//is anything found? if no matching items is found, then combo should not be displayed at all
            cmb.selectedIndex = comboIndex;
            cmb.style.display = "";
        }
    };

    $scope.AssignComboChanged = function (obj) {
        try {
            var combo = document.getElementById('cmb10');
            alert(combo.selectedIndex);
        }
        catch (ex) {
            alert(ex.message);
        }
    };

    $scope.nodeid = function () {
        return $attrs.nodeid;
    };

    $scope.GetFocus1 = function () {
        currentEditAssignNode1 = $scope.nodeid();
    };

    $scope.LostFocus1 = function () {
        currentEditAssignNode1 = -1;
    };

    $scope.GetFocus2 = function () {
        currentEditAssignNode2 = $scope.nodeid();
    };

    $scope.LostFocus2 = function () {
        currentEditAssignNode2 = -1;
    };

    $scope.MouseOver = function () {
        $scope._ShowConnector = true;
        $scope.digest();
    };

    $scope.ShowInputConnector = function () {
        var show = false;

        var conclick = "false";
        if ($scope.OutputConnectorClick() == "true")
            conclick = "true";

        if ($scope.ShowConnector() || $scope.Connected() == "true" || conclick == "true")
            show = true;

        return show;
    };

    $scope.selected = function () {
        var css = "";
        if ($attrs.selected == "true")
            css = "selected";
        else
            css = "normal";
        if ($scope.Covered() == "true")
            css = "hovered";
        //if ($scope.Covered() == "nodrop")
        //    css = "invalidDrop";

        return css;
    };

    $scope.GetConnectorPosition = function (type) {
        try {
            var divElement = angular.element($element[0].querySelector(type));
            return { x: divElement.prop('offsetLeft'), y: divElement.prop('offsetTop') };
        }
        catch (ex) {
            alert(ex.message);
        }
    };

    $scope.MouseLeave = function () {
        $scope._ShowConnector = false;
        $scope.$digest();
    };

    $scope.hideconnectors = function () {
        return $attrs.hideconnectors;
    };

    $scope.ShowConnector = function () {
        if ($scope.hideconnectors() == "false") {
            return $scope._ShowConnector;
        }
        else {
            return false;
        }
    };

    $scope.Connected = function () {
        return $attrs.connected;
    };

    $scope.activeConnectorInput = function () {
        return $attrs.activeconnectorinput;
    };

    $scope.ShowOutputConnector = function (index) {
        var invalidConnector = false;
        var i = false;
        if ($scope.activeConnectorInput() != "") {
            if ($scope.activeConnectorInput().indexOf(index) != -1) {
                i = true;
            }
        }
        if ($scope.activeConnector() != "") {
            if ($scope.activeConnector() != index) {
                if (!i) {
               //     invalidConnector = true;
                }
            }
        }
        var show = false;
        var con = "false";
        if ($scope.Connected() == "true")
            con = "true";
        var cov = "false";
        if ($scope.Covered() == "true")
            cov = "true";
        var hov = "false";
        if ($scope.ShowConnector() == true)
            hov = "true";
        var conclick = "false";
        if ($scope.OutputConnectorClick() == "true")
            conclick = "true";
        //if ((con == "true" || hov == "true" || cov == "true" || conclick == "true") && !invalidConnector) {
        if ((hov == "true" || cov == "true")) {
            show = true;
        }
        return show;
    };

    $scope.OutputConnectorClick = function () {
        return $attrs.connectorclicked;
    };

    $scope.Covered = function () {
        return $attrs.covered;
    };

    this.document = document;
    $scope._ShowConnector = false;
    $scope._Connected = "X";

    //
    // Wrap jQuery so it can easily be  mocked for testing.
    //
    this.jQuery = function (element) {
        return $(element);
    }

}])